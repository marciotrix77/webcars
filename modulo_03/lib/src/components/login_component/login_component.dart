import 'dart:convert';

import 'package:angular/angular.dart';

import 'package:angular_forms/angular_forms.dart';
import 'package:angular_router/angular_router.dart';
import 'package:carros/src/routes/routes.dart';
import 'package:http/http.dart';
import '../../model/login.dart';

@Component(
  selector: 'login',
  styleUrls: ['login_component.css'],
  templateUrl: 'login_component.html',
  directives: [coreDirectives, formDirectives]
)
class LoginComponent{

  Router _router;

  LoginComponent(this._router);

  void Login()async{    
    _router.navigate(RoutePaths.painel.toUrl());
  }

}