// **************************************************************************
// Generator: AngularDart Compiler
// **************************************************************************

import 'login_component.dart';
export 'login_component.dart';
import 'package:angular/src/di/reflector.dart' as _ngRef;
import '../../model/login.template.dart' as _ref0;
import 'package:angular/angular.template.dart' as _ref1;
import 'package:angular_forms/angular_forms.template.dart' as _ref2;
import 'package:angular_router/angular_router.template.dart' as _ref3;
import 'package:carros/src/routes/routes.template.dart' as _ref4;
import 'package:carros/src/components/login_component/login_component.css.shim.dart' as import0;
import 'package:angular/src/core/linker/app_view.dart';
import 'login_component.dart' as import2;
import 'package:angular_forms/src/directives/ng_form.dart' as import3;
import 'package:angular/src/core/linker/style_encapsulation.dart' as import4;
import 'package:angular/src/core/linker/view_type.dart' as import5;
import 'package:angular/src/core/change_detection/change_detection.dart';
import 'dart:html' as import7;
import 'package:angular/src/runtime.dart' as import8;
import 'package:angular/angular.dart';
import 'package:angular/src/runtime/dom_helpers.dart' as import10;
import 'package:angular/src/core/linker/app_view_utils.dart' as import11;
import 'package:angular_forms/src/directives/control_container.dart' as import12;
import 'package:angular/src/di/errors.dart' as import13;
import 'package:angular_router/src/router/router.dart' as import14;

final List<dynamic> styles$LoginComponent = [import0.styles];

class ViewLoginComponent0 extends AppView<import2.LoginComponent> {
  import3.NgForm _NgForm_5_5;
  static import4.ComponentStyles _componentStyles;
  ViewLoginComponent0(AppView<dynamic> parentView, int parentIndex) : super(import5.ViewType.component, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways) {
    initComponentStyles();
    rootEl = import7.document.createElement('login');
  }
  static String get _debugComponentUrl {
    return (import8.isDevMode ? 'asset:carros/lib/src/components/login_component/login_component.dart' : null);
  }

  @override
  ComponentRef<import2.LoginComponent> build() {
    final _ctx = ctx;
    final _rootEl = rootEl;
    final import7.HtmlElement parentRenderNode = initViewRoot(_rootEl);
    final doc = import7.document;
    final _el_0 = import10.appendDiv(doc, parentRenderNode);
    this.updateChildClass(_el_0, 'd-flex justify-content-center align-items-center');
    import10.setAttribute(_el_0, 'id', 'area-form');
    addShimC(_el_0);
    final _el_1 = import10.appendDiv(doc, _el_0);
    this.updateChildClass(_el_1, 'row');
    import10.setAttribute(_el_1, 'id', 'linha-form');
    addShimC(_el_1);
    final _el_2 = import10.appendDiv(doc, _el_1);
    this.updateChildClass(_el_2, 'p-0 col-6 d-flex justify-content-end');
    addShimC(_el_2);
    final _el_3 = import10.appendElement(doc, _el_2, 'img');
    import10.setAttribute(_el_3, 'alt', 'Responsive image');
    this.updateChildClass(_el_3, 'img-fluid');
    import10.setAttribute(_el_3, 'id', 'img-form');
    import10.setAttribute(_el_3, 'src', 'https://i.pinimg.com/564x/2f/b5/68/2fb568d54a82da6d5a80d249f99e6ec3.jpg');
    addShimE(_el_3);
    final _el_4 = import10.appendDiv(doc, _el_1);
    this.updateChildClass(_el_4, 'p-0 col-6 d-flex justify-content-start');
    addShimC(_el_4);
    final _el_5 = import10.appendElement(doc, _el_4, 'form');
    this.updateChildClass(_el_5, 'p-4 bg-danger text-light');
    addShimC(_el_5);
    _NgForm_5_5 = import3.NgForm(null);
    final _el_6 = import10.appendDiv(doc, _el_5);
    import10.setAttribute(_el_6, 'id', 'brand-login');
    addShimC(_el_6);
    final _el_7 = import10.appendElement(doc, _el_6, 'h2');
    this.updateChildClass(_el_7, 'text-center');
    addShimE(_el_7);
    final _text_8 = import10.appendText(_el_7, 'WebCars');
    final _el_9 = import10.appendElement(doc, _el_5, 'br');
    addShimE(_el_9);
    final _el_10 = import10.appendDiv(doc, _el_5);
    this.updateChildClass(_el_10, 'form-group');
    addShimC(_el_10);
    final _el_11 = import10.appendElement(doc, _el_10, 'label');
    import10.setAttribute(_el_11, 'for', 'exampleDropdownFormEmail2');
    addShimE(_el_11);
    final _text_12 = import10.appendText(_el_11, 'Usuario');
    final _text_13 = import10.appendText(_el_10, ' ');
    final _el_14 = import10.appendElement(doc, _el_10, 'input');
    this.updateChildClass(_el_14, 'form-control');
    import10.setAttribute(_el_14, 'id', 'usuarioId');
    import10.setAttribute(_el_14, 'type', 'usuario');
    addShimC(_el_14);
    final _el_15 = import10.appendDiv(doc, _el_5);
    this.updateChildClass(_el_15, 'form-group');
    addShimC(_el_15);
    final _el_16 = import10.appendElement(doc, _el_15, 'label');
    import10.setAttribute(_el_16, 'for', 'exampleDropdownFormPassword2');
    addShimE(_el_16);
    final _text_17 = import10.appendText(_el_16, 'Senha');
    final _text_18 = import10.appendText(_el_15, ' ');
    final _el_19 = import10.appendElement(doc, _el_15, 'input');
    this.updateChildClass(_el_19, 'form-control');
    import10.setAttribute(_el_19, 'id', 'senhaId');
    import10.setAttribute(_el_19, 'type', 'password');
    addShimC(_el_19);
    final _el_20 = import10.appendDiv(doc, _el_5);
    this.updateChildClass(_el_20, 'form-group');
    addShimC(_el_20);
    final _el_21 = import10.appendDiv(doc, _el_20);
    this.updateChildClass(_el_21, 'form-check');
    addShimC(_el_21);
    final _el_22 = import10.appendElement(doc, _el_21, 'input');
    this.updateChildClass(_el_22, 'form-check-input');
    import10.setAttribute(_el_22, 'id', 'dropdownCheck2');
    import10.setAttribute(_el_22, 'type', 'checkbox');
    addShimC(_el_22);
    final _text_23 = import10.appendText(_el_21, ' ');
    final _el_24 = import10.appendElement(doc, _el_21, 'label');
    this.updateChildClass(_el_24, 'form-check-label');
    import10.setAttribute(_el_24, 'for', 'dropdownCheck2');
    addShimE(_el_24);
    final _text_25 = import10.appendText(_el_24, 'Mantenha-me Conectado');
    final _el_26 = import10.appendElement(doc, _el_5, 'button');
    this.updateChildClass(_el_26, 'btn btn-dark');
    import10.setAttribute(_el_26, 'type', 'submit');
    addShimC(_el_26);
    final _text_27 = import10.appendText(_el_26, 'Acessar');
    import11.appViewUtils.eventManager.addEventListener(_el_5, 'submit', eventHandler1(_NgForm_5_5.onSubmit));
    _el_5.addEventListener('reset', eventHandler1(_NgForm_5_5.onReset));
    _el_26.addEventListener('click', eventHandler0(_ctx.Login));
    init0();
  }

  @override
  dynamic injectorGetInternal(dynamic token, int nodeIndex, dynamic notFoundResult) {
    if (((identical(token, import3.NgForm) || identical(token, import12.ControlContainer)) && ((5 <= nodeIndex) && (nodeIndex <= 27)))) {
      return _NgForm_5_5;
    }
    return notFoundResult;
  }

  @override
  void initComponentStyles() {
    var styles = _componentStyles;
    if (identical(styles, null)) {
      (_componentStyles = (styles = (_componentStyles = import4.ComponentStyles.scoped(styles$LoginComponent, _debugComponentUrl))));
    }
    componentStyles = styles;
  }
}

const ComponentFactory<import2.LoginComponent> _LoginComponentNgFactory = const ComponentFactory('login', viewFactory_LoginComponentHost0);
ComponentFactory<import2.LoginComponent> get LoginComponentNgFactory {
  return _LoginComponentNgFactory;
}

final List<dynamic> styles$LoginComponentHost = const [];

class _ViewLoginComponentHost0 extends AppView<import2.LoginComponent> {
  ViewLoginComponent0 _compView_0;
  import2.LoginComponent _LoginComponent_0_5;
  _ViewLoginComponentHost0(AppView<dynamic> parentView, int parentIndex) : super(import5.ViewType.host, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways);
  @override
  ComponentRef<import2.LoginComponent> build() {
    _compView_0 = ViewLoginComponent0(this, 0);
    rootEl = _compView_0.rootEl;
    _LoginComponent_0_5 = (import8.isDevMode
        ? import13.debugInjectorWrap(import2.LoginComponent, () {
            return import2.LoginComponent(this.injectorGet(import14.Router, viewData.parentIndex));
          })
        : import2.LoginComponent(this.injectorGet(import14.Router, viewData.parentIndex)));
    _compView_0.create(_LoginComponent_0_5, projectedNodes);
    init1(rootEl);
    return ComponentRef(0, this, rootEl, _LoginComponent_0_5);
  }

  @override
  void detectChangesInternal() {
    _compView_0.detectChanges();
  }

  @override
  void destroyInternal() {
    _compView_0.destroyInternalState();
  }
}

AppView<import2.LoginComponent> viewFactory_LoginComponentHost0(AppView<dynamic> parentView, int parentIndex) {
  return _ViewLoginComponentHost0(parentView, parentIndex);
}

var _visited = false;
void initReflector() {
  if (_visited) {
    return;
  }
  _visited = true;

  _ngRef.registerComponent(LoginComponent, LoginComponentNgFactory);
  _ref0.initReflector();
  _ref1.initReflector();
  _ref2.initReflector();
  _ref3.initReflector();
  _ref4.initReflector();
}
