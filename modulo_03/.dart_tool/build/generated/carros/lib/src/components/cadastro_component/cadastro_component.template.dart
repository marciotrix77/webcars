// **************************************************************************
// Generator: AngularDart Compiler
// **************************************************************************

import 'cadastro_component.dart';
export 'cadastro_component.dart';
import 'package:angular/src/di/reflector.dart' as _ngRef;
import 'package:angular/angular.template.dart' as _ref0;
import 'package:angular_forms/angular_forms.template.dart' as _ref1;
import 'package:carros/src/model/carro.template.dart' as _ref2;
import 'package:carros/src/services/carro_service.template.dart' as _ref3;
import 'package:carros/src/components/cadastro_component/cadastro_component.css.shim.dart' as import0;
import 'package:angular/src/core/linker/app_view.dart';
import 'cadastro_component.dart' as import2;
import 'package:angular_forms/src/directives/ng_form.dart' as import3;
import 'package:angular_forms/src/directives/validators.dart' as import4;
import 'package:angular_forms/src/directives/default_value_accessor.dart' as import5;
import 'package:angular_forms/src/directives/control_value_accessor.dart' as import6;
import 'package:angular_forms/src/directives/ng_model.dart' as import7;
import 'dart:html' as import8;
import 'package:angular/src/core/linker/style_encapsulation.dart' as import9;
import 'package:angular/src/core/linker/view_type.dart' as import10;
import 'package:angular/src/core/change_detection/change_detection.dart';
import 'package:angular/src/runtime.dart' as import12;
import 'package:angular/angular.dart';
import 'package:angular/src/runtime/dom_helpers.dart' as import14;
import 'package:angular/src/core/linker/app_view_utils.dart' as import15;
import 'package:angular/src/core/di/opaque_token.dart' as import16;
import 'package:angular_forms/src/directives/control_value_accessor.dart' as import17;
import 'package:angular_forms/src/directives/ng_control.dart' as import18;
import 'package:angular_forms/src/directives/control_container.dart' as import19;
import 'package:angular/src/di/errors.dart' as import20;
import '../../services/carro_service.dart' as import21;

final List<dynamic> styles$CadastroComponent = [import0.styles];

class ViewCadastroComponent0 extends AppView<import2.CadastroComponent> {
  import3.NgForm _NgForm_1_5;
  import4.RequiredValidator _RequiredValidator_7_5;
  List<dynamic> _NgValidators_7_6;
  import5.DefaultValueAccessor _DefaultValueAccessor_7_7;
  List<import6.ControlValueAccessor<dynamic>> _NgValueAccessor_7_8;
  import7.NgModel _NgModel_7_9;
  import4.RequiredValidator _RequiredValidator_12_5;
  List<dynamic> _NgValidators_12_6;
  import5.DefaultValueAccessor _DefaultValueAccessor_12_7;
  List<import6.ControlValueAccessor<dynamic>> _NgValueAccessor_12_8;
  import7.NgModel _NgModel_12_9;
  import4.RequiredValidator _RequiredValidator_18_5;
  List<dynamic> _NgValidators_18_6;
  import5.DefaultValueAccessor _DefaultValueAccessor_18_7;
  List<import6.ControlValueAccessor<dynamic>> _NgValueAccessor_18_8;
  import7.NgModel _NgModel_18_9;
  import4.RequiredValidator _RequiredValidator_23_5;
  List<dynamic> _NgValidators_23_6;
  import5.DefaultValueAccessor _DefaultValueAccessor_23_7;
  List<import6.ControlValueAccessor<dynamic>> _NgValueAccessor_23_8;
  import7.NgModel _NgModel_23_9;
  import4.RequiredValidator _RequiredValidator_28_5;
  List<dynamic> _NgValidators_28_6;
  import5.DefaultValueAccessor _DefaultValueAccessor_28_7;
  List<import6.ControlValueAccessor<dynamic>> _NgValueAccessor_28_8;
  import7.NgModel _NgModel_28_9;
  bool _expr_0;
  bool _expr_3;
  bool _expr_6;
  bool _expr_9;
  bool _expr_12;
  import8.InputElement _el_7;
  import8.InputElement _el_12;
  import8.InputElement _el_18;
  import8.InputElement _el_23;
  import8.InputElement _el_28;
  static import9.ComponentStyles _componentStyles;
  ViewCadastroComponent0(AppView<dynamic> parentView, int parentIndex) : super(import10.ViewType.component, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways) {
    initComponentStyles();
    rootEl = import8.document.createElement('cadastro');
  }
  static String get _debugComponentUrl {
    return (import12.isDevMode ? 'asset:carros/lib/src/components/cadastro_component/cadastro_component.dart' : null);
  }

  @override
  ComponentRef<import2.CadastroComponent> build() {
    final _ctx = ctx;
    final _rootEl = rootEl;
    final import8.HtmlElement parentRenderNode = initViewRoot(_rootEl);
    final doc = import8.document;
    final _el_0 = import14.appendDiv(doc, parentRenderNode);
    import14.setAttribute(_el_0, 'id', 'cadastro-carro');
    addShimC(_el_0);
    final _el_1 = import14.appendElement(doc, _el_0, 'form');
    addShimC(_el_1);
    _NgForm_1_5 = import3.NgForm(null);
    final _el_2 = import14.appendDiv(doc, _el_1);
    this.updateChildClass(_el_2, 'form-row');
    addShimC(_el_2);
    final _el_3 = import14.appendDiv(doc, _el_2);
    this.updateChildClass(_el_3, 'form-group col-md-6');
    addShimC(_el_3);
    final _el_4 = import14.appendElement(doc, _el_3, 'label');
    import14.setAttribute(_el_4, 'for', 'nomeCarro');
    addShimE(_el_4);
    final _text_5 = import14.appendText(_el_4, 'Nome do Carro');
    final _text_6 = import14.appendText(_el_3, ' ');
    _el_7 = import14.appendElement(doc, _el_3, 'input');
    this.updateChildClass(_el_7, 'form-control');
    import14.setAttribute(_el_7, 'id', 'nomeCarro');
    import14.setAttribute(_el_7, 'required', '');
    import14.setAttribute(_el_7, 'type', 'text');
    addShimC(_el_7);
    _RequiredValidator_7_5 = import4.RequiredValidator();
    _NgValidators_7_6 = [_RequiredValidator_7_5];
    _DefaultValueAccessor_7_7 = import5.DefaultValueAccessor(_el_7);
    _NgValueAccessor_7_8 = [_DefaultValueAccessor_7_7];
    _NgModel_7_9 = import7.NgModel(_NgValidators_7_6, _NgValueAccessor_7_8);
    final _el_8 = import14.appendDiv(doc, _el_2);
    this.updateChildClass(_el_8, 'form-group col-md-6');
    addShimC(_el_8);
    final _el_9 = import14.appendElement(doc, _el_8, 'label');
    import14.setAttribute(_el_9, 'for', 'nomeFabricante');
    addShimE(_el_9);
    final _text_10 = import14.appendText(_el_9, 'Fabricante');
    final _text_11 = import14.appendText(_el_8, ' ');
    _el_12 = import14.appendElement(doc, _el_8, 'input');
    this.updateChildClass(_el_12, 'form-control');
    import14.setAttribute(_el_12, 'id', 'nomeFabricante');
    import14.setAttribute(_el_12, 'required', '');
    import14.setAttribute(_el_12, 'type', 'text');
    addShimC(_el_12);
    _RequiredValidator_12_5 = import4.RequiredValidator();
    _NgValidators_12_6 = [_RequiredValidator_12_5];
    _DefaultValueAccessor_12_7 = import5.DefaultValueAccessor(_el_12);
    _NgValueAccessor_12_8 = [_DefaultValueAccessor_12_7];
    _NgModel_12_9 = import7.NgModel(_NgValidators_12_6, _NgValueAccessor_12_8);
    final _el_13 = import14.appendDiv(doc, _el_1);
    this.updateChildClass(_el_13, 'form-row');
    addShimC(_el_13);
    final _el_14 = import14.appendDiv(doc, _el_13);
    this.updateChildClass(_el_14, 'form-group col-md-3');
    addShimC(_el_14);
    final _el_15 = import14.appendElement(doc, _el_14, 'label');
    import14.setAttribute(_el_15, 'for', 'anoFabricacao');
    addShimE(_el_15);
    final _text_16 = import14.appendText(_el_15, 'Ano de Fabricação');
    final _text_17 = import14.appendText(_el_14, ' ');
    _el_18 = import14.appendElement(doc, _el_14, 'input');
    this.updateChildClass(_el_18, 'form-control');
    import14.setAttribute(_el_18, 'id', 'anoFabricacao');
    import14.setAttribute(_el_18, 'required', '');
    import14.setAttribute(_el_18, 'type', 'text');
    addShimC(_el_18);
    _RequiredValidator_18_5 = import4.RequiredValidator();
    _NgValidators_18_6 = [_RequiredValidator_18_5];
    _DefaultValueAccessor_18_7 = import5.DefaultValueAccessor(_el_18);
    _NgValueAccessor_18_8 = [_DefaultValueAccessor_18_7];
    _NgModel_18_9 = import7.NgModel(_NgValidators_18_6, _NgValueAccessor_18_8);
    final _el_19 = import14.appendDiv(doc, _el_13);
    this.updateChildClass(_el_19, 'form-group col-md-3');
    addShimC(_el_19);
    final _el_20 = import14.appendElement(doc, _el_19, 'label');
    import14.setAttribute(_el_20, 'for', 'preco');
    addShimE(_el_20);
    final _text_21 = import14.appendText(_el_20, 'Preço');
    final _text_22 = import14.appendText(_el_19, ' ');
    _el_23 = import14.appendElement(doc, _el_19, 'input');
    this.updateChildClass(_el_23, 'form-control');
    import14.setAttribute(_el_23, 'id', 'preco');
    import14.setAttribute(_el_23, 'required', '');
    import14.setAttribute(_el_23, 'type', 'text');
    addShimC(_el_23);
    _RequiredValidator_23_5 = import4.RequiredValidator();
    _NgValidators_23_6 = [_RequiredValidator_23_5];
    _DefaultValueAccessor_23_7 = import5.DefaultValueAccessor(_el_23);
    _NgValueAccessor_23_8 = [_DefaultValueAccessor_23_7];
    _NgModel_23_9 = import7.NgModel(_NgValidators_23_6, _NgValueAccessor_23_8);
    final _el_24 = import14.appendDiv(doc, _el_13);
    this.updateChildClass(_el_24, 'form-group col-md-6');
    addShimC(_el_24);
    final _el_25 = import14.appendElement(doc, _el_24, 'label');
    import14.setAttribute(_el_25, 'for', 'imagem');
    addShimE(_el_25);
    final _text_26 = import14.appendText(_el_25, 'Imagem');
    final _text_27 = import14.appendText(_el_24, ' ');
    _el_28 = import14.appendElement(doc, _el_24, 'input');
    this.updateChildClass(_el_28, 'form-control');
    import14.setAttribute(_el_28, 'id', 'imagem');
    import14.setAttribute(_el_28, 'required', '');
    import14.setAttribute(_el_28, 'type', 'text');
    addShimC(_el_28);
    _RequiredValidator_28_5 = import4.RequiredValidator();
    _NgValidators_28_6 = [_RequiredValidator_28_5];
    _DefaultValueAccessor_28_7 = import5.DefaultValueAccessor(_el_28);
    _NgValueAccessor_28_8 = [_DefaultValueAccessor_28_7];
    _NgModel_28_9 = import7.NgModel(_NgValidators_28_6, _NgValueAccessor_28_8);
    final _el_29 = import14.appendElement(doc, _el_1, 'button');
    this.updateChildClass(_el_29, 'btn btn-dark');
    import14.setAttribute(_el_29, 'id', 'btn-cadastrar');
    import14.setAttribute(_el_29, 'type', 'submit');
    addShimC(_el_29);
    final _text_30 = import14.appendText(_el_29, 'Cadastrar');
    final _text_31 = import14.appendText(_el_1, ' ');
    final _el_32 = import14.appendElement(doc, _el_1, 'button');
    this.updateChildClass(_el_32, 'btn btn-success');
    import14.setAttribute(_el_32, 'id', 'btn-alterar');
    import14.setAttribute(_el_32, 'type', 'submit');
    addShimC(_el_32);
    final _text_33 = import14.appendText(_el_32, 'Alterar');
    final _text_34 = import14.appendText(_el_1, ' ');
    final _el_35 = import14.appendElement(doc, _el_1, 'button');
    this.updateChildClass(_el_35, 'btn btn-danger');
    import14.setAttribute(_el_35, 'type', 'reset');
    addShimC(_el_35);
    final _text_36 = import14.appendText(_el_35, 'Limpar');
    final _el_37 = import14.appendDiv(doc, _el_1);
    this.updateChildClass(_el_37, 'alert alert-success mt-3 col-4');
    import14.setAttribute(_el_37, 'id', 'alert-sucesso');
    import14.setAttribute(_el_37, 'role', 'alert');
    addShimC(_el_37);
    final _el_38 = import14.appendDiv(doc, _el_1);
    this.updateChildClass(_el_38, 'alert alert-danger mt-3 col-4');
    import14.setAttribute(_el_38, 'id', 'alert-erro');
    import14.setAttribute(_el_38, 'role', 'alert');
    addShimC(_el_38);
    import15.appViewUtils.eventManager.addEventListener(_el_1, 'submit', eventHandler1(_NgForm_1_5.onSubmit));
    _el_1.addEventListener('reset', eventHandler1(_NgForm_1_5.onReset));
    _el_7.addEventListener('blur', eventHandler0(_DefaultValueAccessor_7_7.touchHandler));
    _el_7.addEventListener('input', eventHandler1(_handle_input_7_2));
    final subscription_0 = _NgModel_7_9.update.listen(eventHandler1(_handle_ngModelChange_7_0));
    _el_12.addEventListener('blur', eventHandler0(_DefaultValueAccessor_12_7.touchHandler));
    _el_12.addEventListener('input', eventHandler1(_handle_input_12_2));
    final subscription_1 = _NgModel_12_9.update.listen(eventHandler1(_handle_ngModelChange_12_0));
    _el_18.addEventListener('blur', eventHandler0(_DefaultValueAccessor_18_7.touchHandler));
    _el_18.addEventListener('input', eventHandler1(_handle_input_18_2));
    final subscription_2 = _NgModel_18_9.update.listen(eventHandler1(_handle_ngModelChange_18_0));
    _el_23.addEventListener('blur', eventHandler0(_DefaultValueAccessor_23_7.touchHandler));
    _el_23.addEventListener('input', eventHandler1(_handle_input_23_2));
    final subscription_3 = _NgModel_23_9.update.listen(eventHandler1(_handle_ngModelChange_23_0));
    _el_28.addEventListener('blur', eventHandler0(_DefaultValueAccessor_28_7.touchHandler));
    _el_28.addEventListener('input', eventHandler1(_handle_input_28_2));
    final subscription_4 = _NgModel_28_9.update.listen(eventHandler1(_handle_ngModelChange_28_0));
    _el_29.addEventListener('click', eventHandler0(_ctx.CadastrarCarro));
    _el_32.addEventListener('click', eventHandler0(_ctx.AlterarCarro));
    _el_35.addEventListener('click', eventHandler0(_ctx.LimparCampos));
    init(const [], [subscription_0, subscription_1, subscription_2, subscription_3, subscription_4]);
  }

  @override
  dynamic injectorGetInternal(dynamic token, int nodeIndex, dynamic notFoundResult) {
    if (((1 <= nodeIndex) && (nodeIndex <= 38))) {
      if ((7 == nodeIndex)) {
        if (identical(token, const import16.MultiToken<dynamic>('NgValidators'))) {
          return _NgValidators_7_6;
        }
        if (identical(token, const import16.MultiToken<import17.ControlValueAccessor<dynamic>>('NgValueAccessor'))) {
          return _NgValueAccessor_7_8;
        }
        if ((identical(token, import7.NgModel) || identical(token, import18.NgControl))) {
          return _NgModel_7_9;
        }
      }
      if ((12 == nodeIndex)) {
        if (identical(token, const import16.MultiToken<dynamic>('NgValidators'))) {
          return _NgValidators_12_6;
        }
        if (identical(token, const import16.MultiToken<import17.ControlValueAccessor<dynamic>>('NgValueAccessor'))) {
          return _NgValueAccessor_12_8;
        }
        if ((identical(token, import7.NgModel) || identical(token, import18.NgControl))) {
          return _NgModel_12_9;
        }
      }
      if ((18 == nodeIndex)) {
        if (identical(token, const import16.MultiToken<dynamic>('NgValidators'))) {
          return _NgValidators_18_6;
        }
        if (identical(token, const import16.MultiToken<import17.ControlValueAccessor<dynamic>>('NgValueAccessor'))) {
          return _NgValueAccessor_18_8;
        }
        if ((identical(token, import7.NgModel) || identical(token, import18.NgControl))) {
          return _NgModel_18_9;
        }
      }
      if ((23 == nodeIndex)) {
        if (identical(token, const import16.MultiToken<dynamic>('NgValidators'))) {
          return _NgValidators_23_6;
        }
        if (identical(token, const import16.MultiToken<import17.ControlValueAccessor<dynamic>>('NgValueAccessor'))) {
          return _NgValueAccessor_23_8;
        }
        if ((identical(token, import7.NgModel) || identical(token, import18.NgControl))) {
          return _NgModel_23_9;
        }
      }
      if ((28 == nodeIndex)) {
        if (identical(token, const import16.MultiToken<dynamic>('NgValidators'))) {
          return _NgValidators_28_6;
        }
        if (identical(token, const import16.MultiToken<import17.ControlValueAccessor<dynamic>>('NgValueAccessor'))) {
          return _NgValueAccessor_28_8;
        }
        if ((identical(token, import7.NgModel) || identical(token, import18.NgControl))) {
          return _NgModel_28_9;
        }
      }
      if ((identical(token, import3.NgForm) || identical(token, import19.ControlContainer))) {
        return _NgForm_1_5;
      }
    }
    return notFoundResult;
  }

  @override
  void detectChangesInternal() {
    final _ctx = ctx;
    bool changed = false;
    bool firstCheck = (this.cdState == 0);
    final import7.NgModel local_nomeCarro = _NgModel_7_9;
    final import7.NgModel local_nomeFabricante = _NgModel_12_9;
    final import7.NgModel local_anoFabricacao = _NgModel_18_9;
    final import7.NgModel local_preco = _NgModel_23_9;
    final import7.NgModel local_imagem = _NgModel_28_9;
    if (firstCheck) {
      (_RequiredValidator_7_5.required = true);
    }
    changed = false;
    _NgModel_7_9.model = _ctx.carroCadastro.nomeCarro;
    _NgModel_7_9.ngAfterChanges();
    if (((!import15.AppViewUtils.throwOnChanges) && firstCheck)) {
      _NgModel_7_9.ngOnInit();
    }
    if (firstCheck) {
      (_RequiredValidator_12_5.required = true);
    }
    changed = false;
    _NgModel_12_9.model = _ctx.carroCadastro.nomeFabricante;
    _NgModel_12_9.ngAfterChanges();
    if (((!import15.AppViewUtils.throwOnChanges) && firstCheck)) {
      _NgModel_12_9.ngOnInit();
    }
    if (firstCheck) {
      (_RequiredValidator_18_5.required = true);
    }
    changed = false;
    _NgModel_18_9.model = _ctx.carroCadastro.anoFabricacao;
    _NgModel_18_9.ngAfterChanges();
    if (((!import15.AppViewUtils.throwOnChanges) && firstCheck)) {
      _NgModel_18_9.ngOnInit();
    }
    if (firstCheck) {
      (_RequiredValidator_23_5.required = true);
    }
    changed = false;
    _NgModel_23_9.model = _ctx.carroCadastro.preco;
    _NgModel_23_9.ngAfterChanges();
    if (((!import15.AppViewUtils.throwOnChanges) && firstCheck)) {
      _NgModel_23_9.ngOnInit();
    }
    if (firstCheck) {
      (_RequiredValidator_28_5.required = true);
    }
    changed = false;
    _NgModel_28_9.model = _ctx.carroCadastro.imagem;
    _NgModel_28_9.ngAfterChanges();
    if (((!import15.AppViewUtils.throwOnChanges) && firstCheck)) {
      _NgModel_28_9.ngOnInit();
    }
    final currVal_0 = local_nomeCarro.valid;
    if (import15.checkBinding(_expr_0, currVal_0)) {
      import14.updateClassBinding(_el_7, 'is-valid', currVal_0);
      _expr_0 = currVal_0;
    }
    final currVal_3 = local_nomeFabricante.valid;
    if (import15.checkBinding(_expr_3, currVal_3)) {
      import14.updateClassBinding(_el_12, 'is-valid', currVal_3);
      _expr_3 = currVal_3;
    }
    final currVal_6 = local_anoFabricacao.valid;
    if (import15.checkBinding(_expr_6, currVal_6)) {
      import14.updateClassBinding(_el_18, 'is-valid', currVal_6);
      _expr_6 = currVal_6;
    }
    final currVal_9 = local_preco.valid;
    if (import15.checkBinding(_expr_9, currVal_9)) {
      import14.updateClassBinding(_el_23, 'is-valid', currVal_9);
      _expr_9 = currVal_9;
    }
    final currVal_12 = local_imagem.valid;
    if (import15.checkBinding(_expr_12, currVal_12)) {
      import14.updateClassBinding(_el_28, 'is-valid', currVal_12);
      _expr_12 = currVal_12;
    }
  }

  void _handle_ngModelChange_7_0($event) {
    final _ctx = ctx;
    _ctx.carroCadastro.nomeCarro = $event;
  }

  void _handle_input_7_2($event) {
    _DefaultValueAccessor_7_7.handleChange($event.target.value);
  }

  void _handle_ngModelChange_12_0($event) {
    final _ctx = ctx;
    _ctx.carroCadastro.nomeFabricante = $event;
  }

  void _handle_input_12_2($event) {
    _DefaultValueAccessor_12_7.handleChange($event.target.value);
  }

  void _handle_ngModelChange_18_0($event) {
    final _ctx = ctx;
    _ctx.carroCadastro.anoFabricacao = $event;
  }

  void _handle_input_18_2($event) {
    _DefaultValueAccessor_18_7.handleChange($event.target.value);
  }

  void _handle_ngModelChange_23_0($event) {
    final _ctx = ctx;
    _ctx.carroCadastro.preco = $event;
  }

  void _handle_input_23_2($event) {
    _DefaultValueAccessor_23_7.handleChange($event.target.value);
  }

  void _handle_ngModelChange_28_0($event) {
    final _ctx = ctx;
    _ctx.carroCadastro.imagem = $event;
  }

  void _handle_input_28_2($event) {
    _DefaultValueAccessor_28_7.handleChange($event.target.value);
  }

  @override
  void initComponentStyles() {
    var styles = _componentStyles;
    if (identical(styles, null)) {
      (_componentStyles = (styles = (_componentStyles = import9.ComponentStyles.scoped(styles$CadastroComponent, _debugComponentUrl))));
    }
    componentStyles = styles;
  }
}

const ComponentFactory<import2.CadastroComponent> _CadastroComponentNgFactory = const ComponentFactory('cadastro', viewFactory_CadastroComponentHost0);
ComponentFactory<import2.CadastroComponent> get CadastroComponentNgFactory {
  return _CadastroComponentNgFactory;
}

final List<dynamic> styles$CadastroComponentHost = const [];

class _ViewCadastroComponentHost0 extends AppView<import2.CadastroComponent> {
  ViewCadastroComponent0 _compView_0;
  import2.CadastroComponent _CadastroComponent_0_5;
  _ViewCadastroComponentHost0(AppView<dynamic> parentView, int parentIndex) : super(import10.ViewType.host, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways);
  @override
  ComponentRef<import2.CadastroComponent> build() {
    _compView_0 = ViewCadastroComponent0(this, 0);
    rootEl = _compView_0.rootEl;
    _CadastroComponent_0_5 = (import12.isDevMode
        ? import20.debugInjectorWrap(import2.CadastroComponent, () {
            return import2.CadastroComponent(this.injectorGet(import21.CarroService, viewData.parentIndex));
          })
        : import2.CadastroComponent(this.injectorGet(import21.CarroService, viewData.parentIndex)));
    _compView_0.create(_CadastroComponent_0_5, projectedNodes);
    init1(rootEl);
    return ComponentRef(0, this, rootEl, _CadastroComponent_0_5);
  }

  @override
  void detectChangesInternal() {
    bool firstCheck = (this.cdState == 0);
    if (((!import15.AppViewUtils.throwOnChanges) && firstCheck)) {
      _CadastroComponent_0_5.ngOnInit();
    }
    _compView_0.detectChanges();
  }

  @override
  void destroyInternal() {
    _compView_0.destroyInternalState();
  }
}

AppView<import2.CadastroComponent> viewFactory_CadastroComponentHost0(AppView<dynamic> parentView, int parentIndex) {
  return _ViewCadastroComponentHost0(parentView, parentIndex);
}

var _visited = false;
void initReflector() {
  if (_visited) {
    return;
  }
  _visited = true;

  _ngRef.registerComponent(CadastroComponent, CadastroComponentNgFactory);
  _ref0.initReflector();
  _ref1.initReflector();
  _ref2.initReflector();
  _ref3.initReflector();
}
