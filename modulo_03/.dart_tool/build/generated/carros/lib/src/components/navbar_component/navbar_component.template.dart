// **************************************************************************
// Generator: AngularDart Compiler
// **************************************************************************

import 'navbar_component.dart';
export 'navbar_component.dart';
import 'package:angular/src/di/reflector.dart' as _ngRef;
import 'package:angular/angular.template.dart' as _ref0;
import 'package:angular_router/angular_router.template.dart' as _ref1;
import 'package:carros/src/routes/route_paths.template.dart' as _ref2;
import 'package:carros/src/services/carro_service.template.dart' as _ref3;
import 'package:carros/src/components/navbar_component/navbar_component.css.shim.dart' as import0;
import 'package:angular/src/core/linker/app_view.dart';
import 'navbar_component.dart' as import2;
import 'dart:html' as import3;
import 'package:angular/src/core/linker/style_encapsulation.dart' as import4;
import 'package:angular/src/core/linker/view_type.dart' as import5;
import 'package:angular/src/core/change_detection/change_detection.dart';
import 'package:angular/src/runtime.dart' as import7;
import 'package:angular/angular.dart';
import 'package:angular/src/runtime/dom_helpers.dart' as import9;
import 'package:angular/src/di/errors.dart' as import10;
import '../../services/carro_service.dart' as import11;
import 'package:angular_router/src/router/router.dart' as import12;

final List<dynamic> styles$NavbarComponent = [import0.styles];

class ViewNavbarComponent0 extends AppView<import2.NavbarComponent> {
  import3.InputElement _el_4;
  static import4.ComponentStyles _componentStyles;
  ViewNavbarComponent0(AppView<dynamic> parentView, int parentIndex) : super(import5.ViewType.component, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways) {
    initComponentStyles();
    rootEl = import3.document.createElement('navbar');
  }
  static String get _debugComponentUrl {
    return (import7.isDevMode ? 'asset:carros/lib/src/components/navbar_component/navbar_component.dart' : null);
  }

  @override
  ComponentRef<import2.NavbarComponent> build() {
    final _ctx = ctx;
    final _rootEl = rootEl;
    final import3.HtmlElement parentRenderNode = initViewRoot(_rootEl);
    final doc = import3.document;
    final _el_0 = import9.appendElement(doc, parentRenderNode, 'nav');
    this.updateChildClass(_el_0, 'navbar navbar-dark bg-dark');
    addShimE(_el_0);
    final _el_1 = import9.appendElement(doc, _el_0, 'a');
    this.updateChildClass(_el_1, 'navbar-brand');
    addShimC(_el_1);
    final _text_2 = import9.appendText(_el_1, 'WebCars');
    final _el_3 = import9.appendElement(doc, _el_0, 'form');
    this.updateChildClass(_el_3, 'form-inline');
    addShimC(_el_3);
    _el_4 = import9.appendElement(doc, _el_3, 'input');
    import9.setAttribute(_el_4, 'aria-label', 'Search');
    this.updateChildClass(_el_4, 'form-control mr-sm-2');
    import9.setAttribute(_el_4, 'id', 'termoPesquisa');
    import9.setAttribute(_el_4, 'placeholder', 'Search');
    import9.setAttribute(_el_4, 'type', 'search');
    addShimC(_el_4);
    final _text_5 = import9.appendText(_el_3, ' ');
    final _el_6 = import9.appendElement(doc, _el_3, 'i');
    import9.setAttribute(_el_6, 'aria-hidden', 'true');
    this.updateChildClass(_el_6, 'fa fa-sign-out text-light');
    import9.setAttribute(_el_6, 'id', 'icone-exit');
    addShimE(_el_6);
    _el_4.addEventListener('keyup', eventHandler1(_handle_keyup_4_0));
    _el_4.addEventListener('change', eventHandler1(_handle_change_4_1));
    _el_6.addEventListener('click', eventHandler0(_ctx.Login));
    init0();
  }

  void _handle_keyup_4_0($event) {
    final local_termoPesquisa = _el_4;
    final _ctx = ctx;
    _ctx.PesquisarCarro(local_termoPesquisa.value);
  }

  void _handle_change_4_1($event) {
    final local_termoPesquisa = _el_4;
    final _ctx = ctx;
    _ctx.PesquisarCarro(local_termoPesquisa.value);
  }

  @override
  void initComponentStyles() {
    var styles = _componentStyles;
    if (identical(styles, null)) {
      (_componentStyles = (styles = (_componentStyles = import4.ComponentStyles.scoped(styles$NavbarComponent, _debugComponentUrl))));
    }
    componentStyles = styles;
  }
}

const ComponentFactory<import2.NavbarComponent> _NavbarComponentNgFactory = const ComponentFactory('navbar', viewFactory_NavbarComponentHost0);
ComponentFactory<import2.NavbarComponent> get NavbarComponentNgFactory {
  return _NavbarComponentNgFactory;
}

final List<dynamic> styles$NavbarComponentHost = const [];

class _ViewNavbarComponentHost0 extends AppView<import2.NavbarComponent> {
  ViewNavbarComponent0 _compView_0;
  import2.NavbarComponent _NavbarComponent_0_5;
  _ViewNavbarComponentHost0(AppView<dynamic> parentView, int parentIndex) : super(import5.ViewType.host, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways);
  @override
  ComponentRef<import2.NavbarComponent> build() {
    _compView_0 = ViewNavbarComponent0(this, 0);
    rootEl = _compView_0.rootEl;
    _NavbarComponent_0_5 = (import7.isDevMode
        ? import10.debugInjectorWrap(import2.NavbarComponent, () {
            return import2.NavbarComponent(this.injectorGet(import11.CarroService, viewData.parentIndex), this.injectorGet(import12.Router, viewData.parentIndex));
          })
        : import2.NavbarComponent(this.injectorGet(import11.CarroService, viewData.parentIndex), this.injectorGet(import12.Router, viewData.parentIndex)));
    _compView_0.create(_NavbarComponent_0_5, projectedNodes);
    init1(rootEl);
    return ComponentRef(0, this, rootEl, _NavbarComponent_0_5);
  }

  @override
  void detectChangesInternal() {
    _compView_0.detectChanges();
  }

  @override
  void destroyInternal() {
    _compView_0.destroyInternalState();
  }
}

AppView<import2.NavbarComponent> viewFactory_NavbarComponentHost0(AppView<dynamic> parentView, int parentIndex) {
  return _ViewNavbarComponentHost0(parentView, parentIndex);
}

var _visited = false;
void initReflector() {
  if (_visited) {
    return;
  }
  _visited = true;

  _ngRef.registerComponent(NavbarComponent, NavbarComponentNgFactory);
  _ref0.initReflector();
  _ref1.initReflector();
  _ref2.initReflector();
  _ref3.initReflector();
}
