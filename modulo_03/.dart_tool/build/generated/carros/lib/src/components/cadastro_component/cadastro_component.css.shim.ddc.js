define(['dart_sdk', 'packages/angular_router/src/directives/router_outlet_directive', 'packages/angular/src/bootstrap/modules', 'packages/angular/src/core/change_detection/change_detection', 'packages/angular/angular.template', 'packages/stream_transform/src/aggregate_sample', 'packages/http/src/base_client', 'packages/angular/src/runtime/text_binding', 'packages/angular/src/runtime/interpolate', 'packages/angular_forms/src/directives', 'packages/angular_forms/angular_forms.template', 'packages/angular_router/angular_router.template'], function(dart_sdk, packages__angular_router__src__directives__router_outlet_directive, packages__angular__src__bootstrap__modules, packages__angular__src__core__change_detection__change_detection, packages__angular__angular$46template, packages__stream_transform__src__aggregate_sample, packages__http__src__base_client, packages__angular__src__runtime__text_binding, packages__angular__src__runtime__interpolate, packages__angular_forms__src__directives, packages__angular_forms__angular_forms$46template, packages__angular_router__angular_router$46template) {
  'use strict';
  const core = dart_sdk.core;
  const async = dart_sdk.async;
  const _js_helper = dart_sdk._js_helper;
  const convert = dart_sdk.convert;
  const _interceptors = dart_sdk._interceptors;
  const html = dart_sdk.html;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const router = packages__angular_router__src__directives__router_outlet_directive.src__router__router;
  const route_definition = packages__angular_router__src__directives__router_outlet_directive.src__route_definition;
  const route_path = packages__angular_router__src__directives__router_outlet_directive.src__route_path;
  const view_type = packages__angular__src__bootstrap__modules.src__core__linker__view_type;
  const dom_helpers = packages__angular__src__bootstrap__modules.src__runtime__dom_helpers;
  const style_encapsulation = packages__angular__src__bootstrap__modules.src__core__linker__style_encapsulation;
  const app_view = packages__angular__src__bootstrap__modules.src__core__linker__app_view;
  const component_factory = packages__angular__src__bootstrap__modules.src__core__linker__component_factory;
  const app_view_utils = packages__angular__src__bootstrap__modules.src__core__linker__app_view_utils;
  const view_container = packages__angular__src__bootstrap__modules.src__core__linker__view_container;
  const template_ref = packages__angular__src__bootstrap__modules.src__core__linker__template_ref;
  const ng_for = packages__angular__src__bootstrap__modules.src__common__directives__ng_for;
  const optimizations = packages__angular__src__core__change_detection__change_detection.src__runtime__optimizations;
  const reflector = packages__angular__src__core__change_detection__change_detection.src__di__reflector;
  const errors = packages__angular__src__core__change_detection__change_detection.src__di__errors;
  const lifecycle_hooks = packages__angular__src__core__change_detection__change_detection.src__core__metadata__lifecycle_hooks;
  const opaque_token = packages__angular__src__core__change_detection__change_detection.src__core__di__opaque_token;
  const angular$46template = packages__angular__angular$46template.angular$46template;
  const rate_limit = packages__stream_transform__src__aggregate_sample.src__rate_limit;
  const response = packages__http__src__base_client.src__response;
  const client = packages__http__src__base_client.src__client;
  const text_binding = packages__angular__src__runtime__text_binding.src__runtime__text_binding;
  const interpolate = packages__angular__src__runtime__interpolate.src__runtime__interpolate;
  const ng_form = packages__angular_forms__src__directives.src__directives__ng_form;
  const validators = packages__angular_forms__src__directives.src__directives__validators;
  const default_value_accessor = packages__angular_forms__src__directives.src__directives__default_value_accessor;
  const control_value_accessor = packages__angular_forms__src__directives.src__directives__control_value_accessor;
  const ng_model = packages__angular_forms__src__directives.src__directives__ng_model;
  const ng_control = packages__angular_forms__src__directives.src__directives__ng_control;
  const control_container = packages__angular_forms__src__directives.src__directives__control_container;
  const model = packages__angular_forms__src__directives.src__model;
  const angular_forms$46template = packages__angular_forms__angular_forms$46template.angular_forms$46template;
  const angular_router$46template = packages__angular_router__angular_router$46template.angular_router$46template;
  var navbar_component$46css$46shim = Object.create(dart.library);
  var not_found_component = Object.create(dart.library);
  var login_component = Object.create(dart.library);
  var login$ = Object.create(dart.library);
  var routes = Object.create(dart.library);
  var not_found_component$46template = Object.create(dart.library);
  var not_found_component$46css$46shim = Object.create(dart.library);
  var painel_component$46template = Object.create(dart.library);
  var carro_service = Object.create(dart.library);
  var carro$ = Object.create(dart.library);
  var tabela_component = Object.create(dart.library);
  var tabela_component$46template = Object.create(dart.library);
  var tabela_component$46css$46shim = Object.create(dart.library);
  var carro_service$46template = Object.create(dart.library);
  var carro$46template = Object.create(dart.library);
  var cadastro_component = Object.create(dart.library);
  var cadastro_component$46template = Object.create(dart.library);
  var cadastro_component$46css$46shim = Object.create(dart.library);
  var navbar_component = Object.create(dart.library);
  var route_paths = Object.create(dart.library);
  var navbar_component$46template = Object.create(dart.library);
  var route_paths$46template = Object.create(dart.library);
  var painel_component = Object.create(dart.library);
  var painel_component$46css$46shim = Object.create(dart.library);
  var login_component$46template = Object.create(dart.library);
  var login_component$46css$46shim = Object.create(dart.library);
  var routes$46template = Object.create(dart.library);
  var login$46template = Object.create(dart.library);
  var $_get = dartx._get;
  var $createElement = dartx.createElement;
  var $append = dartx.append;
  var $sort = dartx.sort;
  var $last = dartx.last;
  var $map = dartx.map;
  var $toList = dartx.toList;
  var $add = dartx.add;
  var $remove = dartx.remove;
  var $firstWhere = dartx.firstWhere;
  var $addEventListener = dartx.addEventListener;
  var $display = dartx.display;
  var $text = dartx.text;
  var IdentityMapOfStringL$dynamic = () => (IdentityMapOfStringL$dynamic = dart.constFn(_js_helper.IdentityMap$(core.String, dart.dynamic)))();
  var MapOfStringL$dynamic = () => (MapOfStringL$dynamic = dart.constFn(core.Map$(core.String, dart.dynamic)))();
  var JSArrayOfRouteDefinitionL = () => (JSArrayOfRouteDefinitionL = dart.constFn(_interceptors.JSArray$(route_definition.RouteDefinition)))();
  var ComponentRefOfNotFoundComponentL = () => (ComponentRefOfNotFoundComponentL = dart.constFn(component_factory.ComponentRef$(not_found_component.NotFoundComponent)))();
  var ComponentFactoryOfNotFoundComponentL = () => (ComponentFactoryOfNotFoundComponentL = dart.constFn(component_factory.ComponentFactory$(not_found_component.NotFoundComponent)))();
  var AppViewOfNotFoundComponentL = () => (AppViewOfNotFoundComponentL = dart.constFn(app_view.AppView$(not_found_component.NotFoundComponent)))();
  var AppViewLAndintLToAppViewLOfNotFoundComponentL = () => (AppViewLAndintLToAppViewLOfNotFoundComponentL = dart.constFn(dart.fnType(AppViewOfNotFoundComponentL(), [app_view.AppView, core.int])))();
  var VoidToNavbarComponentL = () => (VoidToNavbarComponentL = dart.constFn(dart.fnType(navbar_component.NavbarComponent, [])))();
  var VoidToCadastroComponentL = () => (VoidToCadastroComponentL = dart.constFn(dart.fnType(cadastro_component.CadastroComponent, [])))();
  var VoidToTabelaComponentL = () => (VoidToTabelaComponentL = dart.constFn(dart.fnType(tabela_component.TabelaComponent, [])))();
  var ComponentRefOfPainelComponentL = () => (ComponentRefOfPainelComponentL = dart.constFn(component_factory.ComponentRef$(painel_component.PainelComponent)))();
  var ComponentFactoryOfPainelComponentL = () => (ComponentFactoryOfPainelComponentL = dart.constFn(component_factory.ComponentFactory$(painel_component.PainelComponent)))();
  var AppViewOfPainelComponentL = () => (AppViewOfPainelComponentL = dart.constFn(app_view.AppView$(painel_component.PainelComponent)))();
  var AppViewLAndintLToAppViewLOfPainelComponentL = () => (AppViewLAndintLToAppViewLOfPainelComponentL = dart.constFn(dart.fnType(AppViewOfPainelComponentL(), [app_view.AppView, core.int])))();
  var StreamControllerOfCarroL = () => (StreamControllerOfCarroL = dart.constFn(async.StreamController$(carro$.Carro)))();
  var StreamControllerOfStringL = () => (StreamControllerOfStringL = dart.constFn(async.StreamController$(core.String)))();
  var ListOfCarroL = () => (ListOfCarroL = dart.constFn(core.List$(carro$.Carro)))();
  var StreamControllerOfListLOfCarroL = () => (StreamControllerOfListLOfCarroL = dart.constFn(async.StreamController$(ListOfCarroL())))();
  var JSArrayOfCarroL = () => (JSArrayOfCarroL = dart.constFn(_interceptors.JSArray$(carro$.Carro)))();
  var FutureOfNullN = () => (FutureOfNullN = dart.constFn(async.Future$(core.Null)))();
  var StringLToFutureLOfNullN = () => (StringLToFutureLOfNullN = dart.constFn(dart.fnType(FutureOfNullN(), [core.String])))();
  var FutureOrOfListLOfCarroL = () => (FutureOrOfListLOfCarroL = dart.constFn(async.FutureOr$(ListOfCarroL())))();
  var CarroLAndCarroLTointL = () => (CarroLAndCarroLTointL = dart.constFn(dart.fnType(core.int, [carro$.Carro, carro$.Carro])))();
  var dynamicToCarroL = () => (dynamicToCarroL = dart.constFn(dart.fnType(carro$.Carro, [dart.dynamic])))();
  var IdentityMapOfStringL$StringL = () => (IdentityMapOfStringL$StringL = dart.constFn(_js_helper.IdentityMap$(core.String, core.String)))();
  var IdentityMapOfStringL$CarroL = () => (IdentityMapOfStringL$CarroL = dart.constFn(_js_helper.IdentityMap$(core.String, carro$.Carro)))();
  var MapOfStringL$StringL = () => (MapOfStringL$StringL = dart.constFn(core.Map$(core.String, core.String)))();
  var CarroLToboolL = () => (CarroLToboolL = dart.constFn(dart.fnType(core.bool, [carro$.Carro])))();
  var CarroLToNullN = () => (CarroLToNullN = dart.constFn(dart.fnType(core.Null, [carro$.Carro])))();
  var ListLOfCarroLToNullN = () => (ListLOfCarroLToNullN = dart.constFn(dart.fnType(core.Null, [ListOfCarroL()])))();
  var AppViewOfvoid = () => (AppViewOfvoid = dart.constFn(app_view.AppView$(dart.void)))();
  var AppViewLAndintLToAppViewLOfvoid = () => (AppViewLAndintLToAppViewLOfvoid = dart.constFn(dart.fnType(AppViewOfvoid(), [app_view.AppView, core.int])))();
  var ComponentRefOfTabelaComponentL = () => (ComponentRefOfTabelaComponentL = dart.constFn(component_factory.ComponentRef$(tabela_component.TabelaComponent)))();
  var ComponentFactoryOfTabelaComponentL = () => (ComponentFactoryOfTabelaComponentL = dart.constFn(component_factory.ComponentFactory$(tabela_component.TabelaComponent)))();
  var AppViewOfTabelaComponentL = () => (AppViewOfTabelaComponentL = dart.constFn(app_view.AppView$(tabela_component.TabelaComponent)))();
  var AppViewLAndintLToAppViewLOfTabelaComponentL = () => (AppViewLAndintLToAppViewLOfTabelaComponentL = dart.constFn(dart.fnType(AppViewOfTabelaComponentL(), [app_view.AppView, core.int])))();
  var JSArrayOfControlValueAccessorL = () => (JSArrayOfControlValueAccessorL = dart.constFn(_interceptors.JSArray$(control_value_accessor.ControlValueAccessor)))();
  var StreamSubscriptionOfvoid = () => (StreamSubscriptionOfvoid = dart.constFn(async.StreamSubscription$(dart.void)))();
  var JSArrayOfStreamSubscriptionLOfvoid = () => (JSArrayOfStreamSubscriptionLOfvoid = dart.constFn(_interceptors.JSArray$(StreamSubscriptionOfvoid())))();
  var MultiTokenOfControlValueAccessorL = () => (MultiTokenOfControlValueAccessorL = dart.constFn(opaque_token.MultiToken$(control_value_accessor.ControlValueAccessor)))();
  var ControlContainerOfAbstractControlGroupL = () => (ControlContainerOfAbstractControlGroupL = dart.constFn(control_container.ControlContainer$(model.AbstractControlGroup)))();
  var ComponentRefOfCadastroComponentL = () => (ComponentRefOfCadastroComponentL = dart.constFn(component_factory.ComponentRef$(cadastro_component.CadastroComponent)))();
  var ComponentFactoryOfCadastroComponentL = () => (ComponentFactoryOfCadastroComponentL = dart.constFn(component_factory.ComponentFactory$(cadastro_component.CadastroComponent)))();
  var AppViewOfCadastroComponentL = () => (AppViewOfCadastroComponentL = dart.constFn(app_view.AppView$(cadastro_component.CadastroComponent)))();
  var AppViewLAndintLToAppViewLOfCadastroComponentL = () => (AppViewLAndintLToAppViewLOfCadastroComponentL = dart.constFn(dart.fnType(AppViewOfCadastroComponentL(), [app_view.AppView, core.int])))();
  var ComponentRefOfNavbarComponentL = () => (ComponentRefOfNavbarComponentL = dart.constFn(component_factory.ComponentRef$(navbar_component.NavbarComponent)))();
  var ComponentFactoryOfNavbarComponentL = () => (ComponentFactoryOfNavbarComponentL = dart.constFn(component_factory.ComponentFactory$(navbar_component.NavbarComponent)))();
  var AppViewOfNavbarComponentL = () => (AppViewOfNavbarComponentL = dart.constFn(app_view.AppView$(navbar_component.NavbarComponent)))();
  var AppViewLAndintLToAppViewLOfNavbarComponentL = () => (AppViewLAndintLToAppViewLOfNavbarComponentL = dart.constFn(dart.fnType(AppViewOfNavbarComponentL(), [app_view.AppView, core.int])))();
  var VoidToLoginComponentL = () => (VoidToLoginComponentL = dart.constFn(dart.fnType(login_component.LoginComponent, [])))();
  var ComponentRefOfLoginComponentL = () => (ComponentRefOfLoginComponentL = dart.constFn(component_factory.ComponentRef$(login_component.LoginComponent)))();
  var ComponentFactoryOfLoginComponentL = () => (ComponentFactoryOfLoginComponentL = dart.constFn(component_factory.ComponentFactory$(login_component.LoginComponent)))();
  var AppViewOfLoginComponentL = () => (AppViewOfLoginComponentL = dart.constFn(app_view.AppView$(login_component.LoginComponent)))();
  var AppViewLAndintLToAppViewLOfLoginComponentL = () => (AppViewLAndintLToAppViewLOfLoginComponentL = dart.constFn(dart.fnType(AppViewOfLoginComponentL(), [app_view.AppView, core.int])))();
  const CT = Object.create(null);
  var L4 = "package:carros/src/components/not_found_component/not_found_component.template.dart";
  var L3 = "package:carros/src/routes/routes.dart";
  var L6 = "package:carros/src/components/painel_component/painel_component.template.dart";
  var L15 = "package:carros/src/components/navbar_component/navbar_component.template.dart";
  var L1 = "package:carros/src/components/login_component/login_component.dart";
  var L2 = "package:carros/src/model/login.dart";
  var L10 = "package:carros/src/components/tabela_component/tabela_component.template.dart";
  var L0 = "package:carros/src/components/not_found_component/not_found_component.dart";
  var L8 = "package:carros/src/model/carro.dart";
  var L5 = "package:carros/src/components/painel_component/painel_component.dart";
  var L7 = "package:carros/src/services/carro_service.dart";
  var L13 = "package:carros/src/components/navbar_component/navbar_component.dart";
  var L14 = "package:carros/src/routes/route_paths.dart";
  var L16 = "package:carros/src/components/login_component/login_component.template.dart";
  var L11 = "package:carros/src/components/cadastro_component/cadastro_component.dart";
  var L9 = "package:carros/src/components/tabela_component/tabela_component.dart";
  var L12 = "package:carros/src/components/cadastro_component/cadastro_component.template.dart";
  dart.defineLazy(CT, {
    get C1() {
      return C1 = dart.fn(not_found_component$46template.viewFactory_NotFoundComponentHost0, AppViewLAndintLToAppViewLOfNotFoundComponentL());
    },
    get C0() {
      return C0 = dart.const({
        __proto__: ComponentFactoryOfNotFoundComponentL().prototype,
        [ComponentFactory__viewFactory]: C1 || CT.C1,
        [ComponentFactory_selector]: "not-found"
      });
    },
    get C2() {
      return C2 = dart.constList([], dart.dynamic);
    },
    get C4() {
      return C4 = dart.fn(painel_component$46template.viewFactory_PainelComponentHost0, AppViewLAndintLToAppViewLOfPainelComponentL());
    },
    get C3() {
      return C3 = dart.const({
        __proto__: ComponentFactoryOfPainelComponentL().prototype,
        [ComponentFactory__viewFactory]: C4 || CT.C4,
        [ComponentFactory_selector]: "painel"
      });
    },
    get C5() {
      return C5 = dart.fn(tabela_component$46template.viewFactory_TabelaComponent1, AppViewLAndintLToAppViewLOfvoid());
    },
    get C7() {
      return C7 = dart.fn(tabela_component$46template.viewFactory_TabelaComponentHost0, AppViewLAndintLToAppViewLOfTabelaComponentL());
    },
    get C6() {
      return C6 = dart.const({
        __proto__: ComponentFactoryOfTabelaComponentL().prototype,
        [ComponentFactory__viewFactory]: C7 || CT.C7,
        [ComponentFactory_selector]: "tabela"
      });
    },
    get C8() {
      return C8 = dart.constList([], core.Object);
    },
    get C9() {
      return C9 = dart.const({
        __proto__: opaque_token.MultiToken.prototype,
        [OpaqueToken__uniqueName]: "NgValidators"
      });
    },
    get C10() {
      return C10 = dart.const({
        __proto__: MultiTokenOfControlValueAccessorL().prototype,
        [OpaqueToken__uniqueName]: "NgValueAccessor"
      });
    },
    get C12() {
      return C12 = dart.fn(cadastro_component$46template.viewFactory_CadastroComponentHost0, AppViewLAndintLToAppViewLOfCadastroComponentL());
    },
    get C11() {
      return C11 = dart.const({
        __proto__: ComponentFactoryOfCadastroComponentL().prototype,
        [ComponentFactory__viewFactory]: C12 || CT.C12,
        [ComponentFactory_selector]: "cadastro"
      });
    },
    get C14() {
      return C14 = dart.fn(navbar_component$46template.viewFactory_NavbarComponentHost0, AppViewLAndintLToAppViewLOfNavbarComponentL());
    },
    get C13() {
      return C13 = dart.const({
        __proto__: ComponentFactoryOfNavbarComponentL().prototype,
        [ComponentFactory__viewFactory]: C14 || CT.C14,
        [ComponentFactory_selector]: "navbar"
      });
    },
    get C16() {
      return C16 = dart.fn(login_component$46template.viewFactory_LoginComponentHost0, AppViewLAndintLToAppViewLOfLoginComponentL());
    },
    get C15() {
      return C15 = dart.const({
        __proto__: ComponentFactoryOfLoginComponentL().prototype,
        [ComponentFactory__viewFactory]: C16 || CT.C16,
        [ComponentFactory_selector]: "login"
      });
    }
  }, false);
  dart.defineLazy(navbar_component$46css$46shim, {
    /*navbar_component$46css$46shim.styles*/get styles() {
      return ["#icone-exit._ngcontent-%ID%{font-size:30px}"];
    }
  }, true);
  not_found_component.NotFoundComponent = class NotFoundComponent extends core.Object {};
  (not_found_component.NotFoundComponent.new = function() {
    ;
  }).prototype = not_found_component.NotFoundComponent.prototype;
  dart.addTypeTests(not_found_component.NotFoundComponent);
  dart.addTypeCaches(not_found_component.NotFoundComponent);
  dart.setLibraryUri(not_found_component.NotFoundComponent, L0);
  var _router$ = dart.privateName(login_component, "_router");
  login_component.LoginComponent = class LoginComponent extends core.Object {
    Login() {
      return async.async(dart.void, (function* Login() {
        this[_router$].navigate(route_paths.RoutePaths.painel.toUrl());
      }).bind(this));
    }
  };
  (login_component.LoginComponent.new = function(_router) {
    this[_router$] = _router;
    ;
  }).prototype = login_component.LoginComponent.prototype;
  dart.addTypeTests(login_component.LoginComponent);
  dart.addTypeCaches(login_component.LoginComponent);
  dart.setMethodSignature(login_component.LoginComponent, () => ({
    __proto__: dart.getMethods(login_component.LoginComponent.__proto__),
    Login: dart.fnType(dart.void, [])
  }));
  dart.setLibraryUri(login_component.LoginComponent, L1);
  dart.setFieldSignature(login_component.LoginComponent, () => ({
    __proto__: dart.getFields(login_component.LoginComponent.__proto__),
    [_router$]: dart.fieldType(router.Router)
  }));
  var id$ = dart.privateName(login$, "Login.id");
  var login$0 = dart.privateName(login$, "Login.login");
  var nome$ = dart.privateName(login$, "Login.nome");
  var email$ = dart.privateName(login$, "Login.email");
  var urlFoto$ = dart.privateName(login$, "Login.urlFoto");
  var token$ = dart.privateName(login$, "Login.token");
  var roles$ = dart.privateName(login$, "Login.roles");
  login$.Login = class Login extends core.Object {
    get id() {
      return this[id$];
    }
    set id(value) {
      this[id$] = value;
    }
    get login() {
      return this[login$0];
    }
    set login(value) {
      this[login$0] = value;
    }
    get nome() {
      return this[nome$];
    }
    set nome(value) {
      this[nome$] = value;
    }
    get email() {
      return this[email$];
    }
    set email(value) {
      this[email$] = value;
    }
    get urlFoto() {
      return this[urlFoto$];
    }
    set urlFoto(value) {
      this[urlFoto$] = value;
    }
    get token() {
      return this[token$];
    }
    set token(value) {
      this[token$] = value;
    }
    get roles() {
      return this[roles$];
    }
    set roles(value) {
      this[roles$] = value;
    }
    copyWith(opts) {
      let t0, t0$, t0$0, t0$1, t0$2, t0$3, t0$4;
      let id = opts && 'id' in opts ? opts.id : null;
      let login = opts && 'login' in opts ? opts.login : null;
      let nome = opts && 'nome' in opts ? opts.nome : null;
      let email = opts && 'email' in opts ? opts.email : null;
      let urlFoto = opts && 'urlFoto' in opts ? opts.urlFoto : null;
      let token = opts && 'token' in opts ? opts.token : null;
      let roles = opts && 'roles' in opts ? opts.roles : null;
      return new login$.Login.new({id: (t0 = id, t0 == null ? this.id : t0), login: (t0$ = login, t0$ == null ? this.login : t0$), nome: (t0$0 = nome, t0$0 == null ? this.nome : t0$0), email: (t0$1 = email, t0$1 == null ? this.email : t0$1), urlFoto: (t0$2 = urlFoto, t0$2 == null ? this.urlFoto : t0$2), token: (t0$3 = token, t0$3 == null ? this.token : t0$3), roles: (t0$4 = roles, t0$4 == null ? this.roles : t0$4)});
    }
    toMap() {
      let t0, t0$, t0$0, t0$1, t0$2, t0$3, t0$4;
      return new (IdentityMapOfStringL$dynamic()).from(["id", (t0 = this.id, t0 == null ? null : dart.dsend(t0, 'toMap', [])), "login", (t0$ = this.login, t0$ == null ? null : dart.dsend(t0$, 'toMap', [])), "nome", (t0$0 = this.nome, t0$0 == null ? null : dart.dsend(t0$0, 'toMap', [])), "email", (t0$1 = this.email, t0$1 == null ? null : dart.dsend(t0$1, 'toMap', [])), "urlFoto", (t0$2 = this.urlFoto, t0$2 == null ? null : dart.dsend(t0$2, 'toMap', [])), "token", (t0$3 = this.token, t0$3 == null ? null : dart.dsend(t0$3, 'toMap', [])), "roles", (t0$4 = this.roles, t0$4 == null ? null : dart.dsend(t0$4, 'toMap', []))]);
    }
    static fromMap(map) {
      if (map == null) return null;
      return new login$.Login.new({id: map[$_get]("id"), login: map[$_get]("login"), nome: map[$_get]("nome"), email: map[$_get]("email"), urlFoto: map[$_get]("urlFoto"), token: map[$_get]("token"), roles: map[$_get]("roles")});
    }
    toJson() {
      return convert.json.encode(this.toMap());
    }
    static fromJson(source) {
      return login$.Login.fromMap(MapOfStringL$dynamic().as(convert.json.decode(source)));
    }
    toString() {
      return "Login(id: " + dart.str(this.id) + ", login: " + dart.str(this.login) + ", nome: " + dart.str(this.nome) + ", email: " + dart.str(this.email) + ", urlFoto: " + dart.str(this.urlFoto) + ", token: " + dart.str(this.token) + ", roles: " + dart.str(this.roles) + ")";
    }
  };
  (login$.Login.new = function(opts) {
    let id = opts && 'id' in opts ? opts.id : null;
    let login = opts && 'login' in opts ? opts.login : null;
    let nome = opts && 'nome' in opts ? opts.nome : null;
    let email = opts && 'email' in opts ? opts.email : null;
    let urlFoto = opts && 'urlFoto' in opts ? opts.urlFoto : null;
    let token = opts && 'token' in opts ? opts.token : null;
    let roles = opts && 'roles' in opts ? opts.roles : null;
    this[id$] = id;
    this[login$0] = login;
    this[nome$] = nome;
    this[email$] = email;
    this[urlFoto$] = urlFoto;
    this[token$] = token;
    this[roles$] = roles;
    ;
  }).prototype = login$.Login.prototype;
  dart.addTypeTests(login$.Login);
  dart.addTypeCaches(login$.Login);
  dart.setMethodSignature(login$.Login, () => ({
    __proto__: dart.getMethods(login$.Login.__proto__),
    copyWith: dart.fnType(login$.Login, [], {email: dart.dynamic, id: dart.dynamic, login: dart.dynamic, nome: dart.dynamic, roles: dart.dynamic, token: dart.dynamic, urlFoto: dart.dynamic}, {}),
    toMap: dart.fnType(core.Map$(core.String, dart.dynamic), []),
    toJson: dart.fnType(core.String, [])
  }));
  dart.setLibraryUri(login$.Login, L2);
  dart.setFieldSignature(login$.Login, () => ({
    __proto__: dart.getFields(login$.Login.__proto__),
    id: dart.fieldType(dart.dynamic),
    login: dart.fieldType(dart.dynamic),
    nome: dart.fieldType(dart.dynamic),
    email: dart.fieldType(dart.dynamic),
    urlFoto: dart.fieldType(dart.dynamic),
    token: dart.fieldType(dart.dynamic),
    roles: dart.fieldType(dart.dynamic)
  }));
  dart.defineExtensionMethods(login$.Login, ['toString']);
  routes.Routes = class Routes extends core.Object {};
  (routes.Routes.new = function() {
    ;
  }).prototype = routes.Routes.prototype;
  dart.addTypeTests(routes.Routes);
  dart.addTypeCaches(routes.Routes);
  dart.setLibraryUri(routes.Routes, L3);
  dart.defineLazy(routes.Routes, {
    /*routes.Routes.login*/get login() {
      return new route_definition.ComponentRouteDefinition.__({routePath: route_paths.RoutePaths.login, component: login_component$46template.LoginComponentNgFactory});
    },
    /*routes.Routes.painel*/get painel() {
      return new route_definition.ComponentRouteDefinition.__({routePath: route_paths.RoutePaths.painel, component: painel_component$46template.PainelComponentNgFactory});
    },
    /*routes.Routes.notfound*/get notfound() {
      return new route_definition.ComponentRouteDefinition.__({routePath: route_paths.RoutePaths.notfound, component: not_found_component$46template.NotFoundComponentNgFactory});
    },
    /*routes.Routes.all*/get all() {
      return JSArrayOfRouteDefinitionL().of([routes.Routes.login, routes.Routes.painel, routes.Routes.notfound, new route_definition.RedirectRouteDefinition.__({path: "", redirectTo: route_paths.RoutePaths.login.toUrl()}), new route_definition.RedirectRouteDefinition.__({path: ".+", redirectTo: route_paths.RoutePaths.notfound.toUrl()})]);
    }
  }, true);
  not_found_component$46template.ViewNotFoundComponent0 = class ViewNotFoundComponent0 extends app_view.AppView$(not_found_component.NotFoundComponent) {
    static get _debugComponentUrl() {
      return dart.test(optimizations.isDevMode) ? "asset:carros/lib/src/components/not_found_component/not_found_component.dart" : null;
    }
    build() {
      let _rootEl = this.rootEl;
      let parentRenderNode = this.initViewRoot(_rootEl);
      let doc = html.document;
      let _el_0 = dom_helpers.appendElement(doc, parentRenderNode, "h1");
      this.addShimE(_el_0);
      let _text_1 = dom_helpers.appendText(_el_0, "Página Não Existe");
      this.init0();
    }
    initComponentStyles() {
      let styles = not_found_component$46template.ViewNotFoundComponent0._componentStyles;
      if (styles == null) {
        not_found_component$46template.ViewNotFoundComponent0._componentStyles = styles = not_found_component$46template.ViewNotFoundComponent0._componentStyles = style_encapsulation.ComponentStyles.scoped(not_found_component$46template.styles$NotFoundComponent, not_found_component$46template.ViewNotFoundComponent0._debugComponentUrl);
      }
      this.componentStyles = styles;
    }
  };
  (not_found_component$46template.ViewNotFoundComponent0.new = function(parentView, parentIndex) {
    not_found_component$46template.ViewNotFoundComponent0.__proto__.new.call(this, view_type.ViewType.component, parentView, parentIndex, 3);
    this.initComponentStyles();
    this.rootEl = html.HtmlElement.as(html.document[$createElement]("not-found"));
  }).prototype = not_found_component$46template.ViewNotFoundComponent0.prototype;
  dart.addTypeTests(not_found_component$46template.ViewNotFoundComponent0);
  dart.addTypeCaches(not_found_component$46template.ViewNotFoundComponent0);
  dart.setLibraryUri(not_found_component$46template.ViewNotFoundComponent0, L4);
  dart.defineLazy(not_found_component$46template.ViewNotFoundComponent0, {
    /*not_found_component$46template.ViewNotFoundComponent0._componentStyles*/get _componentStyles() {
      return null;
    },
    set _componentStyles(_) {}
  }, true);
  var _compView_0 = dart.privateName(not_found_component$46template, "_compView_0");
  var _NotFoundComponent_0_5 = dart.privateName(not_found_component$46template, "_NotFoundComponent_0_5");
  not_found_component$46template._ViewNotFoundComponentHost0 = class _ViewNotFoundComponentHost0 extends app_view.AppView$(not_found_component.NotFoundComponent) {
    build() {
      this[_compView_0] = new not_found_component$46template.ViewNotFoundComponent0.new(this, 0);
      this.rootEl = this[_compView_0].rootEl;
      this[_NotFoundComponent_0_5] = new not_found_component.NotFoundComponent.new();
      this[_compView_0].create(this[_NotFoundComponent_0_5], this.projectedNodes);
      this.init1(this.rootEl);
      return new (ComponentRefOfNotFoundComponentL()).new(0, this, this.rootEl, this[_NotFoundComponent_0_5]);
    }
    detectChangesInternal() {
      this[_compView_0].detectChanges();
    }
    destroyInternal() {
      this[_compView_0].destroyInternalState();
    }
  };
  (not_found_component$46template._ViewNotFoundComponentHost0.new = function(parentView, parentIndex) {
    this[_compView_0] = null;
    this[_NotFoundComponent_0_5] = null;
    not_found_component$46template._ViewNotFoundComponentHost0.__proto__.new.call(this, view_type.ViewType.host, parentView, parentIndex, 3);
    ;
  }).prototype = not_found_component$46template._ViewNotFoundComponentHost0.prototype;
  dart.addTypeTests(not_found_component$46template._ViewNotFoundComponentHost0);
  dart.addTypeCaches(not_found_component$46template._ViewNotFoundComponentHost0);
  dart.setLibraryUri(not_found_component$46template._ViewNotFoundComponentHost0, L4);
  dart.setFieldSignature(not_found_component$46template._ViewNotFoundComponentHost0, () => ({
    __proto__: dart.getFields(not_found_component$46template._ViewNotFoundComponentHost0.__proto__),
    [_compView_0]: dart.fieldType(not_found_component$46template.ViewNotFoundComponent0),
    [_NotFoundComponent_0_5]: dart.fieldType(not_found_component.NotFoundComponent)
  }));
  not_found_component$46template.viewFactory_NotFoundComponentHost0 = function viewFactory_NotFoundComponentHost0(parentView, parentIndex) {
    return new not_found_component$46template._ViewNotFoundComponentHost0.new(parentView, parentIndex);
  };
  not_found_component$46template.initReflector = function initReflector() {
    if (dart.test(not_found_component$46template._visited)) {
      return;
    }
    not_found_component$46template._visited = true;
    reflector.registerComponent(dart.wrapType(not_found_component.NotFoundComponent), not_found_component$46template.NotFoundComponentNgFactory);
    angular$46template.initReflector();
  };
  dart.copyProperties(not_found_component$46template, {
    get NotFoundComponentNgFactory() {
      return not_found_component$46template._NotFoundComponentNgFactory;
    }
  });
  var C1;
  var ComponentFactory__viewFactory = dart.privateName(component_factory, "ComponentFactory._viewFactory");
  var ComponentFactory_selector = dart.privateName(component_factory, "ComponentFactory.selector");
  var C0;
  var C2;
  dart.defineLazy(not_found_component$46template, {
    /*not_found_component$46template.styles$NotFoundComponent*/get styles$NotFoundComponent() {
      return [not_found_component$46css$46shim.styles];
    },
    /*not_found_component$46template._NotFoundComponentNgFactory*/get _NotFoundComponentNgFactory() {
      return C0 || CT.C0;
    },
    /*not_found_component$46template.styles$NotFoundComponentHost*/get styles$NotFoundComponentHost() {
      return C2 || CT.C2;
    },
    /*not_found_component$46template._visited*/get _visited() {
      return false;
    },
    set _visited(_) {}
  }, true);
  dart.defineLazy(not_found_component$46css$46shim, {
    /*not_found_component$46css$46shim.styles*/get styles() {
      return [""];
    }
  }, true);
  var _compView_0$ = dart.privateName(painel_component$46template, "_compView_0");
  var _NavbarComponent_0_5 = dart.privateName(painel_component$46template, "_NavbarComponent_0_5");
  var _compView_1 = dart.privateName(painel_component$46template, "_compView_1");
  var _CadastroComponent_1_5 = dart.privateName(painel_component$46template, "_CadastroComponent_1_5");
  var _compView_2 = dart.privateName(painel_component$46template, "_compView_2");
  var _TabelaComponent_2_5 = dart.privateName(painel_component$46template, "_TabelaComponent_2_5");
  painel_component.PainelComponent = class PainelComponent extends core.Object {};
  (painel_component.PainelComponent.new = function() {
    ;
  }).prototype = painel_component.PainelComponent.prototype;
  dart.addTypeTests(painel_component.PainelComponent);
  dart.addTypeCaches(painel_component.PainelComponent);
  dart.setLibraryUri(painel_component.PainelComponent, L5);
  painel_component$46template.ViewPainelComponent0 = class ViewPainelComponent0 extends app_view.AppView$(painel_component.PainelComponent) {
    static get _debugComponentUrl() {
      return dart.test(optimizations.isDevMode) ? "asset:carros/lib/src/components/painel_component/painel_component.dart" : null;
    }
    build() {
      let _rootEl = this.rootEl;
      let parentRenderNode = this.initViewRoot(_rootEl);
      this[_compView_0$] = new navbar_component$46template.ViewNavbarComponent0.new(this, 0);
      let _el_0 = this[_compView_0$].rootEl;
      parentRenderNode[$append](_el_0);
      this.addShimC(_el_0);
      this[_NavbarComponent_0_5] = dart.test(optimizations.isDevMode) ? errors.debugInjectorWrap(navbar_component.NavbarComponent, dart.wrapType(navbar_component.NavbarComponent), dart.fn(() => new navbar_component.NavbarComponent.new(carro_service.CarroService.as(this.parentView.injectorGet(dart.wrapType(carro_service.CarroService), this.viewData.parentIndex)), router.Router.as(this.parentView.injectorGet(dart.wrapType(router.Router), this.viewData.parentIndex))), VoidToNavbarComponentL())) : new navbar_component.NavbarComponent.new(carro_service.CarroService.as(this.parentView.injectorGet(dart.wrapType(carro_service.CarroService), this.viewData.parentIndex)), router.Router.as(this.parentView.injectorGet(dart.wrapType(router.Router), this.viewData.parentIndex)));
      this[_compView_0$].create0(this[_NavbarComponent_0_5]);
      this[_compView_1] = new cadastro_component$46template.ViewCadastroComponent0.new(this, 1);
      let _el_1 = this[_compView_1].rootEl;
      parentRenderNode[$append](_el_1);
      this.addShimC(_el_1);
      this[_CadastroComponent_1_5] = dart.test(optimizations.isDevMode) ? errors.debugInjectorWrap(cadastro_component.CadastroComponent, dart.wrapType(cadastro_component.CadastroComponent), dart.fn(() => new cadastro_component.CadastroComponent.new(carro_service.CarroService.as(this.parentView.injectorGet(dart.wrapType(carro_service.CarroService), this.viewData.parentIndex))), VoidToCadastroComponentL())) : new cadastro_component.CadastroComponent.new(carro_service.CarroService.as(this.parentView.injectorGet(dart.wrapType(carro_service.CarroService), this.viewData.parentIndex)));
      this[_compView_1].create0(this[_CadastroComponent_1_5]);
      this[_compView_2] = new tabela_component$46template.ViewTabelaComponent0.new(this, 2);
      let _el_2 = this[_compView_2].rootEl;
      parentRenderNode[$append](_el_2);
      this.addShimC(_el_2);
      this[_TabelaComponent_2_5] = dart.test(optimizations.isDevMode) ? errors.debugInjectorWrap(tabela_component.TabelaComponent, dart.wrapType(tabela_component.TabelaComponent), dart.fn(() => new tabela_component.TabelaComponent.new(carro_service.CarroService.as(this.parentView.injectorGet(dart.wrapType(carro_service.CarroService), this.viewData.parentIndex))), VoidToTabelaComponentL())) : new tabela_component.TabelaComponent.new(carro_service.CarroService.as(this.parentView.injectorGet(dart.wrapType(carro_service.CarroService), this.viewData.parentIndex)));
      this[_compView_2].create0(this[_TabelaComponent_2_5]);
      this.init0();
    }
    detectChangesInternal() {
      let firstCheck = this.cdState === 0;
      if (!dart.test(app_view_utils.AppViewUtils.throwOnChanges) && firstCheck) {
        this[_CadastroComponent_1_5].ngOnInit();
      }
      if (!dart.test(app_view_utils.AppViewUtils.throwOnChanges) && firstCheck) {
        this[_TabelaComponent_2_5].ngOnInit();
      }
      this[_compView_0$].detectChanges();
      this[_compView_1].detectChanges();
      this[_compView_2].detectChanges();
    }
    destroyInternal() {
      this[_compView_0$].destroyInternalState();
      this[_compView_1].destroyInternalState();
      this[_compView_2].destroyInternalState();
    }
    initComponentStyles() {
      let styles = painel_component$46template.ViewPainelComponent0._componentStyles;
      if (styles == null) {
        painel_component$46template.ViewPainelComponent0._componentStyles = styles = painel_component$46template.ViewPainelComponent0._componentStyles = style_encapsulation.ComponentStyles.scoped(painel_component$46template.styles$PainelComponent, painel_component$46template.ViewPainelComponent0._debugComponentUrl);
      }
      this.componentStyles = styles;
    }
  };
  (painel_component$46template.ViewPainelComponent0.new = function(parentView, parentIndex) {
    this[_compView_0$] = null;
    this[_NavbarComponent_0_5] = null;
    this[_compView_1] = null;
    this[_CadastroComponent_1_5] = null;
    this[_compView_2] = null;
    this[_TabelaComponent_2_5] = null;
    painel_component$46template.ViewPainelComponent0.__proto__.new.call(this, view_type.ViewType.component, parentView, parentIndex, 3);
    this.initComponentStyles();
    this.rootEl = html.HtmlElement.as(html.document[$createElement]("painel"));
  }).prototype = painel_component$46template.ViewPainelComponent0.prototype;
  dart.addTypeTests(painel_component$46template.ViewPainelComponent0);
  dart.addTypeCaches(painel_component$46template.ViewPainelComponent0);
  dart.setLibraryUri(painel_component$46template.ViewPainelComponent0, L6);
  dart.setFieldSignature(painel_component$46template.ViewPainelComponent0, () => ({
    __proto__: dart.getFields(painel_component$46template.ViewPainelComponent0.__proto__),
    [_compView_0$]: dart.fieldType(navbar_component$46template.ViewNavbarComponent0),
    [_NavbarComponent_0_5]: dart.fieldType(navbar_component.NavbarComponent),
    [_compView_1]: dart.fieldType(cadastro_component$46template.ViewCadastroComponent0),
    [_CadastroComponent_1_5]: dart.fieldType(cadastro_component.CadastroComponent),
    [_compView_2]: dart.fieldType(tabela_component$46template.ViewTabelaComponent0),
    [_TabelaComponent_2_5]: dart.fieldType(tabela_component.TabelaComponent)
  }));
  dart.defineLazy(painel_component$46template.ViewPainelComponent0, {
    /*painel_component$46template.ViewPainelComponent0._componentStyles*/get _componentStyles() {
      return null;
    },
    set _componentStyles(_) {}
  }, true);
  var _PainelComponent_0_5 = dart.privateName(painel_component$46template, "_PainelComponent_0_5");
  painel_component$46template._ViewPainelComponentHost0 = class _ViewPainelComponentHost0 extends app_view.AppView$(painel_component.PainelComponent) {
    build() {
      this[_compView_0$] = new painel_component$46template.ViewPainelComponent0.new(this, 0);
      this.rootEl = this[_compView_0$].rootEl;
      this[_PainelComponent_0_5] = new painel_component.PainelComponent.new();
      this[_compView_0$].create(this[_PainelComponent_0_5], this.projectedNodes);
      this.init1(this.rootEl);
      return new (ComponentRefOfPainelComponentL()).new(0, this, this.rootEl, this[_PainelComponent_0_5]);
    }
    detectChangesInternal() {
      this[_compView_0$].detectChanges();
    }
    destroyInternal() {
      this[_compView_0$].destroyInternalState();
    }
  };
  (painel_component$46template._ViewPainelComponentHost0.new = function(parentView, parentIndex) {
    this[_compView_0$] = null;
    this[_PainelComponent_0_5] = null;
    painel_component$46template._ViewPainelComponentHost0.__proto__.new.call(this, view_type.ViewType.host, parentView, parentIndex, 3);
    ;
  }).prototype = painel_component$46template._ViewPainelComponentHost0.prototype;
  dart.addTypeTests(painel_component$46template._ViewPainelComponentHost0);
  dart.addTypeCaches(painel_component$46template._ViewPainelComponentHost0);
  dart.setLibraryUri(painel_component$46template._ViewPainelComponentHost0, L6);
  dart.setFieldSignature(painel_component$46template._ViewPainelComponentHost0, () => ({
    __proto__: dart.getFields(painel_component$46template._ViewPainelComponentHost0.__proto__),
    [_compView_0$]: dart.fieldType(painel_component$46template.ViewPainelComponent0),
    [_PainelComponent_0_5]: dart.fieldType(painel_component.PainelComponent)
  }));
  painel_component$46template.viewFactory_PainelComponentHost0 = function viewFactory_PainelComponentHost0(parentView, parentIndex) {
    return new painel_component$46template._ViewPainelComponentHost0.new(parentView, parentIndex);
  };
  painel_component$46template.initReflector = function initReflector$() {
    if (dart.test(painel_component$46template._visited)) {
      return;
    }
    painel_component$46template._visited = true;
    reflector.registerComponent(dart.wrapType(painel_component.PainelComponent), painel_component$46template.PainelComponentNgFactory);
    cadastro_component$46template.initReflector();
    navbar_component$46template.initReflector();
    tabela_component$46template.initReflector();
    angular$46template.initReflector();
  };
  dart.copyProperties(painel_component$46template, {
    get PainelComponentNgFactory() {
      return painel_component$46template._PainelComponentNgFactory;
    }
  });
  var C4;
  var C3;
  dart.defineLazy(painel_component$46template, {
    /*painel_component$46template.styles$PainelComponent*/get styles$PainelComponent() {
      return [painel_component$46css$46shim.styles];
    },
    /*painel_component$46template._PainelComponentNgFactory*/get _PainelComponentNgFactory() {
      return C3 || CT.C3;
    },
    /*painel_component$46template.styles$PainelComponentHost*/get styles$PainelComponentHost() {
      return C2 || CT.C2;
    },
    /*painel_component$46template._visited*/get _visited() {
      return false;
    },
    set _visited(_) {}
  }, true);
  var _http$ = dart.privateName(carro_service, "_http");
  var _extractData = dart.privateName(carro_service, "_extractData");
  var _handleError = dart.privateName(carro_service, "_handleError");
  var streamControllerCarro = dart.privateName(carro_service, "CarroService.streamControllerCarro");
  var streamControllerPesquisa = dart.privateName(carro_service, "CarroService.streamControllerPesquisa");
  var streamControllerListaCarro = dart.privateName(carro_service, "CarroService.streamControllerListaCarro");
  var streamCarro = dart.privateName(carro_service, "CarroService.streamCarro");
  var streamPesquisa = dart.privateName(carro_service, "CarroService.streamPesquisa");
  var streamListaCarro = dart.privateName(carro_service, "CarroService.streamListaCarro");
  var listaCarros = dart.privateName(carro_service, "CarroService.listaCarros");
  carro_service.CarroService = class CarroService extends core.Object {
    get streamControllerCarro() {
      return this[streamControllerCarro];
    }
    set streamControllerCarro(value) {
      this[streamControllerCarro] = value;
    }
    get streamControllerPesquisa() {
      return this[streamControllerPesquisa];
    }
    set streamControllerPesquisa(value) {
      this[streamControllerPesquisa] = value;
    }
    get streamControllerListaCarro() {
      return this[streamControllerListaCarro];
    }
    set streamControllerListaCarro(value) {
      this[streamControllerListaCarro] = value;
    }
    get streamCarro() {
      return this[streamCarro];
    }
    set streamCarro(value) {
      this[streamCarro] = value;
    }
    get streamPesquisa() {
      return this[streamPesquisa];
    }
    set streamPesquisa(value) {
      this[streamPesquisa] = value;
    }
    get streamListaCarro() {
      return this[streamListaCarro];
    }
    set streamListaCarro(value) {
      this[streamListaCarro] = value;
    }
    get listaCarros() {
      return this[listaCarros];
    }
    set listaCarros(value) {
      this[listaCarros] = value;
    }
    SelecionarCarro(carro) {
      return async.async(dart.void, (function* SelecionarCarro() {
        let selecionaCarroUrl = "api/carros" + "/" + dart.str(carro.codigoCarro);
        let response = (yield this[_http$].get(selecionaCarroUrl));
        let carroMap = dart.dsend(convert.json.decode(response.body), '_get', ["data"]);
        let carroSelecionado = carro$.Carro.fromMap(MapOfStringL$dynamic().as(carroMap));
        this.streamControllerCarro.add(carroSelecionado);
      }).bind(this));
    }
    pesquisaCarros(termo) {
      return async.async(ListOfCarroL(), (function* pesquisaCarros() {
        try {
          let pesquisaCarrosUrl = "api/carros" + "/?nomeCarro=" + dart.str(termo);
          let response = (yield this[_http$].get(pesquisaCarrosUrl));
          let carros = this[_extractData](response);
          return FutureOrOfListLOfCarroL().as(carros);
        } catch (e$) {
          let e = dart.getThrown(e$);
          dart.throw(this[_handleError](e));
        }
      }).bind(this));
    }
    RetornaProxCodigo() {
      this.listaCarros[$sort](dart.fn((carroa, carrob) => core.int.as(dart.dsend(carroa.codigoCarro, 'compareTo', [carrob.codigoCarro])), CarroLAndCarroLTointL()));
      return core.int.as(dart.dsend(this.listaCarros[$last].codigoCarro, '+', [1]));
    }
    GetListaCarros() {
      return this.listaCarros;
    }
    GetListaCarros2() {
      return async.async(ListOfCarroL(), (function* GetListaCarros2() {
        try {
          let response = (yield this[_http$].get("api/carros"));
          let carros = this[_extractData](response);
          return FutureOrOfListLOfCarroL().as(carros);
        } catch (e$) {
          let e = dart.getThrown(e$);
          dart.throw(this[_handleError](e));
        }
      }).bind(this));
    }
    [_extractData](resp) {
      return core.List.as(dart.dsend(convert.json.decode(resp.body), '_get', ["data"]))[$map](carro$.Carro, dart.fn(carro => carro$.Carro.fromMap(MapOfStringL$dynamic().as(carro)), dynamicToCarroL()))[$toList]();
    }
    [_handleError](e) {
      core.print(e);
      return core.Exception.new("Server error; cause: " + dart.str(e));
    }
    CadastrarCarro(carro) {
      carro.codigoCarro = this.RetornaProxCodigo();
      try {
        this.listaCarros[$add](carro);
        return new (IdentityMapOfStringL$StringL()).from(["codigo", "0", "mensagem", "Carro cadastrado com sucesso!"]);
      } catch (e$) {
        let e = dart.getThrown(e$);
        return new (IdentityMapOfStringL$StringL()).from(["codigo", "-1", "mensagem", "Erro ao tentar cadastrar o carro:" + dart.notNull(core.String.as(e))]);
      }
    }
    CadastrarCarro2(carro) {
      return async.async(MapOfStringL$StringL(), (function* CadastrarCarro2() {
        try {
          let response = (yield this[_http$].post("api/carros", {headers: carro_service.CarroService._headers, body: convert.json.encode(new (IdentityMapOfStringL$CarroL()).from(["carro", carro]))}));
          let listaCarrosAtualizada = this[_extractData](response);
          this.streamControllerListaCarro.add(ListOfCarroL().as(listaCarrosAtualizada));
          return new (IdentityMapOfStringL$StringL()).from(["codigo", "0", "mensagem", "Carro cadastrado com sucesso!"]);
        } catch (e$) {
          let e = dart.getThrown(e$);
          return new (IdentityMapOfStringL$StringL()).from(["codigo", "-1", "mensagem", "Erro ao tentar cadastrar o carro:" + dart.notNull(core.String.as(e))]);
        }
      }).bind(this));
    }
    DeletarCarro(carro) {
      this.listaCarros[$remove](carro);
      this.streamControllerListaCarro.add(this.listaCarros);
    }
    DeletarCarro2(carroExclusao) {
      return async.async(dart.void, (function* DeletarCarro2() {
        let url = "api/carros" + "/" + dart.str(carroExclusao.codigoCarro);
        let response = (yield this[_http$].delete(url));
        let listaCarrosAtualizada = this[_extractData](response);
        this.streamControllerListaCarro.add(ListOfCarroL().as(listaCarrosAtualizada));
      }).bind(this));
    }
    AlterarCarro(carroCadastro) {
      try {
        let carroAtual = this.listaCarros[$firstWhere](dart.fn(carro => dart.equals(carro.codigoCarro, carroCadastro.codigoCarro), CarroLToboolL()));
        this.CloneCarro(carroAtual, carroCadastro);
        return new (IdentityMapOfStringL$StringL()).from(["codigo", "0", "mensagem", "Carro alterado com sucesso!"]);
      } catch (e$) {
        let e = dart.getThrown(e$);
        return new (IdentityMapOfStringL$StringL()).from(["codigo", "-1", "mensagem", "Erro ao tentar alterar o carro:"]);
      }
    }
    AlterarCarro2(carroAlteracao) {
      return async.async(MapOfStringL$StringL(), (function* AlterarCarro2() {
        try {
          let url = "api/carros" + "/" + dart.str(carroAlteracao.codigoCarro);
          let response = (yield this[_http$].put(url, {headers: carro_service.CarroService._headers, body: convert.json.encode(carroAlteracao)}));
          let listaCarrosAtualizada = this[_extractData](response);
          this.streamControllerListaCarro.add(ListOfCarroL().as(listaCarrosAtualizada));
          return new (IdentityMapOfStringL$StringL()).from(["codigo", "0", "mensagem", "Carro alterado com sucesso!"]);
        } catch (e$) {
          let e = dart.getThrown(e$);
          return new (IdentityMapOfStringL$StringL()).from(["codigo", "-1", "mensagem", "Erro ao tentar alterar o carro:"]);
        }
      }).bind(this));
    }
    CloneCarro(carroAtual, carroNovo) {
      carroAtual.anoFabricacao = carroNovo.anoFabricacao;
      carroAtual.imagem = carroNovo.imagem;
      carroAtual.nomeFabricante = carroNovo.nomeFabricante;
      carroAtual.preco = carroNovo.preco;
      carroAtual.nomeCarro = carroNovo.nomeCarro;
    }
  };
  (carro_service.CarroService.new = function(_http) {
    this[streamControllerCarro] = StreamControllerOfCarroL().broadcast();
    this[streamControllerPesquisa] = StreamControllerOfStringL().broadcast();
    this[streamControllerListaCarro] = StreamControllerOfListLOfCarroL().broadcast();
    this[streamCarro] = null;
    this[streamPesquisa] = null;
    this[streamListaCarro] = null;
    this[listaCarros] = JSArrayOfCarroL().of([new carro$.Carro.new(3, "Gol GTI", "Volksvagem", "1994", 15000.2, "https://quatrorodas.abril.com.br/wp-content/uploads/2018/11/gol-gti-modelo-1989-da-volkswagen-de-propriedade-do-publicitc3a1rio-rafael-carmin_1.jpg?quality=70&strip=info"), new carro$.Carro.new(2, "Scort XR3", "Ford", "1995", 16500.2, "https://quatrorodas.abril.com.br/wp-content/uploads/2019/06/qr-721-classicos-xr3-01.tif_-e1559850002131.jpg?quality=70&strip=info"), new carro$.Carro.new(1, "Apolo", "Volksvagem", "1997", 17000.2, "https://quatrorodas.abril.com.br/wp-content/uploads/2016/11/56be66f80e21630a3e127f80qr-667-grandes-brasileiros-02-psd.jpeg?quality=70&strip=all&strip=all"), new carro$.Carro.new(4, "Verona", "Ford", "1998", 18300.2, "https://combustivel.app/imgs/t650/consumo-verona-lx-1-6.jpg")]);
    this[_http$] = _http;
    this.streamCarro = this.streamControllerCarro.stream;
    this.streamPesquisa = rate_limit['RateLimit|debounce'](core.String, this.streamControllerPesquisa.stream, new core.Duration.new({microseconds: 300})).distinct();
    this.streamListaCarro = this.streamControllerListaCarro.stream;
    this.streamPesquisa.listen(dart.fn(termo => async.async(core.Null, (function*() {
      let listaCarroPesquisa = (yield this.pesquisaCarros(termo));
      this.streamControllerListaCarro.add(listaCarroPesquisa);
    }).bind(this)), StringLToFutureLOfNullN()));
  }).prototype = carro_service.CarroService.prototype;
  dart.addTypeTests(carro_service.CarroService);
  dart.addTypeCaches(carro_service.CarroService);
  dart.setMethodSignature(carro_service.CarroService, () => ({
    __proto__: dart.getMethods(carro_service.CarroService.__proto__),
    SelecionarCarro: dart.fnType(dart.void, [carro$.Carro]),
    pesquisaCarros: dart.fnType(async.Future$(core.List$(carro$.Carro)), [dart.dynamic]),
    RetornaProxCodigo: dart.fnType(core.int, []),
    GetListaCarros: dart.fnType(core.List$(carro$.Carro), []),
    GetListaCarros2: dart.fnType(async.Future$(core.List$(carro$.Carro)), []),
    [_extractData]: dart.fnType(dart.dynamic, [response.Response]),
    [_handleError]: dart.fnType(core.Exception, [dart.dynamic]),
    CadastrarCarro: dart.fnType(core.Map$(core.String, core.String), [carro$.Carro]),
    CadastrarCarro2: dart.fnType(async.Future$(core.Map$(core.String, core.String)), [carro$.Carro]),
    DeletarCarro: dart.fnType(dart.void, [carro$.Carro]),
    DeletarCarro2: dart.fnType(async.Future$(dart.void), [carro$.Carro]),
    AlterarCarro: dart.fnType(core.Map$(core.String, core.String), [carro$.Carro]),
    AlterarCarro2: dart.fnType(async.Future$(core.Map$(core.String, core.String)), [carro$.Carro]),
    CloneCarro: dart.fnType(dart.void, [carro$.Carro, carro$.Carro])
  }));
  dart.setLibraryUri(carro_service.CarroService, L7);
  dart.setFieldSignature(carro_service.CarroService, () => ({
    __proto__: dart.getFields(carro_service.CarroService.__proto__),
    streamControllerCarro: dart.fieldType(async.StreamController$(carro$.Carro)),
    streamControllerPesquisa: dart.fieldType(async.StreamController$(core.String)),
    streamControllerListaCarro: dart.fieldType(async.StreamController$(core.List$(carro$.Carro))),
    streamCarro: dart.fieldType(async.Stream$(carro$.Carro)),
    streamPesquisa: dart.fieldType(async.Stream$(core.String)),
    streamListaCarro: dart.fieldType(async.Stream$(core.List$(carro$.Carro))),
    [_http$]: dart.finalFieldType(client.Client),
    listaCarros: dart.fieldType(core.List$(carro$.Carro))
  }));
  dart.defineLazy(carro_service.CarroService, {
    /*carro_service.CarroService._carrosUrl*/get _carrosUrl() {
      return "api/carros";
    },
    /*carro_service.CarroService._headers*/get _headers() {
      return new (IdentityMapOfStringL$StringL()).from(["Content-Type", "application/json"]);
    }
  }, true);
  var codigoCarro$ = dart.privateName(carro$, "Carro.codigoCarro");
  var nomeCarro$ = dart.privateName(carro$, "Carro.nomeCarro");
  var nomeFabricante$ = dart.privateName(carro$, "Carro.nomeFabricante");
  var anoFabricacao$ = dart.privateName(carro$, "Carro.anoFabricacao");
  var preco$ = dart.privateName(carro$, "Carro.preco");
  var imagem$ = dart.privateName(carro$, "Carro.imagem");
  carro$.Carro = class Carro extends core.Object {
    get codigoCarro() {
      return this[codigoCarro$];
    }
    set codigoCarro(value) {
      this[codigoCarro$] = value;
    }
    get nomeCarro() {
      return this[nomeCarro$];
    }
    set nomeCarro(value) {
      this[nomeCarro$] = value;
    }
    get nomeFabricante() {
      return this[nomeFabricante$];
    }
    set nomeFabricante(value) {
      this[nomeFabricante$] = value;
    }
    get anoFabricacao() {
      return this[anoFabricacao$];
    }
    set anoFabricacao(value) {
      this[anoFabricacao$] = value;
    }
    get preco() {
      return this[preco$];
    }
    set preco(value) {
      this[preco$] = value;
    }
    get imagem() {
      return this[imagem$];
    }
    set imagem(value) {
      this[imagem$] = value;
    }
    static fromJson(jason) {
      return carro$.Carro.fromMap(MapOfStringL$dynamic().as(convert.json.decode(jason)));
    }
    static fromMap(carro) {
      return new carro$.Carro.new(carro$._toInt(carro[$_get]("codigoCarro")), carro[$_get]("nomeCarro"), carro[$_get]("nomeFabricante"), carro$._toInt(carro[$_get]("anoFabricacao")), carro$._toDouble(carro[$_get]("preco")), carro[$_get]("imagem"));
    }
    toJson() {
      return new (IdentityMapOfStringL$dynamic()).from(["codigoCarro", this.codigoCarro, "nomeCarro", this.nomeCarro, "nomeFabricante", this.nomeFabricante, "anoFabricacao", this.anoFabricacao, "preco", this.preco, "imagem", this.imagem]);
    }
    CloneCarro(carroAlteracao) {
      this.anoFabricacao = carroAlteracao.anoFabricacao;
      this.imagem = carroAlteracao.imagem;
      this.nomeFabricante = carroAlteracao.nomeFabricante;
      this.preco = carroAlteracao.preco;
      this.nomeCarro = carroAlteracao.nomeCarro;
    }
  };
  (carro$.Carro.new = function(codigoCarro, nomeCarro, nomeFabricante, anoFabricacao, preco, imagem) {
    this[codigoCarro$] = codigoCarro;
    this[nomeCarro$] = nomeCarro;
    this[nomeFabricante$] = nomeFabricante;
    this[anoFabricacao$] = anoFabricacao;
    this[preco$] = preco;
    this[imagem$] = imagem;
    ;
  }).prototype = carro$.Carro.prototype;
  dart.addTypeTests(carro$.Carro);
  dart.addTypeCaches(carro$.Carro);
  dart.setMethodSignature(carro$.Carro, () => ({
    __proto__: dart.getMethods(carro$.Carro.__proto__),
    toJson: dart.fnType(core.Map$(core.String, dart.dynamic), []),
    CloneCarro: dart.fnType(dart.void, [carro$.Carro])
  }));
  dart.setLibraryUri(carro$.Carro, L8);
  dart.setFieldSignature(carro$.Carro, () => ({
    __proto__: dart.getFields(carro$.Carro.__proto__),
    codigoCarro: dart.fieldType(dart.dynamic),
    nomeCarro: dart.fieldType(dart.dynamic),
    nomeFabricante: dart.fieldType(dart.dynamic),
    anoFabricacao: dart.fieldType(dart.dynamic),
    preco: dart.fieldType(dart.dynamic),
    imagem: dart.fieldType(dart.dynamic)
  }));
  carro$._toInt = function _toInt(number) {
    return core.int.is(number) ? number : core.int.parse(core.String.as(number));
  };
  carro$._toDouble = function _toDouble(number) {
    return typeof number == 'number' ? number : core.double.parse(core.String.as(number));
  };
  var _carroServico$ = dart.privateName(tabela_component, "_carroServico");
  var listaCarros$ = dart.privateName(tabela_component, "TabelaComponent.listaCarros");
  var listaCarros2 = dart.privateName(tabela_component, "TabelaComponent.listaCarros2");
  tabela_component.TabelaComponent = class TabelaComponent extends lifecycle_hooks.OnInit {
    get listaCarros() {
      return this[listaCarros$];
    }
    set listaCarros(value) {
      this[listaCarros$] = value;
    }
    get listaCarros2() {
      return this[listaCarros2];
    }
    set listaCarros2(value) {
      this[listaCarros2] = value;
    }
    ngOnInit() {
      return async.async(dart.void, (function* ngOnInit() {
        this.listaCarros = (yield this[_carroServico$].GetListaCarros2());
        this[_carroServico$].streamCarro.listen(dart.fn(carro => {
          core.print("[TABELA_COMPONENT] O Carro " + dart.notNull(core.String.as(carro.nomeCarro)) + "foi recebido no fluxo.");
        }, CarroLToNullN()));
        this[_carroServico$].streamListaCarro.listen(dart.fn(listaCarroPesquisa => {
          this.listaCarros = listaCarroPesquisa;
        }, ListLOfCarroLToNullN()));
      }).bind(this));
    }
    DeletarCarro(carro) {
      this[_carroServico$].DeletarCarro2(carro);
    }
    SelecionarCarro(carro) {
      this[_carroServico$].SelecionarCarro(carro);
    }
  };
  (tabela_component.TabelaComponent.new = function(_carroServico) {
    this[listaCarros$] = null;
    this[listaCarros2] = null;
    this[_carroServico$] = _carroServico;
    ;
  }).prototype = tabela_component.TabelaComponent.prototype;
  dart.addTypeTests(tabela_component.TabelaComponent);
  dart.addTypeCaches(tabela_component.TabelaComponent);
  dart.setMethodSignature(tabela_component.TabelaComponent, () => ({
    __proto__: dart.getMethods(tabela_component.TabelaComponent.__proto__),
    ngOnInit: dart.fnType(dart.void, []),
    DeletarCarro: dart.fnType(dart.void, [carro$.Carro]),
    SelecionarCarro: dart.fnType(dart.void, [carro$.Carro])
  }));
  dart.setLibraryUri(tabela_component.TabelaComponent, L9);
  dart.setFieldSignature(tabela_component.TabelaComponent, () => ({
    __proto__: dart.getFields(tabela_component.TabelaComponent.__proto__),
    [_carroServico$]: dart.finalFieldType(carro_service.CarroService),
    listaCarros: dart.fieldType(core.List$(carro$.Carro)),
    listaCarros2: dart.fieldType(core.List$(carro$.Carro))
  }));
  var _appEl_18 = dart.privateName(tabela_component$46template, "_appEl_18");
  var _NgFor_18_9 = dart.privateName(tabela_component$46template, "_NgFor_18_9");
  var _expr_0 = dart.privateName(tabela_component$46template, "_expr_0");
  var C5;
  tabela_component$46template.ViewTabelaComponent0 = class ViewTabelaComponent0 extends app_view.AppView$(tabela_component.TabelaComponent) {
    static get _debugComponentUrl() {
      return dart.test(optimizations.isDevMode) ? "asset:carros/lib/src/components/tabela_component/tabela_component.dart" : null;
    }
    build() {
      let _rootEl = this.rootEl;
      let parentRenderNode = this.initViewRoot(_rootEl);
      let doc = html.document;
      let _el_0 = dom_helpers.appendDiv(doc, parentRenderNode);
      this.updateChildClass(_el_0, "row");
      dom_helpers.setAttribute(_el_0, "id", "tabela-carro");
      this.addShimC(_el_0);
      let _el_1 = dom_helpers.appendElement(doc, _el_0, "table");
      this.updateChildClass(html.HtmlElement.as(_el_1), "table");
      this.addShimC(html.HtmlElement.as(_el_1));
      let _el_2 = dom_helpers.appendElement(doc, _el_1, "thead");
      this.updateChildClass(html.HtmlElement.as(_el_2), "thead-dark");
      this.addShimE(_el_2);
      let _el_3 = dom_helpers.appendElement(doc, _el_2, "tr");
      this.addShimE(_el_3);
      let _el_4 = dom_helpers.appendElement(doc, _el_3, "th");
      dom_helpers.setAttribute(_el_4, "scope", "col");
      this.addShimE(_el_4);
      let _text_5 = dom_helpers.appendText(_el_4, "Código");
      let _el_6 = dom_helpers.appendElement(doc, _el_3, "th");
      dom_helpers.setAttribute(_el_6, "scope", "col");
      this.addShimE(_el_6);
      let _text_7 = dom_helpers.appendText(_el_6, "Nome do Carro");
      let _el_8 = dom_helpers.appendElement(doc, _el_3, "th");
      dom_helpers.setAttribute(_el_8, "scope", "col");
      this.addShimE(_el_8);
      let _text_9 = dom_helpers.appendText(_el_8, "Fabricante");
      let _el_10 = dom_helpers.appendElement(doc, _el_3, "th");
      dom_helpers.setAttribute(_el_10, "scope", "col");
      this.addShimE(_el_10);
      let _text_11 = dom_helpers.appendText(_el_10, "Ano");
      let _el_12 = dom_helpers.appendElement(doc, _el_3, "th");
      dom_helpers.setAttribute(_el_12, "scope", "col");
      this.addShimE(_el_12);
      let _text_13 = dom_helpers.appendText(_el_12, "Preço");
      let _el_14 = dom_helpers.appendElement(doc, _el_3, "th");
      dom_helpers.setAttribute(_el_14, "scope", "col");
      this.addShimE(_el_14);
      let _text_15 = dom_helpers.appendText(_el_14, "Imagem");
      let _el_16 = dom_helpers.appendElement(doc, _el_3, "th");
      dom_helpers.setAttribute(_el_16, "scope", "col");
      this.addShimE(_el_16);
      let _el_17 = dom_helpers.appendElement(doc, _el_1, "tbody");
      this.addShimE(_el_17);
      let _anchor_18 = dom_helpers.appendAnchor(_el_17);
      this[_appEl_18] = new view_container.ViewContainer.new(18, 17, this, _anchor_18);
      let _TemplateRef_18_8 = new template_ref.TemplateRef.new(this[_appEl_18], C5 || CT.C5);
      this[_NgFor_18_9] = new ng_for.NgFor.new(this[_appEl_18], _TemplateRef_18_8);
      this.init0();
    }
    detectChangesInternal() {
      let _ctx = this.ctx;
      let currVal_0 = _ctx.listaCarros;
      if (dart.test(app_view_utils.checkBinding(this[_expr_0], currVal_0))) {
        this[_NgFor_18_9].ngForOf = currVal_0;
        this[_expr_0] = currVal_0;
      }
      if (!dart.test(app_view_utils.AppViewUtils.throwOnChanges)) {
        this[_NgFor_18_9].ngDoCheck();
      }
      this[_appEl_18].detectChangesInNestedViews();
    }
    destroyInternal() {
      this[_appEl_18].destroyNestedViews();
    }
    initComponentStyles() {
      let styles = tabela_component$46template.ViewTabelaComponent0._componentStyles;
      if (styles == null) {
        tabela_component$46template.ViewTabelaComponent0._componentStyles = styles = tabela_component$46template.ViewTabelaComponent0._componentStyles = style_encapsulation.ComponentStyles.scoped(tabela_component$46template.styles$TabelaComponent, tabela_component$46template.ViewTabelaComponent0._debugComponentUrl);
      }
      this.componentStyles = styles;
    }
  };
  (tabela_component$46template.ViewTabelaComponent0.new = function(parentView, parentIndex) {
    this[_appEl_18] = null;
    this[_NgFor_18_9] = null;
    this[_expr_0] = null;
    tabela_component$46template.ViewTabelaComponent0.__proto__.new.call(this, view_type.ViewType.component, parentView, parentIndex, 3);
    this.initComponentStyles();
    this.rootEl = html.HtmlElement.as(html.document[$createElement]("tabela"));
  }).prototype = tabela_component$46template.ViewTabelaComponent0.prototype;
  dart.addTypeTests(tabela_component$46template.ViewTabelaComponent0);
  dart.addTypeCaches(tabela_component$46template.ViewTabelaComponent0);
  dart.setLibraryUri(tabela_component$46template.ViewTabelaComponent0, L10);
  dart.setFieldSignature(tabela_component$46template.ViewTabelaComponent0, () => ({
    __proto__: dart.getFields(tabela_component$46template.ViewTabelaComponent0.__proto__),
    [_appEl_18]: dart.fieldType(view_container.ViewContainer),
    [_NgFor_18_9]: dart.fieldType(ng_for.NgFor),
    [_expr_0]: dart.fieldType(dart.dynamic)
  }));
  dart.defineLazy(tabela_component$46template.ViewTabelaComponent0, {
    /*tabela_component$46template.ViewTabelaComponent0._componentStyles*/get _componentStyles() {
      return null;
    },
    set _componentStyles(_) {}
  }, true);
  var _textBinding_2 = dart.privateName(tabela_component$46template, "_textBinding_2");
  var _textBinding_4 = dart.privateName(tabela_component$46template, "_textBinding_4");
  var _textBinding_6 = dart.privateName(tabela_component$46template, "_textBinding_6");
  var _textBinding_8 = dart.privateName(tabela_component$46template, "_textBinding_8");
  var _textBinding_10 = dart.privateName(tabela_component$46template, "_textBinding_10");
  var _el_12 = dart.privateName(tabela_component$46template, "_el_12");
  var _handle_click_1_0 = dart.privateName(tabela_component$46template, "_handle_click_1_0");
  var _handle_click_3_0 = dart.privateName(tabela_component$46template, "_handle_click_3_0");
  var _handle_click_5_0 = dart.privateName(tabela_component$46template, "_handle_click_5_0");
  var _handle_click_7_0 = dart.privateName(tabela_component$46template, "_handle_click_7_0");
  var _handle_click_9_0 = dart.privateName(tabela_component$46template, "_handle_click_9_0");
  var _handle_click_11_0 = dart.privateName(tabela_component$46template, "_handle_click_11_0");
  var _handle_click_14_0 = dart.privateName(tabela_component$46template, "_handle_click_14_0");
  tabela_component$46template._ViewTabelaComponent1 = class _ViewTabelaComponent1 extends app_view.AppView$(tabela_component.TabelaComponent) {
    build() {
      let doc = html.document;
      let _el_0 = doc[$createElement]("tr");
      this.addShimE(_el_0);
      let _el_1 = dom_helpers.appendElement(doc, _el_0, "th");
      dom_helpers.setAttribute(_el_1, "scope", "row");
      this.addShimE(_el_1);
      _el_1[$append](this[_textBinding_2].element);
      let _el_3 = dom_helpers.appendElement(doc, _el_0, "td");
      this.addShimE(_el_3);
      _el_3[$append](this[_textBinding_4].element);
      let _el_5 = dom_helpers.appendElement(doc, _el_0, "td");
      this.addShimE(_el_5);
      _el_5[$append](this[_textBinding_6].element);
      let _el_7 = dom_helpers.appendElement(doc, _el_0, "td");
      this.addShimE(_el_7);
      _el_7[$append](this[_textBinding_8].element);
      let _el_9 = dom_helpers.appendElement(doc, _el_0, "td");
      this.addShimE(_el_9);
      _el_9[$append](this[_textBinding_10].element);
      let _el_11 = dom_helpers.appendElement(doc, _el_0, "td");
      this.addShimE(_el_11);
      this[_el_12] = dom_helpers.appendElement(doc, _el_11, "img");
      dom_helpers.setAttribute(this[_el_12], "alt", "Responsive image");
      this.updateChildClass(html.HtmlElement.as(this[_el_12]), "img-fluid img-carro");
      this.addShimE(this[_el_12]);
      let _el_13 = dom_helpers.appendElement(doc, _el_0, "td");
      this.addShimE(_el_13);
      let _el_14 = dom_helpers.appendElement(doc, _el_13, "i");
      dom_helpers.setAttribute(_el_14, "aria-hidden", "true");
      this.updateChildClass(html.HtmlElement.as(_el_14), "fa fa-trash");
      this.addShimE(_el_14);
      _el_1[$addEventListener]("click", this.eventHandler1(html.Event, html.Event, dart.bind(this, _handle_click_1_0)));
      _el_3[$addEventListener]("click", this.eventHandler1(html.Event, html.Event, dart.bind(this, _handle_click_3_0)));
      _el_5[$addEventListener]("click", this.eventHandler1(html.Event, html.Event, dart.bind(this, _handle_click_5_0)));
      _el_7[$addEventListener]("click", this.eventHandler1(html.Event, html.Event, dart.bind(this, _handle_click_7_0)));
      _el_9[$addEventListener]("click", this.eventHandler1(html.Event, html.Event, dart.bind(this, _handle_click_9_0)));
      _el_11[$addEventListener]("click", this.eventHandler1(html.Event, html.Event, dart.bind(this, _handle_click_11_0)));
      _el_14[$addEventListener]("click", this.eventHandler1(html.Event, html.Event, dart.bind(this, _handle_click_14_0)));
      this.init1(_el_0);
    }
    detectChangesInternal() {
      let local_carro = optimizations.unsafeCast(carro$.Carro, this.locals[$_get]("$implicit"));
      this[_textBinding_2].updateText(core.String.as(interpolate.interpolate0(local_carro.codigoCarro)));
      this[_textBinding_4].updateText(core.String.as(interpolate.interpolate0(local_carro.nomeCarro)));
      this[_textBinding_6].updateText(core.String.as(interpolate.interpolate0(local_carro.nomeFabricante)));
      this[_textBinding_8].updateText(core.String.as(interpolate.interpolate0(local_carro.anoFabricacao)));
      this[_textBinding_10].updateText(core.String.as(interpolate.interpolate0(local_carro.preco)));
      let currVal_0 = interpolate.interpolate0(local_carro.imagem);
      if (dart.test(app_view_utils.checkBinding(this[_expr_0], currVal_0))) {
        dom_helpers.setProperty(this[_el_12], "src", app_view_utils.appViewUtils.sanitizer.sanitizeUrl(currVal_0));
        this[_expr_0] = currVal_0;
      }
    }
    [_handle_click_1_0]($36event) {
      let local_carro = optimizations.unsafeCast(carro$.Carro, this.locals[$_get]("$implicit"));
      let _ctx = this.ctx;
      _ctx.SelecionarCarro(local_carro);
    }
    [_handle_click_3_0]($36event) {
      let local_carro = optimizations.unsafeCast(carro$.Carro, this.locals[$_get]("$implicit"));
      let _ctx = this.ctx;
      _ctx.SelecionarCarro(local_carro);
    }
    [_handle_click_5_0]($36event) {
      let local_carro = optimizations.unsafeCast(carro$.Carro, this.locals[$_get]("$implicit"));
      let _ctx = this.ctx;
      _ctx.SelecionarCarro(local_carro);
    }
    [_handle_click_7_0]($36event) {
      let local_carro = optimizations.unsafeCast(carro$.Carro, this.locals[$_get]("$implicit"));
      let _ctx = this.ctx;
      _ctx.SelecionarCarro(local_carro);
    }
    [_handle_click_9_0]($36event) {
      let local_carro = optimizations.unsafeCast(carro$.Carro, this.locals[$_get]("$implicit"));
      let _ctx = this.ctx;
      _ctx.SelecionarCarro(local_carro);
    }
    [_handle_click_11_0]($36event) {
      let local_carro = optimizations.unsafeCast(carro$.Carro, this.locals[$_get]("$implicit"));
      let _ctx = this.ctx;
      _ctx.SelecionarCarro(local_carro);
    }
    [_handle_click_14_0]($36event) {
      let local_carro = optimizations.unsafeCast(carro$.Carro, this.locals[$_get]("$implicit"));
      let _ctx = this.ctx;
      _ctx.DeletarCarro(local_carro);
    }
  };
  (tabela_component$46template._ViewTabelaComponent1.new = function(parentView, parentIndex) {
    this[_textBinding_2] = new text_binding.TextBinding.new();
    this[_textBinding_4] = new text_binding.TextBinding.new();
    this[_textBinding_6] = new text_binding.TextBinding.new();
    this[_textBinding_8] = new text_binding.TextBinding.new();
    this[_textBinding_10] = new text_binding.TextBinding.new();
    this[_expr_0] = null;
    this[_el_12] = null;
    tabela_component$46template._ViewTabelaComponent1.__proto__.new.call(this, view_type.ViewType.embedded, parentView, parentIndex, 3);
    this.initComponentStyles();
  }).prototype = tabela_component$46template._ViewTabelaComponent1.prototype;
  dart.addTypeTests(tabela_component$46template._ViewTabelaComponent1);
  dart.addTypeCaches(tabela_component$46template._ViewTabelaComponent1);
  dart.setMethodSignature(tabela_component$46template._ViewTabelaComponent1, () => ({
    __proto__: dart.getMethods(tabela_component$46template._ViewTabelaComponent1.__proto__),
    [_handle_click_1_0]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_click_3_0]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_click_5_0]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_click_7_0]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_click_9_0]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_click_11_0]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_click_14_0]: dart.fnType(dart.void, [dart.dynamic])
  }));
  dart.setLibraryUri(tabela_component$46template._ViewTabelaComponent1, L10);
  dart.setFieldSignature(tabela_component$46template._ViewTabelaComponent1, () => ({
    __proto__: dart.getFields(tabela_component$46template._ViewTabelaComponent1.__proto__),
    [_textBinding_2]: dart.finalFieldType(text_binding.TextBinding),
    [_textBinding_4]: dart.finalFieldType(text_binding.TextBinding),
    [_textBinding_6]: dart.finalFieldType(text_binding.TextBinding),
    [_textBinding_8]: dart.finalFieldType(text_binding.TextBinding),
    [_textBinding_10]: dart.finalFieldType(text_binding.TextBinding),
    [_expr_0]: dart.fieldType(dart.dynamic),
    [_el_12]: dart.fieldType(html.Element)
  }));
  var _compView_0$0 = dart.privateName(tabela_component$46template, "_compView_0");
  var _TabelaComponent_0_5 = dart.privateName(tabela_component$46template, "_TabelaComponent_0_5");
  tabela_component$46template._ViewTabelaComponentHost0 = class _ViewTabelaComponentHost0 extends app_view.AppView$(tabela_component.TabelaComponent) {
    build() {
      this[_compView_0$0] = new tabela_component$46template.ViewTabelaComponent0.new(this, 0);
      this.rootEl = this[_compView_0$0].rootEl;
      this[_TabelaComponent_0_5] = dart.test(optimizations.isDevMode) ? errors.debugInjectorWrap(tabela_component.TabelaComponent, dart.wrapType(tabela_component.TabelaComponent), dart.fn(() => new tabela_component.TabelaComponent.new(carro_service.CarroService.as(this.injectorGet(dart.wrapType(carro_service.CarroService), this.viewData.parentIndex))), VoidToTabelaComponentL())) : new tabela_component.TabelaComponent.new(carro_service.CarroService.as(this.injectorGet(dart.wrapType(carro_service.CarroService), this.viewData.parentIndex)));
      this[_compView_0$0].create(this[_TabelaComponent_0_5], this.projectedNodes);
      this.init1(this.rootEl);
      return new (ComponentRefOfTabelaComponentL()).new(0, this, this.rootEl, this[_TabelaComponent_0_5]);
    }
    detectChangesInternal() {
      let firstCheck = this.cdState === 0;
      if (!dart.test(app_view_utils.AppViewUtils.throwOnChanges) && firstCheck) {
        this[_TabelaComponent_0_5].ngOnInit();
      }
      this[_compView_0$0].detectChanges();
    }
    destroyInternal() {
      this[_compView_0$0].destroyInternalState();
    }
  };
  (tabela_component$46template._ViewTabelaComponentHost0.new = function(parentView, parentIndex) {
    this[_compView_0$0] = null;
    this[_TabelaComponent_0_5] = null;
    tabela_component$46template._ViewTabelaComponentHost0.__proto__.new.call(this, view_type.ViewType.host, parentView, parentIndex, 3);
    ;
  }).prototype = tabela_component$46template._ViewTabelaComponentHost0.prototype;
  dart.addTypeTests(tabela_component$46template._ViewTabelaComponentHost0);
  dart.addTypeCaches(tabela_component$46template._ViewTabelaComponentHost0);
  dart.setLibraryUri(tabela_component$46template._ViewTabelaComponentHost0, L10);
  dart.setFieldSignature(tabela_component$46template._ViewTabelaComponentHost0, () => ({
    __proto__: dart.getFields(tabela_component$46template._ViewTabelaComponentHost0.__proto__),
    [_compView_0$0]: dart.fieldType(tabela_component$46template.ViewTabelaComponent0),
    [_TabelaComponent_0_5]: dart.fieldType(tabela_component.TabelaComponent)
  }));
  tabela_component$46template.viewFactory_TabelaComponent1 = function viewFactory_TabelaComponent1(parentView, parentIndex) {
    return new tabela_component$46template._ViewTabelaComponent1.new(parentView, parentIndex);
  };
  tabela_component$46template.viewFactory_TabelaComponentHost0 = function viewFactory_TabelaComponentHost0(parentView, parentIndex) {
    return new tabela_component$46template._ViewTabelaComponentHost0.new(parentView, parentIndex);
  };
  tabela_component$46template.initReflector = function initReflector$0() {
    if (dart.test(tabela_component$46template._visited)) {
      return;
    }
    tabela_component$46template._visited = true;
    reflector.registerComponent(dart.wrapType(tabela_component.TabelaComponent), tabela_component$46template.TabelaComponentNgFactory);
    angular$46template.initReflector();
    carro$46template.initReflector();
    carro_service$46template.initReflector();
  };
  dart.copyProperties(tabela_component$46template, {
    get TabelaComponentNgFactory() {
      return tabela_component$46template._TabelaComponentNgFactory;
    }
  });
  var C7;
  var C6;
  dart.defineLazy(tabela_component$46template, {
    /*tabela_component$46template.styles$TabelaComponent*/get styles$TabelaComponent() {
      return [tabela_component$46css$46shim.styles];
    },
    /*tabela_component$46template._TabelaComponentNgFactory*/get _TabelaComponentNgFactory() {
      return C6 || CT.C6;
    },
    /*tabela_component$46template.styles$TabelaComponentHost*/get styles$TabelaComponentHost() {
      return C2 || CT.C2;
    },
    /*tabela_component$46template._visited*/get _visited() {
      return false;
    },
    set _visited(_) {}
  }, true);
  dart.defineLazy(tabela_component$46css$46shim, {
    /*tabela_component$46css$46shim.styles*/get styles() {
      return ["#tabela-carro._ngcontent-%ID%{margin:40px}.img-carro._ngcontent-%ID%{width:100px}"];
    }
  }, true);
  carro_service$46template.initReflector = function initReflector$1() {
    if (dart.test(carro_service$46template._visited)) {
      return;
    }
    carro_service$46template._visited = true;
    carro$46template.initReflector();
  };
  dart.defineLazy(carro_service$46template, {
    /*carro_service$46template._visited*/get _visited() {
      return false;
    },
    set _visited(_) {}
  }, true);
  carro$46template.initReflector = function initReflector$2() {
  };
  var _carroService$ = dart.privateName(cadastro_component, "_carroService");
  var listaCarros$0 = dart.privateName(cadastro_component, "CadastroComponent.listaCarros");
  var carroCadastro = dart.privateName(cadastro_component, "CadastroComponent.carroCadastro");
  cadastro_component.CadastroComponent = class CadastroComponent extends lifecycle_hooks.OnInit {
    get listaCarros() {
      return this[listaCarros$0];
    }
    set listaCarros(value) {
      this[listaCarros$0] = value;
    }
    get carroCadastro() {
      return this[carroCadastro];
    }
    set carroCadastro(value) {
      this[carroCadastro] = value;
    }
    ngOnInit() {
      this[_carroService$].streamCarro.listen(dart.fn(carro => {
        this.AlternaBotaoAlterar("ativar");
        this.carroCadastro = carro;
      }, CarroLToNullN()));
    }
    CadastrarCarro() {
      return async.async(dart.void, (function* CadastrarCarro() {
        let elementoAlerta = null;
        let retornoCadastro = (yield this[_carroService$].CadastrarCarro2(this.carroCadastro));
        if (retornoCadastro[$_get]("codigo") === "0") {
          elementoAlerta = html.document.getElementById("alert-sucesso");
        } else {
          elementoAlerta = html.document.getElementById("alert-erro");
        }
        this.MostraMensagemAlerta(html.Element.as(elementoAlerta), retornoCadastro[$_get]("mensagem"));
      }).bind(this));
    }
    AlterarCarro() {
      return async.async(dart.void, (function* AlterarCarro() {
        let elementoAlerta = null;
        let retornoAlteracao = (yield this[_carroService$].AlterarCarro2(this.carroCadastro));
        if (retornoAlteracao[$_get]("codigo") === "0") {
          elementoAlerta = html.document.getElementById("alert-sucesso");
        } else {
          elementoAlerta = html.document.getElementById("alert-erro");
        }
        this.MostraMensagemAlerta(html.Element.as(elementoAlerta), retornoAlteracao[$_get]("mensagem"));
      }).bind(this));
    }
    AlternaBotaoAlterar(acao) {
      let botaoCadastrar = html.ButtonElement.as(html.document.getElementById("btn-cadastrar"));
      let botaoAlterar = html.ButtonElement.as(html.document.getElementById("btn-alterar"));
      if (acao === "ativar") {
        botaoCadastrar.style[$display] = "none";
        botaoAlterar.style[$display] = "inline-block";
      } else {
        botaoCadastrar.style[$display] = "inline-block";
        botaoAlterar.style[$display] = "none";
      }
    }
    MostraMensagemAlerta(element, mensagem) {
      return async.async(dart.void, (function* MostraMensagemAlerta() {
        element.style[$display] = "block";
        element[$text] = mensagem;
        yield async.Future.delayed(new core.Duration.new({seconds: 3}));
        element.style[$display] = "none";
        this.LimparCampos();
      }).bind(this));
    }
    LimparCampos() {
      this.AlternaBotaoAlterar("desativar");
      this.carroCadastro = new carro$.Carro.new(null, null, null, null, null, null);
    }
  };
  (cadastro_component.CadastroComponent.new = function(_carroService) {
    this[listaCarros$0] = null;
    this[carroCadastro] = new carro$.Carro.new(0, null, null, null, null, null);
    this[_carroService$] = _carroService;
    ;
  }).prototype = cadastro_component.CadastroComponent.prototype;
  dart.addTypeTests(cadastro_component.CadastroComponent);
  dart.addTypeCaches(cadastro_component.CadastroComponent);
  dart.setMethodSignature(cadastro_component.CadastroComponent, () => ({
    __proto__: dart.getMethods(cadastro_component.CadastroComponent.__proto__),
    ngOnInit: dart.fnType(dart.void, []),
    CadastrarCarro: dart.fnType(dart.void, []),
    AlterarCarro: dart.fnType(dart.void, []),
    AlternaBotaoAlterar: dart.fnType(dart.void, [core.String]),
    MostraMensagemAlerta: dart.fnType(dart.void, [html.Element, core.String]),
    LimparCampos: dart.fnType(dart.void, [])
  }));
  dart.setLibraryUri(cadastro_component.CadastroComponent, L11);
  dart.setFieldSignature(cadastro_component.CadastroComponent, () => ({
    __proto__: dart.getFields(cadastro_component.CadastroComponent.__proto__),
    [_carroService$]: dart.finalFieldType(carro_service.CarroService),
    listaCarros: dart.fieldType(dart.dynamic),
    carroCadastro: dart.fieldType(carro$.Carro)
  }));
  var _NgForm_1_5 = dart.privateName(cadastro_component$46template, "_NgForm_1_5");
  var _RequiredValidator_7_5 = dart.privateName(cadastro_component$46template, "_RequiredValidator_7_5");
  var _NgValidators_7_6 = dart.privateName(cadastro_component$46template, "_NgValidators_7_6");
  var _DefaultValueAccessor_7_7 = dart.privateName(cadastro_component$46template, "_DefaultValueAccessor_7_7");
  var _NgValueAccessor_7_8 = dart.privateName(cadastro_component$46template, "_NgValueAccessor_7_8");
  var _NgModel_7_9 = dart.privateName(cadastro_component$46template, "_NgModel_7_9");
  var _RequiredValidator_12_5 = dart.privateName(cadastro_component$46template, "_RequiredValidator_12_5");
  var _NgValidators_12_6 = dart.privateName(cadastro_component$46template, "_NgValidators_12_6");
  var _DefaultValueAccessor_12_7 = dart.privateName(cadastro_component$46template, "_DefaultValueAccessor_12_7");
  var _NgValueAccessor_12_8 = dart.privateName(cadastro_component$46template, "_NgValueAccessor_12_8");
  var _NgModel_12_9 = dart.privateName(cadastro_component$46template, "_NgModel_12_9");
  var _RequiredValidator_18_5 = dart.privateName(cadastro_component$46template, "_RequiredValidator_18_5");
  var _NgValidators_18_6 = dart.privateName(cadastro_component$46template, "_NgValidators_18_6");
  var _DefaultValueAccessor_18_7 = dart.privateName(cadastro_component$46template, "_DefaultValueAccessor_18_7");
  var _NgValueAccessor_18_8 = dart.privateName(cadastro_component$46template, "_NgValueAccessor_18_8");
  var _NgModel_18_9 = dart.privateName(cadastro_component$46template, "_NgModel_18_9");
  var _RequiredValidator_23_5 = dart.privateName(cadastro_component$46template, "_RequiredValidator_23_5");
  var _NgValidators_23_6 = dart.privateName(cadastro_component$46template, "_NgValidators_23_6");
  var _DefaultValueAccessor_23_7 = dart.privateName(cadastro_component$46template, "_DefaultValueAccessor_23_7");
  var _NgValueAccessor_23_8 = dart.privateName(cadastro_component$46template, "_NgValueAccessor_23_8");
  var _NgModel_23_9 = dart.privateName(cadastro_component$46template, "_NgModel_23_9");
  var _RequiredValidator_28_5 = dart.privateName(cadastro_component$46template, "_RequiredValidator_28_5");
  var _NgValidators_28_6 = dart.privateName(cadastro_component$46template, "_NgValidators_28_6");
  var _DefaultValueAccessor_28_7 = dart.privateName(cadastro_component$46template, "_DefaultValueAccessor_28_7");
  var _NgValueAccessor_28_8 = dart.privateName(cadastro_component$46template, "_NgValueAccessor_28_8");
  var _NgModel_28_9 = dart.privateName(cadastro_component$46template, "_NgModel_28_9");
  var _expr_0$ = dart.privateName(cadastro_component$46template, "_expr_0");
  var _expr_3 = dart.privateName(cadastro_component$46template, "_expr_3");
  var _expr_6 = dart.privateName(cadastro_component$46template, "_expr_6");
  var _expr_9 = dart.privateName(cadastro_component$46template, "_expr_9");
  var _expr_12 = dart.privateName(cadastro_component$46template, "_expr_12");
  var _el_7 = dart.privateName(cadastro_component$46template, "_el_7");
  var _el_12$ = dart.privateName(cadastro_component$46template, "_el_12");
  var _el_18 = dart.privateName(cadastro_component$46template, "_el_18");
  var _el_23 = dart.privateName(cadastro_component$46template, "_el_23");
  var _el_28 = dart.privateName(cadastro_component$46template, "_el_28");
  var _handle_input_7_2 = dart.privateName(cadastro_component$46template, "_handle_input_7_2");
  var _handle_ngModelChange_7_0 = dart.privateName(cadastro_component$46template, "_handle_ngModelChange_7_0");
  var _handle_input_12_2 = dart.privateName(cadastro_component$46template, "_handle_input_12_2");
  var _handle_ngModelChange_12_0 = dart.privateName(cadastro_component$46template, "_handle_ngModelChange_12_0");
  var _handle_input_18_2 = dart.privateName(cadastro_component$46template, "_handle_input_18_2");
  var _handle_ngModelChange_18_0 = dart.privateName(cadastro_component$46template, "_handle_ngModelChange_18_0");
  var _handle_input_23_2 = dart.privateName(cadastro_component$46template, "_handle_input_23_2");
  var _handle_ngModelChange_23_0 = dart.privateName(cadastro_component$46template, "_handle_ngModelChange_23_0");
  var _handle_input_28_2 = dart.privateName(cadastro_component$46template, "_handle_input_28_2");
  var _handle_ngModelChange_28_0 = dart.privateName(cadastro_component$46template, "_handle_ngModelChange_28_0");
  var C8;
  var OpaqueToken__uniqueName = dart.privateName(opaque_token, "OpaqueToken._uniqueName");
  var C9;
  var C10;
  cadastro_component$46template.ViewCadastroComponent0 = class ViewCadastroComponent0 extends app_view.AppView$(cadastro_component.CadastroComponent) {
    static get _debugComponentUrl() {
      return dart.test(optimizations.isDevMode) ? "asset:carros/lib/src/components/cadastro_component/cadastro_component.dart" : null;
    }
    build() {
      let _ctx = this.ctx;
      let _rootEl = this.rootEl;
      let parentRenderNode = this.initViewRoot(_rootEl);
      let doc = html.document;
      let _el_0 = dom_helpers.appendDiv(doc, parentRenderNode);
      dom_helpers.setAttribute(_el_0, "id", "cadastro-carro");
      this.addShimC(_el_0);
      let _el_1 = dom_helpers.appendElement(doc, _el_0, "form");
      this.addShimC(html.HtmlElement.as(_el_1));
      this[_NgForm_1_5] = new ng_form.NgForm.new(null);
      let _el_2 = dom_helpers.appendDiv(doc, _el_1);
      this.updateChildClass(_el_2, "form-row");
      this.addShimC(_el_2);
      let _el_3 = dom_helpers.appendDiv(doc, _el_2);
      this.updateChildClass(_el_3, "form-group col-md-6");
      this.addShimC(_el_3);
      let _el_4 = dom_helpers.appendElement(doc, _el_3, "label");
      dom_helpers.setAttribute(_el_4, "for", "nomeCarro");
      this.addShimE(_el_4);
      let _text_5 = dom_helpers.appendText(_el_4, "Nome do Carro");
      let _text_6 = dom_helpers.appendText(_el_3, " ");
      this[_el_7] = html.InputElement.as(dom_helpers.appendElement(doc, _el_3, "input"));
      this.updateChildClass(this[_el_7], "form-control");
      dom_helpers.setAttribute(this[_el_7], "id", "nomeCarro");
      dom_helpers.setAttribute(this[_el_7], "required", "");
      dom_helpers.setAttribute(this[_el_7], "type", "text");
      this.addShimC(this[_el_7]);
      this[_RequiredValidator_7_5] = new validators.RequiredValidator.new();
      this[_NgValidators_7_6] = [this[_RequiredValidator_7_5]];
      this[_DefaultValueAccessor_7_7] = new default_value_accessor.DefaultValueAccessor.new(this[_el_7]);
      this[_NgValueAccessor_7_8] = JSArrayOfControlValueAccessorL().of([this[_DefaultValueAccessor_7_7]]);
      this[_NgModel_7_9] = new ng_model.NgModel.new(this[_NgValidators_7_6], this[_NgValueAccessor_7_8]);
      let _el_8 = dom_helpers.appendDiv(doc, _el_2);
      this.updateChildClass(_el_8, "form-group col-md-6");
      this.addShimC(_el_8);
      let _el_9 = dom_helpers.appendElement(doc, _el_8, "label");
      dom_helpers.setAttribute(_el_9, "for", "nomeFabricante");
      this.addShimE(_el_9);
      let _text_10 = dom_helpers.appendText(_el_9, "Fabricante");
      let _text_11 = dom_helpers.appendText(_el_8, " ");
      this[_el_12$] = html.InputElement.as(dom_helpers.appendElement(doc, _el_8, "input"));
      this.updateChildClass(this[_el_12$], "form-control");
      dom_helpers.setAttribute(this[_el_12$], "id", "nomeFabricante");
      dom_helpers.setAttribute(this[_el_12$], "required", "");
      dom_helpers.setAttribute(this[_el_12$], "type", "text");
      this.addShimC(this[_el_12$]);
      this[_RequiredValidator_12_5] = new validators.RequiredValidator.new();
      this[_NgValidators_12_6] = [this[_RequiredValidator_12_5]];
      this[_DefaultValueAccessor_12_7] = new default_value_accessor.DefaultValueAccessor.new(this[_el_12$]);
      this[_NgValueAccessor_12_8] = JSArrayOfControlValueAccessorL().of([this[_DefaultValueAccessor_12_7]]);
      this[_NgModel_12_9] = new ng_model.NgModel.new(this[_NgValidators_12_6], this[_NgValueAccessor_12_8]);
      let _el_13 = dom_helpers.appendDiv(doc, _el_1);
      this.updateChildClass(_el_13, "form-row");
      this.addShimC(_el_13);
      let _el_14 = dom_helpers.appendDiv(doc, _el_13);
      this.updateChildClass(_el_14, "form-group col-md-3");
      this.addShimC(_el_14);
      let _el_15 = dom_helpers.appendElement(doc, _el_14, "label");
      dom_helpers.setAttribute(_el_15, "for", "anoFabricacao");
      this.addShimE(_el_15);
      let _text_16 = dom_helpers.appendText(_el_15, "Ano de Fabricação");
      let _text_17 = dom_helpers.appendText(_el_14, " ");
      this[_el_18] = html.InputElement.as(dom_helpers.appendElement(doc, _el_14, "input"));
      this.updateChildClass(this[_el_18], "form-control");
      dom_helpers.setAttribute(this[_el_18], "id", "anoFabricacao");
      dom_helpers.setAttribute(this[_el_18], "required", "");
      dom_helpers.setAttribute(this[_el_18], "type", "text");
      this.addShimC(this[_el_18]);
      this[_RequiredValidator_18_5] = new validators.RequiredValidator.new();
      this[_NgValidators_18_6] = [this[_RequiredValidator_18_5]];
      this[_DefaultValueAccessor_18_7] = new default_value_accessor.DefaultValueAccessor.new(this[_el_18]);
      this[_NgValueAccessor_18_8] = JSArrayOfControlValueAccessorL().of([this[_DefaultValueAccessor_18_7]]);
      this[_NgModel_18_9] = new ng_model.NgModel.new(this[_NgValidators_18_6], this[_NgValueAccessor_18_8]);
      let _el_19 = dom_helpers.appendDiv(doc, _el_13);
      this.updateChildClass(_el_19, "form-group col-md-3");
      this.addShimC(_el_19);
      let _el_20 = dom_helpers.appendElement(doc, _el_19, "label");
      dom_helpers.setAttribute(_el_20, "for", "preco");
      this.addShimE(_el_20);
      let _text_21 = dom_helpers.appendText(_el_20, "Preço");
      let _text_22 = dom_helpers.appendText(_el_19, " ");
      this[_el_23] = html.InputElement.as(dom_helpers.appendElement(doc, _el_19, "input"));
      this.updateChildClass(this[_el_23], "form-control");
      dom_helpers.setAttribute(this[_el_23], "id", "preco");
      dom_helpers.setAttribute(this[_el_23], "required", "");
      dom_helpers.setAttribute(this[_el_23], "type", "text");
      this.addShimC(this[_el_23]);
      this[_RequiredValidator_23_5] = new validators.RequiredValidator.new();
      this[_NgValidators_23_6] = [this[_RequiredValidator_23_5]];
      this[_DefaultValueAccessor_23_7] = new default_value_accessor.DefaultValueAccessor.new(this[_el_23]);
      this[_NgValueAccessor_23_8] = JSArrayOfControlValueAccessorL().of([this[_DefaultValueAccessor_23_7]]);
      this[_NgModel_23_9] = new ng_model.NgModel.new(this[_NgValidators_23_6], this[_NgValueAccessor_23_8]);
      let _el_24 = dom_helpers.appendDiv(doc, _el_13);
      this.updateChildClass(_el_24, "form-group col-md-6");
      this.addShimC(_el_24);
      let _el_25 = dom_helpers.appendElement(doc, _el_24, "label");
      dom_helpers.setAttribute(_el_25, "for", "imagem");
      this.addShimE(_el_25);
      let _text_26 = dom_helpers.appendText(_el_25, "Imagem");
      let _text_27 = dom_helpers.appendText(_el_24, " ");
      this[_el_28] = html.InputElement.as(dom_helpers.appendElement(doc, _el_24, "input"));
      this.updateChildClass(this[_el_28], "form-control");
      dom_helpers.setAttribute(this[_el_28], "id", "imagem");
      dom_helpers.setAttribute(this[_el_28], "required", "");
      dom_helpers.setAttribute(this[_el_28], "type", "text");
      this.addShimC(this[_el_28]);
      this[_RequiredValidator_28_5] = new validators.RequiredValidator.new();
      this[_NgValidators_28_6] = [this[_RequiredValidator_28_5]];
      this[_DefaultValueAccessor_28_7] = new default_value_accessor.DefaultValueAccessor.new(this[_el_28]);
      this[_NgValueAccessor_28_8] = JSArrayOfControlValueAccessorL().of([this[_DefaultValueAccessor_28_7]]);
      this[_NgModel_28_9] = new ng_model.NgModel.new(this[_NgValidators_28_6], this[_NgValueAccessor_28_8]);
      let _el_29 = dom_helpers.appendElement(doc, _el_1, "button");
      this.updateChildClass(html.HtmlElement.as(_el_29), "btn btn-dark");
      dom_helpers.setAttribute(_el_29, "id", "btn-cadastrar");
      dom_helpers.setAttribute(_el_29, "type", "submit");
      this.addShimC(html.HtmlElement.as(_el_29));
      let _text_30 = dom_helpers.appendText(_el_29, "Cadastrar");
      let _text_31 = dom_helpers.appendText(_el_1, " ");
      let _el_32 = dom_helpers.appendElement(doc, _el_1, "button");
      this.updateChildClass(html.HtmlElement.as(_el_32), "btn btn-success");
      dom_helpers.setAttribute(_el_32, "id", "btn-alterar");
      dom_helpers.setAttribute(_el_32, "type", "submit");
      this.addShimC(html.HtmlElement.as(_el_32));
      let _text_33 = dom_helpers.appendText(_el_32, "Alterar");
      let _text_34 = dom_helpers.appendText(_el_1, " ");
      let _el_35 = dom_helpers.appendElement(doc, _el_1, "button");
      this.updateChildClass(html.HtmlElement.as(_el_35), "btn btn-danger");
      dom_helpers.setAttribute(_el_35, "type", "reset");
      this.addShimC(html.HtmlElement.as(_el_35));
      let _text_36 = dom_helpers.appendText(_el_35, "Limpar");
      let _el_37 = dom_helpers.appendDiv(doc, _el_1);
      this.updateChildClass(_el_37, "alert alert-success mt-3 col-4");
      dom_helpers.setAttribute(_el_37, "id", "alert-sucesso");
      dom_helpers.setAttribute(_el_37, "role", "alert");
      this.addShimC(_el_37);
      let _el_38 = dom_helpers.appendDiv(doc, _el_1);
      this.updateChildClass(_el_38, "alert alert-danger mt-3 col-4");
      dom_helpers.setAttribute(_el_38, "id", "alert-erro");
      dom_helpers.setAttribute(_el_38, "role", "alert");
      this.addShimC(_el_38);
      app_view_utils.appViewUtils.eventManager.addEventListener(_el_1, "submit", this.eventHandler1(core.Object, html.Event, dart.bind(this[_NgForm_1_5], 'onSubmit')));
      _el_1[$addEventListener]("reset", this.eventHandler1(html.Event, html.Event, dart.bind(this[_NgForm_1_5], 'onReset')));
      this[_el_7][$addEventListener]("blur", this.eventHandler0(html.Event, dart.bind(this[_DefaultValueAccessor_7_7], 'touchHandler')));
      this[_el_7][$addEventListener]("input", this.eventHandler1(html.Event, html.Event, dart.bind(this, _handle_input_7_2)));
      let subscription_0 = this[_NgModel_7_9].update.listen(this.eventHandler1(dart.dynamic, dart.dynamic, dart.bind(this, _handle_ngModelChange_7_0)));
      this[_el_12$][$addEventListener]("blur", this.eventHandler0(html.Event, dart.bind(this[_DefaultValueAccessor_12_7], 'touchHandler')));
      this[_el_12$][$addEventListener]("input", this.eventHandler1(html.Event, html.Event, dart.bind(this, _handle_input_12_2)));
      let subscription_1 = this[_NgModel_12_9].update.listen(this.eventHandler1(dart.dynamic, dart.dynamic, dart.bind(this, _handle_ngModelChange_12_0)));
      this[_el_18][$addEventListener]("blur", this.eventHandler0(html.Event, dart.bind(this[_DefaultValueAccessor_18_7], 'touchHandler')));
      this[_el_18][$addEventListener]("input", this.eventHandler1(html.Event, html.Event, dart.bind(this, _handle_input_18_2)));
      let subscription_2 = this[_NgModel_18_9].update.listen(this.eventHandler1(dart.dynamic, dart.dynamic, dart.bind(this, _handle_ngModelChange_18_0)));
      this[_el_23][$addEventListener]("blur", this.eventHandler0(html.Event, dart.bind(this[_DefaultValueAccessor_23_7], 'touchHandler')));
      this[_el_23][$addEventListener]("input", this.eventHandler1(html.Event, html.Event, dart.bind(this, _handle_input_23_2)));
      let subscription_3 = this[_NgModel_23_9].update.listen(this.eventHandler1(dart.dynamic, dart.dynamic, dart.bind(this, _handle_ngModelChange_23_0)));
      this[_el_28][$addEventListener]("blur", this.eventHandler0(html.Event, dart.bind(this[_DefaultValueAccessor_28_7], 'touchHandler')));
      this[_el_28][$addEventListener]("input", this.eventHandler1(html.Event, html.Event, dart.bind(this, _handle_input_28_2)));
      let subscription_4 = this[_NgModel_28_9].update.listen(this.eventHandler1(dart.dynamic, dart.dynamic, dart.bind(this, _handle_ngModelChange_28_0)));
      _el_29[$addEventListener]("click", this.eventHandler0(html.Event, dart.bind(_ctx, 'CadastrarCarro')));
      _el_32[$addEventListener]("click", this.eventHandler0(html.Event, dart.bind(_ctx, 'AlterarCarro')));
      _el_35[$addEventListener]("click", this.eventHandler0(html.Event, dart.bind(_ctx, 'LimparCampos')));
      this.init(C8 || CT.C8, JSArrayOfStreamSubscriptionLOfvoid().of([subscription_0, subscription_1, subscription_2, subscription_3, subscription_4]));
    }
    injectorGetInternal(token, nodeIndex, notFoundResult) {
      if (1 <= dart.notNull(nodeIndex) && dart.notNull(nodeIndex) <= 38) {
        if (7 === nodeIndex) {
          if (token === (C9 || CT.C9)) {
            return this[_NgValidators_7_6];
          }
          if (token === (C10 || CT.C10)) {
            return this[_NgValueAccessor_7_8];
          }
          if (token === dart.wrapType(ng_model.NgModel) || token === dart.wrapType(ng_control.NgControl)) {
            return this[_NgModel_7_9];
          }
        }
        if (12 === nodeIndex) {
          if (token === (C9 || CT.C9)) {
            return this[_NgValidators_12_6];
          }
          if (token === (C10 || CT.C10)) {
            return this[_NgValueAccessor_12_8];
          }
          if (token === dart.wrapType(ng_model.NgModel) || token === dart.wrapType(ng_control.NgControl)) {
            return this[_NgModel_12_9];
          }
        }
        if (18 === nodeIndex) {
          if (token === (C9 || CT.C9)) {
            return this[_NgValidators_18_6];
          }
          if (token === (C10 || CT.C10)) {
            return this[_NgValueAccessor_18_8];
          }
          if (token === dart.wrapType(ng_model.NgModel) || token === dart.wrapType(ng_control.NgControl)) {
            return this[_NgModel_18_9];
          }
        }
        if (23 === nodeIndex) {
          if (token === (C9 || CT.C9)) {
            return this[_NgValidators_23_6];
          }
          if (token === (C10 || CT.C10)) {
            return this[_NgValueAccessor_23_8];
          }
          if (token === dart.wrapType(ng_model.NgModel) || token === dart.wrapType(ng_control.NgControl)) {
            return this[_NgModel_23_9];
          }
        }
        if (28 === nodeIndex) {
          if (token === (C9 || CT.C9)) {
            return this[_NgValidators_28_6];
          }
          if (token === (C10 || CT.C10)) {
            return this[_NgValueAccessor_28_8];
          }
          if (token === dart.wrapType(ng_model.NgModel) || token === dart.wrapType(ng_control.NgControl)) {
            return this[_NgModel_28_9];
          }
        }
        if (token === dart.wrapType(ng_form.NgForm) || token === dart.wrapType(ControlContainerOfAbstractControlGroupL())) {
          return this[_NgForm_1_5];
        }
      }
      return notFoundResult;
    }
    detectChangesInternal() {
      let _ctx = this.ctx;
      let changed = false;
      let firstCheck = this.cdState === 0;
      let local_nomeCarro = this[_NgModel_7_9];
      let local_nomeFabricante = this[_NgModel_12_9];
      let local_anoFabricacao = this[_NgModel_18_9];
      let local_preco = this[_NgModel_23_9];
      let local_imagem = this[_NgModel_28_9];
      if (firstCheck) {
        this[_RequiredValidator_7_5].required = true;
      }
      changed = false;
      this[_NgModel_7_9].model = _ctx.carroCadastro.nomeCarro;
      this[_NgModel_7_9].ngAfterChanges();
      if (!dart.test(app_view_utils.AppViewUtils.throwOnChanges) && firstCheck) {
        this[_NgModel_7_9].ngOnInit();
      }
      if (firstCheck) {
        this[_RequiredValidator_12_5].required = true;
      }
      changed = false;
      this[_NgModel_12_9].model = _ctx.carroCadastro.nomeFabricante;
      this[_NgModel_12_9].ngAfterChanges();
      if (!dart.test(app_view_utils.AppViewUtils.throwOnChanges) && firstCheck) {
        this[_NgModel_12_9].ngOnInit();
      }
      if (firstCheck) {
        this[_RequiredValidator_18_5].required = true;
      }
      changed = false;
      this[_NgModel_18_9].model = _ctx.carroCadastro.anoFabricacao;
      this[_NgModel_18_9].ngAfterChanges();
      if (!dart.test(app_view_utils.AppViewUtils.throwOnChanges) && firstCheck) {
        this[_NgModel_18_9].ngOnInit();
      }
      if (firstCheck) {
        this[_RequiredValidator_23_5].required = true;
      }
      changed = false;
      this[_NgModel_23_9].model = _ctx.carroCadastro.preco;
      this[_NgModel_23_9].ngAfterChanges();
      if (!dart.test(app_view_utils.AppViewUtils.throwOnChanges) && firstCheck) {
        this[_NgModel_23_9].ngOnInit();
      }
      if (firstCheck) {
        this[_RequiredValidator_28_5].required = true;
      }
      changed = false;
      this[_NgModel_28_9].model = _ctx.carroCadastro.imagem;
      this[_NgModel_28_9].ngAfterChanges();
      if (!dart.test(app_view_utils.AppViewUtils.throwOnChanges) && firstCheck) {
        this[_NgModel_28_9].ngOnInit();
      }
      let currVal_0 = local_nomeCarro.valid;
      if (dart.test(app_view_utils.checkBinding(this[_expr_0$], currVal_0))) {
        dom_helpers.updateClassBinding(this[_el_7], "is-valid", currVal_0);
        this[_expr_0$] = currVal_0;
      }
      let currVal_3 = local_nomeFabricante.valid;
      if (dart.test(app_view_utils.checkBinding(this[_expr_3], currVal_3))) {
        dom_helpers.updateClassBinding(this[_el_12$], "is-valid", currVal_3);
        this[_expr_3] = currVal_3;
      }
      let currVal_6 = local_anoFabricacao.valid;
      if (dart.test(app_view_utils.checkBinding(this[_expr_6], currVal_6))) {
        dom_helpers.updateClassBinding(this[_el_18], "is-valid", currVal_6);
        this[_expr_6] = currVal_6;
      }
      let currVal_9 = local_preco.valid;
      if (dart.test(app_view_utils.checkBinding(this[_expr_9], currVal_9))) {
        dom_helpers.updateClassBinding(this[_el_23], "is-valid", currVal_9);
        this[_expr_9] = currVal_9;
      }
      let currVal_12 = local_imagem.valid;
      if (dart.test(app_view_utils.checkBinding(this[_expr_12], currVal_12))) {
        dom_helpers.updateClassBinding(this[_el_28], "is-valid", currVal_12);
        this[_expr_12] = currVal_12;
      }
    }
    [_handle_ngModelChange_7_0]($36event) {
      let _ctx = this.ctx;
      _ctx.carroCadastro.nomeCarro = $36event;
    }
    [_handle_input_7_2]($36event) {
      this[_DefaultValueAccessor_7_7].handleChange(core.String.as(dart.dload(dart.dload($36event, 'target'), 'value')));
    }
    [_handle_ngModelChange_12_0]($36event) {
      let _ctx = this.ctx;
      _ctx.carroCadastro.nomeFabricante = $36event;
    }
    [_handle_input_12_2]($36event) {
      this[_DefaultValueAccessor_12_7].handleChange(core.String.as(dart.dload(dart.dload($36event, 'target'), 'value')));
    }
    [_handle_ngModelChange_18_0]($36event) {
      let _ctx = this.ctx;
      _ctx.carroCadastro.anoFabricacao = $36event;
    }
    [_handle_input_18_2]($36event) {
      this[_DefaultValueAccessor_18_7].handleChange(core.String.as(dart.dload(dart.dload($36event, 'target'), 'value')));
    }
    [_handle_ngModelChange_23_0]($36event) {
      let _ctx = this.ctx;
      _ctx.carroCadastro.preco = $36event;
    }
    [_handle_input_23_2]($36event) {
      this[_DefaultValueAccessor_23_7].handleChange(core.String.as(dart.dload(dart.dload($36event, 'target'), 'value')));
    }
    [_handle_ngModelChange_28_0]($36event) {
      let _ctx = this.ctx;
      _ctx.carroCadastro.imagem = $36event;
    }
    [_handle_input_28_2]($36event) {
      this[_DefaultValueAccessor_28_7].handleChange(core.String.as(dart.dload(dart.dload($36event, 'target'), 'value')));
    }
    initComponentStyles() {
      let styles = cadastro_component$46template.ViewCadastroComponent0._componentStyles;
      if (styles == null) {
        cadastro_component$46template.ViewCadastroComponent0._componentStyles = styles = cadastro_component$46template.ViewCadastroComponent0._componentStyles = style_encapsulation.ComponentStyles.scoped(cadastro_component$46template.styles$CadastroComponent, cadastro_component$46template.ViewCadastroComponent0._debugComponentUrl);
      }
      this.componentStyles = styles;
    }
  };
  (cadastro_component$46template.ViewCadastroComponent0.new = function(parentView, parentIndex) {
    this[_NgForm_1_5] = null;
    this[_RequiredValidator_7_5] = null;
    this[_NgValidators_7_6] = null;
    this[_DefaultValueAccessor_7_7] = null;
    this[_NgValueAccessor_7_8] = null;
    this[_NgModel_7_9] = null;
    this[_RequiredValidator_12_5] = null;
    this[_NgValidators_12_6] = null;
    this[_DefaultValueAccessor_12_7] = null;
    this[_NgValueAccessor_12_8] = null;
    this[_NgModel_12_9] = null;
    this[_RequiredValidator_18_5] = null;
    this[_NgValidators_18_6] = null;
    this[_DefaultValueAccessor_18_7] = null;
    this[_NgValueAccessor_18_8] = null;
    this[_NgModel_18_9] = null;
    this[_RequiredValidator_23_5] = null;
    this[_NgValidators_23_6] = null;
    this[_DefaultValueAccessor_23_7] = null;
    this[_NgValueAccessor_23_8] = null;
    this[_NgModel_23_9] = null;
    this[_RequiredValidator_28_5] = null;
    this[_NgValidators_28_6] = null;
    this[_DefaultValueAccessor_28_7] = null;
    this[_NgValueAccessor_28_8] = null;
    this[_NgModel_28_9] = null;
    this[_expr_0$] = null;
    this[_expr_3] = null;
    this[_expr_6] = null;
    this[_expr_9] = null;
    this[_expr_12] = null;
    this[_el_7] = null;
    this[_el_12$] = null;
    this[_el_18] = null;
    this[_el_23] = null;
    this[_el_28] = null;
    cadastro_component$46template.ViewCadastroComponent0.__proto__.new.call(this, view_type.ViewType.component, parentView, parentIndex, 3);
    this.initComponentStyles();
    this.rootEl = html.HtmlElement.as(html.document[$createElement]("cadastro"));
  }).prototype = cadastro_component$46template.ViewCadastroComponent0.prototype;
  dart.addTypeTests(cadastro_component$46template.ViewCadastroComponent0);
  dart.addTypeCaches(cadastro_component$46template.ViewCadastroComponent0);
  dart.setMethodSignature(cadastro_component$46template.ViewCadastroComponent0, () => ({
    __proto__: dart.getMethods(cadastro_component$46template.ViewCadastroComponent0.__proto__),
    injectorGetInternal: dart.fnType(dart.dynamic, [dart.dynamic, core.int, dart.dynamic]),
    [_handle_ngModelChange_7_0]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_input_7_2]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_ngModelChange_12_0]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_input_12_2]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_ngModelChange_18_0]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_input_18_2]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_ngModelChange_23_0]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_input_23_2]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_ngModelChange_28_0]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_input_28_2]: dart.fnType(dart.void, [dart.dynamic])
  }));
  dart.setLibraryUri(cadastro_component$46template.ViewCadastroComponent0, L12);
  dart.setFieldSignature(cadastro_component$46template.ViewCadastroComponent0, () => ({
    __proto__: dart.getFields(cadastro_component$46template.ViewCadastroComponent0.__proto__),
    [_NgForm_1_5]: dart.fieldType(ng_form.NgForm),
    [_RequiredValidator_7_5]: dart.fieldType(validators.RequiredValidator),
    [_NgValidators_7_6]: dart.fieldType(core.List),
    [_DefaultValueAccessor_7_7]: dart.fieldType(default_value_accessor.DefaultValueAccessor),
    [_NgValueAccessor_7_8]: dart.fieldType(core.List$(control_value_accessor.ControlValueAccessor)),
    [_NgModel_7_9]: dart.fieldType(ng_model.NgModel),
    [_RequiredValidator_12_5]: dart.fieldType(validators.RequiredValidator),
    [_NgValidators_12_6]: dart.fieldType(core.List),
    [_DefaultValueAccessor_12_7]: dart.fieldType(default_value_accessor.DefaultValueAccessor),
    [_NgValueAccessor_12_8]: dart.fieldType(core.List$(control_value_accessor.ControlValueAccessor)),
    [_NgModel_12_9]: dart.fieldType(ng_model.NgModel),
    [_RequiredValidator_18_5]: dart.fieldType(validators.RequiredValidator),
    [_NgValidators_18_6]: dart.fieldType(core.List),
    [_DefaultValueAccessor_18_7]: dart.fieldType(default_value_accessor.DefaultValueAccessor),
    [_NgValueAccessor_18_8]: dart.fieldType(core.List$(control_value_accessor.ControlValueAccessor)),
    [_NgModel_18_9]: dart.fieldType(ng_model.NgModel),
    [_RequiredValidator_23_5]: dart.fieldType(validators.RequiredValidator),
    [_NgValidators_23_6]: dart.fieldType(core.List),
    [_DefaultValueAccessor_23_7]: dart.fieldType(default_value_accessor.DefaultValueAccessor),
    [_NgValueAccessor_23_8]: dart.fieldType(core.List$(control_value_accessor.ControlValueAccessor)),
    [_NgModel_23_9]: dart.fieldType(ng_model.NgModel),
    [_RequiredValidator_28_5]: dart.fieldType(validators.RequiredValidator),
    [_NgValidators_28_6]: dart.fieldType(core.List),
    [_DefaultValueAccessor_28_7]: dart.fieldType(default_value_accessor.DefaultValueAccessor),
    [_NgValueAccessor_28_8]: dart.fieldType(core.List$(control_value_accessor.ControlValueAccessor)),
    [_NgModel_28_9]: dart.fieldType(ng_model.NgModel),
    [_expr_0$]: dart.fieldType(core.bool),
    [_expr_3]: dart.fieldType(core.bool),
    [_expr_6]: dart.fieldType(core.bool),
    [_expr_9]: dart.fieldType(core.bool),
    [_expr_12]: dart.fieldType(core.bool),
    [_el_7]: dart.fieldType(html.InputElement),
    [_el_12$]: dart.fieldType(html.InputElement),
    [_el_18]: dart.fieldType(html.InputElement),
    [_el_23]: dart.fieldType(html.InputElement),
    [_el_28]: dart.fieldType(html.InputElement)
  }));
  dart.defineLazy(cadastro_component$46template.ViewCadastroComponent0, {
    /*cadastro_component$46template.ViewCadastroComponent0._componentStyles*/get _componentStyles() {
      return null;
    },
    set _componentStyles(_) {}
  }, true);
  var _compView_0$1 = dart.privateName(cadastro_component$46template, "_compView_0");
  var _CadastroComponent_0_5 = dart.privateName(cadastro_component$46template, "_CadastroComponent_0_5");
  cadastro_component$46template._ViewCadastroComponentHost0 = class _ViewCadastroComponentHost0 extends app_view.AppView$(cadastro_component.CadastroComponent) {
    build() {
      this[_compView_0$1] = new cadastro_component$46template.ViewCadastroComponent0.new(this, 0);
      this.rootEl = this[_compView_0$1].rootEl;
      this[_CadastroComponent_0_5] = dart.test(optimizations.isDevMode) ? errors.debugInjectorWrap(cadastro_component.CadastroComponent, dart.wrapType(cadastro_component.CadastroComponent), dart.fn(() => new cadastro_component.CadastroComponent.new(carro_service.CarroService.as(this.injectorGet(dart.wrapType(carro_service.CarroService), this.viewData.parentIndex))), VoidToCadastroComponentL())) : new cadastro_component.CadastroComponent.new(carro_service.CarroService.as(this.injectorGet(dart.wrapType(carro_service.CarroService), this.viewData.parentIndex)));
      this[_compView_0$1].create(this[_CadastroComponent_0_5], this.projectedNodes);
      this.init1(this.rootEl);
      return new (ComponentRefOfCadastroComponentL()).new(0, this, this.rootEl, this[_CadastroComponent_0_5]);
    }
    detectChangesInternal() {
      let firstCheck = this.cdState === 0;
      if (!dart.test(app_view_utils.AppViewUtils.throwOnChanges) && firstCheck) {
        this[_CadastroComponent_0_5].ngOnInit();
      }
      this[_compView_0$1].detectChanges();
    }
    destroyInternal() {
      this[_compView_0$1].destroyInternalState();
    }
  };
  (cadastro_component$46template._ViewCadastroComponentHost0.new = function(parentView, parentIndex) {
    this[_compView_0$1] = null;
    this[_CadastroComponent_0_5] = null;
    cadastro_component$46template._ViewCadastroComponentHost0.__proto__.new.call(this, view_type.ViewType.host, parentView, parentIndex, 3);
    ;
  }).prototype = cadastro_component$46template._ViewCadastroComponentHost0.prototype;
  dart.addTypeTests(cadastro_component$46template._ViewCadastroComponentHost0);
  dart.addTypeCaches(cadastro_component$46template._ViewCadastroComponentHost0);
  dart.setLibraryUri(cadastro_component$46template._ViewCadastroComponentHost0, L12);
  dart.setFieldSignature(cadastro_component$46template._ViewCadastroComponentHost0, () => ({
    __proto__: dart.getFields(cadastro_component$46template._ViewCadastroComponentHost0.__proto__),
    [_compView_0$1]: dart.fieldType(cadastro_component$46template.ViewCadastroComponent0),
    [_CadastroComponent_0_5]: dart.fieldType(cadastro_component.CadastroComponent)
  }));
  cadastro_component$46template.viewFactory_CadastroComponentHost0 = function viewFactory_CadastroComponentHost0(parentView, parentIndex) {
    return new cadastro_component$46template._ViewCadastroComponentHost0.new(parentView, parentIndex);
  };
  cadastro_component$46template.initReflector = function initReflector$3() {
    if (dart.test(cadastro_component$46template._visited)) {
      return;
    }
    cadastro_component$46template._visited = true;
    reflector.registerComponent(dart.wrapType(cadastro_component.CadastroComponent), cadastro_component$46template.CadastroComponentNgFactory);
    angular$46template.initReflector();
    angular_forms$46template.initReflector();
    carro$46template.initReflector();
    carro_service$46template.initReflector();
  };
  dart.copyProperties(cadastro_component$46template, {
    get CadastroComponentNgFactory() {
      return cadastro_component$46template._CadastroComponentNgFactory;
    }
  });
  var C12;
  var C11;
  dart.defineLazy(cadastro_component$46template, {
    /*cadastro_component$46template.styles$CadastroComponent*/get styles$CadastroComponent() {
      return [cadastro_component$46css$46shim.styles];
    },
    /*cadastro_component$46template._CadastroComponentNgFactory*/get _CadastroComponentNgFactory() {
      return C11 || CT.C11;
    },
    /*cadastro_component$46template.styles$CadastroComponentHost*/get styles$CadastroComponentHost() {
      return C2 || CT.C2;
    },
    /*cadastro_component$46template._visited*/get _visited() {
      return false;
    },
    set _visited(_) {}
  }, true);
  dart.defineLazy(cadastro_component$46css$46shim, {
    /*cadastro_component$46css$46shim.styles*/get styles() {
      return ["#cadastro-carro._ngcontent-%ID%{margin:40px}#alert-sucesso._ngcontent-%ID%,#alert-erro._ngcontent-%ID%,.btn-success._ngcontent-%ID%{display:none}"];
    }
  }, true);
  var _carroService$0 = dart.privateName(navbar_component, "_carroService");
  var _router$0 = dart.privateName(navbar_component, "_router");
  navbar_component.NavbarComponent = class NavbarComponent extends core.Object {
    PesquisarCarro(termo) {
      core.print(termo);
      this[_carroService$0].streamControllerPesquisa.add(termo);
    }
    Login() {
      this[_router$0].navigate(route_paths.RoutePaths.login.toUrl());
    }
  };
  (navbar_component.NavbarComponent.new = function(_carroService, _router) {
    this[_carroService$0] = _carroService;
    this[_router$0] = _router;
    ;
  }).prototype = navbar_component.NavbarComponent.prototype;
  dart.addTypeTests(navbar_component.NavbarComponent);
  dart.addTypeCaches(navbar_component.NavbarComponent);
  dart.setMethodSignature(navbar_component.NavbarComponent, () => ({
    __proto__: dart.getMethods(navbar_component.NavbarComponent.__proto__),
    PesquisarCarro: dart.fnType(dart.void, [core.String]),
    Login: dart.fnType(dart.void, [])
  }));
  dart.setLibraryUri(navbar_component.NavbarComponent, L13);
  dart.setFieldSignature(navbar_component.NavbarComponent, () => ({
    __proto__: dart.getFields(navbar_component.NavbarComponent.__proto__),
    [_carroService$0]: dart.finalFieldType(carro_service.CarroService),
    [_router$0]: dart.fieldType(router.Router)
  }));
  route_paths.RoutePaths = class RoutePaths extends core.Object {};
  (route_paths.RoutePaths.new = function() {
    ;
  }).prototype = route_paths.RoutePaths.prototype;
  dart.addTypeTests(route_paths.RoutePaths);
  dart.addTypeCaches(route_paths.RoutePaths);
  dart.setLibraryUri(route_paths.RoutePaths, L14);
  dart.defineLazy(route_paths.RoutePaths, {
    /*route_paths.RoutePaths.login*/get login() {
      return new route_path.RoutePath.new({path: "login"});
    },
    /*route_paths.RoutePaths.painel*/get painel() {
      return new route_path.RoutePath.new({path: "painel"});
    },
    /*route_paths.RoutePaths.notfound*/get notfound() {
      return new route_path.RoutePath.new({path: "notfound"});
    }
  }, true);
  var _el_4 = dart.privateName(navbar_component$46template, "_el_4");
  var _handle_keyup_4_0 = dart.privateName(navbar_component$46template, "_handle_keyup_4_0");
  var _handle_change_4_1 = dart.privateName(navbar_component$46template, "_handle_change_4_1");
  navbar_component$46template.ViewNavbarComponent0 = class ViewNavbarComponent0 extends app_view.AppView$(navbar_component.NavbarComponent) {
    static get _debugComponentUrl() {
      return dart.test(optimizations.isDevMode) ? "asset:carros/lib/src/components/navbar_component/navbar_component.dart" : null;
    }
    build() {
      let _ctx = this.ctx;
      let _rootEl = this.rootEl;
      let parentRenderNode = this.initViewRoot(_rootEl);
      let doc = html.document;
      let _el_0 = dom_helpers.appendElement(doc, parentRenderNode, "nav");
      this.updateChildClass(html.HtmlElement.as(_el_0), "navbar navbar-dark bg-dark");
      this.addShimE(_el_0);
      let _el_1 = dom_helpers.appendElement(doc, _el_0, "a");
      this.updateChildClass(html.HtmlElement.as(_el_1), "navbar-brand");
      this.addShimC(html.HtmlElement.as(_el_1));
      let _text_2 = dom_helpers.appendText(_el_1, "WebCars");
      let _el_3 = dom_helpers.appendElement(doc, _el_0, "form");
      this.updateChildClass(html.HtmlElement.as(_el_3), "form-inline");
      this.addShimC(html.HtmlElement.as(_el_3));
      this[_el_4] = html.InputElement.as(dom_helpers.appendElement(doc, _el_3, "input"));
      dom_helpers.setAttribute(this[_el_4], "aria-label", "Search");
      this.updateChildClass(this[_el_4], "form-control mr-sm-2");
      dom_helpers.setAttribute(this[_el_4], "id", "termoPesquisa");
      dom_helpers.setAttribute(this[_el_4], "placeholder", "Search");
      dom_helpers.setAttribute(this[_el_4], "type", "search");
      this.addShimC(this[_el_4]);
      let _text_5 = dom_helpers.appendText(_el_3, " ");
      let _el_6 = dom_helpers.appendElement(doc, _el_3, "i");
      dom_helpers.setAttribute(_el_6, "aria-hidden", "true");
      this.updateChildClass(html.HtmlElement.as(_el_6), "fa fa-sign-out text-light");
      dom_helpers.setAttribute(_el_6, "id", "icone-exit");
      this.addShimE(_el_6);
      this[_el_4][$addEventListener]("keyup", this.eventHandler1(html.Event, html.Event, dart.bind(this, _handle_keyup_4_0)));
      this[_el_4][$addEventListener]("change", this.eventHandler1(html.Event, html.Event, dart.bind(this, _handle_change_4_1)));
      _el_6[$addEventListener]("click", this.eventHandler0(html.Event, dart.bind(_ctx, 'Login')));
      this.init0();
    }
    [_handle_keyup_4_0]($36event) {
      let local_termoPesquisa = this[_el_4];
      let _ctx = this.ctx;
      _ctx.PesquisarCarro(local_termoPesquisa.value);
    }
    [_handle_change_4_1]($36event) {
      let local_termoPesquisa = this[_el_4];
      let _ctx = this.ctx;
      _ctx.PesquisarCarro(local_termoPesquisa.value);
    }
    initComponentStyles() {
      let styles = navbar_component$46template.ViewNavbarComponent0._componentStyles;
      if (styles == null) {
        navbar_component$46template.ViewNavbarComponent0._componentStyles = styles = navbar_component$46template.ViewNavbarComponent0._componentStyles = style_encapsulation.ComponentStyles.scoped(navbar_component$46template.styles$NavbarComponent, navbar_component$46template.ViewNavbarComponent0._debugComponentUrl);
      }
      this.componentStyles = styles;
    }
  };
  (navbar_component$46template.ViewNavbarComponent0.new = function(parentView, parentIndex) {
    this[_el_4] = null;
    navbar_component$46template.ViewNavbarComponent0.__proto__.new.call(this, view_type.ViewType.component, parentView, parentIndex, 3);
    this.initComponentStyles();
    this.rootEl = html.HtmlElement.as(html.document[$createElement]("navbar"));
  }).prototype = navbar_component$46template.ViewNavbarComponent0.prototype;
  dart.addTypeTests(navbar_component$46template.ViewNavbarComponent0);
  dart.addTypeCaches(navbar_component$46template.ViewNavbarComponent0);
  dart.setMethodSignature(navbar_component$46template.ViewNavbarComponent0, () => ({
    __proto__: dart.getMethods(navbar_component$46template.ViewNavbarComponent0.__proto__),
    [_handle_keyup_4_0]: dart.fnType(dart.void, [dart.dynamic]),
    [_handle_change_4_1]: dart.fnType(dart.void, [dart.dynamic])
  }));
  dart.setLibraryUri(navbar_component$46template.ViewNavbarComponent0, L15);
  dart.setFieldSignature(navbar_component$46template.ViewNavbarComponent0, () => ({
    __proto__: dart.getFields(navbar_component$46template.ViewNavbarComponent0.__proto__),
    [_el_4]: dart.fieldType(html.InputElement)
  }));
  dart.defineLazy(navbar_component$46template.ViewNavbarComponent0, {
    /*navbar_component$46template.ViewNavbarComponent0._componentStyles*/get _componentStyles() {
      return null;
    },
    set _componentStyles(_) {}
  }, true);
  var _compView_0$2 = dart.privateName(navbar_component$46template, "_compView_0");
  var _NavbarComponent_0_5$ = dart.privateName(navbar_component$46template, "_NavbarComponent_0_5");
  navbar_component$46template._ViewNavbarComponentHost0 = class _ViewNavbarComponentHost0 extends app_view.AppView$(navbar_component.NavbarComponent) {
    build() {
      this[_compView_0$2] = new navbar_component$46template.ViewNavbarComponent0.new(this, 0);
      this.rootEl = this[_compView_0$2].rootEl;
      this[_NavbarComponent_0_5$] = dart.test(optimizations.isDevMode) ? errors.debugInjectorWrap(navbar_component.NavbarComponent, dart.wrapType(navbar_component.NavbarComponent), dart.fn(() => new navbar_component.NavbarComponent.new(carro_service.CarroService.as(this.injectorGet(dart.wrapType(carro_service.CarroService), this.viewData.parentIndex)), router.Router.as(this.injectorGet(dart.wrapType(router.Router), this.viewData.parentIndex))), VoidToNavbarComponentL())) : new navbar_component.NavbarComponent.new(carro_service.CarroService.as(this.injectorGet(dart.wrapType(carro_service.CarroService), this.viewData.parentIndex)), router.Router.as(this.injectorGet(dart.wrapType(router.Router), this.viewData.parentIndex)));
      this[_compView_0$2].create(this[_NavbarComponent_0_5$], this.projectedNodes);
      this.init1(this.rootEl);
      return new (ComponentRefOfNavbarComponentL()).new(0, this, this.rootEl, this[_NavbarComponent_0_5$]);
    }
    detectChangesInternal() {
      this[_compView_0$2].detectChanges();
    }
    destroyInternal() {
      this[_compView_0$2].destroyInternalState();
    }
  };
  (navbar_component$46template._ViewNavbarComponentHost0.new = function(parentView, parentIndex) {
    this[_compView_0$2] = null;
    this[_NavbarComponent_0_5$] = null;
    navbar_component$46template._ViewNavbarComponentHost0.__proto__.new.call(this, view_type.ViewType.host, parentView, parentIndex, 3);
    ;
  }).prototype = navbar_component$46template._ViewNavbarComponentHost0.prototype;
  dart.addTypeTests(navbar_component$46template._ViewNavbarComponentHost0);
  dart.addTypeCaches(navbar_component$46template._ViewNavbarComponentHost0);
  dart.setLibraryUri(navbar_component$46template._ViewNavbarComponentHost0, L15);
  dart.setFieldSignature(navbar_component$46template._ViewNavbarComponentHost0, () => ({
    __proto__: dart.getFields(navbar_component$46template._ViewNavbarComponentHost0.__proto__),
    [_compView_0$2]: dart.fieldType(navbar_component$46template.ViewNavbarComponent0),
    [_NavbarComponent_0_5$]: dart.fieldType(navbar_component.NavbarComponent)
  }));
  navbar_component$46template.viewFactory_NavbarComponentHost0 = function viewFactory_NavbarComponentHost0(parentView, parentIndex) {
    return new navbar_component$46template._ViewNavbarComponentHost0.new(parentView, parentIndex);
  };
  navbar_component$46template.initReflector = function initReflector$4() {
    if (dart.test(navbar_component$46template._visited)) {
      return;
    }
    navbar_component$46template._visited = true;
    reflector.registerComponent(dart.wrapType(navbar_component.NavbarComponent), navbar_component$46template.NavbarComponentNgFactory);
    angular$46template.initReflector();
    angular_router$46template.initReflector();
    route_paths$46template.initReflector();
    carro_service$46template.initReflector();
  };
  dart.copyProperties(navbar_component$46template, {
    get NavbarComponentNgFactory() {
      return navbar_component$46template._NavbarComponentNgFactory;
    }
  });
  var C14;
  var C13;
  dart.defineLazy(navbar_component$46template, {
    /*navbar_component$46template.styles$NavbarComponent*/get styles$NavbarComponent() {
      return [navbar_component$46css$46shim.styles];
    },
    /*navbar_component$46template._NavbarComponentNgFactory*/get _NavbarComponentNgFactory() {
      return C13 || CT.C13;
    },
    /*navbar_component$46template.styles$NavbarComponentHost*/get styles$NavbarComponentHost() {
      return C2 || CT.C2;
    },
    /*navbar_component$46template._visited*/get _visited() {
      return false;
    },
    set _visited(_) {}
  }, true);
  route_paths$46template.initReflector = function initReflector$5() {
    if (dart.test(route_paths$46template._visited)) {
      return;
    }
    route_paths$46template._visited = true;
    angular_router$46template.initReflector();
  };
  dart.defineLazy(route_paths$46template, {
    /*route_paths$46template._visited*/get _visited() {
      return false;
    },
    set _visited(_) {}
  }, true);
  dart.defineLazy(painel_component$46css$46shim, {
    /*painel_component$46css$46shim.styles*/get styles() {
      return [""];
    }
  }, true);
  var _NgForm_5_5 = dart.privateName(login_component$46template, "_NgForm_5_5");
  login_component$46template.ViewLoginComponent0 = class ViewLoginComponent0 extends app_view.AppView$(login_component.LoginComponent) {
    static get _debugComponentUrl() {
      return dart.test(optimizations.isDevMode) ? "asset:carros/lib/src/components/login_component/login_component.dart" : null;
    }
    build() {
      let _ctx = this.ctx;
      let _rootEl = this.rootEl;
      let parentRenderNode = this.initViewRoot(_rootEl);
      let doc = html.document;
      let _el_0 = dom_helpers.appendDiv(doc, parentRenderNode);
      this.updateChildClass(_el_0, "d-flex justify-content-center align-items-center");
      dom_helpers.setAttribute(_el_0, "id", "area-form");
      this.addShimC(_el_0);
      let _el_1 = dom_helpers.appendDiv(doc, _el_0);
      this.updateChildClass(_el_1, "row");
      dom_helpers.setAttribute(_el_1, "id", "linha-form");
      this.addShimC(_el_1);
      let _el_2 = dom_helpers.appendDiv(doc, _el_1);
      this.updateChildClass(_el_2, "p-0 col-6 d-flex justify-content-end");
      this.addShimC(_el_2);
      let _el_3 = dom_helpers.appendElement(doc, _el_2, "img");
      dom_helpers.setAttribute(_el_3, "alt", "Responsive image");
      this.updateChildClass(html.HtmlElement.as(_el_3), "img-fluid");
      dom_helpers.setAttribute(_el_3, "id", "img-form");
      dom_helpers.setAttribute(_el_3, "src", "https://i.pinimg.com/564x/2f/b5/68/2fb568d54a82da6d5a80d249f99e6ec3.jpg");
      this.addShimE(_el_3);
      let _el_4 = dom_helpers.appendDiv(doc, _el_1);
      this.updateChildClass(_el_4, "p-0 col-6 d-flex justify-content-start");
      this.addShimC(_el_4);
      let _el_5 = dom_helpers.appendElement(doc, _el_4, "form");
      this.updateChildClass(html.HtmlElement.as(_el_5), "p-4 bg-danger text-light");
      this.addShimC(html.HtmlElement.as(_el_5));
      this[_NgForm_5_5] = new ng_form.NgForm.new(null);
      let _el_6 = dom_helpers.appendDiv(doc, _el_5);
      dom_helpers.setAttribute(_el_6, "id", "brand-login");
      this.addShimC(_el_6);
      let _el_7 = dom_helpers.appendElement(doc, _el_6, "h2");
      this.updateChildClass(html.HtmlElement.as(_el_7), "text-center");
      this.addShimE(_el_7);
      let _text_8 = dom_helpers.appendText(_el_7, "WebCars");
      let _el_9 = dom_helpers.appendElement(doc, _el_5, "br");
      this.addShimE(_el_9);
      let _el_10 = dom_helpers.appendDiv(doc, _el_5);
      this.updateChildClass(_el_10, "form-group");
      this.addShimC(_el_10);
      let _el_11 = dom_helpers.appendElement(doc, _el_10, "label");
      dom_helpers.setAttribute(_el_11, "for", "exampleDropdownFormEmail2");
      this.addShimE(_el_11);
      let _text_12 = dom_helpers.appendText(_el_11, "Usuario");
      let _text_13 = dom_helpers.appendText(_el_10, " ");
      let _el_14 = dom_helpers.appendElement(doc, _el_10, "input");
      this.updateChildClass(html.HtmlElement.as(_el_14), "form-control");
      dom_helpers.setAttribute(_el_14, "id", "usuarioId");
      dom_helpers.setAttribute(_el_14, "type", "usuario");
      this.addShimC(html.HtmlElement.as(_el_14));
      let _el_15 = dom_helpers.appendDiv(doc, _el_5);
      this.updateChildClass(_el_15, "form-group");
      this.addShimC(_el_15);
      let _el_16 = dom_helpers.appendElement(doc, _el_15, "label");
      dom_helpers.setAttribute(_el_16, "for", "exampleDropdownFormPassword2");
      this.addShimE(_el_16);
      let _text_17 = dom_helpers.appendText(_el_16, "Senha");
      let _text_18 = dom_helpers.appendText(_el_15, " ");
      let _el_19 = dom_helpers.appendElement(doc, _el_15, "input");
      this.updateChildClass(html.HtmlElement.as(_el_19), "form-control");
      dom_helpers.setAttribute(_el_19, "id", "senhaId");
      dom_helpers.setAttribute(_el_19, "type", "password");
      this.addShimC(html.HtmlElement.as(_el_19));
      let _el_20 = dom_helpers.appendDiv(doc, _el_5);
      this.updateChildClass(_el_20, "form-group");
      this.addShimC(_el_20);
      let _el_21 = dom_helpers.appendDiv(doc, _el_20);
      this.updateChildClass(_el_21, "form-check");
      this.addShimC(_el_21);
      let _el_22 = dom_helpers.appendElement(doc, _el_21, "input");
      this.updateChildClass(html.HtmlElement.as(_el_22), "form-check-input");
      dom_helpers.setAttribute(_el_22, "id", "dropdownCheck2");
      dom_helpers.setAttribute(_el_22, "type", "checkbox");
      this.addShimC(html.HtmlElement.as(_el_22));
      let _text_23 = dom_helpers.appendText(_el_21, " ");
      let _el_24 = dom_helpers.appendElement(doc, _el_21, "label");
      this.updateChildClass(html.HtmlElement.as(_el_24), "form-check-label");
      dom_helpers.setAttribute(_el_24, "for", "dropdownCheck2");
      this.addShimE(_el_24);
      let _text_25 = dom_helpers.appendText(_el_24, "Mantenha-me Conectado");
      let _el_26 = dom_helpers.appendElement(doc, _el_5, "button");
      this.updateChildClass(html.HtmlElement.as(_el_26), "btn btn-dark");
      dom_helpers.setAttribute(_el_26, "type", "submit");
      this.addShimC(html.HtmlElement.as(_el_26));
      let _text_27 = dom_helpers.appendText(_el_26, "Acessar");
      app_view_utils.appViewUtils.eventManager.addEventListener(_el_5, "submit", this.eventHandler1(core.Object, html.Event, dart.bind(this[_NgForm_5_5], 'onSubmit')));
      _el_5[$addEventListener]("reset", this.eventHandler1(html.Event, html.Event, dart.bind(this[_NgForm_5_5], 'onReset')));
      _el_26[$addEventListener]("click", this.eventHandler0(html.Event, dart.bind(_ctx, 'Login')));
      this.init0();
    }
    injectorGetInternal(token, nodeIndex, notFoundResult) {
      if ((token === dart.wrapType(ng_form.NgForm) || token === dart.wrapType(ControlContainerOfAbstractControlGroupL())) && 5 <= dart.notNull(nodeIndex) && dart.notNull(nodeIndex) <= 27) {
        return this[_NgForm_5_5];
      }
      return notFoundResult;
    }
    initComponentStyles() {
      let styles = login_component$46template.ViewLoginComponent0._componentStyles;
      if (styles == null) {
        login_component$46template.ViewLoginComponent0._componentStyles = styles = login_component$46template.ViewLoginComponent0._componentStyles = style_encapsulation.ComponentStyles.scoped(login_component$46template.styles$LoginComponent, login_component$46template.ViewLoginComponent0._debugComponentUrl);
      }
      this.componentStyles = styles;
    }
  };
  (login_component$46template.ViewLoginComponent0.new = function(parentView, parentIndex) {
    this[_NgForm_5_5] = null;
    login_component$46template.ViewLoginComponent0.__proto__.new.call(this, view_type.ViewType.component, parentView, parentIndex, 3);
    this.initComponentStyles();
    this.rootEl = html.HtmlElement.as(html.document[$createElement]("login"));
  }).prototype = login_component$46template.ViewLoginComponent0.prototype;
  dart.addTypeTests(login_component$46template.ViewLoginComponent0);
  dart.addTypeCaches(login_component$46template.ViewLoginComponent0);
  dart.setMethodSignature(login_component$46template.ViewLoginComponent0, () => ({
    __proto__: dart.getMethods(login_component$46template.ViewLoginComponent0.__proto__),
    injectorGetInternal: dart.fnType(dart.dynamic, [dart.dynamic, core.int, dart.dynamic])
  }));
  dart.setLibraryUri(login_component$46template.ViewLoginComponent0, L16);
  dart.setFieldSignature(login_component$46template.ViewLoginComponent0, () => ({
    __proto__: dart.getFields(login_component$46template.ViewLoginComponent0.__proto__),
    [_NgForm_5_5]: dart.fieldType(ng_form.NgForm)
  }));
  dart.defineLazy(login_component$46template.ViewLoginComponent0, {
    /*login_component$46template.ViewLoginComponent0._componentStyles*/get _componentStyles() {
      return null;
    },
    set _componentStyles(_) {}
  }, true);
  var _compView_0$3 = dart.privateName(login_component$46template, "_compView_0");
  var _LoginComponent_0_5 = dart.privateName(login_component$46template, "_LoginComponent_0_5");
  login_component$46template._ViewLoginComponentHost0 = class _ViewLoginComponentHost0 extends app_view.AppView$(login_component.LoginComponent) {
    build() {
      this[_compView_0$3] = new login_component$46template.ViewLoginComponent0.new(this, 0);
      this.rootEl = this[_compView_0$3].rootEl;
      this[_LoginComponent_0_5] = dart.test(optimizations.isDevMode) ? errors.debugInjectorWrap(login_component.LoginComponent, dart.wrapType(login_component.LoginComponent), dart.fn(() => new login_component.LoginComponent.new(router.Router.as(this.injectorGet(dart.wrapType(router.Router), this.viewData.parentIndex))), VoidToLoginComponentL())) : new login_component.LoginComponent.new(router.Router.as(this.injectorGet(dart.wrapType(router.Router), this.viewData.parentIndex)));
      this[_compView_0$3].create(this[_LoginComponent_0_5], this.projectedNodes);
      this.init1(this.rootEl);
      return new (ComponentRefOfLoginComponentL()).new(0, this, this.rootEl, this[_LoginComponent_0_5]);
    }
    detectChangesInternal() {
      this[_compView_0$3].detectChanges();
    }
    destroyInternal() {
      this[_compView_0$3].destroyInternalState();
    }
  };
  (login_component$46template._ViewLoginComponentHost0.new = function(parentView, parentIndex) {
    this[_compView_0$3] = null;
    this[_LoginComponent_0_5] = null;
    login_component$46template._ViewLoginComponentHost0.__proto__.new.call(this, view_type.ViewType.host, parentView, parentIndex, 3);
    ;
  }).prototype = login_component$46template._ViewLoginComponentHost0.prototype;
  dart.addTypeTests(login_component$46template._ViewLoginComponentHost0);
  dart.addTypeCaches(login_component$46template._ViewLoginComponentHost0);
  dart.setLibraryUri(login_component$46template._ViewLoginComponentHost0, L16);
  dart.setFieldSignature(login_component$46template._ViewLoginComponentHost0, () => ({
    __proto__: dart.getFields(login_component$46template._ViewLoginComponentHost0.__proto__),
    [_compView_0$3]: dart.fieldType(login_component$46template.ViewLoginComponent0),
    [_LoginComponent_0_5]: dart.fieldType(login_component.LoginComponent)
  }));
  login_component$46template.viewFactory_LoginComponentHost0 = function viewFactory_LoginComponentHost0(parentView, parentIndex) {
    return new login_component$46template._ViewLoginComponentHost0.new(parentView, parentIndex);
  };
  login_component$46template.initReflector = function initReflector$6() {
    if (dart.test(login_component$46template._visited)) {
      return;
    }
    login_component$46template._visited = true;
    reflector.registerComponent(dart.wrapType(login_component.LoginComponent), login_component$46template.LoginComponentNgFactory);
    login$46template.initReflector();
    angular$46template.initReflector();
    angular_forms$46template.initReflector();
    angular_router$46template.initReflector();
    routes$46template.initReflector();
  };
  dart.copyProperties(login_component$46template, {
    get LoginComponentNgFactory() {
      return login_component$46template._LoginComponentNgFactory;
    }
  });
  var C16;
  var C15;
  dart.defineLazy(login_component$46template, {
    /*login_component$46template.styles$LoginComponent*/get styles$LoginComponent() {
      return [login_component$46css$46shim.styles];
    },
    /*login_component$46template._LoginComponentNgFactory*/get _LoginComponentNgFactory() {
      return C15 || CT.C15;
    },
    /*login_component$46template.styles$LoginComponentHost*/get styles$LoginComponentHost() {
      return C2 || CT.C2;
    },
    /*login_component$46template._visited*/get _visited() {
      return false;
    },
    set _visited(_) {}
  }, true);
  dart.defineLazy(login_component$46css$46shim, {
    /*login_component$46css$46shim.styles*/get styles() {
      return ["#area-form._ngcontent-%ID%{width:100vw;height:100vh;background-image:url(\"https://www.wallmesh.com/wp-content/uploads/2018/04/winter-jungle-snow-road-wallpaper-hd-3840x2400.jpg\");background-position:center;background-repeat:no-repeat;background-size:cover}#img-form._ngcontent-%ID%{width:250px}#linha-form._ngcontent-%ID%{}"];
    }
  }, true);
  routes$46template.initReflector = function initReflector$7() {
    if (dart.test(routes$46template._visited)) {
      return;
    }
    routes$46template._visited = true;
    login_component$46template.initReflector();
    not_found_component$46template.initReflector();
    painel_component$46template.initReflector();
    angular_router$46template.initReflector();
    route_paths$46template.initReflector();
    route_paths$46template.initReflector();
  };
  dart.defineLazy(routes$46template, {
    /*routes$46template._visited*/get _visited() {
      return false;
    },
    set _visited(_) {}
  }, true);
  login$46template.initReflector = function initReflector$8() {
  };
  dart.trackLibraries("packages/carros/src/components/cadastro_component/cadastro_component.css.shim", {
    "package:carros/src/components/navbar_component/navbar_component.css.shim.dart": navbar_component$46css$46shim,
    "package:carros/src/components/not_found_component/not_found_component.dart": not_found_component,
    "package:carros/src/components/login_component/login_component.dart": login_component,
    "package:carros/src/model/login.dart": login$,
    "package:carros/src/routes/routes.dart": routes,
    "package:carros/src/components/not_found_component/not_found_component.template.dart": not_found_component$46template,
    "package:carros/src/components/not_found_component/not_found_component.css.shim.dart": not_found_component$46css$46shim,
    "package:carros/src/components/painel_component/painel_component.template.dart": painel_component$46template,
    "package:carros/src/services/carro_service.dart": carro_service,
    "package:carros/src/model/carro.dart": carro$,
    "package:carros/src/components/tabela_component/tabela_component.dart": tabela_component,
    "package:carros/src/components/tabela_component/tabela_component.template.dart": tabela_component$46template,
    "package:carros/src/components/tabela_component/tabela_component.css.shim.dart": tabela_component$46css$46shim,
    "package:carros/src/services/carro_service.template.dart": carro_service$46template,
    "package:carros/src/model/carro.template.dart": carro$46template,
    "package:carros/src/components/cadastro_component/cadastro_component.dart": cadastro_component,
    "package:carros/src/components/cadastro_component/cadastro_component.template.dart": cadastro_component$46template,
    "package:carros/src/components/cadastro_component/cadastro_component.css.shim.dart": cadastro_component$46css$46shim,
    "package:carros/src/components/navbar_component/navbar_component.dart": navbar_component,
    "package:carros/src/routes/route_paths.dart": route_paths,
    "package:carros/src/components/navbar_component/navbar_component.template.dart": navbar_component$46template,
    "package:carros/src/routes/route_paths.template.dart": route_paths$46template,
    "package:carros/src/components/painel_component/painel_component.dart": painel_component,
    "package:carros/src/components/painel_component/painel_component.css.shim.dart": painel_component$46css$46shim,
    "package:carros/src/components/login_component/login_component.template.dart": login_component$46template,
    "package:carros/src/components/login_component/login_component.css.shim.dart": login_component$46css$46shim,
    "package:carros/src/routes/routes.template.dart": routes$46template,
    "package:carros/src/model/login.template.dart": login$46template
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["../navbar_component/navbar_component.css.shim.dart","../not_found_component/not_found_component.dart","../login_component/login_component.dart","../../model/login.dart","../../routes/routes.dart","../not_found_component/not_found_component.template.dart","../not_found_component/not_found_component.css.shim.dart","../painel_component/painel_component.dart","../painel_component/painel_component.template.dart","../../services/carro_service.dart","../../model/carro.dart","../tabela_component/tabela_component.dart","../tabela_component/tabela_component.template.dart","../tabela_component/tabela_component.css.shim.dart","../../services/carro_service.template.dart","../../model/carro.template.dart","cadastro_component.dart","cadastro_component.template.dart","cadastro_component.css.shim.dart","../navbar_component/navbar_component.dart","../../routes/route_paths.dart","../navbar_component/navbar_component.template.dart","../../routes/route_paths.template.dart","../painel_component/painel_component.css.shim.dart","../login_component/login_component.template.dart","../login_component/login_component.css.shim.dart","../../routes/routes.template.dart","../../model/login.template.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MAAoB,oCAAM;YAAG,EAAC;;;;;;ECON;;;;;;;ACeZ;AACmC,QAA3C,AAAQ,wBAAoB,AAAO;MACrC;;;;IAJoB;;EAAQ;;;;;;;;;;;;;;;;;;;;IChBxB;;;;;;IACA;;;;;;IACA;;;;;;IACA;;;;;;IACA;;;;;;IACA;;;;;;IACA;;;;;;;;UAaE;UACA;UACA;UACA;UACA;UACA;UACA;AAEJ,YAAO,4BACE,KAAH,EAAE,QAAF,OAAW,uBACF,MAAN,KAAK,SAAL,OAAc,0BACV,OAAL,IAAI,UAAJ,OAAa,2BACN,OAAN,KAAK,UAAL,OAAc,8BACJ,OAAR,OAAO,UAAP,OAAgB,8BACZ,OAAN,KAAK,UAAL,OAAc,4BACR,OAAN,KAAK,UAAL,OAAc;IAEzB;;;AAGE,YAAO,4CACL,kCAAM,OAAI,8BACV,0CAAS,OAAO,+BAChB,0CAAQ,OAAM,gCACd,4CAAS,OAAO,gCAChB,gDAAW,OAAS,gCACpB,4CAAS,OAAO,gCAChB,4CAAS,OAAO;IAEpB;mBAE2C;AACzC,UAAI,AAAI,GAAD,IAAI,MAAM,MAAO;AAExB,YAAO,2BACD,AAAG,GAAA,QAAC,cACD,AAAG,GAAA,QAAC,gBACL,AAAG,GAAA,QAAC,gBACH,AAAG,GAAA,QAAC,mBACF,AAAG,GAAA,QAAC,mBACN,AAAG,GAAA,QAAC,iBACJ,AAAG,GAAA,QAAC;IAEf;;AAEmB,YAAA,AAAK,qBAAO;IAAQ;oBAET;AAAW,YAAM,gDAAQ,AAAK,oBAAO,MAAM;IAAE;;AAIzE,YAAO,AAA4G,yBAAhG,WAAE,uBAAU,cAAK,sBAAS,aAAI,uBAAU,cAAK,yBAAY,gBAAO,uBAAU,cAAK,uBAAU,cAAK;IACnH;;;QA9DO;QACA;QACA;QACA;QACA;QACA;QACA;IANA;IACA;IACA;IACA;IACA;IACA;IACA;;EACL;;;;;;;;;;;;;;;;;;;;;;;;ECqBJ;;;;;MA5Be,mBAAK;YAAG,8DACG,yCACI;;MAGf,oBAAM;YAAG,8DACE,0CACK;;MAGhB,sBAAQ;YAAG,8DACA,4CACO;;MAGlB,iBAAG;YAAoB,iCAClC,qBACA,sBACA,wBACgB,uDACR,gBACiB,AAAM,wCAEf,uDACR,kBACiB,AAAS;;;;;ACVlC,uBAAgB,2BAAY,iFAAiF;IAC/G;;AAIQ,oBAAU;AACU,6BAAmB,kBAAa,OAAO;AAC3D,gBAAc;AACd,kBAAQ,0BAAsB,GAAG,EAAE,gBAAgB,EAAE;AAC5C,MAAf,cAAS,KAAK;AACR,oBAAU,uBAAmB,KAAK,EAAE;AACnC,MAAP;IACF;;AAIM,mBAAS;AACb,UAAI,AAAU,MAAM,IAAE;AAC6G,QAAhI,yEAAoB,SAAU,yEAA2C,2CAAO,yDAA0B;;AAErF,MAAxB,uBAAkB,MAAM;IAC1B;;wEA1BwC,YAAgB;AAAe,mFAAuB,8BAAW,UAAU,EAAE,WAAW;AACzG,IAArB;AACoD,kBAApD,oBAAiB,AAAS,8BAAc;EAC1C;;;;;MAJ+B,sEAAgB;;;;;;;;;AA2CA,MAA7C,oBAAc,8DAAuB,MAAM;AAChB,MAA3B,cAAS,AAAY;AAC+B,MAApD,+BAAiC;AACyB,MAA1D,AAAY,yBAAO,8BAAwB;AAC9B,MAAb,WAAM;AACN,YAAO,8CAAa,GAAG,MAAM,aAAQ;IACvC;;AAI6B,MAA3B,AAAY;IACd;;AAIoC,MAAlC,AAAY;IACd;;6EAnB6C,YAAgB;IAFtC;IACG;AACkD,wFAAuB,yBAAM,UAAU,EAAE,WAAW;;EAAsC;;;;;;;;;kHAsBjF,YAAgB;AACrG,UAAO,oEAA4B,UAAU,EAAE,WAAW;EAC5D;;AAIE,kBAAI;AACF;;AAEa,IAAf,0CAAW;AAE4D,IAAvE,4BAAyB,sDAAmB;AACvB,IAArB;EACF;;;AA3CE,YAAO;IACT;;;;;;;;MApCoB,uDAAwB;YAAG,EAAS;;MAiCN,0DAA2B;;;MAKzD,2DAA4B;;;MA+B5C,uCAAQ;YAAG;;;;;MCxFK,uCAAM;YAAG,EAAC;;;;;;;;;;;;ECa9B;;;;;;ACiCI,uBAAiB,2BAAY,2EAA2E;IAC1G;;AAIQ,oBAAU;AACW,6BAAmB,kBAAa,OAAO;AACf,MAAnD,qBAAsB,yDAAqB,MAAM;AAC3C,kBAAQ,AAAY;AACI,MAA9B,AAAiB,gBAAD,UAAQ,KAAK;AACd,MAAf,cAAS,KAAK;AAKoJ,MAJlK,uCAAiC,2BAC3B,2DAAmC,iDAAiB,cACnC,uEAAgB,AAAW,4BAAqB,2CAAc,AAAS,8CAAc,AAAW,4BAAqB,8BAAQ,AAAS,2DAE/I,uEAAgB,AAAW,4BAAqB,2CAAc,AAAS,8CAAc,AAAW,4BAAqB,8BAAQ,AAAS;AAC3G,MAAzC,AAAY,2BAAQ;AACiC,MAArD,oBAAsB,6DAAuB,MAAM;AAC7C,kBAAQ,AAAY;AACI,MAA9B,AAAiB,gBAAD,UAAQ,KAAK;AACd,MAAf,cAAS,KAAK;AAKuF,MAJrG,yCAAmC,2BAC7B,+DAAmC,qDAAmB,cACrC,2EAAkB,AAAW,4BAAqB,2CAAc,AAAS,6DAElF,2EAAkB,AAAW,4BAAqB,2CAAc,AAAS;AAC5C,MAA3C,AAAY,0BAAQ;AAC+B,MAAnD,oBAAsB,yDAAqB,MAAM;AAC3C,kBAAQ,AAAY;AACI,MAA9B,AAAiB,gBAAD,UAAQ,KAAK;AACd,MAAf,cAAS,KAAK;AAKqF,MAJnG,uCAAiC,2BAC3B,2DAAmC,iDAAiB,cACnC,uEAAgB,AAAW,4BAAqB,2CAAc,AAAS,2DAEhF,uEAAgB,AAAW,4BAAqB,2CAAc,AAAS;AAC5C,MAAzC,AAAY,0BAAQ;AACb,MAAP;IACF;;AAIO,uBAAmB,AAAQ,iBAAG;AACnC,qBAA6B,+CAAmB,UAAU;AACvB,QAAjC,AAAuB;;AAEzB,qBAA6B,+CAAmB,UAAU;AACzB,QAA/B,AAAqB;;AAEI,MAA3B,AAAY;AACe,MAA3B,AAAY;AACe,MAA3B,AAAY;IACd;;AAIoC,MAAlC,AAAY;AACsB,MAAlC,AAAY;AACsB,MAAlC,AAAY;IACd;;AAIM,mBAAS;AACb,UAAI,AAAU,MAAM,IAAE;AAC2G,QAA9H,oEAAoB,SAAU,oEAA2C,2CAAO,oDAAwB;;AAEnF,MAAxB,uBAAkB,MAAM;IAC1B;;mEAzEsC,YAAgB;IAPzB;IACL;IACO;IACL;IACG;IACL;AAE6C,8EAAwB,8BAAW,UAAU,EAAE,WAAW;AACxG,IAArB;AACkD,kBAAlD,oBAAkB,AAAS,8BAAc;EAC3C;;;;;;;;;;;;;;MAJ+B,iEAAgB;;;;;;;;AA0FF,MAA3C,qBAAc,yDAAqB,MAAM;AACd,MAA3B,cAAS,AAAY;AAC2B,MAAhD,6BAA+B;AACyB,MAAxD,AAAY,0BAAO,4BAAsB;AAC5B,MAAb,WAAM;AACN,YAAO,4CAAa,GAAG,MAAM,aAAQ;IACvC;;AAI6B,MAA3B,AAAY;IACd;;AAIoC,MAAlC,AAAY;IACd;;wEAnB2C,YAAgB;IAFtC;IACG;AACkD,mFAAwB,yBAAM,UAAU,EAAE,WAAW;;EAAsC;;;;;;;;;2GAsBpF,YAAgB;AACjG,UAAO,+DAA0B,UAAU,EAAE,WAAW;EAC1D;;AAIE,kBAAI;AACF;;AAEa,IAAf,uCAAW;AAEwD,IAAnE,4BAAyB,iDAAiB;AACrB,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;EACF;;;AA9CE,YAAO;IACT;;;;;MAzFoB,kDAAsB;YAAG,EAAS;;MAsFN,qDAAyB;;;MAKrD,sDAA0B;;;MA+B1C,oCAAQ;YAAG;;;;;;;;;;;;;;;IC/IW;;;;;;IAEC;;;;;;IAEK;;;;;;IAEhB;;;;;;IAEC;;;;;;IAEK;;;;;;IAyCR;;;;;;oBArBe;AAAP;AACZ,gCAAwD,eAAtB,eAAG,AAAM,KAAD;AAC1C,wBAAW,MAAM,AAAM,iBAAI,iBAAiB;AAC5C,uBAAqC,WAA1B,AAAK,oBAAO,AAAS,QAAD,iBAAO;AACtC,+BAAyB,+CAAQ,QAAQ;AACJ,QAA3C,AAAsB,+BAAI,gBAAgB;MAC5C;;mBAImC;AAAD;AAChC;AACQ,kCAAuD,eAArB,0BAAc,KAAK;AACrD,0BAAW,MAAM,AAAM,iBAAI,iBAAiB;AAC5C,uBAAS,mBAAa,QAAQ;AACpC,8CAAO,MAAM;;cACN;AACc,UAArB,WAAM,mBAAa,CAAC;;MAExB;;;AAUwF,MAAtF,AAAY,wBAAK,SAAC,QAAQ,WAAW,YAAmB,WAAnB,AAAO,MAAD,4BAAuB,AAAO,MAAD;AACxE,yBAAoC,WAA7B,AAAY,AAAK,2CAAc;IACxC;;AAGE,YAAO;IACT;;AAEmC;AACjC;AACQ,0BAAW,MAAM,AAAM;AACvB,uBAAS,mBAAa,QAAQ;AACpC,8CAAO,MAAM;;cACN;AACc,UAArB,WAAM,mBAAa,CAAC;;MAExB;;mBAE8B;AAC5B,YAAgC,AAC/B,AACA,cAFsB,WAAtB,AAAK,oBAAO,AAAK,IAAD,iBAAO,8BACnB,QAAC,SAAgB,+CAAQ,KAAK;IAC1B;mBAEoB;AACrB,MAAR,WAAM,CAAC;AACP,YAAO,oBAAU,AAAyB,mCAAF,CAAC;IAC3C;mBAEuC;AACE,MAAvC,AAAM,KAAD,eAAe;AACpB;AACwB,QAAtB,AAAY,uBAAI,KAAK;AACrB,cAAO,4CAAC,UAAS,KAAK,YAAW;;YAC5B;AACL,cAAO,4CAAC,UAAS,MAAM,YAAW,AAAoC,kEAAE,CAAC;;IAE7E;oBAEgD;AAAP;AACvC;AACU,0BAAW,MAAM,AAAM,0CACpB,2CAAgB,AAAK,oBAAO,0CAAC,SAAS,KAAK;AAC9C,sCAAwB,mBAAa,QAAQ;AACE,UAArD,AAA2B,sDAAI,qBAAqB;AACtD,gBAAO,4CAAC,UAAS,KAAK,YAAW;;cAC5B;AACL,gBAAO,4CAAC,UAAS,MAAM,YAAW,AAAoC,kEAAE,CAAC;;MAE7E;;iBAGwB;AACG,MAAzB,AAAY,0BAAO,KAAK;AACmB,MAA3C,AAA2B,oCAAI;IACjC;kBAEiC;AAAP;AAClB,kBAAkD,eAA9B,eAAG,AAAc,aAAD;AACpC,wBAAW,MAAM,AAAM,oBAAO,GAAG;AACjC,oCAAwB,mBAAa,QAAQ;AACE,QAArD,AAA2B,sDAAI,qBAAqB;MACtD;;iBAGsC;AACpC;AACM,yBAAa,AAAY,8BAAW,QAAC,SAA4B,YAAlB,AAAM,KAAD,cAAgB,AAAc,aAAD;AAChD,QAArC,gBAAW,UAAU,EAAE,aAAa;AACpC,cAAO,4CAAC,UAAS,KAAK,YAAW;;YAC5B;AACL,cAAO,4CAAC,UAAS,MAAM,YAAW;;IAEtC;kBAG+C;AAAP;AACtC;AACQ,oBAAmD,eAA/B,eAAG,AAAe,cAAD;AACrC,0BACF,MAAM,AAAM,iBAAI,GAAG,YAAW,2CAAgB,AAAK,oBAAO,cAAc;AACtE,sCAAwB,mBAAa,QAAQ;AACE,UAArD,AAA2B,sDAAI,qBAAqB;AACpD,gBAAO,4CAAC,UAAS,KAAK,YAAW;;cAC5B;AACL,gBAAO,4CAAC,UAAS,MAAM,YAAW;;MAEtC;;eAGsB,YAAkB;AACY,MAAlD,AAAW,UAAD,iBAAiB,AAAU,SAAD;AACA,MAApC,AAAW,UAAD,UAAU,AAAU,SAAD;AACuB,MAApD,AAAW,UAAD,kBAAkB,AAAU,SAAD;AACH,MAAlC,AAAW,UAAD,SAAS,AAAU,SAAD;AACc,MAA1C,AAAW,UAAD,aAAa,AAAU,SAAD;IAClC;;6CAxIkB;IAlBM,8BAAwB;IAEvB,iCAA2B;IAEtB,mCAA6B;IAE7C;IAEC;IAEK;IAyCR,oBAAc,sBACxB,qBAAM,GAAG,WAAW,cAAc,QAAQ,SAAS,8KACnD,qBAAM,GAAG,aAAa,QAAQ,QAAQ,SAAS,sIAC/C,qBAAM,GAAG,SAAS,cAAc,QAAQ,SAAS,8JACjD,qBAAM,GAAG,UAAU,QAAQ,QAAQ,SAAS;IArC5B;AAC0B,IAA1C,mBAAc,AAAsB;AAGvB,IAFb,sBACG,AACA,8CAFc,AAAyB,sCAC9B,qCAAuB;AAEiB,IAApD,wBAAmB,AAA2B;AAI5C,IAHF,AAAe,2BAAO,QAAC;AACjB,gCAAqB,MAAM,oBAAe,KAAK;AACD,MAAlD,AAA2B,oCAAI,kBAAkB;IAClD;EACH;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MAhBa,qCAAU;;;MAIV,mCAAQ;YAAG,4CAAC,gBAAgB;;;;;;;;;;ICvBrC;;;;;;IACA;;;;;;IACA;;;;;;IACA;;;;;;IACA;;;;;;IACA;;;;;;oBAI4B;AAAU,YAAM,gDAAQ,AAAK,oBAAO,KAAK;IAAE;mBAE9B;AAC3C,kCAAM,cAAO,AAAK,KAAA,QAAC,iBAAgB,AAAK,KAAA,QAAC,cAAa,AAAK,KAAA,QAAC,mBAAmB,cAAO,AAAK,KAAA,QAAC,mBAAmB,iBAAU,AAAK,KAAA,QAAC,WAAW,AAAK,KAAA,QAAC;IAAU;;AAG1J,wDAAC,eAAc,kBAAa,aAAY,gBAAW,kBAAiB,qBAAgB,iBAAgB,oBAAe,SAAQ,YAAO,UAAS;IAAO;eAE9H;AACwB,MAA5C,qBAAgB,AAAe,cAAD;AACA,MAA9B,cAAS,AAAe,cAAD;AACuB,MAA9C,sBAAiB,AAAe,cAAD;AACH,MAA5B,aAAQ,AAAe,cAAD;AACc,MAApC,iBAAY,AAAe,cAAD;IAC5B;;+BAhBW,aAAkB,WAAgB,gBAAqB,eAAoB,OAAY;IAAvF;IAAkB;IAAgB;IAAqB;IAAoB;IAAY;;EAAO;;;;;;;;;;;;;;;;;;kCAoBhG;AAAW,UAAO,aAAP,MAAM,IAAU,MAAM,GAAO,8BAAM,MAAM;EAAC;wCAE/C;AAAW,UAAO,QAAP,MAAM,eAAa,MAAM,GAAU,iCAAM,MAAM;EAAC;;;;;ICjB9D;;;;;;IAEA;;;;;;;AAKC;AAEwC,QAAnD,oBAAc,MAAM,AAAc;AAIhC,QAFF,AAAc,AAAY,wCAAO,QAAC;AACkD,UAAlF,WAAO,AAA8B,AAAkB,4DAAhB,AAAM,KAAD,eAAa;;AAIzD,QAFF,AAAc,AAAiB,6CAAO,QAAC;AACL,UAAhC,mBAAc,kBAAkB;;MAEpC;;iBAEwB;AACa,MAAlC,AAAc,mCAAc,KAAK;IACpC;oBAE2B;AACW,MAApC,AAAc,qCAAgB,KAAK;IACrC;;mDArBqB;IAJT;IAEA;IAES;;EAAc;;;;;;;;;;;;;;;;;;;;;;ACuBjC,uBAAgB,2BAAY,2EAA2E;IACzG;;AAIQ,oBAAU;AACU,6BAAmB,kBAAa,OAAO;AAC3D,gBAAc;AACd,kBAAQ,sBAAmB,GAAG,EAAE,gBAAgB;AACnB,MAAnC,AAAK,sBAAiB,KAAK,EAAE;AACqB,MAAlD,yBAAsB,KAAK,EAAE,MAAM;AACpB,MAAf,cAAS,KAAK;AACR,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACZ,MAArC,AAAK,0CAAiB,KAAK,GAAE;AACd,MAAf,kCAAS,KAAK;AACR,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACP,MAA1C,AAAK,0CAAiB,KAAK,GAAE;AACd,MAAf,cAAS,KAAK;AACR,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AAClC,MAAf,cAAS,KAAK;AACR,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACL,MAA5C,yBAAsB,KAAK,EAAE,SAAS;AACvB,MAAf,cAAS,KAAK;AACR,oBAAU,uBAAoB,KAAK,EAAE;AACrC,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACL,MAA5C,yBAAsB,KAAK,EAAE,SAAS;AACvB,MAAf,cAAS,KAAK;AACR,oBAAU,uBAAoB,KAAK,EAAE;AACrC,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACL,MAA5C,yBAAsB,KAAK,EAAE,SAAS;AACvB,MAAf,cAAS,KAAK;AACR,oBAAU,uBAAoB,KAAK,EAAE;AACrC,mBAAS,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACL,MAA7C,yBAAsB,MAAM,EAAE,SAAS;AACvB,MAAhB,cAAS,MAAM;AACT,qBAAW,uBAAoB,MAAM,EAAE;AACvC,mBAAS,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACL,MAA7C,yBAAsB,MAAM,EAAE,SAAS;AACvB,MAAhB,cAAS,MAAM;AACT,qBAAW,uBAAoB,MAAM,EAAE;AACvC,mBAAS,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACL,MAA7C,yBAAsB,MAAM,EAAE,SAAS;AACvB,MAAhB,cAAS,MAAM;AACT,qBAAW,uBAAoB,MAAM,EAAE;AACvC,mBAAS,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACL,MAA7C,yBAAsB,MAAM,EAAE,SAAS;AACvB,MAAhB,cAAS,MAAM;AACT,mBAAS,0BAAuB,GAAG,EAAE,KAAK,EAAE;AAClC,MAAhB,cAAS,MAAM;AACT,uBAAa,yBAAsB,MAAM;AACI,MAAnD,kBAAY,qCAAc,IAAI,IAAI,MAAM,UAAU;AACtC,8BAAoB,iCAAY;AACa,MAAzD,oBAAsB,qBAAM,iBAAW,iBAAiB;AACjD,MAAP;IACF;;AAIQ,iBAAO;AACP,sBAAY,AAAK,IAAD;AACtB,oBAAI,4BAAsB,eAAS,SAAS;AACX,QAA/B,AAAY,4BAAU,SAAS;AACZ,QAAnB,gBAAU,SAAS;;AAErB,qBAA4B;AACH,QAAvB,AAAY;;AAEwB,MAAtC,AAAU;IACZ;;AAIgC,MAA9B,AAAU;IACZ;;AAIM,mBAAS;AACb,UAAI,AAAU,MAAM,IAAE;AAC2G,QAA9H,oEAAoB,SAAU,oEAA2C,2CAAO,oDAAwB;;AAEnF,MAAxB,uBAAkB,MAAM;IAC1B;;mEAvFsC,YAAgB;IAJxC;IACA;IACV;AAEiE,8EAAuB,8BAAW,UAAU,EAAE,WAAW;AACvG,IAArB;AACiD,kBAAjD,oBAAiB,AAAS,8BAAc;EAC1C;;;;;;;;;;;MAJ+B,iEAAgB;;;;;;;;;;;;;;;;;;;;AA6GvC,gBAAc;AACd,kBAAQ,AAAI,GAAD,iBAAe;AACjB,MAAf,cAAS,KAAK;AACR,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACL,MAA5C,yBAAsB,KAAK,EAAE,SAAS;AACvB,MAAf,cAAS,KAAK;AACsB,MAApC,AAAM,KAAD,UAAQ,AAAe;AACtB,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AAClC,MAAf,cAAS,KAAK;AACsB,MAApC,AAAM,KAAD,UAAQ,AAAe;AACtB,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AAClC,MAAf,cAAS,KAAK;AACsB,MAApC,AAAM,KAAD,UAAQ,AAAe;AACtB,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AAClC,MAAf,cAAS,KAAK;AACsB,MAApC,AAAM,KAAD,UAAQ,AAAe;AACtB,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AAClC,MAAf,cAAS,KAAK;AACuB,MAArC,AAAM,KAAD,UAAQ,AAAgB;AACvB,mBAAS,0BAAuB,GAAG,EAAE,KAAK,EAAE;AAClC,MAAhB,cAAS,MAAM;AACoC,MAAnD,eAAS,0BAAuB,GAAG,EAAE,MAAM,EAAE;AACW,MAAxD,yBAAsB,cAAQ,OAAO;AACe,MAApD,AAAK,0CAAiB,eAAQ;AACd,MAAhB,cAAS;AACH,mBAAS,0BAAuB,GAAG,EAAE,KAAK,EAAE;AAClC,MAAhB,cAAS,MAAM;AACT,mBAAS,0BAAuB,GAAG,EAAE,MAAM,EAAE;AACC,MAApD,yBAAsB,MAAM,EAAE,eAAe;AACD,MAA5C,AAAK,0CAAiB,MAAM,GAAE;AACd,MAAhB,cAAS,MAAM;AACkD,MAAjE,AAAM,KAAD,oBAAkB,SAAS,qDAAc;AACmB,MAAjE,AAAM,KAAD,oBAAkB,SAAS,qDAAc;AACmB,MAAjE,AAAM,KAAD,oBAAkB,SAAS,qDAAc;AACmB,MAAjE,AAAM,KAAD,oBAAkB,SAAS,qDAAc;AACmB,MAAjE,AAAM,KAAD,oBAAkB,SAAS,qDAAc;AACqB,MAAnE,AAAO,MAAD,oBAAkB,SAAS,qDAAc;AACoB,MAAnE,AAAO,MAAD,oBAAkB,SAAS,qDAAc;AACnC,MAAZ,WAAM,KAAK;IACb;;AAIQ,wBAAc,uCAAmC,AAAM,mBAAC;AACW,MAAzE,AAAe,+CAAW,yBAAsB,AAAY,WAAD;AACY,MAAvE,AAAe,+CAAW,yBAAsB,AAAY,WAAD;AACiB,MAA5E,AAAe,+CAAW,yBAAsB,AAAY,WAAD;AACgB,MAA3E,AAAe,+CAAW,yBAAsB,AAAY,WAAD;AACS,MAApE,AAAgB,gDAAW,yBAAsB,AAAY,WAAD;AACtD,sBAAY,yBAAsB,AAAY,WAAD;AACnD,oBAAI,4BAAsB,eAAS,SAAS;AACiD,QAA3F,wBAAqB,cAAQ,OAAgB,AAAa,AAAU,kDAAY,SAAS;AACtE,QAAnB,gBAAU,SAAS;;IAEvB;wBAEuB;AACf,wBAAc,uCAAmC,AAAM,mBAAC;AACxD,iBAAO;AACoB,MAAjC,AAAK,IAAD,iBAAiB,WAAW;IAClC;wBAEuB;AACf,wBAAc,uCAAmC,AAAM,mBAAC;AACxD,iBAAO;AACoB,MAAjC,AAAK,IAAD,iBAAiB,WAAW;IAClC;wBAEuB;AACf,wBAAc,uCAAmC,AAAM,mBAAC;AACxD,iBAAO;AACoB,MAAjC,AAAK,IAAD,iBAAiB,WAAW;IAClC;wBAEuB;AACf,wBAAc,uCAAmC,AAAM,mBAAC;AACxD,iBAAO;AACoB,MAAjC,AAAK,IAAD,iBAAiB,WAAW;IAClC;wBAEuB;AACf,wBAAc,uCAAmC,AAAM,mBAAC;AACxD,iBAAO;AACoB,MAAjC,AAAK,IAAD,iBAAiB,WAAW;IAClC;yBAEwB;AAChB,wBAAc,uCAAmC,AAAM,mBAAC;AACxD,iBAAO;AACoB,MAAjC,AAAK,IAAD,iBAAiB,WAAW;IAClC;yBAEwB;AAChB,wBAAc,uCAAmC,AAAM,mBAAC;AACxD,iBAAO;AACiB,MAA9B,AAAK,IAAD,cAAc,WAAW;IAC/B;;oEArGuC,YAAgB;IAP5B,uBAA0B;IAC1B,uBAA0B;IAC1B,uBAA0B;IAC1B,uBAA0B;IAC1B,wBAA2B;IAClD;IACY;AACsD,+EAAuB,6BAAU,UAAU,EAAE,WAAW;AACvG,IAArB;EACF;;;;;;;;;;;;;;;;;;;;;;;;;;;;AAkH6C,MAA3C,sBAAc,yDAAqB,MAAM;AACd,MAA3B,cAAS,AAAY;AAKwE,MAJ7F,uCAAgC,2BAC1B,2DAAmC,iDAAiB,cACnC,uEAAgB,AAAK,iBAAqB,2CAAc,AAAS,2DAE1E,uEAAgB,AAAK,iBAAqB,2CAAc,AAAS;AACvB,MAAxD,AAAY,2BAAO,4BAAsB;AAC5B,MAAb,WAAM;AACN,YAAO,4CAAa,GAAG,MAAM,aAAQ;IACvC;;AAIO,uBAAmB,AAAQ,iBAAG;AACnC,qBAA6B,+CAAmB,UAAU;AACzB,QAA/B,AAAqB;;AAEI,MAA3B,AAAY;IACd;;AAIoC,MAAlC,AAAY;IACd;;wEA3B2C,YAAgB;IAFtC;IACG;AACkD,mFAAuB,yBAAM,UAAU,EAAE,WAAW;;EAAsC;;;;;;;;;mGAT1G,YAAgB;AAC1E,UAAO,2DAAsB,UAAU,EAAE,WAAW;EACtD;2GAqCmF,YAAgB;AACjG,UAAO,+DAA0B,UAAU,EAAE,WAAW;EAC1D;;AAIE,kBAAI;AACF;;AAEa,IAAf,uCAAW;AAEwD,IAAnE,4BAAyB,iDAAiB;AACrB,IAArB;AACqB,IAArB;AACqB,IAArB;EACF;;;AAzKE,YAAO;IACT;;;;;MApGoB,kDAAsB;YAAG,EAAS;;MAiGN,qDAAyB;;;MAyHrD,sDAA0B;;;MAuC1C,oCAAQ;YAAG;;;;;MC/RK,oCAAM;YAAG,EAAC;;;;ACU5B,kBAAI;AACF;;AAEa,IAAf,oCAAW;AAEU,IAArB;EACF;;MARI,iCAAQ;YAAG;;;;;ECAO;;;;;ICUhB;;;;;;IAWE;;;;;;;AAHF,MAHF,AAAc,AAAY,wCAAO,QAAC;AACH,QAA7B,yBAAoB;AACC,QAArB,qBAAgB,KAAK;;IAEzB;;AAMmB;AACb;AACA,+BAAkB,MAAM,AAAc,qCAAgB;AAC1D,YAAI,AAAe,AAAW,eAAX,QAAC,cAAa;AAC0B,UAAzD,iBAAiB,AAAS,6BAAe;;AAEa,UAAtD,iBAAiB,AAAS,6BAAe;;AAEqB,QAAhE,0CAAqB,cAAc,GAAC,AAAe,eAAA,QAAC;MACtD;;;AAEiB;AACX;AAGA,gCAAmB,MAAM,AAAc,mCAAc;AACzD,YAAI,AAAgB,AAAW,gBAAX,QAAC,cAAa;AACyB,UAAzD,iBAAiB,AAAS,6BAAe;;AAEa,UAAtD,iBAAiB,AAAS,6BAAe;;AAEsB,QAAjE,0CAAqB,cAAc,GAAC,AAAgB,gBAAA,QAAC;MACvD;;wBAGgC;AAChB,iDAAiB,AAAS,6BAAe;AACzC,+CAAe,AAAS,6BAAe;AACrD,UAAI,AAAK,IAAD,KAAI;AAC2B,QAArC,AAAe,AAAM,cAAP,mBAAiB;AACY,QAA3C,AAAa,AAAM,YAAP,mBAAiB;;AAEgB,QAA7C,AAAe,AAAM,cAAP,mBAAiB;AACI,QAAnC,AAAa,AAAM,YAAP,mBAAiB;;IAEjC;yBAEkC,SAAgB;AAAzB;AACQ,QAA/B,AAAQ,AAAM,OAAP,mBAAiB;AACD,QAAvB,AAAQ,OAAD,UAAQ,QAAQ;AACmB,QAA1C,MAAa,qBAAQ,gCAAkB;AACT,QAA9B,AAAQ,AAAM,OAAP,mBAAiB;AACV,QAAd;MACF;;;AAGkC,MAAhC,yBAAoB;AACqC,MAAzD,qBAAgB,qBAAM,MAAM,MAAM,MAAM,MAAM,MAAM;IACtD;;uDAlDuB;IAbnB;IAWE,sBAAgB,qBAAM,GAAG,MAAM,MAAM,MAAM,MAAM;IAEhC;;EAAc;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ACgDnC,uBAAiB,2BAAY,+EAA+E;IAC9G;;AAIQ,iBAAO;AACP,oBAAU;AACU,6BAAmB,kBAAa,OAAO;AAC3D,gBAAc;AACd,kBAAQ,sBAAmB,GAAG,EAAE,gBAAgB;AACF,MAApD,yBAAsB,KAAK,EAAE,MAAM;AACpB,MAAf,cAAS,KAAK;AACR,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AAClC,MAAf,kCAAS,KAAK;AACoB,MAAlC,oBAAsB,uBAAO;AACvB,kBAAQ,sBAAmB,GAAG,EAAE,KAAK;AACH,MAAxC,AAAK,sBAAiB,KAAK,EAAE;AACd,MAAf,cAAS,KAAK;AACR,kBAAQ,sBAAmB,GAAG,EAAE,KAAK;AACQ,MAAnD,AAAK,sBAAiB,KAAK,EAAE;AACd,MAAf,cAAS,KAAK;AACR,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACD,MAAhD,yBAAsB,KAAK,EAAE,OAAO;AACrB,MAAf,cAAS,KAAK;AACR,oBAAU,uBAAoB,KAAK,EAAE;AACrC,oBAAU,uBAAoB,KAAK,EAAE;AACQ,oBAAnD,qBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACC,MAA5C,AAAK,sBAAiB,aAAO;AACkB,MAA/C,yBAAsB,aAAO,MAAM;AACS,MAA5C,yBAAsB,aAAO,YAAY;AACG,MAA5C,yBAAsB,aAAO,QAAQ;AACtB,MAAf,cAAS;AAC2C,MAApD,+BAAiC;AACW,MAA5C,0BAAoB,CAAC;AAC0C,MAA/D,kCAAoC,oDAAqB;AACP,MAAlD,6BAAuB,qCAAC;AAC+C,MAAvE,qBAAuB,yBAAQ,yBAAmB;AAC5C,kBAAQ,sBAAmB,GAAG,EAAE,KAAK;AACQ,MAAnD,AAAK,sBAAiB,KAAK,EAAE;AACd,MAAf,cAAS,KAAK;AACR,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACI,MAArD,yBAAsB,KAAK,EAAE,OAAO;AACrB,MAAf,cAAS,KAAK;AACR,qBAAW,uBAAoB,KAAK,EAAE;AACtC,qBAAW,uBAAoB,KAAK,EAAE;AACQ,sBAApD,qBAAS,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACC,MAA7C,AAAK,sBAAiB,eAAQ;AACuB,MAArD,yBAAsB,eAAQ,MAAM;AACS,MAA7C,yBAAsB,eAAQ,YAAY;AACG,MAA7C,yBAAsB,eAAQ,QAAQ;AACtB,MAAhB,cAAS;AAC4C,MAArD,gCAAkC;AACY,MAA9C,2BAAqB,CAAC;AAC2C,MAAjE,mCAAqC,oDAAqB;AACN,MAApD,8BAAwB,qCAAC;AACiD,MAA1E,sBAAwB,yBAAQ,0BAAoB;AAC9C,mBAAS,sBAAmB,GAAG,EAAE,KAAK;AACH,MAAzC,AAAK,sBAAiB,MAAM,EAAE;AACd,MAAhB,cAAS,MAAM;AACT,mBAAS,sBAAmB,GAAG,EAAE,MAAM;AACO,MAApD,AAAK,sBAAiB,MAAM,EAAE;AACd,MAAhB,cAAS,MAAM;AACT,mBAAS,0BAAuB,GAAG,EAAE,MAAM,EAAE;AACE,MAArD,yBAAsB,MAAM,EAAE,OAAO;AACrB,MAAhB,cAAS,MAAM;AACT,qBAAW,uBAAoB,MAAM,EAAE;AACvC,qBAAW,uBAAoB,MAAM,EAAE;AACQ,qBAArD,qBAAS,0BAAuB,GAAG,EAAE,MAAM,EAAE;AACA,MAA7C,AAAK,sBAAiB,cAAQ;AACsB,MAApD,yBAAsB,cAAQ,MAAM;AACS,MAA7C,yBAAsB,cAAQ,YAAY;AACG,MAA7C,yBAAsB,cAAQ,QAAQ;AACtB,MAAhB,cAAS;AAC4C,MAArD,gCAAkC;AACY,MAA9C,2BAAqB,CAAC;AAC2C,MAAjE,mCAAqC,oDAAqB;AACN,MAApD,8BAAwB,qCAAC;AACiD,MAA1E,sBAAwB,yBAAQ,0BAAoB;AAC9C,mBAAS,sBAAmB,GAAG,EAAE,MAAM;AACO,MAApD,AAAK,sBAAiB,MAAM,EAAE;AACd,MAAhB,cAAS,MAAM;AACT,mBAAS,0BAAuB,GAAG,EAAE,MAAM,EAAE;AACN,MAA7C,yBAAsB,MAAM,EAAE,OAAO;AACrB,MAAhB,cAAS,MAAM;AACT,qBAAW,uBAAoB,MAAM,EAAE;AACvC,qBAAW,uBAAoB,MAAM,EAAE;AACQ,qBAArD,qBAAS,0BAAuB,GAAG,EAAE,MAAM,EAAE;AACA,MAA7C,AAAK,sBAAiB,cAAQ;AACc,MAA5C,yBAAsB,cAAQ,MAAM;AACS,MAA7C,yBAAsB,cAAQ,YAAY;AACG,MAA7C,yBAAsB,cAAQ,QAAQ;AACtB,MAAhB,cAAS;AAC4C,MAArD,gCAAkC;AACY,MAA9C,2BAAqB,CAAC;AAC2C,MAAjE,mCAAqC,oDAAqB;AACN,MAApD,8BAAwB,qCAAC;AACiD,MAA1E,sBAAwB,yBAAQ,0BAAoB;AAC9C,mBAAS,sBAAmB,GAAG,EAAE,MAAM;AACO,MAApD,AAAK,sBAAiB,MAAM,EAAE;AACd,MAAhB,cAAS,MAAM;AACT,mBAAS,0BAAuB,GAAG,EAAE,MAAM,EAAE;AACL,MAA9C,yBAAsB,MAAM,EAAE,OAAO;AACrB,MAAhB,cAAS,MAAM;AACT,qBAAW,uBAAoB,MAAM,EAAE;AACvC,qBAAW,uBAAoB,MAAM,EAAE;AACQ,qBAArD,qBAAS,0BAAuB,GAAG,EAAE,MAAM,EAAE;AACA,MAA7C,AAAK,sBAAiB,cAAQ;AACe,MAA7C,yBAAsB,cAAQ,MAAM;AACS,MAA7C,yBAAsB,cAAQ,YAAY;AACG,MAA7C,yBAAsB,cAAQ,QAAQ;AACtB,MAAhB,cAAS;AAC4C,MAArD,gCAAkC;AACY,MAA9C,2BAAqB,CAAC;AAC2C,MAAjE,mCAAqC,oDAAqB;AACN,MAApD,8BAAwB,qCAAC;AACiD,MAA1E,sBAAwB,yBAAQ,0BAAoB;AAC9C,mBAAS,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACL,MAA7C,AAAK,0CAAiB,MAAM,GAAE;AACsB,MAApD,yBAAsB,MAAM,EAAE,MAAM;AACW,MAA/C,yBAAsB,MAAM,EAAE,QAAQ;AACtB,MAAhB,kCAAS,MAAM;AACT,qBAAW,uBAAoB,MAAM,EAAE;AACvC,qBAAW,uBAAoB,KAAK,EAAE;AACtC,mBAAS,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACF,MAAhD,AAAK,0CAAiB,MAAM,GAAE;AACoB,MAAlD,yBAAsB,MAAM,EAAE,MAAM;AACW,MAA/C,yBAAsB,MAAM,EAAE,QAAQ;AACtB,MAAhB,kCAAS,MAAM;AACT,qBAAW,uBAAoB,MAAM,EAAE;AACvC,qBAAW,uBAAoB,KAAK,EAAE;AACtC,mBAAS,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACH,MAA/C,AAAK,0CAAiB,MAAM,GAAE;AACgB,MAA9C,yBAAsB,MAAM,EAAE,QAAQ;AACtB,MAAhB,kCAAS,MAAM;AACT,qBAAW,uBAAoB,MAAM,EAAE;AACvC,mBAAS,sBAAmB,GAAG,EAAE,KAAK;AACmB,MAA/D,AAAK,sBAAiB,MAAM,EAAE;AACsB,MAApD,yBAAsB,MAAM,EAAE,MAAM;AACU,MAA9C,yBAAsB,MAAM,EAAE,QAAQ;AACtB,MAAhB,cAAS,MAAM;AACT,mBAAS,sBAAmB,GAAG,EAAE,KAAK;AACkB,MAA9D,AAAK,sBAAiB,MAAM,EAAE;AACmB,MAAjD,yBAAsB,MAAM,EAAE,MAAM;AACU,MAA9C,yBAAsB,MAAM,EAAE,QAAQ;AACtB,MAAhB,cAAS,MAAM;AAC0F,MAAhG,AAAa,AAAa,0DAAiB,KAAK,EAAE,UAAU,4CAA0B,UAAZ;AAChB,MAAnE,AAAM,KAAD,oBAAkB,SAAS,2CAA0B,UAAZ;AACuC,MAArF,AAAM,+BAAiB,QAAQ,+BAAwC,UAA1B;AACoB,MAAjE,AAAM,+BAAiB,SAAS,qDAAc;AACxC,2BAAiB,AAAa,AAAO,iCAAO,yDAAc;AACuB,MAAvF,AAAO,iCAAiB,QAAQ,+BAAyC,UAA3B;AACqB,MAAnE,AAAO,iCAAiB,SAAS,qDAAc;AACzC,2BAAiB,AAAc,AAAO,kCAAO,yDAAc;AACsB,MAAvF,AAAO,gCAAiB,QAAQ,+BAAyC,UAA3B;AACqB,MAAnE,AAAO,gCAAiB,SAAS,qDAAc;AACzC,2BAAiB,AAAc,AAAO,kCAAO,yDAAc;AACsB,MAAvF,AAAO,gCAAiB,QAAQ,+BAAyC,UAA3B;AACqB,MAAnE,AAAO,gCAAiB,SAAS,qDAAc;AACzC,2BAAiB,AAAc,AAAO,kCAAO,yDAAc;AACsB,MAAvF,AAAO,gCAAiB,QAAQ,+BAAyC,UAA3B;AACqB,MAAnE,AAAO,gCAAiB,SAAS,qDAAc;AACzC,2BAAiB,AAAc,AAAO,kCAAO,yDAAc;AACG,MAApE,AAAO,MAAD,oBAAkB,SAAS,+BAAmB,UAAL,IAAI;AACe,MAAlE,AAAO,MAAD,oBAAkB,SAAS,+BAAmB,UAAL,IAAI;AACe,MAAlE,AAAO,MAAD,oBAAkB,SAAS,+BAAmB,UAAL,IAAI;AAC6C,MAAhG,uBAAe,yCAAC,cAAc,EAAE,cAAc,EAAE,cAAc,EAAE,cAAc,EAAE,cAAc;IAChG;wBAGoC,OAAW,WAAmB;AAChE,UAAM,AAAE,kBAAG,SAAS,KAAgB,aAAV,SAAS,KAAI;AACrC,YAAK,AAAE,MAAG,SAAS;AACjB,cAAI,AAAU,KAAK;AACjB,kBAAO;;AAET,cAAI,AAAU,KAAK;AACjB,kBAAO;;AAET,cAAK,AAAU,KAAK,KAAU,mCAAY,AAAU,KAAK,KAAW;AAClE,kBAAO;;;AAGX,YAAK,AAAG,OAAG,SAAS;AAClB,cAAI,AAAU,KAAK;AACjB,kBAAO;;AAET,cAAI,AAAU,KAAK;AACjB,kBAAO;;AAET,cAAK,AAAU,KAAK,KAAU,mCAAY,AAAU,KAAK,KAAW;AAClE,kBAAO;;;AAGX,YAAK,AAAG,OAAG,SAAS;AAClB,cAAI,AAAU,KAAK;AACjB,kBAAO;;AAET,cAAI,AAAU,KAAK;AACjB,kBAAO;;AAET,cAAK,AAAU,KAAK,KAAU,mCAAY,AAAU,KAAK,KAAW;AAClE,kBAAO;;;AAGX,YAAK,AAAG,OAAG,SAAS;AAClB,cAAI,AAAU,KAAK;AACjB,kBAAO;;AAET,cAAI,AAAU,KAAK;AACjB,kBAAO;;AAET,cAAK,AAAU,KAAK,KAAU,mCAAY,AAAU,KAAK,KAAW;AAClE,kBAAO;;;AAGX,YAAK,AAAG,OAAG,SAAS;AAClB,cAAI,AAAU,KAAK;AACjB,kBAAO;;AAET,cAAI,AAAU,KAAK;AACjB,kBAAO;;AAET,cAAK,AAAU,KAAK,KAAU,mCAAY,AAAU,KAAK,KAAW;AAClE,kBAAO;;;AAGX,YAAK,AAAU,KAAK,KAAU,iCAAW,AAAU,KAAK,KAAW;AACjE,gBAAO;;;AAGX,YAAO,eAAc;IACvB;;AAIQ,iBAAO;AACR,oBAAU;AACV,uBAAmB,AAAQ,iBAAG;AACb,4BAAkB;AAClB,iCAAuB;AACvB,gCAAsB;AACtB,wBAAc;AACd,yBAAe;AACrC,UAAI,UAAU;AAC4B,QAAvC,AAAuB,wCAAW;;AAEtB,MAAf,UAAU;AACuC,MAAjD,AAAa,2BAAQ,AAAK,AAAc,IAAf;AACI,MAA7B,AAAa;AACb,qBAA6B,+CAAmB,UAAU;AACjC,QAAvB,AAAa;;AAEf,UAAI,UAAU;AAC6B,QAAxC,AAAwB,yCAAW;;AAEvB,MAAf,UAAU;AAC6C,MAAvD,AAAc,4BAAQ,AAAK,AAAc,IAAf;AACI,MAA9B,AAAc;AACd,qBAA6B,+CAAmB,UAAU;AAChC,QAAxB,AAAc;;AAEhB,UAAI,UAAU;AAC6B,QAAxC,AAAwB,yCAAW;;AAEvB,MAAf,UAAU;AAC4C,MAAtD,AAAc,4BAAQ,AAAK,AAAc,IAAf;AACI,MAA9B,AAAc;AACd,qBAA6B,+CAAmB,UAAU;AAChC,QAAxB,AAAc;;AAEhB,UAAI,UAAU;AAC6B,QAAxC,AAAwB,yCAAW;;AAEvB,MAAf,UAAU;AACoC,MAA9C,AAAc,4BAAQ,AAAK,AAAc,IAAf;AACI,MAA9B,AAAc;AACd,qBAA6B,+CAAmB,UAAU;AAChC,QAAxB,AAAc;;AAEhB,UAAI,UAAU;AAC6B,QAAxC,AAAwB,yCAAW;;AAEvB,MAAf,UAAU;AACqC,MAA/C,AAAc,4BAAQ,AAAK,AAAc,IAAf;AACI,MAA9B,AAAc;AACd,qBAA6B,+CAAmB,UAAU;AAChC,QAAxB,AAAc;;AAEV,sBAAY,AAAgB,eAAD;AACjC,oBAAI,4BAAsB,gBAAS,SAAS;AACe,QAAzD,+BAA4B,aAAO,YAAY,SAAS;AACrC,QAAnB,iBAAU,SAAS;;AAEf,sBAAY,AAAqB,oBAAD;AACtC,oBAAI,4BAAsB,eAAS,SAAS;AACgB,QAA1D,+BAA4B,eAAQ,YAAY,SAAS;AACtC,QAAnB,gBAAU,SAAS;;AAEf,sBAAY,AAAoB,mBAAD;AACrC,oBAAI,4BAAsB,eAAS,SAAS;AACgB,QAA1D,+BAA4B,cAAQ,YAAY,SAAS;AACtC,QAAnB,gBAAU,SAAS;;AAEf,sBAAY,AAAY,WAAD;AAC7B,oBAAI,4BAAsB,eAAS,SAAS;AACgB,QAA1D,+BAA4B,cAAQ,YAAY,SAAS;AACtC,QAAnB,gBAAU,SAAS;;AAEf,uBAAa,AAAa,YAAD;AAC/B,oBAAI,4BAAsB,gBAAU,UAAU;AACe,QAA3D,+BAA4B,cAAQ,YAAY,UAAU;AACrC,QAArB,iBAAW,UAAU;;IAEzB;gCAE+B;AACvB,iBAAO;AACwB,MAArC,AAAK,AAAc,IAAf,2BAA2B;IACjC;wBAEuB;AACsC,MAA3D,AAA0B,4DAA2B,WAAP,WAAP;IACzC;iCAEgC;AACxB,iBAAO;AAC6B,MAA1C,AAAK,AAAc,IAAf,gCAAgC;IACtC;yBAEwB;AACsC,MAA5D,AAA2B,6DAA2B,WAAP,WAAP;IAC1C;iCAEgC;AACxB,iBAAO;AAC4B,MAAzC,AAAK,AAAc,IAAf,+BAA+B;IACrC;yBAEwB;AACsC,MAA5D,AAA2B,6DAA2B,WAAP,WAAP;IAC1C;iCAEgC;AACxB,iBAAO;AACoB,MAAjC,AAAK,AAAc,IAAf,uBAAuB;IAC7B;yBAEwB;AACsC,MAA5D,AAA2B,6DAA2B,WAAP,WAAP;IAC1C;iCAEgC;AACxB,iBAAO;AACqB,MAAlC,AAAK,AAAc,IAAf,wBAAwB;IAC9B;yBAEwB;AACsC,MAA5D,AAA2B,6DAA2B,WAAP,WAAP;IAC1C;;AAIM,mBAAS;AACb,UAAI,AAAU,MAAM,IAAE;AAC6G,QAAhI,wEAAoB,SAAU,wEAA2C,2CAAO,wDAA0B;;AAErF,MAAxB,uBAAkB,MAAM;IAC1B;;uEApXwC,YAAgB;IArCzC;IACW;IACZ;IACe;IACe;IAC5B;IACU;IACZ;IACe;IACe;IAC5B;IACU;IACZ;IACe;IACe;IAC5B;IACU;IACZ;IACe;IACe;IAC5B;IACU;IACZ;IACe;IACe;IAC5B;IACX;IACA;IACA;IACA;IACA;IACgB;IACA;IACA;IACA;IACA;AAEkD,kFAAwB,8BAAW,UAAU,EAAE,WAAW;AAC1G,IAArB;AACmD,kBAAnD,oBAAiB,AAAS,8BAAc;EAC1C;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MAJ+B,qEAAgB;;;;;;;;;AAqYA,MAA7C,sBAAc,6DAAuB,MAAM;AAChB,MAA3B,cAAS,AAAY;AAK0E,MAJ/F,yCAAmC,2BAC7B,+DAAmC,qDAAmB,cACrC,2EAAkB,AAAK,iBAAqB,2CAAc,AAAS,6DAE5E,2EAAkB,AAAK,iBAAqB,2CAAc,AAAS;AACvB,MAA1D,AAAY,2BAAO,8BAAwB;AAC9B,MAAb,WAAM;AACN,YAAO,8CAAa,GAAG,MAAM,aAAQ;IACvC;;AAIO,uBAAmB,AAAQ,iBAAG;AACnC,qBAA6B,+CAAmB,UAAU;AACvB,QAAjC,AAAuB;;AAEE,MAA3B,AAAY;IACd;;AAIoC,MAAlC,AAAY;IACd;;4EA3B6C,YAAgB;IAFtC;IACG;AACkD,uFAAwB,yBAAM,UAAU,EAAE,WAAW;;EAAsC;;;;;;;;;iHA8BlF,YAAgB;AACrG,UAAO,mEAA4B,UAAU,EAAE,WAAW;EAC5D;;AAIE,kBAAI;AACF;;AAEa,IAAf,yCAAW;AAE4D,IAAvE,4BAAyB,qDAAmB;AACvB,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;EACF;;;AAtDE,YAAO;IACT;;;;;MAlaoB,sDAAwB;YAAG,EAAS;;MA+ZN,yDAA2B;;;MAKzD,0DAA4B;;;MAuC5C,sCAAQ;YAAG;;;;;MC7eK,sCAAM;YAAG,EAAC;;;;;;mBCkBD;AACb,MAAZ,WAAM,KAAK;AACsC,MAAjD,AAAc,AAAyB,mDAAI,KAAK;IAClD;;AAG4C,MAA1C,AAAQ,yBAAoB,AAAM;IACpC;;mDATqB,eAAoB;IAApB;IAAoB;;EAAQ;;;;;;;;;;;;;;;;;ECVnD;;;;;MAHe,4BAAK;YAAG,qCAAgB;;MACxB,6BAAM;YAAG,qCAAgB;;MACzB,+BAAQ;YAAG,qCAAgB;;;;;;;;AC8BtC,uBAAgB,2BAAY,2EAA2E;IACzG;;AAIQ,iBAAO;AACP,oBAAU;AACU,6BAAmB,kBAAa,OAAO;AAC3D,gBAAc;AACd,kBAAQ,0BAAsB,GAAG,EAAE,gBAAgB,EAAE;AACD,MAA1D,AAAK,0CAAiB,KAAK,GAAE;AACd,MAAf,cAAS,KAAK;AACR,kBAAQ,0BAAsB,GAAG,EAAE,KAAK,EAAE;AACJ,MAA5C,AAAK,0CAAiB,KAAK,GAAE;AACd,MAAf,kCAAS,KAAK;AACR,oBAAU,uBAAmB,KAAK,EAAE;AACpC,kBAAQ,0BAAsB,GAAG,EAAE,KAAK,EAAE;AACL,MAA3C,AAAK,0CAAiB,KAAK,GAAE;AACd,MAAf,kCAAS,KAAK;AACoC,oBAAlD,qBAAQ,0BAAsB,GAAG,EAAE,KAAK,EAAE;AACS,MAAnD,yBAAqB,aAAO,cAAc;AACU,MAApD,AAAK,sBAAiB,aAAO;AACqB,MAAlD,yBAAqB,aAAO,MAAM;AACkB,MAApD,yBAAqB,aAAO,eAAe;AACE,MAA7C,yBAAqB,aAAO,QAAQ;AACrB,MAAf,cAAS;AACH,oBAAU,uBAAmB,KAAK,EAAE;AACpC,kBAAQ,0BAAsB,GAAG,EAAE,KAAK,EAAE;AACE,MAAlD,yBAAqB,KAAK,EAAE,eAAe;AACc,MAAzD,AAAK,0CAAiB,KAAK,GAAE;AACkB,MAA/C,yBAAqB,KAAK,EAAE,MAAM;AACnB,MAAf,cAAS,KAAK;AACmD,MAAjE,AAAM,+BAAiB,SAAS,qDAAc;AACqB,MAAnE,AAAM,+BAAiB,UAAU,qDAAc;AACW,MAA1D,AAAM,KAAD,oBAAkB,SAAS,+BAAmB,UAAL,IAAI;AAC3C,MAAP;IACF;wBAEuB;AACf,gCAAsB;AACtB,iBAAO;AACiC,MAA9C,AAAK,IAAD,gBAAgB,AAAoB,mBAAD;IACzC;yBAEwB;AAChB,gCAAsB;AACtB,iBAAO;AACiC,MAA9C,AAAK,IAAD,gBAAgB,AAAoB,mBAAD;IACzC;;AAIM,mBAAS;AACb,UAAI,AAAU,MAAM,IAAE;AAC2G,QAA9H,oEAAoB,SAAU,oEAA2C,2CAAO,oDAAwB;;AAEnF,MAAxB,uBAAkB,MAAM;IAC1B;;mEA9DsC,YAAgB;IAFjC;AAEgD,8EAAuB,8BAAW,UAAU,EAAE,WAAW;AACvG,IAArB;AACiD,kBAAjD,oBAAiB,AAAS,8BAAc;EAC1C;;;;;;;;;;;;;;MAJ+B,iEAAgB;;;;;;;;;AA+EF,MAA3C,sBAAc,yDAAqB,MAAM;AACd,MAA3B,cAAS,AAAY;AAKiI,MAJtJ,wCAAgC,2BAC1B,2DAAmC,iDAAiB,cACnC,uEAAgB,AAAK,iBAAqB,2CAAc,AAAS,8CAAc,AAAK,iBAAqB,8BAAQ,AAAS,2DAEnI,uEAAgB,AAAK,iBAAqB,2CAAc,AAAS,8CAAc,AAAK,iBAAqB,8BAAQ,AAAS;AAChF,MAAxD,AAAY,2BAAO,6BAAsB;AAC5B,MAAb,WAAM;AACN,YAAO,4CAAa,GAAG,MAAM,aAAQ;IACvC;;AAI6B,MAA3B,AAAY;IACd;;AAIoC,MAAlC,AAAY;IACd;;wEAvB2C,YAAgB;IAFtC;IACG;AACkD,mFAAuB,yBAAM,UAAU,EAAE,WAAW;;EAAsC;;;;;;;;;2GA0BnF,YAAgB;AACjG,UAAO,+DAA0B,UAAU,EAAE,WAAW;EAC1D;;AAIE,kBAAI;AACF;;AAEa,IAAf,uCAAW;AAEwD,IAAnE,4BAAyB,iDAAiB;AACrB,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;EACF;;;AAlDE,YAAO;IACT;;;;;MAzEoB,kDAAsB;YAAG,EAAS;;MAsEN,qDAAyB;;;MAKrD,sDAA0B;;;MAmC1C,oCAAQ;YAAG;;;;;AC7Hb,kBAAI;AACF;;AAEa,IAAf,kCAAW;AAEU,IAArB;EACF;;MARI,+BAAQ;YAAG;;;;;MCRK,oCAAM;YAAG,EAAC;;;;;;ACsC1B,uBAAgB,2BAAY,yEAAyE;IACvG;;AAIQ,iBAAO;AACP,oBAAU;AACU,6BAAmB,kBAAa,OAAO;AAC3D,gBAAc;AACd,kBAAQ,sBAAmB,GAAG,EAAE,gBAAgB;AAC0B,MAAhF,AAAK,sBAAiB,KAAK,EAAE;AACkB,MAA/C,yBAAsB,KAAK,EAAE,MAAM;AACpB,MAAf,cAAS,KAAK;AACR,kBAAQ,sBAAmB,GAAG,EAAE,KAAK;AACR,MAAnC,AAAK,sBAAiB,KAAK,EAAE;AACmB,MAAhD,yBAAsB,KAAK,EAAE,MAAM;AACpB,MAAf,cAAS,KAAK;AACR,kBAAQ,sBAAmB,GAAG,EAAE,KAAK;AACyB,MAApE,AAAK,sBAAiB,KAAK,EAAE;AACd,MAAf,cAAS,KAAK;AACR,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACM,MAAvD,yBAAsB,KAAK,EAAE,OAAO;AACK,MAAzC,AAAK,0CAAiB,KAAK,GAAE;AACiB,MAA9C,yBAAsB,KAAK,EAAE,MAAM;AAC2E,MAA9G,yBAAsB,KAAK,EAAE,OAAO;AACrB,MAAf,cAAS,KAAK;AACR,kBAAQ,sBAAmB,GAAG,EAAE,KAAK;AAC2B,MAAtE,AAAK,sBAAiB,KAAK,EAAE;AACd,MAAf,cAAS,KAAK;AACR,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACO,MAAxD,AAAK,0CAAiB,KAAK,GAAE;AACd,MAAf,kCAAS,KAAK;AACoB,MAAlC,oBAAsB,uBAAO;AACvB,kBAAQ,sBAAmB,GAAG,EAAE,KAAK;AACM,MAAjD,yBAAsB,KAAK,EAAE,MAAM;AACpB,MAAf,cAAS,KAAK;AACR,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACN,MAA3C,AAAK,0CAAiB,KAAK,GAAE;AACd,MAAf,cAAS,KAAK;AACR,oBAAU,uBAAoB,KAAK,EAAE;AACrC,kBAAQ,0BAAuB,GAAG,EAAE,KAAK,EAAE;AAClC,MAAf,cAAS,KAAK;AACR,mBAAS,sBAAmB,GAAG,EAAE,KAAK;AACD,MAA3C,AAAK,sBAAiB,MAAM,EAAE;AACd,MAAhB,cAAS,MAAM;AACT,mBAAS,0BAAuB,GAAG,EAAE,MAAM,EAAE;AACc,MAAjE,yBAAsB,MAAM,EAAE,OAAO;AACrB,MAAhB,cAAS,MAAM;AACT,qBAAW,uBAAoB,MAAM,EAAE;AACvC,qBAAW,uBAAoB,MAAM,EAAE;AACvC,mBAAS,0BAAuB,GAAG,EAAE,MAAM,EAAE;AACN,MAA7C,AAAK,0CAAiB,MAAM,GAAE;AACkB,MAAhD,yBAAsB,MAAM,EAAE,MAAM;AACY,MAAhD,yBAAsB,MAAM,EAAE,QAAQ;AACtB,MAAhB,kCAAS,MAAM;AACT,mBAAS,sBAAmB,GAAG,EAAE,KAAK;AACD,MAA3C,AAAK,sBAAiB,MAAM,EAAE;AACd,MAAhB,cAAS,MAAM;AACT,mBAAS,0BAAuB,GAAG,EAAE,MAAM,EAAE;AACiB,MAApE,yBAAsB,MAAM,EAAE,OAAO;AACrB,MAAhB,cAAS,MAAM;AACT,qBAAW,uBAAoB,MAAM,EAAE;AACvC,qBAAW,uBAAoB,MAAM,EAAE;AACvC,mBAAS,0BAAuB,GAAG,EAAE,MAAM,EAAE;AACN,MAA7C,AAAK,0CAAiB,MAAM,GAAE;AACgB,MAA9C,yBAAsB,MAAM,EAAE,MAAM;AACa,MAAjD,yBAAsB,MAAM,EAAE,QAAQ;AACtB,MAAhB,kCAAS,MAAM;AACT,mBAAS,sBAAmB,GAAG,EAAE,KAAK;AACD,MAA3C,AAAK,sBAAiB,MAAM,EAAE;AACd,MAAhB,cAAS,MAAM;AACT,mBAAS,sBAAmB,GAAG,EAAE,MAAM;AACF,MAA3C,AAAK,sBAAiB,MAAM,EAAE;AACd,MAAhB,cAAS,MAAM;AACT,mBAAS,0BAAuB,GAAG,EAAE,MAAM,EAAE;AACF,MAAjD,AAAK,0CAAiB,MAAM,GAAE;AACuB,MAArD,yBAAsB,MAAM,EAAE,MAAM;AACa,MAAjD,yBAAsB,MAAM,EAAE,QAAQ;AACtB,MAAhB,kCAAS,MAAM;AACT,qBAAW,uBAAoB,MAAM,EAAE;AACvC,mBAAS,0BAAuB,GAAG,EAAE,MAAM,EAAE;AACF,MAAjD,AAAK,0CAAiB,MAAM,GAAE;AACwB,MAAtD,yBAAsB,MAAM,EAAE,OAAO;AACrB,MAAhB,cAAS,MAAM;AACT,qBAAW,uBAAoB,MAAM,EAAE;AACvC,mBAAS,0BAAuB,GAAG,EAAE,KAAK,EAAE;AACL,MAA7C,AAAK,0CAAiB,MAAM,GAAE;AACiB,MAA/C,yBAAsB,MAAM,EAAE,QAAQ;AACtB,MAAhB,kCAAS,MAAM;AACT,qBAAW,uBAAoB,MAAM,EAAE;AAC4D,MAAhG,AAAa,AAAa,0DAAiB,KAAK,EAAE,UAAU,4CAA0B,UAAZ;AAChB,MAAnE,AAAM,KAAD,oBAAkB,SAAS,2CAA0B,UAAZ;AACa,MAA3D,AAAO,MAAD,oBAAkB,SAAS,+BAAmB,UAAL,IAAI;AAC5C,MAAP;IACF;wBAGoC,OAAW,WAAmB;AAChE,WAAM,AAAU,KAAK,KAAU,iCAAW,AAAU,KAAK,KAAW,6DAAwB,AAAE,kBAAG,SAAS,KAAgB,aAAV,SAAS,KAAI;AAC3H,cAAO;;AAET,YAAO,eAAc;IACvB;;AAIM,mBAAS;AACb,UAAI,AAAU,MAAM,IAAE;AAC0G,QAA7H,kEAAoB,SAAU,kEAA2C,2CAAO,kDAAuB;;AAElF,MAAxB,uBAAkB,MAAM;IAC1B;;iEApHqC,YAAgB;IAFtC;AAEqD,4EAAuB,8BAAW,UAAU,EAAE,WAAW;AACtG,IAArB;AACgD,kBAAhD,oBAAiB,AAAS,8BAAc;EAC1C;;;;;;;;;;;;;MAJ+B,+DAAgB;;;;;;;;;AAqIH,MAA1C,sBAAc,uDAAoB,MAAM;AACb,MAA3B,cAAS,AAAY;AAKiE,MAJtF,sCAA+B,2BACzB,yDAAmC,+CAAgB,cAClC,wDAAe,AAAK,iBAAqB,8BAAQ,AAAS,0DAEnE,wDAAe,AAAK,iBAAqB,8BAAQ,AAAS;AACjB,MAAvD,AAAY,2BAAO,2BAAqB;AAC3B,MAAb,WAAM;AACN,YAAO,2CAAa,GAAG,MAAM,aAAQ;IACvC;;AAI6B,MAA3B,AAAY;IACd;;AAIoC,MAAlC,AAAY;IACd;;sEAvB0C,YAAgB;IAFtC;IACG;AACkD,iFAAuB,yBAAM,UAAU,EAAE,WAAW;;EAAsC;;;;;;;;;wGA0BpF,YAAgB;AAC/F,UAAO,6DAAyB,UAAU,EAAE,WAAW;EACzD;;AAIE,kBAAI;AACF;;AAEa,IAAf,sCAAW;AAEsD,IAAjE,4BAAyB,+CAAgB;AACpB,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;EACF;;;AAnDE,YAAO;IACT;;;;;MA/HoB,gDAAqB;YAAG,EAAS;;MA4HN,mDAAwB;;;MAKnD,oDAAyB;;;MAmCzC,mCAAQ;YAAG;;;;;MChMK,mCAAM;YAAG,EAAC;;;;ACe5B,kBAAI;AACF;;AAEa,IAAf,6BAAW;AAEU,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;EACF;;MAbI,0BAAQ;YAAG;;;;;ECLO","file":"cadastro_component.css.shim.ddc.js"}');
  // Exports:
  return {
    src__components__navbar_component__navbar_component$46css$46shim: navbar_component$46css$46shim,
    src__components__not_found_component__not_found_component: not_found_component,
    src__components__login_component__login_component: login_component,
    src__model__login: login$,
    src__routes__routes: routes,
    src__components__not_found_component__not_found_component$46template: not_found_component$46template,
    src__components__not_found_component__not_found_component$46css$46shim: not_found_component$46css$46shim,
    src__components__painel_component__painel_component$46template: painel_component$46template,
    src__services__carro_service: carro_service,
    src__model__carro: carro$,
    src__components__tabela_component__tabela_component: tabela_component,
    src__components__tabela_component__tabela_component$46template: tabela_component$46template,
    src__components__tabela_component__tabela_component$46css$46shim: tabela_component$46css$46shim,
    src__services__carro_service$46template: carro_service$46template,
    src__model__carro$46template: carro$46template,
    src__components__cadastro_component__cadastro_component: cadastro_component,
    src__components__cadastro_component__cadastro_component$46template: cadastro_component$46template,
    src__components__cadastro_component__cadastro_component$46css$46shim: cadastro_component$46css$46shim,
    src__components__navbar_component__navbar_component: navbar_component,
    src__routes__route_paths: route_paths,
    src__components__navbar_component__navbar_component$46template: navbar_component$46template,
    src__routes__route_paths$46template: route_paths$46template,
    src__components__painel_component__painel_component: painel_component,
    src__components__painel_component__painel_component$46css$46shim: painel_component$46css$46shim,
    src__components__login_component__login_component$46template: login_component$46template,
    src__components__login_component__login_component$46css$46shim: login_component$46css$46shim,
    src__routes__routes$46template: routes$46template,
    src__model__login$46template: login$46template
  };
});

//# sourceMappingURL=cadastro_component.css.shim.ddc.js.map
