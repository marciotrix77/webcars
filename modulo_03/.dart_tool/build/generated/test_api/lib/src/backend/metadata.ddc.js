define(['dart_sdk', 'packages/collection/src/canonicalized_map', 'packages/boolean_selector/boolean_selector', 'packages/collection/collection', 'packages/source_span/source_span', 'packages/async/async', 'packages/term_glyph/src/generated/ascii_glyph_set', 'packages/matcher/src/core_matchers', 'packages/stream_channel/stream_channel', 'packages/path/path', 'packages/string_scanner/src/eager_span_scanner'], function(dart_sdk, packages__collection__src__canonicalized_map, packages__boolean_selector__boolean_selector, packages__collection__collection, packages__source_span__source_span, packages__async__async, packages__term_glyph__src__generated__ascii_glyph_set, packages__matcher__src__core_matchers, packages__stream_channel__stream_channel, packages__path__path, packages__string_scanner__src__eager_span_scanner) {
  'use strict';
  const core = dart_sdk.core;
  const _js_helper = dart_sdk._js_helper;
  const collection = dart_sdk.collection;
  const async = dart_sdk.async;
  const math = dart_sdk.math;
  const _native_typed_data = dart_sdk._native_typed_data;
  const convert = dart_sdk.convert;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const unmodifiable_wrappers = packages__collection__src__canonicalized_map.src__unmodifiable_wrappers;
  const boolean_selector = packages__boolean_selector__boolean_selector.boolean_selector;
  const impl = packages__boolean_selector__boolean_selector.src__impl;
  const all = packages__boolean_selector__boolean_selector.src__all;
  const functions = packages__collection__collection.src__functions;
  const span_exception = packages__source_span__source_span.src__span_exception;
  const span = packages__source_span__source_span.src__span;
  const cancelable_operation = packages__async__async.src__cancelable_operation;
  const stream_sink_transformer = packages__async__async.src__stream_sink_transformer;
  const top_level = packages__term_glyph__src__generated__ascii_glyph_set.src__generated__top_level;
  const description = packages__matcher__src__core_matchers.src__description;
  const stream_channel_transformer = packages__stream_channel__stream_channel.src__stream_channel_transformer;
  const path = packages__path__path.path;
  const style = packages__path__path.src__style;
  const string_scanner = packages__string_scanner__src__eager_span_scanner.src__string_scanner;
  var suite_platform = Object.create(dart.library);
  var runtime = Object.create(dart.library);
  var operating_system = Object.create(dart.library);
  var skip$ = Object.create(dart.library);
  var metadata$ = Object.create(dart.library);
  var platform_selector = Object.create(dart.library);
  var utils = Object.create(dart.library);
  var timeout$ = Object.create(dart.library);
  var $_get = dartx._get;
  var $firstWhere = dartx.firstWhere;
  var $contains = dartx.contains;
  var $_equals = dartx._equals;
  var $toSet = dartx.toSet;
  var $map = dartx.map;
  var $_set = dartx._set;
  var $forEach = dartx.forEach;
  var $any = dartx.any;
  var $keys = dartx.keys;
  var $toList = dartx.toList;
  var $remove = dartx.remove;
  var $fold = dartx.fold;
  var $where = dartx.where;
  var $isEmpty = dartx.isEmpty;
  var $length = dartx.length;
  var $add = dartx.add;
  var $toString = dartx.toString;
  var $hashCode = dartx.hashCode;
  var $replaceFirst = dartx.replaceFirst;
  var $times = dartx['*'];
  var $first = dartx.first;
  var $take = dartx.take;
  var $join = dartx.join;
  var $last = dartx.last;
  var $startsWith = dartx.startsWith;
  var $replaceAll = dartx.replaceAll;
  var $split = dartx.split;
  var $substring = dartx.substring;
  var $indexOf = dartx.indexOf;
  var $modulo = dartx['%'];
  var $truncate = dartx.truncate;
  var $skip = dartx.skip;
  var $round = dartx.round;
  var IdentityMapOfStringL$ObjectL = () => (IdentityMapOfStringL$ObjectL = dart.constFn(_js_helper.IdentityMap$(core.String, core.Object)))();
  var RuntimeLToboolL = () => (RuntimeLToboolL = dart.constFn(dart.fnType(core.bool, [runtime.Runtime])))();
  var OperatingSystemLToboolL = () => (OperatingSystemLToboolL = dart.constFn(dart.fnType(core.bool, [operating_system.OperatingSystem])))();
  var VoidToNullN = () => (VoidToNullN = dart.constFn(dart.fnType(core.Null, [])))();
  var UnmodifiableSetViewOfStringL = () => (UnmodifiableSetViewOfStringL = dart.constFn(unmodifiable_wrappers.UnmodifiableSetView$(core.String)))();
  var _IdentityHashSetOfStringL = () => (_IdentityHashSetOfStringL = dart.constFn(collection._IdentityHashSet$(core.String)))();
  var UnmodifiableMapViewOfPlatformSelectorL$MetadataL = () => (UnmodifiableMapViewOfPlatformSelectorL$MetadataL = dart.constFn(collection.UnmodifiableMapView$(platform_selector.PlatformSelector, metadata$.Metadata)))();
  var UnmodifiableMapViewOfBooleanSelectorL$MetadataL = () => (UnmodifiableMapViewOfBooleanSelectorL$MetadataL = dart.constFn(collection.UnmodifiableMapView$(boolean_selector.BooleanSelector, metadata$.Metadata)))();
  var LinkedHashSetOfStringL = () => (LinkedHashSetOfStringL = dart.constFn(collection.LinkedHashSet$(core.String)))();
  var LinkedHashMapOfPlatformSelectorL$MetadataL = () => (LinkedHashMapOfPlatformSelectorL$MetadataL = dart.constFn(collection.LinkedHashMap$(platform_selector.PlatformSelector, metadata$.Metadata)))();
  var dynamicToPlatformSelectorL = () => (dynamicToPlatformSelectorL = dart.constFn(dart.fnType(platform_selector.PlatformSelector, [dart.dynamic])))();
  var dynamicToMetadataL = () => (dynamicToMetadataL = dart.constFn(dart.fnType(metadata$.Metadata, [dart.dynamic])))();
  var MapEntryOfBooleanSelectorL$MetadataL = () => (MapEntryOfBooleanSelectorL$MetadataL = dart.constFn(core.MapEntry$(boolean_selector.BooleanSelector, metadata$.Metadata)))();
  var dynamicAnddynamicToMapEntryLOfBooleanSelectorL$MetadataL = () => (dynamicAnddynamicToMapEntryLOfBooleanSelectorL$MetadataL = dart.constFn(dart.fnType(MapEntryOfBooleanSelectorL$MetadataL(), [dart.dynamic, dart.dynamic])))();
  var LinkedMapOfPlatformSelectorL$MetadataL = () => (LinkedMapOfPlatformSelectorL$MetadataL = dart.constFn(_js_helper.LinkedMap$(platform_selector.PlatformSelector, metadata$.Metadata)))();
  var StringLAnddynamicToNullN = () => (StringLAnddynamicToNullN = dart.constFn(dart.fnType(core.Null, [core.String, dart.dynamic])))();
  var dynamicToboolL = () => (dynamicToboolL = dart.constFn(dart.fnType(core.bool, [dart.dynamic])))();
  var VoidToMetadataL = () => (VoidToMetadataL = dart.constFn(dart.fnType(metadata$.Metadata, [])))();
  var LinkedHashMapOfBooleanSelectorL$MetadataL = () => (LinkedHashMapOfBooleanSelectorL$MetadataL = dart.constFn(collection.LinkedHashMap$(boolean_selector.BooleanSelector, metadata$.Metadata)))();
  var MetadataLAndBooleanSelectorLToMetadataL = () => (MetadataLAndBooleanSelectorLToMetadataL = dart.constFn(dart.fnType(metadata$.Metadata, [metadata$.Metadata, boolean_selector.BooleanSelector])))();
  var StringLToboolL = () => (StringLToboolL = dart.constFn(dart.fnType(core.bool, [core.String])))();
  var StringLToStringL = () => (StringLToStringL = dart.constFn(dart.fnType(core.String, [core.String])))();
  var PlatformSelectorLAndMetadataLToNullN = () => (PlatformSelectorLAndMetadataLToNullN = dart.constFn(dart.fnType(core.Null, [platform_selector.PlatformSelector, metadata$.Metadata])))();
  var MetadataLAndMetadataLToMetadataL = () => (MetadataLAndMetadataLToMetadataL = dart.constFn(dart.fnType(metadata$.Metadata, [metadata$.Metadata, metadata$.Metadata])))();
  var MapEntryOfStringL$dynamic = () => (MapEntryOfStringL$dynamic = dart.constFn(core.MapEntry$(core.String, dart.dynamic)))();
  var BooleanSelectorLAndMetadataLToMapEntryLOfStringL$dynamic = () => (BooleanSelectorLAndMetadataLToMapEntryLOfStringL$dynamic = dart.constFn(dart.fnType(MapEntryOfStringL$dynamic(), [boolean_selector.BooleanSelector, metadata$.Metadata])))();
  var IdentityMapOfStringL$dynamic = () => (IdentityMapOfStringL$dynamic = dart.constFn(_js_helper.IdentityMap$(core.String, dart.dynamic)))();
  var IdentityMapOfStringL$numL = () => (IdentityMapOfStringL$numL = dart.constFn(_js_helper.IdentityMap$(core.String, core.num)))();
  var VoidToBooleanSelectorL = () => (VoidToBooleanSelectorL = dart.constFn(dart.fnType(boolean_selector.BooleanSelector, [])))();
  var VoidTovoid = () => (VoidTovoid = dart.constFn(dart.fnType(dart.void, [])))();
  var RuntimeLToStringL = () => (RuntimeLToStringL = dart.constFn(dart.fnType(core.String, [runtime.Runtime])))();
  var OperatingSystemLToStringL = () => (OperatingSystemLToStringL = dart.constFn(dart.fnType(core.String, [operating_system.OperatingSystem])))();
  var FutureOfListL = () => (FutureOfListL = dart.constFn(async.Future$(core.List)))();
  var VoidToFutureLOfListL = () => (VoidToFutureLOfListL = dart.constFn(dart.fnType(FutureOfListL(), [])))();
  var dynamicAnddynamicToNullN = () => (dynamicAnddynamicToNullN = dart.constFn(dart.fnType(core.Null, [dart.dynamic, dart.dynamic])))();
  var ListOfintL = () => (ListOfintL = dart.constFn(core.List$(core.int)))();
  var _StreamSubscriptionTransformerOfListLOfintL$StringL = () => (_StreamSubscriptionTransformerOfListLOfintL$StringL = dart.constFn(async._StreamSubscriptionTransformer$(ListOfintL(), core.String)))();
  var StreamSubscriptionOfStringL = () => (StreamSubscriptionOfStringL = dart.constFn(async.StreamSubscription$(core.String)))();
  var StreamOfListLOfintL = () => (StreamOfListLOfintL = dart.constFn(async.Stream$(ListOfintL())))();
  var StreamLOfListLOfintLAndboolLToStreamSubscriptionLOfStringL = () => (StreamLOfListLOfintLAndboolLToStreamSubscriptionLOfStringL = dart.constFn(dart.fnType(StreamSubscriptionOfStringL(), [StreamOfListLOfintL(), core.bool])))();
  var StreamChannelTransformerOfStringL$StringL = () => (StreamChannelTransformerOfStringL$StringL = dart.constFn(stream_channel_transformer.StreamChannelTransformer$(core.String, core.String)))();
  var StreamSinkTransformerOfStringL$StringL = () => (StreamSinkTransformerOfStringL$StringL = dart.constFn(stream_sink_transformer.StreamSinkTransformer$(core.String, core.String)))();
  var EventSinkOfStringL = () => (EventSinkOfStringL = dart.constFn(async.EventSink$(core.String)))();
  var StringLAndEventSinkLOfStringLTovoid = () => (StringLAndEventSinkLOfStringLTovoid = dart.constFn(dart.fnType(dart.void, [core.String, EventSinkOfStringL()])))();
  var VoidToOperatingSystemL = () => (VoidToOperatingSystemL = dart.constFn(dart.fnType(operating_system.OperatingSystem, [])))();
  const CT = Object.create(null);
  var L2 = "package:test_api/src/backend/operating_system.dart";
  var L6 = "package:test_api/src/utils.dart";
  var L5 = "package:test_api/src/backend/platform_selector.dart";
  var L4 = "package:test_api/src/backend/metadata.dart";
  var L0 = "package:test_api/src/backend/suite_platform.dart";
  var L1 = "package:test_api/src/backend/runtime.dart";
  var L7 = "package:test_api/src/frontend/timeout.dart";
  var L3 = "package:test_api/src/frontend/skip.dart";
  dart.defineLazy(CT, {
    get C0() {
      return C0 = dart.const({
        __proto__: runtime.Runtime.prototype,
        [isHeadless$]: false,
        [isBlink$]: false,
        [isJS$]: false,
        [isBrowser$]: false,
        [isDartVM$]: true,
        [parent$]: null,
        [identifier$]: "vm",
        [name$]: "VM"
      });
    },
    get C1() {
      return C1 = dart.const({
        __proto__: runtime.Runtime.prototype,
        [isHeadless$]: false,
        [isBlink$]: true,
        [isJS$]: true,
        [isBrowser$]: true,
        [isDartVM$]: false,
        [parent$]: null,
        [identifier$]: "chrome",
        [name$]: "Chrome"
      });
    },
    get C2() {
      return C2 = dart.const({
        __proto__: runtime.Runtime.prototype,
        [isHeadless$]: true,
        [isBlink$]: true,
        [isJS$]: true,
        [isBrowser$]: true,
        [isDartVM$]: false,
        [parent$]: null,
        [identifier$]: "phantomjs",
        [name$]: "PhantomJS"
      });
    },
    get C3() {
      return C3 = dart.const({
        __proto__: runtime.Runtime.prototype,
        [isHeadless$]: false,
        [isBlink$]: false,
        [isJS$]: true,
        [isBrowser$]: true,
        [isDartVM$]: false,
        [parent$]: null,
        [identifier$]: "firefox",
        [name$]: "Firefox"
      });
    },
    get C4() {
      return C4 = dart.const({
        __proto__: runtime.Runtime.prototype,
        [isHeadless$]: false,
        [isBlink$]: false,
        [isJS$]: true,
        [isBrowser$]: true,
        [isDartVM$]: false,
        [parent$]: null,
        [identifier$]: "safari",
        [name$]: "Safari"
      });
    },
    get C5() {
      return C5 = dart.const({
        __proto__: runtime.Runtime.prototype,
        [isHeadless$]: false,
        [isBlink$]: false,
        [isJS$]: true,
        [isBrowser$]: true,
        [isDartVM$]: false,
        [parent$]: null,
        [identifier$]: "ie",
        [name$]: "Internet Explorer"
      });
    },
    get C6() {
      return C6 = dart.const({
        __proto__: runtime.Runtime.prototype,
        [isHeadless$]: false,
        [isBlink$]: false,
        [isJS$]: true,
        [isBrowser$]: false,
        [isDartVM$]: false,
        [parent$]: null,
        [identifier$]: "node",
        [name$]: "Node.js"
      });
    },
    get C7() {
      return C7 = dart.constList([C0 || CT.C0, C1 || CT.C1, C2 || CT.C2, C3 || CT.C3, C4 || CT.C4, C5 || CT.C5, C6 || CT.C6], runtime.Runtime);
    },
    get C8() {
      return C8 = dart.const({
        __proto__: operating_system.OperatingSystem.prototype,
        [identifier$0]: "windows",
        [name$0]: "Windows"
      });
    },
    get C9() {
      return C9 = dart.const({
        __proto__: operating_system.OperatingSystem.prototype,
        [identifier$0]: "mac-os",
        [name$0]: "OS X"
      });
    },
    get C10() {
      return C10 = dart.const({
        __proto__: operating_system.OperatingSystem.prototype,
        [identifier$0]: "linux",
        [name$0]: "Linux"
      });
    },
    get C11() {
      return C11 = dart.const({
        __proto__: operating_system.OperatingSystem.prototype,
        [identifier$0]: "android",
        [name$0]: "Android"
      });
    },
    get C12() {
      return C12 = dart.const({
        __proto__: operating_system.OperatingSystem.prototype,
        [identifier$0]: "ios",
        [name$0]: "iOS"
      });
    },
    get C13() {
      return C13 = dart.const({
        __proto__: operating_system.OperatingSystem.prototype,
        [identifier$0]: "none",
        [name$0]: "none"
      });
    },
    get C14() {
      return C14 = dart.constList([C8 || CT.C8, C9 || CT.C9, C10 || CT.C10, C11 || CT.C11, C12 || CT.C12], operating_system.OperatingSystem);
    },
    get C15() {
      return C15 = dart.const({
        __proto__: timeout$.Timeout.prototype,
        [Timeout_scaleFactor]: 1,
        [Timeout_duration]: null
      });
    },
    get C16() {
      return C16 = dart.constMap(platform_selector.PlatformSelector, metadata$.Metadata, []);
    },
    get C17() {
      return C17 = dart.constMap(boolean_selector.BooleanSelector, metadata$.Metadata, []);
    },
    get C20() {
      return C20 = dart.constList([], core.String);
    },
    get C19() {
      return C19 = dart.const({
        __proto__: all.All.prototype,
        [All_variables]: C20 || CT.C20
      });
    },
    get C18() {
      return C18 = dart.const({
        __proto__: platform_selector.PlatformSelector.prototype,
        [_span$]: null,
        [_inner$]: C19 || CT.C19
      });
    },
    get C21() {
      return C21 = dart.fn(utils.addBullet, StringLToStringL());
    },
    get C22() {
      return C22 = dart.const({
        __proto__: convert.LineSplitter.prototype
      });
    },
    get C23() {
      return C23 = dart.const({
        __proto__: timeout$.Timeout.prototype,
        [Timeout_scaleFactor]: null,
        [Timeout_duration]: null
      });
    }
  }, false);
  var runtime$ = dart.privateName(suite_platform, "SuitePlatform.runtime");
  var os$ = dart.privateName(suite_platform, "SuitePlatform.os");
  var inGoogle$ = dart.privateName(suite_platform, "SuitePlatform.inGoogle");
  suite_platform.SuitePlatform = class SuitePlatform extends core.Object {
    get runtime() {
      return this[runtime$];
    }
    set runtime(value) {
      super.runtime = value;
    }
    get os() {
      return this[os$];
    }
    set os(value) {
      super.os = value;
    }
    get inGoogle() {
      return this[inGoogle$];
    }
    set inGoogle(value) {
      super.inGoogle = value;
    }
    static deserialize(serialized) {
      let map = core.Map.as(serialized);
      return new suite_platform.SuitePlatform.new(runtime.Runtime.deserialize(map[$_get]("runtime")), {os: operating_system.OperatingSystem.find(core.String.as(map[$_get]("os"))), inGoogle: core.bool.as(map[$_get]("inGoogle"))});
    }
    serialize() {
      return new (IdentityMapOfStringL$ObjectL()).from(["runtime", this.runtime.serialize(), "os", this.os.identifier, "inGoogle", this.inGoogle]);
    }
  };
  (suite_platform.SuitePlatform.new = function(runtime, opts) {
    let t0;
    let os = opts && 'os' in opts ? opts.os : null;
    let inGoogle = opts && 'inGoogle' in opts ? opts.inGoogle : false;
    this[runtime$] = runtime;
    this[inGoogle$] = inGoogle;
    this[os$] = (t0 = os, t0 == null ? operating_system.OperatingSystem.none : t0);
    if (dart.test(this.runtime.isBrowser) && !dart.equals(this.os, operating_system.OperatingSystem.none)) {
      dart.throw(new core.ArgumentError.new("No OS should be passed for runtime \"" + dart.str(this.runtime) + "\"."));
    }
  }).prototype = suite_platform.SuitePlatform.prototype;
  dart.addTypeTests(suite_platform.SuitePlatform);
  dart.addTypeCaches(suite_platform.SuitePlatform);
  dart.setMethodSignature(suite_platform.SuitePlatform, () => ({
    __proto__: dart.getMethods(suite_platform.SuitePlatform.__proto__),
    serialize: dart.fnType(core.Object, [])
  }));
  dart.setLibraryUri(suite_platform.SuitePlatform, L0);
  dart.setFieldSignature(suite_platform.SuitePlatform, () => ({
    __proto__: dart.getFields(suite_platform.SuitePlatform.__proto__),
    runtime: dart.finalFieldType(runtime.Runtime),
    os: dart.finalFieldType(operating_system.OperatingSystem),
    inGoogle: dart.finalFieldType(core.bool)
  }));
  var name$ = dart.privateName(runtime, "Runtime.name");
  var identifier$ = dart.privateName(runtime, "Runtime.identifier");
  var parent$ = dart.privateName(runtime, "Runtime.parent");
  var isDartVM$ = dart.privateName(runtime, "Runtime.isDartVM");
  var isBrowser$ = dart.privateName(runtime, "Runtime.isBrowser");
  var isJS$ = dart.privateName(runtime, "Runtime.isJS");
  var isBlink$ = dart.privateName(runtime, "Runtime.isBlink");
  var isHeadless$ = dart.privateName(runtime, "Runtime.isHeadless");
  var C0;
  var C1;
  var C2;
  var C3;
  var C4;
  var C5;
  var C6;
  var C7;
  runtime.Runtime = class Runtime extends core.Object {
    get name() {
      return this[name$];
    }
    set name(value) {
      super.name = value;
    }
    get identifier() {
      return this[identifier$];
    }
    set identifier(value) {
      super.identifier = value;
    }
    get parent() {
      return this[parent$];
    }
    set parent(value) {
      super.parent = value;
    }
    get isDartVM() {
      return this[isDartVM$];
    }
    set isDartVM(value) {
      super.isDartVM = value;
    }
    get isBrowser() {
      return this[isBrowser$];
    }
    set isBrowser(value) {
      super.isBrowser = value;
    }
    get isJS() {
      return this[isJS$];
    }
    set isJS(value) {
      super.isJS = value;
    }
    get isBlink() {
      return this[isBlink$];
    }
    set isBlink(value) {
      super.isBlink = value;
    }
    get isHeadless() {
      return this[isHeadless$];
    }
    set isHeadless(value) {
      super.isHeadless = value;
    }
    get isChild() {
      return this.parent != null;
    }
    get root() {
      let t0;
      t0 = this.parent;
      return t0 == null ? this : t0;
    }
    static deserialize(serialized) {
      if (typeof serialized == 'string') {
        return runtime.Runtime.builtIn[$firstWhere](dart.fn(platform => platform.identifier == serialized, RuntimeLToboolL()));
      }
      let map = core.Map.as(serialized);
      let parent = map[$_get]("parent");
      if (parent != null) {
        return new runtime.Runtime._child(core.String.as(map[$_get]("name")), core.String.as(map[$_get]("identifier")), runtime.Runtime.deserialize(parent));
      }
      return new runtime.Runtime.new(core.String.as(map[$_get]("name")), core.String.as(map[$_get]("identifier")), {isDartVM: core.bool.as(map[$_get]("isDartVM")), isBrowser: core.bool.as(map[$_get]("isBrowser")), isJS: core.bool.as(map[$_get]("isJS")), isBlink: core.bool.as(map[$_get]("isBlink")), isHeadless: core.bool.as(map[$_get]("isHeadless"))});
    }
    serialize() {
      if (dart.test(runtime.Runtime.builtIn[$contains](this))) return this.identifier;
      if (this.parent != null) {
        return new (IdentityMapOfStringL$ObjectL()).from(["name", this.name, "identifier", this.identifier, "parent", this.parent.serialize()]);
      }
      return new (IdentityMapOfStringL$ObjectL()).from(["name", this.name, "identifier", this.identifier, "isDartVM", this.isDartVM, "isBrowser", this.isBrowser, "isJS", this.isJS, "isBlink", this.isBlink, "isHeadless", this.isHeadless]);
    }
    extend(name, identifier) {
      if (this.parent == null) return new runtime.Runtime._child(name, identifier, this);
      dart.throw(new core.StateError.new("A child platform may not be extended."));
    }
    toString() {
      return this.name;
    }
  };
  (runtime.Runtime.new = function(name, identifier, opts) {
    let isDartVM = opts && 'isDartVM' in opts ? opts.isDartVM : false;
    let isBrowser = opts && 'isBrowser' in opts ? opts.isBrowser : false;
    let isJS = opts && 'isJS' in opts ? opts.isJS : false;
    let isBlink = opts && 'isBlink' in opts ? opts.isBlink : false;
    let isHeadless = opts && 'isHeadless' in opts ? opts.isHeadless : false;
    this[name$] = name;
    this[identifier$] = identifier;
    this[isDartVM$] = isDartVM;
    this[isBrowser$] = isBrowser;
    this[isJS$] = isJS;
    this[isBlink$] = isBlink;
    this[isHeadless$] = isHeadless;
    this[parent$] = null;
    ;
  }).prototype = runtime.Runtime.prototype;
  (runtime.Runtime._child = function(name, identifier, parent) {
    this[name$] = name;
    this[identifier$] = identifier;
    this[parent$] = parent;
    this[isDartVM$] = parent.isDartVM;
    this[isBrowser$] = parent.isBrowser;
    this[isJS$] = parent.isJS;
    this[isBlink$] = parent.isBlink;
    this[isHeadless$] = parent.isHeadless;
    ;
  }).prototype = runtime.Runtime.prototype;
  dart.addTypeTests(runtime.Runtime);
  dart.addTypeCaches(runtime.Runtime);
  dart.setMethodSignature(runtime.Runtime, () => ({
    __proto__: dart.getMethods(runtime.Runtime.__proto__),
    serialize: dart.fnType(core.Object, []),
    extend: dart.fnType(runtime.Runtime, [core.String, core.String])
  }));
  dart.setGetterSignature(runtime.Runtime, () => ({
    __proto__: dart.getGetters(runtime.Runtime.__proto__),
    isChild: core.bool,
    root: runtime.Runtime
  }));
  dart.setLibraryUri(runtime.Runtime, L1);
  dart.setFieldSignature(runtime.Runtime, () => ({
    __proto__: dart.getFields(runtime.Runtime.__proto__),
    name: dart.finalFieldType(core.String),
    identifier: dart.finalFieldType(core.String),
    parent: dart.finalFieldType(runtime.Runtime),
    isDartVM: dart.finalFieldType(core.bool),
    isBrowser: dart.finalFieldType(core.bool),
    isJS: dart.finalFieldType(core.bool),
    isBlink: dart.finalFieldType(core.bool),
    isHeadless: dart.finalFieldType(core.bool)
  }));
  dart.defineExtensionMethods(runtime.Runtime, ['toString']);
  dart.defineLazy(runtime.Runtime, {
    /*runtime.Runtime.vm*/get vm() {
      return C0 || CT.C0;
    },
    /*runtime.Runtime.chrome*/get chrome() {
      return C1 || CT.C1;
    },
    /*runtime.Runtime.phantomJS*/get phantomJS() {
      return C2 || CT.C2;
    },
    /*runtime.Runtime.firefox*/get firefox() {
      return C3 || CT.C3;
    },
    /*runtime.Runtime.safari*/get safari() {
      return C4 || CT.C4;
    },
    /*runtime.Runtime.internetExplorer*/get internetExplorer() {
      return C5 || CT.C5;
    },
    /*runtime.Runtime.nodeJS*/get nodeJS() {
      return C6 || CT.C6;
    },
    /*runtime.Runtime.builtIn*/get builtIn() {
      return C7 || CT.C7;
    }
  }, true);
  var name$0 = dart.privateName(operating_system, "OperatingSystem.name");
  var identifier$0 = dart.privateName(operating_system, "OperatingSystem.identifier");
  var C8;
  var C9;
  var C10;
  var C11;
  var C12;
  var C13;
  var C14;
  operating_system.OperatingSystem = class OperatingSystem extends core.Object {
    get name() {
      return this[name$0];
    }
    set name(value) {
      super.name = value;
    }
    get identifier() {
      return this[identifier$0];
    }
    set identifier(value) {
      super.identifier = value;
    }
    static find(identifier) {
      return operating_system.OperatingSystem.all[$firstWhere](dart.fn(platform => platform.identifier == identifier, OperatingSystemLToboolL()), {orElse: dart.fn(() => null, VoidToNullN())});
    }
    static findByIoName(name) {
      switch (name) {
        case "windows":
        {
          return operating_system.OperatingSystem.windows;
        }
        case "macos":
        {
          return operating_system.OperatingSystem.macOS;
        }
        case "linux":
        {
          return operating_system.OperatingSystem.linux;
        }
        case "android":
        {
          return operating_system.OperatingSystem.android;
        }
        case "ios":
        {
          return operating_system.OperatingSystem.iOS;
        }
        default:
        {
          return operating_system.OperatingSystem.none;
        }
      }
    }
    get isPosix() {
      return !this[$_equals](operating_system.OperatingSystem.windows) && !this[$_equals](operating_system.OperatingSystem.none);
    }
    toString() {
      return this.name;
    }
  };
  (operating_system.OperatingSystem.__ = function(name, identifier) {
    this[name$0] = name;
    this[identifier$0] = identifier;
    ;
  }).prototype = operating_system.OperatingSystem.prototype;
  dart.addTypeTests(operating_system.OperatingSystem);
  dart.addTypeCaches(operating_system.OperatingSystem);
  dart.setGetterSignature(operating_system.OperatingSystem, () => ({
    __proto__: dart.getGetters(operating_system.OperatingSystem.__proto__),
    isPosix: core.bool
  }));
  dart.setLibraryUri(operating_system.OperatingSystem, L2);
  dart.setFieldSignature(operating_system.OperatingSystem, () => ({
    __proto__: dart.getFields(operating_system.OperatingSystem.__proto__),
    name: dart.finalFieldType(core.String),
    identifier: dart.finalFieldType(core.String)
  }));
  dart.defineExtensionMethods(operating_system.OperatingSystem, ['toString']);
  dart.defineLazy(operating_system.OperatingSystem, {
    /*operating_system.OperatingSystem.windows*/get windows() {
      return C8 || CT.C8;
    },
    /*operating_system.OperatingSystem.macOS*/get macOS() {
      return C9 || CT.C9;
    },
    /*operating_system.OperatingSystem.linux*/get linux() {
      return C10 || CT.C10;
    },
    /*operating_system.OperatingSystem.android*/get android() {
      return C11 || CT.C11;
    },
    /*operating_system.OperatingSystem.iOS*/get iOS() {
      return C12 || CT.C12;
    },
    /*operating_system.OperatingSystem.none*/get none() {
      return C13 || CT.C13;
    },
    /*operating_system.OperatingSystem.all*/get all() {
      return C14 || CT.C14;
    }
  }, true);
  var reason$ = dart.privateName(skip$, "Skip.reason");
  skip$.Skip = class Skip extends core.Object {
    get reason() {
      return this[reason$];
    }
    set reason(value) {
      super.reason = value;
    }
  };
  (skip$.Skip.new = function(reason = null) {
    this[reason$] = reason;
    ;
  }).prototype = skip$.Skip.prototype;
  dart.addTypeTests(skip$.Skip);
  dart.addTypeCaches(skip$.Skip);
  dart.setLibraryUri(skip$.Skip, L3);
  dart.setFieldSignature(skip$.Skip, () => ({
    __proto__: dart.getFields(skip$.Skip.__proto__),
    reason: dart.finalFieldType(core.String)
  }));
  var Timeout_scaleFactor = dart.privateName(timeout$, "Timeout.scaleFactor");
  var Timeout_duration = dart.privateName(timeout$, "Timeout.duration");
  var C15;
  var _skip = dart.privateName(metadata$, "_skip");
  var _verboseTrace = dart.privateName(metadata$, "_verboseTrace");
  var _chainStackTraces = dart.privateName(metadata$, "_chainStackTraces");
  var _retry = dart.privateName(metadata$, "_retry");
  var C16;
  var C17;
  var _validateTags = dart.privateName(metadata$, "_validateTags");
  var _serializeTimeout = dart.privateName(metadata$, "_serializeTimeout");
  var testOn$ = dart.privateName(metadata$, "Metadata.testOn");
  var timeout$0 = dart.privateName(metadata$, "Metadata.timeout");
  var skipReason$ = dart.privateName(metadata$, "Metadata.skipReason");
  var tags$ = dart.privateName(metadata$, "Metadata.tags");
  var onPlatform$ = dart.privateName(metadata$, "Metadata.onPlatform");
  var forTag$ = dart.privateName(metadata$, "Metadata.forTag");
  metadata$.Metadata = class Metadata extends core.Object {
    get testOn() {
      return this[testOn$];
    }
    set testOn(value) {
      super.testOn = value;
    }
    get timeout() {
      return this[timeout$0];
    }
    set timeout(value) {
      super.timeout = value;
    }
    get skipReason() {
      return this[skipReason$];
    }
    set skipReason(value) {
      super.skipReason = value;
    }
    get tags() {
      return this[tags$];
    }
    set tags(value) {
      super.tags = value;
    }
    get onPlatform() {
      return this[onPlatform$];
    }
    set onPlatform(value) {
      super.onPlatform = value;
    }
    get forTag() {
      return this[forTag$];
    }
    set forTag(value) {
      super.forTag = value;
    }
    get skip() {
      let t0;
      t0 = this[_skip];
      return t0 == null ? false : t0;
    }
    get verboseTrace() {
      let t0;
      t0 = this[_verboseTrace];
      return t0 == null ? false : t0;
    }
    get chainStackTraces() {
      let t0;
      t0 = this[_chainStackTraces];
      return t0 == null ? true : t0;
    }
    get retry() {
      let t0;
      t0 = this[_retry];
      return t0 == null ? 0 : t0;
    }
    static _parseOnPlatform(onPlatform) {
      if (onPlatform == null) return new (LinkedMapOfPlatformSelectorL$MetadataL()).new();
      let result = new (LinkedMapOfPlatformSelectorL$MetadataL()).new();
      onPlatform[$forEach](dart.fn((platform, metadata) => {
        if (timeout$.Timeout.is(metadata) || skip$.Skip.is(metadata)) {
          metadata = [metadata];
        } else if (!core.List.is(metadata)) {
          dart.throw(new core.ArgumentError.new("Metadata for platform \"" + dart.str(platform) + "\" must be a " + "Timeout, Skip, or List of those; was \"" + dart.str(metadata) + "\"."));
        }
        let selector = new platform_selector.PlatformSelector.parse(platform);
        let timeout = null;
        let skip = null;
        for (let metadatum of core.Iterable.as(metadata)) {
          if (timeout$.Timeout.is(metadatum)) {
            if (timeout != null) {
              dart.throw(new core.ArgumentError.new("Only a single Timeout may be declared for " + "\"" + dart.str(platform) + "\"."));
            }
            timeout = metadatum;
          } else if (skip$.Skip.is(metadatum)) {
            if (skip != null) {
              dart.throw(new core.ArgumentError.new("Only a single Skip may be declared for " + "\"" + dart.str(platform) + "\"."));
            }
            skip = metadatum.reason == null ? true : metadatum.reason;
          } else {
            dart.throw(new core.ArgumentError.new("Metadata for platform \"" + dart.str(platform) + "\" must be a " + "Timeout, Skip, or List of those; was \"" + dart.str(metadata) + "\"."));
          }
        }
        result[$_set](selector, new metadata$.Metadata.parse({timeout: timeout, skip: skip}));
      }, StringLAnddynamicToNullN()));
      return result;
    }
    static _parseTags(tags) {
      if (tags == null) return new (_IdentityHashSetOfStringL()).new();
      if (typeof tags == 'string') return LinkedHashSetOfStringL().from([tags]);
      if (!core.Iterable.is(tags)) {
        dart.throw(new core.ArgumentError.value(tags, "tags", "must be either a String or an Iterable."));
      }
      if (dart.test(core.Iterable.as(tags)[$any](dart.fn(tag => !(typeof tag == 'string'), dynamicToboolL())))) {
        dart.throw(new core.ArgumentError.value(tags, "tags", "must contain only Strings."));
      }
      return LinkedHashSetOfStringL().from(core.Iterable.as(tags));
    }
    static new(opts) {
      let testOn = opts && 'testOn' in opts ? opts.testOn : null;
      let timeout = opts && 'timeout' in opts ? opts.timeout : null;
      let skip = opts && 'skip' in opts ? opts.skip : null;
      let verboseTrace = opts && 'verboseTrace' in opts ? opts.verboseTrace : null;
      let chainStackTraces = opts && 'chainStackTraces' in opts ? opts.chainStackTraces : null;
      let retry = opts && 'retry' in opts ? opts.retry : null;
      let skipReason = opts && 'skipReason' in opts ? opts.skipReason : null;
      let tags = opts && 'tags' in opts ? opts.tags : null;
      let onPlatform = opts && 'onPlatform' in opts ? opts.onPlatform : null;
      let forTag = opts && 'forTag' in opts ? opts.forTag : null;
      function _unresolved() {
        return new metadata$.Metadata.__({testOn: testOn, timeout: timeout, skip: skip, verboseTrace: verboseTrace, chainStackTraces: chainStackTraces, retry: retry, skipReason: skipReason, tags: tags, onPlatform: onPlatform, forTag: forTag});
      }
      dart.fn(_unresolved, VoidToMetadataL());
      if (forTag == null || tags == null) return _unresolved();
      tags = LinkedHashSetOfStringL().from(tags);
      forTag = LinkedHashMapOfBooleanSelectorL$MetadataL().from(forTag);
      let empty = new metadata$.Metadata.__();
      let merged = forTag[$keys][$toList]()[$fold](metadata$.Metadata, empty, dart.fn((merged, selector) => {
        if (!dart.test(selector.evaluate(tags))) return merged;
        return merged.merge(forTag[$remove](selector));
      }, MetadataLAndBooleanSelectorLToMetadataL()));
      if (dart.equals(merged, empty)) return _unresolved();
      return merged.merge(_unresolved());
    }
    static _deserializeTimeout(serialized) {
      if (dart.equals(serialized, "none")) return timeout$.Timeout.none;
      let scaleFactor = dart.dsend(serialized, '_get', ["scaleFactor"]);
      if (scaleFactor != null) return new timeout$.Timeout.factor(core.num.as(scaleFactor));
      return new timeout$.Timeout.new(new core.Duration.new({microseconds: core.int.as(dart.dsend(serialized, '_get', ["duration"]))}));
    }
    [_validateTags]() {
      let invalidTags = this.tags[$where](dart.fn(tag => !tag[$contains](utils.anchoredHyphenatedIdentifier), StringLToboolL()))[$map](core.String, dart.fn(tag => "\"" + dart.str(tag) + "\"", StringLToStringL()))[$toList]();
      if (dart.test(invalidTags[$isEmpty])) return;
      dart.throw(new core.ArgumentError.new("Invalid " + dart.str(utils.pluralize("tag", invalidTags[$length])) + " " + dart.str(utils.toSentence(invalidTags)) + ". Tags must be (optionally hyphenated) " + "Dart identifiers."));
    }
    validatePlatformSelectors(validVariables) {
      this.testOn.validate(validVariables);
      this.onPlatform[$forEach](dart.fn((selector, metadata) => {
        selector.validate(validVariables);
        metadata.validatePlatformSelectors(validVariables);
      }, PlatformSelectorLAndMetadataLToNullN()));
    }
    merge(other) {
      let t0, t0$, t0$0, t0$1, t0$2;
      return metadata$.Metadata.new({testOn: this.testOn.intersection(other.testOn), timeout: this.timeout.merge(other.timeout), skip: (t0 = other[_skip], t0 == null ? this[_skip] : t0), skipReason: (t0$ = other.skipReason, t0$ == null ? this.skipReason : t0$), verboseTrace: (t0$0 = other[_verboseTrace], t0$0 == null ? this[_verboseTrace] : t0$0), chainStackTraces: (t0$1 = other[_chainStackTraces], t0$1 == null ? this[_chainStackTraces] : t0$1), retry: (t0$2 = other[_retry], t0$2 == null ? this[_retry] : t0$2), tags: this.tags.union(other.tags), onPlatform: functions.mergeMaps(platform_selector.PlatformSelector, metadata$.Metadata, this.onPlatform, other.onPlatform, {value: dart.fn((metadata1, metadata2) => metadata1.merge(metadata2), MetadataLAndMetadataLToMetadataL())}), forTag: functions.mergeMaps(boolean_selector.BooleanSelector, metadata$.Metadata, this.forTag, other.forTag, {value: dart.fn((metadata1, metadata2) => metadata1.merge(metadata2), MetadataLAndMetadataLToMetadataL())})});
    }
    change(opts) {
      let testOn = opts && 'testOn' in opts ? opts.testOn : null;
      let timeout = opts && 'timeout' in opts ? opts.timeout : null;
      let skip = opts && 'skip' in opts ? opts.skip : null;
      let verboseTrace = opts && 'verboseTrace' in opts ? opts.verboseTrace : null;
      let chainStackTraces = opts && 'chainStackTraces' in opts ? opts.chainStackTraces : null;
      let retry = opts && 'retry' in opts ? opts.retry : null;
      let skipReason = opts && 'skipReason' in opts ? opts.skipReason : null;
      let onPlatform = opts && 'onPlatform' in opts ? opts.onPlatform : null;
      let tags = opts && 'tags' in opts ? opts.tags : null;
      let forTag = opts && 'forTag' in opts ? opts.forTag : null;
      testOn == null ? testOn = this.testOn : null;
      timeout == null ? timeout = this.timeout : null;
      skip == null ? skip = this[_skip] : null;
      verboseTrace == null ? verboseTrace = this[_verboseTrace] : null;
      chainStackTraces == null ? chainStackTraces = this[_chainStackTraces] : null;
      retry == null ? retry = this[_retry] : null;
      skipReason == null ? skipReason = this.skipReason : null;
      onPlatform == null ? onPlatform = this.onPlatform : null;
      tags == null ? tags = this.tags : null;
      forTag == null ? forTag = this.forTag : null;
      return metadata$.Metadata.new({testOn: testOn, timeout: timeout, skip: skip, verboseTrace: verboseTrace, chainStackTraces: chainStackTraces, skipReason: skipReason, onPlatform: onPlatform, tags: tags, forTag: forTag, retry: retry});
    }
    forPlatform(platform) {
      if (dart.test(this.onPlatform[$isEmpty])) return this;
      let metadata = this;
      this.onPlatform[$forEach](dart.fn((platformSelector, platformMetadata) => {
        if (!dart.test(platformSelector.evaluate(platform))) return;
        metadata = metadata.merge(platformMetadata);
      }, PlatformSelectorLAndMetadataLToNullN()));
      return metadata.change({onPlatform: new (LinkedMapOfPlatformSelectorL$MetadataL()).new()});
    }
    serialize() {
      let serializedOnPlatform = [];
      this.onPlatform[$forEach](dart.fn((key, value) => {
        serializedOnPlatform[$add]([dart.toString(key), value.serialize()]);
      }, PlatformSelectorLAndMetadataLToNullN()));
      return new (IdentityMapOfStringL$dynamic()).from(["testOn", dart.equals(this.testOn, platform_selector.PlatformSelector.all) ? null : dart.toString(this.testOn), "timeout", this[_serializeTimeout](this.timeout), "skip", this[_skip], "skipReason", this.skipReason, "verboseTrace", this[_verboseTrace], "chainStackTraces", this[_chainStackTraces], "retry", this[_retry], "tags", this.tags[$toList](), "onPlatform", serializedOnPlatform, "forTag", this.forTag[$map](core.String, dart.dynamic, dart.fn((selector, metadata) => new (MapEntryOfStringL$dynamic()).__(dart.toString(selector), metadata.serialize()), BooleanSelectorLAndMetadataLToMapEntryLOfStringL$dynamic()))]);
    }
    [_serializeTimeout](timeout) {
      if (dart.equals(timeout, timeout$.Timeout.none)) return "none";
      return new (IdentityMapOfStringL$numL()).from(["duration", timeout.duration == null ? null : timeout.duration.inMicroseconds, "scaleFactor", timeout.scaleFactor]);
    }
  };
  (metadata$.Metadata.__ = function(opts) {
    let testOn = opts && 'testOn' in opts ? opts.testOn : null;
    let timeout = opts && 'timeout' in opts ? opts.timeout : null;
    let skip = opts && 'skip' in opts ? opts.skip : null;
    let skipReason = opts && 'skipReason' in opts ? opts.skipReason : null;
    let verboseTrace = opts && 'verboseTrace' in opts ? opts.verboseTrace : null;
    let chainStackTraces = opts && 'chainStackTraces' in opts ? opts.chainStackTraces : null;
    let retry = opts && 'retry' in opts ? opts.retry : null;
    let tags = opts && 'tags' in opts ? opts.tags : null;
    let onPlatform = opts && 'onPlatform' in opts ? opts.onPlatform : null;
    let forTag = opts && 'forTag' in opts ? opts.forTag : null;
    this[skipReason$] = skipReason;
    this[testOn$] = testOn == null ? platform_selector.PlatformSelector.all : testOn;
    this[timeout$0] = timeout == null ? C15 || CT.C15 : timeout;
    this[_skip] = skip;
    this[_verboseTrace] = verboseTrace;
    this[_chainStackTraces] = chainStackTraces;
    this[_retry] = retry;
    this[tags$] = new (UnmodifiableSetViewOfStringL()).new(tags == null ? new (_IdentityHashSetOfStringL()).new() : tags[$toSet]());
    this[onPlatform$] = onPlatform == null ? C16 || CT.C16 : new (UnmodifiableMapViewOfPlatformSelectorL$MetadataL()).new(onPlatform);
    this[forTag$] = forTag == null ? C17 || CT.C17 : new (UnmodifiableMapViewOfBooleanSelectorL$MetadataL()).new(forTag);
    if (retry != null) core.RangeError.checkNotNegative(retry, "retry");
    this[_validateTags]();
  }).prototype = metadata$.Metadata.prototype;
  (metadata$.Metadata.parse = function(opts) {
    let testOn = opts && 'testOn' in opts ? opts.testOn : null;
    let timeout = opts && 'timeout' in opts ? opts.timeout : null;
    let skip = opts && 'skip' in opts ? opts.skip : null;
    let verboseTrace = opts && 'verboseTrace' in opts ? opts.verboseTrace : null;
    let chainStackTraces = opts && 'chainStackTraces' in opts ? opts.chainStackTraces : null;
    let retry = opts && 'retry' in opts ? opts.retry : null;
    let onPlatform = opts && 'onPlatform' in opts ? opts.onPlatform : null;
    let tags = opts && 'tags' in opts ? opts.tags : null;
    this[testOn$] = testOn == null ? platform_selector.PlatformSelector.all : new platform_selector.PlatformSelector.parse(testOn);
    this[timeout$0] = timeout == null ? C15 || CT.C15 : timeout;
    this[_skip] = skip == null ? null : !dart.equals(skip, false);
    this[_verboseTrace] = verboseTrace;
    this[_chainStackTraces] = chainStackTraces;
    this[_retry] = retry;
    this[skipReason$] = typeof skip == 'string' ? skip : null;
    this[onPlatform$] = metadata$.Metadata._parseOnPlatform(onPlatform);
    this[tags$] = metadata$.Metadata._parseTags(tags);
    this[forTag$] = C17 || CT.C17;
    if (skip != null && !(typeof skip == 'string') && !(typeof skip == 'boolean')) {
      dart.throw(new core.ArgumentError.new("\"skip\" must be a String or a bool, was \"" + dart.str(skip) + "\"."));
    }
    if (retry != null) core.RangeError.checkNotNegative(retry, "retry");
    this[_validateTags]();
  }).prototype = metadata$.Metadata.prototype;
  (metadata$.Metadata.deserialize = function(serialized) {
    this[testOn$] = dart.dsend(serialized, '_get', ["testOn"]) == null ? platform_selector.PlatformSelector.all : new platform_selector.PlatformSelector.parse(core.String.as(dart.dsend(serialized, '_get', ["testOn"])));
    this[timeout$0] = metadata$.Metadata._deserializeTimeout(dart.dsend(serialized, '_get', ["timeout"]));
    this[_skip] = core.bool.as(dart.dsend(serialized, '_get', ["skip"]));
    this[skipReason$] = core.String.as(dart.dsend(serialized, '_get', ["skipReason"]));
    this[_verboseTrace] = core.bool.as(dart.dsend(serialized, '_get', ["verboseTrace"]));
    this[_chainStackTraces] = core.bool.as(dart.dsend(serialized, '_get', ["chainStackTraces"]));
    this[_retry] = core.int.as(dart.dsend(serialized, '_get', ["retry"]));
    this[tags$] = LinkedHashSetOfStringL().from(core.Iterable.as(dart.dsend(serialized, '_get', ["tags"])));
    this[onPlatform$] = LinkedHashMapOfPlatformSelectorL$MetadataL().fromIterable(core.Iterable.as(dart.dsend(serialized, '_get', ["onPlatform"])), {key: dart.fn(pair => new platform_selector.PlatformSelector.parse(core.String.as(dart.dload(pair, 'first'))), dynamicToPlatformSelectorL()), value: dart.fn(pair => new metadata$.Metadata.deserialize(dart.dload(pair, 'last')), dynamicToMetadataL())});
    this[forTag$] = core.Map.as(dart.dsend(serialized, '_get', ["forTag"]))[$map](boolean_selector.BooleanSelector, metadata$.Metadata, dart.fn((key, nested) => new (MapEntryOfBooleanSelectorL$MetadataL()).__(new impl.BooleanSelectorImpl.parse(core.String.as(key)), new metadata$.Metadata.deserialize(nested)), dynamicAnddynamicToMapEntryLOfBooleanSelectorL$MetadataL()));
    ;
  }).prototype = metadata$.Metadata.prototype;
  dart.addTypeTests(metadata$.Metadata);
  dart.addTypeCaches(metadata$.Metadata);
  dart.setMethodSignature(metadata$.Metadata, () => ({
    __proto__: dart.getMethods(metadata$.Metadata.__proto__),
    [_validateTags]: dart.fnType(dart.void, []),
    validatePlatformSelectors: dart.fnType(dart.void, [core.Set$(core.String)]),
    merge: dart.fnType(metadata$.Metadata, [metadata$.Metadata]),
    change: dart.fnType(metadata$.Metadata, [], {chainStackTraces: core.bool, forTag: core.Map$(boolean_selector.BooleanSelector, metadata$.Metadata), onPlatform: core.Map$(platform_selector.PlatformSelector, metadata$.Metadata), retry: core.int, skip: core.bool, skipReason: core.String, tags: core.Set$(core.String), testOn: platform_selector.PlatformSelector, timeout: timeout$.Timeout, verboseTrace: core.bool}, {}),
    forPlatform: dart.fnType(metadata$.Metadata, [suite_platform.SuitePlatform]),
    serialize: dart.fnType(dart.dynamic, []),
    [_serializeTimeout]: dart.fnType(dart.dynamic, [timeout$.Timeout])
  }));
  dart.setGetterSignature(metadata$.Metadata, () => ({
    __proto__: dart.getGetters(metadata$.Metadata.__proto__),
    skip: core.bool,
    verboseTrace: core.bool,
    chainStackTraces: core.bool,
    retry: core.int
  }));
  dart.setLibraryUri(metadata$.Metadata, L4);
  dart.setFieldSignature(metadata$.Metadata, () => ({
    __proto__: dart.getFields(metadata$.Metadata.__proto__),
    testOn: dart.finalFieldType(platform_selector.PlatformSelector),
    timeout: dart.finalFieldType(timeout$.Timeout),
    [_skip]: dart.finalFieldType(core.bool),
    skipReason: dart.finalFieldType(core.String),
    [_verboseTrace]: dart.finalFieldType(core.bool),
    [_chainStackTraces]: dart.finalFieldType(core.bool),
    tags: dart.finalFieldType(core.Set$(core.String)),
    [_retry]: dart.finalFieldType(core.int),
    onPlatform: dart.finalFieldType(core.Map$(platform_selector.PlatformSelector, metadata$.Metadata)),
    forTag: dart.finalFieldType(core.Map$(boolean_selector.BooleanSelector, metadata$.Metadata))
  }));
  dart.defineLazy(metadata$.Metadata, {
    /*metadata$.Metadata.empty*/get empty() {
      return new metadata$.Metadata.__();
    }
  }, true);
  var _inner = dart.privateName(platform_selector, "_inner");
  var _span = dart.privateName(platform_selector, "_span");
  var _inner$ = dart.privateName(platform_selector, "PlatformSelector._inner");
  var _span$ = dart.privateName(platform_selector, "PlatformSelector._span");
  var C20;
  var All_variables = dart.privateName(all, "All.variables");
  var C19;
  var C18;
  platform_selector.PlatformSelector = class PlatformSelector extends core.Object {
    get [_inner]() {
      return this[_inner$];
    }
    set [_inner](value) {
      super[_inner] = value;
    }
    get [_span]() {
      return this[_span$];
    }
    set [_span](value) {
      super[_span] = value;
    }
    static _wrapFormatException(T, body, span) {
      if (span == null) return body();
      try {
        return body();
      } catch (e) {
        let error = dart.getThrown(e);
        if (core.FormatException.is(error)) {
          dart.throw(new span_exception.SourceSpanFormatException.new(error.message, span));
        } else
          throw e;
      }
    }
    validate(validVariables) {
      if (this === platform_selector.PlatformSelector.all) return;
      platform_selector.PlatformSelector._wrapFormatException(dart.void, dart.fn(() => this[_inner].validate(dart.fn(name => dart.test(platform_selector._universalValidVariables.contains(name)) || dart.test(validVariables.contains(name)), StringLToboolL())), VoidTovoid()), this[_span]);
    }
    evaluate(platform) {
      return this[_inner].evaluate(dart.fn(variable => {
        let t1;
        if (variable == platform.runtime.identifier) return true;
        if (variable == (t1 = platform.runtime.parent, t1 == null ? null : t1.identifier)) return true;
        if (variable == platform.os.identifier) return true;
        switch (variable) {
          case "dart-vm":
          {
            return platform.runtime.isDartVM;
          }
          case "browser":
          {
            return platform.runtime.isBrowser;
          }
          case "js":
          {
            return platform.runtime.isJS;
          }
          case "blink":
          {
            return platform.runtime.isBlink;
          }
          case "posix":
          {
            return platform.os.isPosix;
          }
          case "google":
          {
            return platform.inGoogle;
          }
          default:
          {
            return false;
          }
        }
      }, StringLToboolL()));
    }
    intersection(other) {
      if (dart.equals(other, platform_selector.PlatformSelector.all)) return this;
      return new platform_selector.PlatformSelector.__(this[_inner].intersection(other[_inner]));
    }
    toString() {
      return dart.toString(this[_inner]);
    }
    _equals(other) {
      if (other == null) return false;
      return platform_selector.PlatformSelector.is(other) && dart.equals(this[_inner], other[_inner]);
    }
    get hashCode() {
      return dart.hashCode(this[_inner]);
    }
  };
  (platform_selector.PlatformSelector.parse = function(selector, span = null) {
    this[_inner$] = platform_selector.PlatformSelector._wrapFormatException(boolean_selector.BooleanSelector, dart.fn(() => new impl.BooleanSelectorImpl.parse(selector), VoidToBooleanSelectorL()), span);
    this[_span$] = span;
    ;
  }).prototype = platform_selector.PlatformSelector.prototype;
  (platform_selector.PlatformSelector.__ = function(_inner) {
    this[_inner$] = _inner;
    this[_span$] = null;
    ;
  }).prototype = platform_selector.PlatformSelector.prototype;
  dart.addTypeTests(platform_selector.PlatformSelector);
  dart.addTypeCaches(platform_selector.PlatformSelector);
  dart.setMethodSignature(platform_selector.PlatformSelector, () => ({
    __proto__: dart.getMethods(platform_selector.PlatformSelector.__proto__),
    validate: dart.fnType(dart.void, [core.Set$(core.String)]),
    evaluate: dart.fnType(core.bool, [suite_platform.SuitePlatform]),
    intersection: dart.fnType(platform_selector.PlatformSelector, [platform_selector.PlatformSelector])
  }));
  dart.setLibraryUri(platform_selector.PlatformSelector, L5);
  dart.setFieldSignature(platform_selector.PlatformSelector, () => ({
    __proto__: dart.getFields(platform_selector.PlatformSelector.__proto__),
    [_inner]: dart.finalFieldType(boolean_selector.BooleanSelector),
    [_span]: dart.finalFieldType(span.SourceSpan)
  }));
  dart.defineExtensionMethods(platform_selector.PlatformSelector, ['toString', '_equals']);
  dart.defineExtensionAccessors(platform_selector.PlatformSelector, ['hashCode']);
  dart.defineLazy(platform_selector.PlatformSelector, {
    /*platform_selector.PlatformSelector.all*/get all() {
      return C18 || CT.C18;
    }
  }, true);
  dart.defineLazy(platform_selector, {
    /*platform_selector._universalValidVariables*/get _universalValidVariables() {
      let t1;
      return t1 = LinkedHashSetOfStringL().from(["posix", "dart-vm", "browser", "js", "blink", "google"]), t1.addAll(runtime.Runtime.builtIn[$map](core.String, dart.fn(runtime => runtime.identifier, RuntimeLToStringL()))), t1.addAll(operating_system.OperatingSystem.all[$map](core.String, dart.fn(os => os.identifier, OperatingSystemLToStringL()))), t1;
    }
  }, true);
  const _is_Pair_default = Symbol('_is_Pair_default');
  var first$ = dart.privateName(utils, "Pair.first");
  var last$ = dart.privateName(utils, "Pair.last");
  utils.Pair$ = dart.generic((E, F) => {
    class Pair extends core.Object {
      get first() {
        return this[first$];
      }
      set first(value) {
        this[first$] = E.as(value);
      }
      get last() {
        return this[last$];
      }
      set last(value) {
        this[last$] = F.as(value);
      }
      toString() {
        return "(" + dart.str(this.first) + ", " + dart.str(this.last) + ")";
      }
      _equals(other) {
        if (other == null) return false;
        if (!utils.Pair.is(other)) return false;
        return dart.equals(dart.dload(other, 'first'), this.first) && dart.equals(dart.dload(other, 'last'), this.last);
      }
      get hashCode() {
        return (dart.notNull(dart.hashCode(this.first)) ^ dart.notNull(dart.hashCode(this.last))) >>> 0;
      }
    }
    (Pair.new = function(first, last) {
      this[first$] = first;
      this[last$] = last;
      ;
    }).prototype = Pair.prototype;
    dart.addTypeTests(Pair);
    Pair.prototype[_is_Pair_default] = true;
    dart.addTypeCaches(Pair);
    dart.setLibraryUri(Pair, L6);
    dart.setFieldSignature(Pair, () => ({
      __proto__: dart.getFields(Pair.__proto__),
      first: dart.fieldType(E),
      last: dart.fieldType(F)
    }));
    dart.defineExtensionMethods(Pair, ['toString', '_equals']);
    dart.defineExtensionAccessors(Pair, ['hashCode']);
    return Pair;
  });
  utils.Pair = utils.Pair$();
  dart.addTypeTests(utils.Pair, _is_Pair_default);
  var C21;
  utils.getErrorMessage = function getErrorMessage(error) {
    return dart.toString(error)[$replaceFirst](utils._exceptionPrefix, "");
  };
  utils.indent = function indent(string, opts) {
    let size = opts && 'size' in opts ? opts.size : null;
    let first = opts && 'first' in opts ? opts.first : null;
    size == null ? size = first == null ? 2 : first.length : null;
    return utils.prefixLines(string, " "[$times](size), {first: first});
  };
  utils.toSentence = function toSentence(iter, opts) {
    let t1;
    let conjunction = opts && 'conjunction' in opts ? opts.conjunction : null;
    if (iter[$length] === 1) return dart.toString(iter[$first]);
    let result = iter[$take](dart.notNull(iter[$length]) - 1)[$join](", ");
    if (dart.notNull(iter[$length]) > 2) result = dart.notNull(result) + ",";
    return dart.str(result) + " " + dart.str((t1 = conjunction, t1 == null ? "and" : t1)) + " " + dart.str(iter[$last]);
  };
  utils.pluralize = function pluralize(name, number, opts) {
    let plural = opts && 'plural' in opts ? opts.plural : null;
    if (number === 1) return name;
    if (plural != null) return plural;
    return dart.str(name) + "s";
  };
  utils.a = function a(noun) {
    return noun[$startsWith](utils._vowel) ? "an " + dart.str(noun) : "a " + dart.str(noun);
  };
  utils.withoutColors = function withoutColors(str) {
    return str[$replaceAll](utils._colorCode, "");
  };
  utils.mergeUnmodifiableMaps = function mergeUnmodifiableMaps(K, V, map1, map2, opts) {
    let value = opts && 'value' in opts ? opts.value : null;
    if (dart.test(map1[$isEmpty])) return map2;
    if (dart.test(map2[$isEmpty])) return map1;
    return functions.mergeMaps(K, V, map1, map2, {value: value});
  };
  utils.truncate = function truncate(text, maxLength) {
    if (text.length <= dart.notNull(maxLength)) return text;
    let words = text[$split](" ");
    if (dart.notNull(words[$length]) > 1) {
      let i = words[$length];
      let length = words[$first].length + 4;
      do {
        i = dart.notNull(i) - 1;
        length = length + (1 + words[$_get](i).length);
      } while (length <= dart.notNull(maxLength) && dart.notNull(i) > 0);
      if (length > dart.notNull(maxLength) || i === 0) i = dart.notNull(i) + 1;
      if (dart.notNull(i) < dart.notNull(words[$length]) - 4) {
        let buffer = new core.StringBuffer.new();
        buffer.write(words[$first]);
        buffer.write(" ...");
        for (; dart.notNull(i) < dart.notNull(words[$length]); i = dart.notNull(i) + 1) {
          buffer.write(" ");
          buffer.write(words[$_get](i));
        }
        return buffer.toString();
      }
    }
    let result = text[$substring](text.length - dart.notNull(maxLength) + 4);
    let firstSpace = result[$indexOf](" ");
    if (firstSpace > 0) {
      result = result[$substring](firstSpace);
    }
    return "..." + result;
  };
  utils.niceDuration = function niceDuration(duration) {
    let minutes = duration.inMinutes;
    let seconds = duration.inSeconds[$modulo](60);
    let decaseconds = (duration.inMilliseconds[$modulo](1000) / 100)[$truncate]();
    let buffer = new core.StringBuffer.new();
    if (minutes !== 0) buffer.write(dart.str(minutes) + " minutes");
    if (minutes === 0 || seconds !== 0) {
      if (minutes !== 0) buffer.write(", ");
      buffer.write(seconds);
      if (decaseconds !== 0) buffer.write("." + dart.str(decaseconds));
      buffer.write(" seconds");
    }
    return buffer.toString();
  };
  utils.inCompletionOrder = function inCompletionOrder(T, operations) {
    let operationSet = operations[$toSet]();
    let controller = async.StreamController$(T).new({sync: true, onCancel: dart.fn(() => async.Future.wait(dart.dynamic, operationSet[$map](async.Future, dart.fn(operation => operation.cancel(), dart.fnType(async.Future, [cancelable_operation.CancelableOperation$(T)])))), VoidToFutureLOfListL())});
    for (let operation of operationSet) {
      operation.value.then(dart.void, dart.fn(value => controller.add(value), dart.fnType(dart.void, [T]))).catchError(dart.bind(controller, 'addError')).whenComplete(dart.fn(() => {
        operationSet.remove(operation);
        if (dart.test(operationSet[$isEmpty])) controller.close();
      }, VoidToNullN()));
    }
    return controller.stream;
  };
  utils.invoke = function invoke(fn) {
    fn();
  };
  utils.randomBase64 = function randomBase64(bytes, opts) {
    let seed = opts && 'seed' in opts ? opts.seed : null;
    let random = math.Random.new(seed);
    let data = _native_typed_data.NativeUint8List.new(bytes);
    for (let i = 0; i < dart.notNull(bytes); i = i + 1) {
      data[$_set](i, random.nextInt(256));
    }
    return convert.base64Encode(data);
  };
  utils.ensureJsonEncodable = function ensureJsonEncodable(message) {
    if (message == null || typeof message == 'string' || typeof message == 'number' || typeof message == 'boolean') {
    } else if (core.List.is(message)) {
      for (let element of message) {
        utils.ensureJsonEncodable(element);
      }
    } else if (core.Map.is(message)) {
      message[$forEach](dart.fn((key, value) => {
        if (!(typeof key == 'string')) {
          dart.throw(new core.ArgumentError.new(dart.str(message) + " can't be JSON-encoded."));
        }
        utils.ensureJsonEncodable(value);
      }, dynamicAnddynamicToNullN()));
    } else {
      dart.throw(new core.ArgumentError.value(dart.str(message) + " can't be JSON-encoded."));
    }
  };
  utils.addBullet = function addBullet(text) {
    return utils.prefixLines(text, "  ", {first: dart.str(top_level.bullet) + " "});
  };
  utils.bullet = function bullet(strings) {
    return strings[$map](core.String, C21 || CT.C21)[$join]("\n");
  };
  utils.prefixLines = function prefixLines(text, prefix, opts) {
    let t1, t1$;
    let first = opts && 'first' in opts ? opts.first : null;
    let last = opts && 'last' in opts ? opts.last : null;
    let single = opts && 'single' in opts ? opts.single : null;
    first == null ? first = prefix : null;
    last == null ? last = prefix : null;
    single == null ? single = (t1$ = (t1 = first, t1 == null ? last : t1), t1$ == null ? prefix : t1$) : null;
    let lines = text[$split]("\n");
    if (lines[$length] === 1) return dart.str(single) + dart.str(text);
    let buffer = new core.StringBuffer.new(dart.str(first) + dart.str(lines[$first]) + "\n");
    for (let line of lines[$skip](1)[$take](dart.notNull(lines[$length]) - 2)) {
      buffer.writeln(dart.str(prefix) + dart.str(line));
    }
    buffer.write(dart.str(last) + dart.str(lines[$last]));
    return buffer.toString();
  };
  utils.prettyPrint = function prettyPrint(value) {
    return dart.toString(new description.StringDescription.new().addDescriptionOf(value));
  };
  var C22;
  dart.defineLazy(utils, {
    /*utils.lineSplitter*/get lineSplitter() {
      return new (_StreamSubscriptionTransformerOfListLOfintL$StringL()).new(dart.fn((stream, cancelOnError) => stream.transform(core.String, convert.utf8.decoder).transform(core.String, C22 || CT.C22).listen(null, {cancelOnError: cancelOnError}), StreamLOfListLOfintLAndboolLToStreamSubscriptionLOfStringL()));
    },
    /*utils.chunksToLines*/get chunksToLines() {
      return new (StreamChannelTransformerOfStringL$StringL()).new(C22 || CT.C22, StreamSinkTransformerOfStringL$StringL().fromHandlers({handleData: dart.fn((data, sink) => sink.add(dart.str(data) + "\n"), StringLAndEventSinkLOfStringLTovoid())}));
    },
    /*utils._exceptionPrefix*/get _exceptionPrefix() {
      return core.RegExp.new("^([A-Z][a-zA-Z]*)?(Exception|Error): ");
    },
    /*utils._vowel*/get _vowel() {
      return core.RegExp.new("[aeiou]");
    },
    /*utils._macOSDirectories*/get _macOSDirectories() {
      return LinkedHashSetOfStringL().from(["/Applications", "/Library", "/Network", "/System", "/Users"]);
    },
    /*utils.currentOSGuess*/get currentOSGuess() {
      return dart.fn(() => {
        if (dart.equals(path.style, style.Style.url)) return operating_system.OperatingSystem.none;
        if (dart.equals(path.style, style.Style.windows)) return operating_system.OperatingSystem.windows;
        if (dart.test(utils._macOSDirectories[$any](dart.bind(path.current, $startsWith)))) return operating_system.OperatingSystem.macOS;
        return operating_system.OperatingSystem.linux;
      }, VoidToOperatingSystemL())();
    },
    /*utils._hyphenatedIdentifier*/get _hyphenatedIdentifier() {
      return core.RegExp.new("[a-zA-Z_-][a-zA-Z0-9_-]*");
    },
    /*utils.anchoredHyphenatedIdentifier*/get anchoredHyphenatedIdentifier() {
      return core.RegExp.new("^" + dart.str(utils._hyphenatedIdentifier.pattern) + "$");
    },
    /*utils._colorCode*/get _colorCode() {
      return core.RegExp.new("\\[[0-9;]+m");
    }
  }, true);
  var C23;
  timeout$.Timeout = class Timeout extends core.Object {
    get duration() {
      return this[duration$];
    }
    set duration(value) {
      super.duration = value;
    }
    get scaleFactor() {
      return this[scaleFactor$];
    }
    set scaleFactor(value) {
      super.scaleFactor = value;
    }
    static parse(timeout) {
      let scanner = new string_scanner.StringScanner.new(timeout);
      if (dart.test(scanner.scan("none"))) {
        scanner.expectDone();
        return timeout$.Timeout.none;
      }
      scanner.expect(timeout$._untilUnit, {name: "number"});
      let number = core.double.parse(scanner.lastMatch._get(0));
      if (dart.test(scanner.scan("x")) || dart.test(scanner.scan("X"))) {
        scanner.expectDone();
        return new timeout$.Timeout.factor(number);
      }
      let microseconds = 0.0;
      while (true) {
        scanner.expect(timeout$._unit, {name: "unit"});
        microseconds = microseconds + dart.notNull(timeout$.Timeout._microsecondsFor(number, scanner.lastMatch._get(0)));
        scanner.scan(timeout$._whitespace);
        if (!dart.test(scanner.scan(timeout$._untilUnit))) break;
        number = core.double.parse(scanner.lastMatch._get(0));
      }
      scanner.expectDone();
      return new timeout$.Timeout.new(new core.Duration.new({microseconds: microseconds[$round]()}));
    }
    static _microsecondsFor(number, unit) {
      switch (unit) {
        case "d":
        {
          return dart.notNull(number) * 24 * 60 * 60 * 1000000;
        }
        case "h":
        {
          return dart.notNull(number) * 60 * 60 * 1000000;
        }
        case "m":
        {
          return dart.notNull(number) * 60 * 1000000;
        }
        case "s":
        {
          return dart.notNull(number) * 1000000;
        }
        case "ms":
        {
          return dart.notNull(number) * 1000;
        }
        case "us":
        {
          return number;
        }
        default:
        {
          dart.throw(new core.ArgumentError.new("Unknown unit " + dart.str(unit) + "."));
        }
      }
    }
    merge(other) {
      if (this._equals(timeout$.Timeout.none) || dart.equals(other, timeout$.Timeout.none)) return timeout$.Timeout.none;
      if (other.duration != null) return new timeout$.Timeout.new(other.duration);
      if (this.duration != null) return new timeout$.Timeout.new(this.duration['*'](other.scaleFactor));
      return new timeout$.Timeout.factor(dart.notNull(this.scaleFactor) * dart.notNull(other.scaleFactor));
    }
    apply(base) {
      if (this._equals(timeout$.Timeout.none)) return null;
      return this.duration == null ? base['*'](this.scaleFactor) : this.duration;
    }
    get hashCode() {
      return (dart.notNull(dart.hashCode(this.duration)) ^ 5 * dart.hashCode(this.scaleFactor)) >>> 0;
    }
    _equals(other) {
      if (other == null) return false;
      return timeout$.Timeout.is(other) && dart.equals(other.duration, this.duration) && other.scaleFactor == this.scaleFactor;
    }
    toString() {
      if (this.duration != null) return dart.toString(this.duration);
      if (this.scaleFactor != null) return dart.str(this.scaleFactor) + "x";
      return "none";
    }
  };
  (timeout$.Timeout.new = function(duration) {
    this[duration$] = duration;
    this[scaleFactor$] = null;
    ;
  }).prototype = timeout$.Timeout.prototype;
  (timeout$.Timeout.factor = function(scaleFactor) {
    this[scaleFactor$] = scaleFactor;
    this[duration$] = null;
    ;
  }).prototype = timeout$.Timeout.prototype;
  (timeout$.Timeout._none = function() {
    this[scaleFactor$] = null;
    this[duration$] = null;
    ;
  }).prototype = timeout$.Timeout.prototype;
  dart.addTypeTests(timeout$.Timeout);
  dart.addTypeCaches(timeout$.Timeout);
  const duration$ = Timeout_duration;
  const scaleFactor$ = Timeout_scaleFactor;
  dart.setMethodSignature(timeout$.Timeout, () => ({
    __proto__: dart.getMethods(timeout$.Timeout.__proto__),
    merge: dart.fnType(timeout$.Timeout, [timeout$.Timeout]),
    apply: dart.fnType(core.Duration, [core.Duration])
  }));
  dart.setLibraryUri(timeout$.Timeout, L7);
  dart.setFieldSignature(timeout$.Timeout, () => ({
    __proto__: dart.getFields(timeout$.Timeout.__proto__),
    duration: dart.finalFieldType(core.Duration),
    scaleFactor: dart.finalFieldType(core.num)
  }));
  dart.defineExtensionMethods(timeout$.Timeout, ['_equals', 'toString']);
  dart.defineExtensionAccessors(timeout$.Timeout, ['hashCode']);
  dart.defineLazy(timeout$.Timeout, {
    /*timeout$.Timeout.none*/get none() {
      return C23 || CT.C23;
    }
  }, true);
  dart.defineLazy(timeout$, {
    /*timeout$._untilUnit*/get _untilUnit() {
      return core.RegExp.new("[^a-df-z\\s]+", {caseSensitive: false});
    },
    /*timeout$._unit*/get _unit() {
      return core.RegExp.new("([um]s|[dhms])", {caseSensitive: false});
    },
    /*timeout$._whitespace*/get _whitespace() {
      return core.RegExp.new("\\s+");
    }
  }, true);
  dart.trackLibraries("packages/test_api/src/backend/metadata", {
    "package:test_api/src/backend/suite_platform.dart": suite_platform,
    "package:test_api/src/backend/runtime.dart": runtime,
    "package:test_api/src/backend/operating_system.dart": operating_system,
    "package:test_api/src/frontend/skip.dart": skip$,
    "package:test_api/src/backend/metadata.dart": metadata$,
    "package:test_api/src/backend/platform_selector.dart": platform_selector,
    "package:test_api/src/utils.dart": utils,
    "package:test_api/src/frontend/timeout.dart": timeout$
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["suite_platform.dart","runtime.dart","operating_system.dart","../frontend/skip.dart","metadata.dart","platform_selector.dart","../utils.dart","../frontend/timeout.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;IAUgB;;;;;;IAMQ;;;;;;IAGX;;;;;;uBAgB8B;AACnC,gBAAiB,YAAX,UAAU;AACpB,YAAO,sCAAsB,4BAAY,AAAG,GAAA,QAAC,kBACrB,sCAAe,eAAV,AAAG,GAAA,QAAC,mBACH,aAAhB,AAAG,GAAA,QAAC;IACpB;;AAIsB,wDAChB,WAAW,AAAQ,0BACnB,MAAM,AAAG,oBACT,YAAY;IACb;;+CAtBc;;QAA0B;QAAS;IAAnC;IAAmC;IAC7C,aAAK,KAAH,EAAE,QAAF,OAAsB;AAC/B,kBAAI,AAAQ,wCAAkB,SAAsB;AACmB,MAArE,WAAM,2BAAc,AAAgD,mDAAV,gBAAO;;EAErE;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ICgBa;;;;;;IAGA;;;;;;IAIC;;;;;;IAMH;;;;;;IAGA;;;;;;IAGA;;;;;;IAGA;;;;;;IAGA;;;;;;;AAfS,YAAA,AAAO,gBAAG;IAAI;;;AAqBd;0BAAU;IAAI;uBAmBC;AACjC,UAAe,OAAX,UAAU;AACZ,cAAO,AACF,sCAAW,QAAC,YAAa,AAAS,AAAW,QAAZ,eAAe,UAAU;;AAG7D,gBAAiB,YAAX,UAAU;AAChB,mBAAS,AAAG,GAAA,QAAC;AACjB,UAAI,MAAM,IAAI;AAKZ,cAAe,4BAAmB,eAAZ,AAAG,GAAA,QAAC,UAAqC,eAAlB,AAAG,GAAA,QAAC,gBACrC,4BAAY,MAAM;;AAGhC,YAAO,yBAAoB,eAAZ,AAAG,GAAA,QAAC,UAAqC,eAAlB,AAAG,GAAA,QAAC,2BACZ,aAAhB,AAAG,GAAA,QAAC,yBACc,aAAjB,AAAG,GAAA,QAAC,qBACG,aAAZ,AAAG,GAAA,QAAC,mBACc,aAAf,AAAG,GAAA,QAAC,yBACiB,aAAlB,AAAG,GAAA,QAAC;IACtB;;AAKE,oBAAI,AAAQ,mCAAS,QAAO,MAAO;AAEnC,UAAI,eAAU;AACZ,cAAO,4CACL,QAAQ,WACR,cAAc,iBACd,UAAU,AAAO;;AAIrB,YAAO,4CACL,QAAQ,WACR,cAAc,iBACd,YAAY,eACZ,aAAa,gBACb,QAAQ,WACR,WAAW,cACX,cAAc;IAElB;WAMsB,MAAa;AACjC,UAAI,AAAO,eAAG,MAAM,MAAe,4BAAO,IAAI,EAAE,UAAU,EAAE;AACH,MAAzD,WAAM,wBAAW;IACnB;;AAEqB;IAAI;;kCA3EN,MAAW;QACpB;QACD;QACA;QACA;QACA;IALU;IAAW;IACpB;IACD;IACA;IACA;IACA;IACI,gBAAE;;EAAI;qCAEC,MAAW,YAAiB;IAA5B;IAAW;IAAiB;IACjC,kBAAE,AAAO,MAAD;IACP,mBAAE,AAAO,MAAD;IACb,cAAE,AAAO,MAAD;IACL,iBAAE,AAAO,MAAD;IACL,oBAAE,AAAO,MAAD;;EAAW;;;;;;;;;;;;;;;;;;;;;;;;;;;MAnFf,kBAAE;;;MAGF,sBAAM;;;MAIN,yBAAS;;;MAIT,uBAAO;;;MAIP,sBAAM;;;MAIN,gCAAgB;;;MAIhB,sBAAM;;;MAGA,uBAAO;;;;;;;;;;;;;;ICiCrB;;;;;;IAGA;;;;;;gBA7BsB;AAC/B,YAAA,AAAI,mDAAW,QAAC,YAAa,AAAS,AAAW,QAAZ,eAAe,UAAU,uCAClD,cAAM;IAAK;wBAMgB;AACzC,cAAQ,IAAI;;;AAER,gBAAO;;;;AAEP,gBAAO;;;;AAEP,gBAAO;;;;AAEP,gBAAO;;;;AAEP,gBAAO;;;;AAEP,gBAAO;;;IAEb;;AASoB,YAAgB,EAAhB,eAAQ,8CAAW,eAAQ;IAAI;;AAI9B;IAAI;;kDAFI,MAAW;IAAX;IAAW;;EAAW;;;;;;;;;;;;;;;MAlEtC,wCAAO;;;MAGP,sCAAK;;;MAGL,sCAAK;;;MAML,wCAAO;;;MAMP,oCAAG;;;MAMH,qCAAI;;;MAGJ,oCAAG;;;;;;IC/BH;;;;;;;;IAMI;;EAAQ;;;;;;;;;;;;;;;;;;;;;;;;;;ICcF;;;;;;IAGT;;;;;;IAOD;;;;;;IAWK;;;;;;IAUoB;;;;;;IAUD;;;;;;;;AAnCpB;0BAAS;IAAK;;;AAON;0BAAiB;IAAK;;;AAIlB;0BAAqB;IAAI;;;AAOrC;0BAAU;IAAC;4BAqBH;AACvB,UAAI,AAAW,UAAD,IAAI,MAAM,MAAO;AAE3B,mBAAqC;AAmCvC,MAlCF,AAAW,UAAD,WAAS,SAAC,UAAU;AAC5B,YAAa,oBAAT,QAAQ,KAAwB,cAAT,QAAQ;AACZ,UAArB,WAAW,CAAC,QAAQ;cACf,MAAa,aAAT,QAAQ;AAEuC,UADxD,WAAM,2BAAa,AAAC,sCAAyB,QAAQ,sBACjD,qDAAwC,QAAQ;;AAGlD,uBAA4B,6CAAM,QAAQ;AAEtC;AACA;AACR,iBAAS,8BAAa,QAAQ;AAC5B,cAAc,oBAAV,SAAS;AACX,gBAAI,OAAO,IAAI;AAEM,cADnB,WAAM,2BAAa,AAAC,+CAChB,gBAAG,QAAQ;;AAGE,YAAnB,UAAU,SAAS;gBACd,KAAc,cAAV,SAAS;AAClB,gBAAI,IAAI,IAAI;AAES,cADnB,WAAM,2BAAa,AAAC,4CAChB,gBAAG,QAAQ;;AAGwC,YAAzD,OAAO,AAAU,AAAO,SAAR,WAAW,OAAO,OAAO,AAAU,SAAD;;AAGM,YADxD,WAAM,2BAAa,AAAC,sCAAyB,QAAQ,sBACjD,qDAAwC,QAAQ;;;AAIO,QAA/D,AAAM,MAAA,QAAC,QAAQ,EAAa,uCAAe,OAAO,QAAQ,IAAI;;AAEhE,YAAO,OAAM;IACf;sBAK8B;AAC5B,UAAI,AAAK,IAAD,IAAI,MAAM,MAAO;AACzB,UAAS,OAAL,IAAI,cAAY,MAAW,+BAAK,CAAC,IAAI;AACzC,WAAS,iBAAL,IAAI;AAEsD,QAD5D,WAAoB,6BAChB,IAAI,EAAE,QAAQ;;AAGpB,oBAAU,AAAa,iBAAlB,IAAI,QAAkB,QAAC,OAAc,EAAF,OAAJ,GAAG;AACgC,QAArE,WAAoB,6BAAM,IAAI,EAAE,QAAQ;;AAG1C,YAAW,+BAAU,iBAAL,IAAI;IACtB;;UAUsB;UACV;UACH;UACA;UACA;UACD;UACG;UACU;UACe;UACD;AAEjC;AAAiB,cAAS,oCACd,MAAM,WACL,OAAO,QACV,IAAI,gBACI,YAAY,oBACR,gBAAgB,SAC3B,KAAK,cACA,UAAU,QAChB,IAAI,cACE,UAAU,UACd,MAAM;;;AAIlB,UAAI,AAAO,MAAD,IAAI,QAAQ,AAAK,IAAD,IAAI,MAAM,MAAO,AAAW,YAAA;AACjC,MAArB,OAAW,8BAAK,IAAI;AACK,MAAzB,SAAa,iDAAK,MAAM;AAKpB,kBAAiB;AACjB,mBAAS,AAAO,AAAK,AAAS,MAAf,8CAAoB,KAAK,EAAE,SAAU,QAAQ;AAC9D,uBAAK,AAAS,QAAD,UAAU,IAAI,IAAG,MAAO,OAAM;AAC3C,cAAO,AAAO,OAAD,OAAO,AAAO,MAAD,UAAQ,QAAQ;;AAG5C,UAAW,YAAP,MAAM,EAAI,KAAK,GAAE,MAAO,AAAW,YAAA;AACvC,YAAO,AAAO,OAAD,OAAO,AAAW,WAAA;IACjC;+BAoFmC;AACjC,UAAe,YAAX,UAAU,EAAI,SAAQ,MAAe;AACrC,wBAAwB,WAAV,UAAU,WAAC;AAC7B,UAAI,WAAW,IAAI,MAAM,MAAe,6BAAmB,YAAZ,WAAW;AAC1D,YAAO,0BAAQ,qCAA8C,YAAb,WAAV,UAAU,WAAC;IACnD;;AAKM,wBAAc,AACb,AACA,AACA,kBAFM,QAAC,OAAQ,CAAC,AAAI,GAAD,YAAU,2EACzB,QAAC,OAAQ,AAAQ,gBAAL,GAAG;AAGxB,oBAAI,AAAY,WAAD,aAAU;AAID,MAFxB,WAAM,2BAAa,AAAC,sBAAW,gBAAU,OAAO,AAAY,WAAD,cAAS,eAC7D,iBAAW,WAAW,KAAE,4CAC3B;IACN;8BAK2C;AACV,MAA/B,AAAO,qBAAS,cAAc;AAI5B,MAHF,AAAW,0BAAQ,SAAC,UAAU;AACK,QAAjC,AAAS,QAAD,UAAU,cAAc;AACkB,QAAlD,AAAS,QAAD,2BAA2B,cAAc;;IAErD;UAOwB;;AAAU,6CACtB,AAAO,yBAAa,AAAM,KAAD,mBACxB,AAAQ,mBAAM,AAAM,KAAD,kBACV,KAAZ,AAAM,KAAD,eAAC,OAAS,gCACQ,MAAjB,AAAM,KAAD,oBAAC,OAAc,uCACE,OAApB,AAAM,KAAD,yBAAC,OAAiB,gDACK,OAAxB,AAAM,KAAD,6BAAC,OAAqB,yCACzB,OAAb,AAAM,KAAD,kBAAC,OAAU,4BACjB,AAAK,gBAAM,AAAM,KAAD,oBACV,4EAAU,iBAAY,AAAM,KAAD,qBAC5B,SAAC,WAAW,cAAc,AAAU,SAAD,OAAO,SAAS,kDACtD,0EAAU,aAAQ,AAAM,KAAD,iBACpB,SAAC,WAAW,cAAc,AAAU,SAAD,OAAO,SAAS;IAAG;;UAI/C;UACV;UACH;UACA;UACA;UACD;UACG;UACyB;UACpB;UACmB;AACX,MAAtB,AAAO,MAAD,IAAC,OAAP,SAAgB,cAAT;AACiB,MAAxB,AAAQ,OAAD,IAAC,OAAR,UAAiB,eAAT;AACW,MAAnB,AAAK,IAAD,IAAC,OAAL,OAAc,cAAT;AAC8B,MAAnC,AAAa,YAAD,IAAC,OAAb,eAAsB,sBAAT;AAC8B,MAA3C,AAAiB,gBAAD,IAAC,OAAjB,mBAA0B,0BAAT;AACI,MAArB,AAAM,KAAD,IAAC,OAAN,QAAe,eAAT;AACwB,MAA9B,AAAW,UAAD,IAAC,OAAX,aAAoB,kBAAT;AACmB,MAA9B,AAAW,UAAD,IAAC,OAAX,aAAoB,kBAAT;AACO,MAAlB,AAAK,IAAD,IAAC,OAAL,OAAc,YAAT;AACiB,MAAtB,AAAO,MAAD,IAAC,OAAP,SAAgB,cAAT;AACP,YAAO,iCACK,MAAM,WACL,OAAO,QACV,IAAI,gBACI,YAAY,oBACR,gBAAgB,cACtB,UAAU,cACV,UAAU,QAChB,IAAI,UACF,MAAM,SACP,KAAK;IAClB;gBAImC;AACjC,oBAAI,AAAW,4BAAS,MAAO;AAE3B,qBAAW;AAIb,MAHF,AAAW,0BAAQ,SAAC,kBAAkB;AACpC,uBAAK,AAAiB,gBAAD,UAAU,QAAQ,IAAG;AACC,QAA3C,WAAW,AAAS,QAAD,OAAO,gBAAgB;;AAE5C,YAAO,AAAS,SAAD,qBAAoB;IACrC;;AAMM,iCAAuB;AAGzB,MAFF,AAAW,0BAAQ,SAAC,KAAK;AACsC,QAA7D,AAAqB,oBAAD,OAAK,CAAK,cAAJ,GAAG,GAAa,AAAM,KAAD;;AAGjD,YAAO,4CACL,UAAiB,YAAP,aAA2B,0CAAM,OAAc,cAAP,cAClD,WAAW,wBAAkB,eAC7B,QAAQ,aACR,cAAc,iBACd,gBAAgB,qBAChB,oBAAoB,yBACpB,SAAS,cACT,QAAQ,AAAK,sBACb,cAAc,oBAAoB,EAClC,UAAU,AAAO,6CAAI,SAAC,UAAU,aAC5B,qCAAkB,cAAT,QAAQ,GAAa,AAAS,QAAD;IAE9C;wBAG0B;AACxB,UAAY,YAAR,OAAO,EAAY,wBAAM,MAAO;AACpC,YAAO,yCACL,YACI,AAAQ,AAAS,OAAV,aAAa,OAAO,OAAO,AAAQ,AAAS,OAAV,0BAC7C,eAAe,AAAQ,OAAD;IAE1B;;;QAlNsB;QACV;QACH;QACA;QACA;QACA;QACD;QACa;QACe;QACD;IAN1B;IAOI,gBAAE,AAAO,MAAD,IAAI,OAAwB,yCAAM,MAAM;IAC/C,kBAAE,AAAQ,OAAD,IAAI,uBAAiC,OAAO;IACvD,cAAE,IAAI;IACE,sBAAE,YAAY;IACV,0BAAE,gBAAgB;IAC7B,eAAE,KAAK;IACT,cAAE,yCAAoB,AAAK,IAAD,IAAI,OAAO,0CAAQ,AAAK,IAAD;IAC3C,oBACP,AAAW,UAAD,IAAI,uBAAkB,6DAAoB,UAAU;IAC3D,gBAAE,AAAO,MAAD,IAAI,uBAAkB,4DAAoB,MAAM;AACnE,QAAI,KAAK,IAAI,MAAiB,AAAgC,iCAAf,KAAK,EAAE;AACvC,IAAf;EACF;;QAOY;QACA;QACA;QACH;QACA;QACD;QACiB;QACrB;IACS,gBAAE,AAAO,MAAD,IAAI,OACI,yCACA,6CAAM,MAAM;IAC3B,kBAAE,AAAQ,OAAD,IAAI,uBAAiC,OAAO;IACvD,cAAE,AAAK,IAAD,IAAI,OAAO,OAAY,aAAL,IAAI,EAAI;IACxB,sBAAE,YAAY;IACV,0BAAE,gBAAgB;IAC7B,eAAE,KAAK;IACH,oBAAO,OAAL,IAAI,eAAa,IAAI,GAAG;IAC1B,oBAAE,oCAAiB,UAAU;IACnC,cAAE,8BAAW,IAAI;IACf;AACX,QAAI,IAAI,IAAI,UAAa,OAAL,IAAI,mBAAoB,OAAL,IAAI;AAC6B,MAAtE,WAAM,2BAAc,AAAiD,yDAAP,IAAI;;AAGpE,QAAI,KAAK,IAAI,MAAiB,AAAgC,iCAAf,KAAK,EAAE;AAEvC,IAAf;EACF;6CAGqB;IACR,gBAAY,AAAW,WAArB,UAAU,WAAC,cAAa,OACV,yCACA,6CAA2B,eAAX,WAAV,UAAU,WAAC;IAChC,kBAAE,uCAA8B,WAAV,UAAU,WAAC;IACnC,cAAqB,aAAT,WAAV,UAAU,WAAC;IACR,oBAA2B,eAAf,WAAV,UAAU,WAAC;IACV,sBAA6B,aAAjB,WAAV,UAAU,WAAC;IACT,0BAAiC,aAArB,WAAV,UAAU,WAAC;IACxB,eAAsB,YAAV,WAAV,UAAU,WAAC;IACf,cAAM,8BAAwB,iBAAT,WAAV,UAAU,WAAC;IAChB,oBAAM,0DAAsC,iBAAf,WAAV,UAAU,WAAC,uBAChC,QAAC,QAA0B,6CAAiB,eAAN,WAAL,IAAI,oDACnC,QAAC,QAAkB,mCAAiB,WAAL,IAAI;IACvC,gBAAwB,AAAQ,YAAnB,WAAV,UAAU,WAAC,wEAAsB,SAAC,KAAK,WAAW,gDACxC,mCAAU,eAAJ,GAAG,IAChB,mCAAY,MAAM;;EAAG;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MA7O3B,wBAAK;YAAY;;;;;;;;;;;;ICIR;;;;;;IAGL;;;;;;mCAiBkB,MAAmB;AACpD,UAAI,AAAK,IAAD,IAAI,MAAM,MAAO,AAAI,KAAA;AAE7B;AACE,cAAO,AAAI,KAAA;;YACe;AAA1B;AACoD,UAApD,WAAM,iDAA0B,AAAM,KAAD,UAAU,IAAI;;;;IAEvD;aAK0B;AACxB,UAAI,AAAU,SAAM,wCAAM;AAMhB,MAJV,mEACI,cAAM,AAAO,sBAAS,QAAC,QACqB,UAAxC,AAAyB,oDAAS,IAAI,gBACtC,AAAe,cAAD,UAAU,IAAI,uCAChC;IACN;aAK4B;AAC1B,YAAO,AAAO,uBAAS,QAAQ;;AAC7B,YAAI,AAAS,QAAD,IAAI,AAAS,AAAQ,QAAT,qBAAqB,MAAO;AACpD,YAAI,AAAS,QAAD,UAAI,AAAS,AAAQ,QAAT,8BAAS,OAAQ,gBAAY,MAAO;AAC5D,YAAI,AAAS,QAAD,IAAI,AAAS,AAAG,QAAJ,gBAAgB,MAAO;AAC/C,gBAAQ,QAAQ;;;AAEZ,kBAAO,AAAS,AAAQ,SAAT;;;;AAEf,kBAAO,AAAS,AAAQ,SAAT;;;;AAEf,kBAAO,AAAS,AAAQ,SAAT;;;;AAEf,kBAAO,AAAS,AAAQ,SAAT;;;;AAEf,kBAAO,AAAS,AAAG,SAAJ;;;;AAEf,kBAAO,AAAS,SAAD;;;;AAEf,kBAAO;;;;IAGf;iBAI+C;AAC7C,UAAU,YAAN,KAAK,EAAqB,yCAAK,MAAO;AAC1C,YAAwB,2CAAE,AAAO,0BAAa,AAAM,KAAD;IACrD;;AAEqB,YAAO,eAAP;IAAiB;;UAErB;AACb,YAAM,AAAoB,uCAA1B,KAAK,KAA+B,YAAP,cAAU,AAAM,KAAD;IAAO;;AAEnC,YAAO,eAAP;IAAe;;uDAzEL,UAAsB;IACvC,gBACH,0FAAqB,cAAsB,mCAAM,QAAQ,8BAAG,IAAI;IAC9D,eAAE,IAAI;;;;IAEY;IAAgB,eAAE;;EAAI;;;;;;;;;;;;;;;;;;MAjBvC,sCAAG;;;;;MAbZ,0CAAwB;;kBAC1B,8BAAiB,CAAC,SAAS,WAAW,WAAW,MAAM,SAAS,YAC5D,UAAe,AAAQ,2CAAI,QAAC,WAAY,AAAQ,OAAD,qCAC/C,UAAuB,AAAI,wDAAI,QAAC,MAAO,AAAG,EAAD;;;;;;;;MC2D7C;;;;;;MACA;;;;;;;AAImB,cAAA,AAAiB,gBAAd,cAAK,gBAAG,aAAI;MAAE;;YAErB;AACf,aAAU,cAAN,KAAK,GAAW,MAAO;AAC3B,cAAmB,AAAS,aAAf,WAAN,KAAK,YAAU,eAAoB,YAAL,WAAN,KAAK,WAAS;MAC/C;;AAEoB,cAAe,eAAT,cAAN,4BAAsB,cAAL;MAAa;;yBATxC,OAAY;MAAZ;MAAY;;IAAK;;;;;;;;;;;;;;;;;mDAgBN;AACnB,UAAM,AAAW,eAAjB,KAAK,iBAAyB,wBAAkB;EAAG;iCAMlC;QAAa;QAAa;AACJ,IAAzC,AAAK,IAAD,IAAC,OAAL,OAAS,AAAM,KAAD,IAAI,OAAO,IAAI,AAAM,KAAD,UAA7B;AACL,UAAO,mBAAY,MAAM,EAAE,AAAI,YAAE,IAAI,WAAS,KAAK;EACrD;yCAO2B;;QAAc;AACvC,QAAI,AAAK,AAAO,IAAR,cAAW,GAAG,MAAkB,eAAX,AAAK,IAAD;AAE7B,iBAAS,AAAK,AAAsB,IAAvB,QAAkB,aAAZ,AAAK,IAAD,aAAU,UAAQ;AAC7C,QAAgB,aAAZ,AAAK,IAAD,aAAU,GAAG,AAAa,SAAN,aAAP,MAAM,IAAI;AAC/B,UAAqD,UAA5C,MAAM,oBAAe,KAAZ,WAAW,QAAX,OAAe,eAAM,eAAG,AAAK,IAAD;EAChD;uCAMwB,MAAU;QAAgB;AAChD,QAAI,AAAO,MAAD,KAAI,GAAG,MAAO,KAAI;AAC5B,QAAI,MAAM,IAAI,MAAM,MAAO,OAAM;AACjC,UAAiB,UAAP,IAAI,IAAC;EACjB;uBAIgB;AAAS,UAAA,AAAK,KAAD,cAAY,gBAAU,AAAW,iBAAN,IAAI,IAAI,AAAS,gBAAL,IAAI;EAAC;+CAM7C;AAAQ,UAAA,AAAI,IAAD,cAAY,kBAAY;EAAG;qEAMlB,MAAgB;QACzD;AACL,kBAAI,AAAK,IAAD,aAAU,MAAO,KAAI;AAC7B,kBAAI,AAAK,IAAD,aAAU,MAAO,KAAI;AAC7B,UAAO,2BAAU,IAAI,EAAE,IAAI,UAAS,KAAK;EAC3C;qCAMuB,MAAU;AAE/B,QAAI,AAAK,AAAO,IAAR,wBAAW,SAAS,GAAE,MAAO,KAAI;AAGrC,gBAAQ,AAAK,IAAD,SAAO;AACvB,QAAiB,aAAb,AAAM,KAAD,aAAU;AACb,cAAI,AAAM,KAAD;AACT,mBAAS,AAAM,AAAM,AAAO,KAAd,kBAAgB;AAClC;AACK,QAAH,IAAC,aAAD,CAAC;AAC4B,QAA7B,SAAA,AAAO,MAAD,IAAI,AAAE,IAAE,AAAK,AAAI,KAAJ,QAAC,CAAC;eACd,AAAO,MAAD,iBAAI,SAAS,KAAM,aAAF,CAAC,IAAG;AACpC,UAAI,AAAO,MAAD,gBAAG,SAAS,KAAI,AAAE,CAAD,KAAI,GAAG,AAAG,IAAF,aAAD,CAAC;AACnC,UAAM,aAAF,CAAC,IAAgB,aAAb,AAAM,KAAD,aAAU;AAEjB,qBAAS;AACY,QAAzB,AAAO,MAAD,OAAO,AAAM,KAAD;AACE,QAApB,AAAO,MAAD,OAAO;AACb,eAAS,aAAF,CAAC,iBAAG,AAAM,KAAD,YAAS,IAAC,aAAD,CAAC;AACP,UAAjB,AAAO,MAAD,OAAO;AACS,UAAtB,AAAO,MAAD,OAAO,AAAK,KAAA,QAAC,CAAC;;AAEtB,cAAO,AAAO,OAAD;;;AAMb,iBAAS,AAAK,IAAD,aAAW,AAAK,AAAO,AAAY,IAApB,uBAAU,SAAS,IAAG;AAClD,qBAAa,AAAO,MAAD,WAAS;AAChC,QAAI,AAAW,UAAD,GAAG;AACsB,MAArC,SAAS,AAAO,MAAD,aAAW,UAAU;;AAEtC,UAAO,AAAY,SAAP,MAAM;EACpB;6CAG6B;AACvB,kBAAU,AAAS,QAAD;AAClB,kBAAU,AAAS,AAAU,QAAX,oBAAa;AAC/B,sBAA+C,CAAhC,AAAS,AAAe,QAAhB,yBAAkB,QAAS;AAElD,iBAAS;AACb,QAAI,OAAO,KAAI,GAAG,AAAO,AAAyB,MAA1B,OAAyB,SAAhB,OAAO;AAExC,QAAI,AAAQ,OAAD,KAAI,KAAK,OAAO,KAAI;AAC7B,UAAI,OAAO,KAAI,GAAG,AAAO,AAAW,MAAZ,OAAO;AACV,MAArB,AAAO,MAAD,OAAO,OAAO;AACpB,UAAI,WAAW,KAAI,GAAG,AAAO,AAAsB,MAAvB,OAAO,AAAe,eAAZ,WAAW;AACzB,MAAxB,AAAO,MAAD,OAAO;;AAGf,UAAO,AAAO,OAAD;EACf;0DAOgE;AAC1D,uBAAe,AAAW,UAAD;AACzB,qBAAa,sCACP,gBACI,cACM,gCAAK,AAAa,YAAD,qBAAK,QAAC,aAAc,AAAU,SAAD;AAGlE,aAAS,YAAa,aAAY;AAO9B,MANF,AAAU,AACL,AACA,AACA,SAHI,uBACC,QAAC,SAAU,AAAW,UAAD,KAAK,KAAK,4CACd,UAAX,UAAU,4BACR;AACc,QAA9B,AAAa,YAAD,QAAQ,SAAS;AAC7B,sBAAI,AAAa,YAAD,aAAU,AAAW,AAAO,UAAR;;;AAIxC,UAAO,AAAW,WAAD;EACnB;iCAMY;AACN,IAAJ,AAAE,EAAA;EACJ;6CAKwB;QAAY;AAC9B,iBAAc,gBAAO,IAAI;AACzB,eAAO,uCAAU,KAAK;AAC1B,aAAS,IAAI,GAAG,AAAE,CAAD,gBAAG,KAAK,GAAE,IAAA,AAAC,CAAA;AACG,MAA7B,AAAI,IAAA,QAAC,CAAC,EAAI,AAAO,MAAD,SAAS;;AAE3B,UAAO,sBAAa,IAAI;EAC1B;2DAGgC;AAC9B,QAAI,AAAQ,OAAD,IAAI,QACH,OAAR,OAAO,gBACC,OAAR,OAAO,gBACC,OAAR,OAAO;UAEJ,KAAY,aAAR,OAAO;AAChB,eAAS,UAAW,QAAO;AACG,QAA5B,0BAAoB,OAAO;;UAExB,KAAY,YAAR,OAAO;AAOd,MANF,AAAQ,OAAD,WAAS,SAAC,KAAK;AACpB,cAAQ,OAAJ,GAAG;AACiD,UAAtD,WAAM,2BAA+C,SAA/B,OAAO;;AAGL,QAA1B,0BAAoB,KAAK;;;AAGiC,MAA5D,WAAoB,6BAAuC,SAA/B,OAAO;;EAEvC;uCAGwB;AACpB,6BAAY,IAAI,EAAE,cAA+B,SAAT,oBAAO;EAAG;iCAGvB;AAAY,UAAA,AAAQ,AAAe,QAAhB,0CAAqB;EAAK;2CAQlD,MAAa;;QAC3B;QAAc;QAAa;AACrB,IAAhB,AAAM,KAAD,IAAC,OAAN,QAAU,MAAM,GAAV;AACS,IAAf,AAAK,IAAD,IAAC,OAAL,OAAS,MAAM,GAAV;AAC6B,IAAlC,AAAO,MAAD,IAAC,OAAP,UAAyB,OAAR,KAAN,KAAK,QAAL,OAAS,IAAI,eAAP,OAAW,MAAM,UAA3B;AAEH,gBAAQ,AAAK,IAAD,SAAO;AACvB,QAAI,AAAM,AAAO,KAAR,cAAW,GAAG,MAAqB,UAAZ,MAAM,aAAC,IAAI;AAEvC,iBAAS,0BAAqC,SAAtB,KAAK,aAAE,AAAM,KAAD,YAAO;AAG/C,aAAS,OAAQ,AAAM,AAAQ,MAAT,QAAM,UAAqB,aAAb,AAAM,KAAD,aAAU;AACnB,MAA9B,AAAO,MAAD,SAAuB,SAAZ,MAAM,aAAC,IAAI;;AAEI,IAAlC,AAAO,MAAD,OAA2B,SAAlB,IAAI,aAAE,AAAM,KAAD;AAC1B,UAAO,AAAO,OAAD;EACf;2CAMmB;AACf,UAA4C,eAA5C,AAAoB,yDAAiB,KAAK;EAAY;;;MAzSpD,kBAAY;YAAG,iEACjB,SAAC,QAAQ,kBAAkB,AACtB,AACA,AACA,MAH4B,wBAClB,AAAK,mEAER,sBAAqB,aAAa;;MAO5C,mBAAa;YAAG,sEAEI,mEACN,SAAC,MAAM,SAAS,AAAK,IAAD,KAAc,SAAP,IAAI;;MAI7C,sBAAgB;YAAG,iBAAO;;MAG1B,YAAM;YAAG,iBAAO;;MAKhB,uBAAiB;YAAG,+BACtB,CAAC,iBAAiB,YAAY,YAAY,WAAW;;MAQnC,oBAAc;YAAI,AAKtC;AAJA,YAAY,YAAN,YAAiB,kBAAK,MAAuB;AACnD,YAAY,YAAN,YAAiB,sBAAS,MAAuB;AACvD,sBAAI,AAAkB,8BAAc,UAAR,8BAAqB,MAAuB;AACxE,cAAuB;;;MAOnB,2BAAqB;YAAG,iBAAO;;MAI/B,kCAA4B;YAC9B,iBAAO,AAAqC,eAAjC,AAAsB,uCAAQ;;MA+DvC,gBAAU;YAAG,iBAAO;;;;;ICrGT;;;;;;IAUL;;;;;;iBA0BmB;AACvB,oBAAU,qCAAc,OAAO;AAGnC,oBAAI,AAAQ,OAAD,MAAM;AACK,QAApB,AAAQ,OAAD;AACP,cAAe;;AAIyB,MAA1C,AAAQ,OAAD,QAAQ,4BAAkB;AAC7B,mBAAgB,kBAAM,AAAQ,AAAS,OAAV,gBAAW;AAG5C,oBAAI,AAAQ,OAAD,MAAM,mBAAQ,AAAQ,OAAD,MAAM;AAChB,QAApB,AAAQ,OAAD;AACP,cAAe,6BAAO,MAAM;;AAK1B,yBAAe;AACnB,aAAO;AAC8B,QAAnC,AAAQ,OAAD,QAAQ,uBAAa;AACkC,QAA9D,eAAA,AAAa,YAAD,gBAAI,kCAAiB,MAAM,EAAE,AAAQ,AAAS,OAAV,gBAAW;AAElC,QAAzB,AAAQ,OAAD,MAAM;AAGb,uBAAK,AAAQ,OAAD,MAAM,uBAAa;AACY,QAA3C,SAAgB,kBAAM,AAAQ,AAAS,OAAV,gBAAW;;AAGtB,MAApB,AAAQ,OAAD;AACP,YAAO,0BAAQ,qCAAuB,AAAa,YAAD;IACpD;4BAGsC,QAAe;AACnD,cAAQ,IAAI;;;AAER,gBAAc,AAAK,AAAK,AAAK,cAAtB,MAAM,IAAG,KAAK,KAAK,KAAK;;;;AAE/B,gBAAc,AAAK,AAAK,cAAjB,MAAM,IAAG,KAAK,KAAK;;;;AAE1B,gBAAc,AAAK,cAAZ,MAAM,IAAG,KAAK;;;;AAErB,gBAAc,cAAP,MAAM,IAAG;;;;AAEhB,gBAAc,cAAP,MAAM,IAAG;;;;AAEhB,gBAAO,OAAM;;;;AAE6B,UAA1C,WAAM,2BAAc,AAAqB,2BAAN,IAAI;;;IAE7C;UAQsB;AACpB,UAAI,AAAK,aAAG,0BAAc,YAAN,KAAK,EAAI,wBAAM,MAAO;AAC1C,UAAI,AAAM,KAAD,aAAa,MAAM,MAAO,0BAAQ,AAAM,KAAD;AAChD,UAAI,iBAAY,MAAM,MAAO,0BAAQ,AAAS,mBAAE,AAAM,KAAD;AACrD,YAAe,6BAAmB,aAAZ,iCAAc,AAAM,KAAD;IAC3C;UAKwB;AACtB,UAAI,AAAK,aAAG,wBAAM,MAAO;AACzB,YAAO,AAAS,kBAAG,OAAO,AAAK,IAAD,MAAG,oBAAc;IACjD;;AAEoB,YAAkB,eAAT,cAAT,kBAAoB,AAAE,IAAc,cAAZ;IAAoB;;UAE/C;AACb,YAAM,AACqB,qBAD3B,KAAK,KACU,YAAf,AAAM,KAAD,WAAa,kBAClB,AAAM,AAAY,KAAb,gBAAgB;IAAW;;AAGlC,UAAI,iBAAY,MAAM,MAAgB,eAAT;AAC7B,UAAI,oBAAe,MAAM,MAAwB,UAAd,oBAAY;AAC/C,YAAO;IACT;;;IAhHmB;IAAwB,qBAAE;;EAAI;;IAGvB;IAAwB,kBAAE;;EAAI;;IAGtC,qBAAE;IACL,kBAAE;;EAAI;;;;;;;;;;;;;;;;;;;MA1BR,qBAAI;;;;;MAfb,mBAAU;YAAG,iBAAO,iCAAgC;;MAGpD,cAAK;YAAG,iBAAO,kCAAkC;;MAGjD,oBAAW;YAAG,iBAAO","file":"metadata.ddc.js"}');
  // Exports:
  return {
    src__backend__suite_platform: suite_platform,
    src__backend__runtime: runtime,
    src__backend__operating_system: operating_system,
    src__frontend__skip: skip$,
    src__backend__metadata: metadata$,
    src__backend__platform_selector: platform_selector,
    src__utils: utils,
    src__frontend__timeout: timeout$
  };
});

//# sourceMappingURL=metadata.ddc.js.map
