define(['dart_sdk', 'packages/http/src/base_client'], (function load__packages__http__src__mock_client(dart_sdk, packages__http__src__base_client) {
  'use strict';
  const core = dart_sdk.core;
  const async = dart_sdk.async;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const request$ = packages__http__src__base_client.src__request;
  const streamed_response = packages__http__src__base_client.src__streamed_response;
  const byte_stream = packages__http__src__base_client.src__byte_stream;
  const base_request = packages__http__src__base_client.src__base_request;
  const base_client = packages__http__src__base_client.src__base_client;
  var mock_client = Object.create(dart.library);
  var testing = Object.create(dart.library);
  var $addAll = dartx.addAll;
  var StreamedResponseL = () => (StreamedResponseL = dart.constFn(dart.legacy(streamed_response.StreamedResponse)))();
  var FutureOfStreamedResponseL = () => (FutureOfStreamedResponseL = dart.constFn(async.Future$(StreamedResponseL())))();
  var FutureLOfStreamedResponseL = () => (FutureLOfStreamedResponseL = dart.constFn(dart.legacy(FutureOfStreamedResponseL())))();
  var BaseRequestL = () => (BaseRequestL = dart.constFn(dart.legacy(base_request.BaseRequest)))();
  var ByteStreamL = () => (ByteStreamL = dart.constFn(dart.legacy(byte_stream.ByteStream)))();
  var BaseRequestLAndByteStreamLToFutureLOfStreamedResponseL = () => (BaseRequestLAndByteStreamLToFutureLOfStreamedResponseL = dart.constFn(dart.fnType(FutureLOfStreamedResponseL(), [BaseRequestL(), ByteStreamL()])))();
  const CT = Object.create(null);
  var L0 = "package:http/src/mock_client.dart";
  var _handler$ = dart.privateName(mock_client, "_handler");
  mock_client.MockClient = class MockClient extends base_client.BaseClient {
    send(request) {
      return async.async(StreamedResponseL(), (function* send() {
        let t1, t0;
        let bodyStream = request.finalize();
        return yield (t0 = request, t1 = bodyStream, this[_handler$](t0, t1));
      }).bind(this));
    }
  };
  (mock_client.MockClient.__ = function(_handler) {
    this[_handler$] = _handler;
    ;
  }).prototype = mock_client.MockClient.prototype;
  (mock_client.MockClient.new = function(fn) {
    mock_client.MockClient.__.call(this, dart.fn((baseRequest, bodyStream) => async.async(StreamedResponseL(), function*() {
      let t0;
      let bodyBytes = (yield bodyStream.toBytes());
      let request = (t0 = new request$.Request.new(baseRequest.method, baseRequest.url), (() => {
        t0.persistentConnection = baseRequest.persistentConnection;
        t0.followRedirects = baseRequest.followRedirects;
        t0.maxRedirects = baseRequest.maxRedirects;
        t0.headers[$addAll](baseRequest.headers);
        t0.bodyBytes = bodyBytes;
        t0.finalize();
        return t0;
      })());
      let response = (yield fn(request));
      return new streamed_response.StreamedResponse.new(byte_stream.ByteStream.fromBytes(response.bodyBytes), response.statusCode, {contentLength: response.contentLength, request: baseRequest, headers: response.headers, isRedirect: response.isRedirect, persistentConnection: response.persistentConnection, reasonPhrase: response.reasonPhrase});
    }), BaseRequestLAndByteStreamLToFutureLOfStreamedResponseL()));
  }).prototype = mock_client.MockClient.prototype;
  (mock_client.MockClient.streaming = function(fn) {
    mock_client.MockClient.__.call(this, dart.fn((request, bodyStream) => async.async(StreamedResponseL(), function*() {
      let response = (yield fn(request, bodyStream));
      return new streamed_response.StreamedResponse.new(response.stream, response.statusCode, {contentLength: response.contentLength, request: request, headers: response.headers, isRedirect: response.isRedirect, persistentConnection: response.persistentConnection, reasonPhrase: response.reasonPhrase});
    }), BaseRequestLAndByteStreamLToFutureLOfStreamedResponseL()));
  }).prototype = mock_client.MockClient.prototype;
  dart.addTypeTests(mock_client.MockClient);
  dart.addTypeCaches(mock_client.MockClient);
  dart.setMethodSignature(mock_client.MockClient, () => ({
    __proto__: dart.getMethods(mock_client.MockClient.__proto__),
    send: dart.fnType(dart.legacy(async.Future$(dart.legacy(streamed_response.StreamedResponse))), [dart.legacy(base_request.BaseRequest)])
  }));
  dart.setLibraryUri(mock_client.MockClient, L0);
  dart.setFieldSignature(mock_client.MockClient, () => ({
    __proto__: dart.getFields(mock_client.MockClient.__proto__),
    [_handler$]: dart.finalFieldType(dart.legacy(dart.fnType(dart.legacy(async.Future$(dart.legacy(streamed_response.StreamedResponse))), [dart.legacy(base_request.BaseRequest), dart.legacy(byte_stream.ByteStream)])))
  }));
  dart.trackLibraries("packages/http/src/mock_client", {
    "package:http/src/mock_client.dart": mock_client,
    "package:http/testing.dart": testing
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["mock_client.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;SAoE4C;AAAb;;AACvB,yBAAa,AAAQ,OAAD;AACxB,cAAO,aAAe,OAAO,OAAE,UAAU,EAA5B,AAAQ;MACvB;;;;IA5CkB;;EAAS;yCAIE;yCAChB,SAAC,aAAa;;AACb,uBAAY,MAAM,AAAW,UAAD;AAC9B,0BAAU,yBAAQ,AAAY,WAAD,SAAS,AAAY,WAAD,OAAvC;AACV,kCAAuB,AAAY,WAAD;AAClC,6BAAkB,AAAY,WAAD;AAC7B,0BAAe,AAAY,WAAD;AAClB,QAAR,oBAAe,AAAY,WAAD;AAC1B,uBAAY,SAAS;AACrB;;;AAEE,sBAAW,MAAM,AAAE,EAAA,CAAC,OAAO;AACjC,YAAO,4CACQ,iCAAU,AAAS,QAAD,aAAa,AAAS,QAAD,6BACnC,AAAS,QAAD,yBACd,WAAW,WACX,AAAS,QAAD,sBACL,AAAS,QAAD,mCACE,AAAS,QAAD,qCAChB,AAAS,QAAD;IAC3B;EAAC;+CAIqC;yCAChC,SAAC,SAAS;AACT,sBAAW,MAAM,AAAE,EAAA,CAAC,OAAO,EAAE,UAAU;AAC7C,YAAO,4CAAiB,AAAS,QAAD,SAAS,AAAS,QAAD,6BAC9B,AAAS,QAAD,yBACd,OAAO,WACP,AAAS,QAAD,sBACL,AAAS,QAAD,mCACE,AAAS,QAAD,qCAChB,AAAS,QAAD;IAC3B;EAAC","file":"mock_client.ddc.js"}');
  // Exports:
  return {
    src__mock_client: mock_client,
    testing: testing
  };
}));

//# sourceMappingURL=mock_client.ddc.js.map
