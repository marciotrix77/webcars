// **************************************************************************
// Generator: AngularDart Compiler
// **************************************************************************

import 'login_component.dart';
export 'login_component.dart';
import 'package:angular/src/di/reflector.dart' as _ngRef;
import 'package:angular/angular.template.dart' as _ref0;
import 'package:angular_forms/angular_forms.template.dart' as _ref1;
import 'package:angular_router/angular_router.template.dart' as _ref2;
import 'package:carros/src/routes/routes.template.dart' as _ref3;
import 'package:carros/src/services/login_service.template.dart' as _ref4;
import 'package:carros/src/components/login_component/login_component.css.shim.dart' as import0;
import 'package:angular/src/core/linker/app_view.dart';
import 'login_component.dart' as import2;
import 'package:angular_forms/src/directives/ng_form.dart' as import3;
import 'package:angular_forms/src/directives/validators.dart' as import4;
import 'package:angular_forms/src/directives/default_value_accessor.dart' as import5;
import 'package:angular_forms/src/directives/control_value_accessor.dart' as import6;
import 'package:angular_forms/src/directives/ng_model.dart' as import7;
import 'package:angular_forms/src/directives/checkbox_value_accessor.dart' as import8;
import 'dart:html' as import9;
import 'package:angular/src/core/linker/style_encapsulation.dart' as import10;
import 'package:angular/src/core/linker/view_type.dart' as import11;
import 'package:angular/src/core/change_detection/change_detection.dart';
import 'package:angular/src/runtime.dart' as import13;
import 'package:angular/angular.dart';
import 'package:angular/src/runtime/dom_helpers.dart' as import15;
import 'package:angular/src/core/linker/app_view_utils.dart' as import16;
import 'package:angular/src/core/di/opaque_token.dart' as import17;
import 'package:angular_forms/src/directives/control_value_accessor.dart' as import18;
import 'package:angular_forms/src/directives/ng_control.dart' as import19;
import 'package:angular_forms/src/directives/control_container.dart' as import20;
import 'package:angular/src/di/errors.dart' as import21;
import 'package:angular_router/src/router/router.dart' as import22;
import '../../services/login_service.dart' as import23;

final List<dynamic> styles$LoginComponent = [import0.styles];

class ViewLoginComponent0 extends AppView<import2.LoginComponent> {
  import3.NgForm _NgForm_5_5;
  import4.RequiredValidator _RequiredValidator_14_5;
  List<dynamic> _NgValidators_14_6;
  import5.DefaultValueAccessor _DefaultValueAccessor_14_7;
  List<import6.ControlValueAccessor<dynamic>> _NgValueAccessor_14_8;
  import7.NgModel _NgModel_14_9;
  import4.RequiredValidator _RequiredValidator_19_5;
  List<dynamic> _NgValidators_19_6;
  import5.DefaultValueAccessor _DefaultValueAccessor_19_7;
  List<import6.ControlValueAccessor<dynamic>> _NgValueAccessor_19_8;
  import7.NgModel _NgModel_19_9;
  import8.CheckboxControlValueAccessor _CheckboxControlValueAccessor_22_5;
  List<import6.ControlValueAccessor<dynamic>> _NgValueAccessor_22_6;
  import7.NgModel _NgModel_22_7;
  bool _expr_0;
  bool _expr_3;
  import9.InputElement _el_14;
  import9.InputElement _el_19;
  static import10.ComponentStyles _componentStyles;
  ViewLoginComponent0(AppView<dynamic> parentView, int parentIndex) : super(import11.ViewType.component, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways) {
    initComponentStyles();
    rootEl = import9.document.createElement('login');
  }
  static String get _debugComponentUrl {
    return (import13.isDevMode ? 'asset:carros/lib/src/components/login_component/login_component.dart' : null);
  }

  @override
  ComponentRef<import2.LoginComponent> build() {
    final _ctx = ctx;
    final _rootEl = rootEl;
    final import9.HtmlElement parentRenderNode = initViewRoot(_rootEl);
    final doc = import9.document;
    final _el_0 = import15.appendDiv(doc, parentRenderNode);
    this.updateChildClass(_el_0, 'd-flex justify-content-center align-items-center');
    import15.setAttribute(_el_0, 'id', 'area-form');
    addShimC(_el_0);
    final _el_1 = import15.appendDiv(doc, _el_0);
    this.updateChildClass(_el_1, 'row');
    import15.setAttribute(_el_1, 'id', 'linha-form');
    addShimC(_el_1);
    final _el_2 = import15.appendDiv(doc, _el_1);
    this.updateChildClass(_el_2, 'p-0 col-6 d-flex justify-content-end');
    addShimC(_el_2);
    final _el_3 = import15.appendElement(doc, _el_2, 'img');
    import15.setAttribute(_el_3, 'alt', 'Responsive image');
    this.updateChildClass(_el_3, 'img-fluid');
    import15.setAttribute(_el_3, 'id', 'img-form');
    import15.setAttribute(_el_3, 'src', 'https://i.pinimg.com/564x/2f/b5/68/2fb568d54a82da6d5a80d249f99e6ec3.jpg');
    addShimE(_el_3);
    final _el_4 = import15.appendDiv(doc, _el_1);
    this.updateChildClass(_el_4, 'p-0 col-6 d-flex justify-content-start');
    addShimC(_el_4);
    final _el_5 = import15.appendElement(doc, _el_4, 'form');
    this.updateChildClass(_el_5, 'p-4 bg-danger text-light');
    addShimC(_el_5);
    _NgForm_5_5 = import3.NgForm(null);
    final _el_6 = import15.appendDiv(doc, _el_5);
    import15.setAttribute(_el_6, 'id', 'brand-login');
    addShimC(_el_6);
    final _el_7 = import15.appendElement(doc, _el_6, 'h2');
    this.updateChildClass(_el_7, 'text-center');
    addShimE(_el_7);
    final _text_8 = import15.appendText(_el_7, 'WebCars');
    final _el_9 = import15.appendElement(doc, _el_5, 'br');
    addShimE(_el_9);
    final _el_10 = import15.appendDiv(doc, _el_5);
    this.updateChildClass(_el_10, 'form-group');
    addShimC(_el_10);
    final _el_11 = import15.appendElement(doc, _el_10, 'label');
    import15.setAttribute(_el_11, 'for', 'usuario');
    addShimE(_el_11);
    final _text_12 = import15.appendText(_el_11, 'Usuário');
    final _text_13 = import15.appendText(_el_10, ' ');
    _el_14 = import15.appendElement(doc, _el_10, 'input');
    this.updateChildClass(_el_14, 'form-control');
    import15.setAttribute(_el_14, 'id', 'usuarioId');
    import15.setAttribute(_el_14, 'required', '');
    import15.setAttribute(_el_14, 'type', 'text');
    addShimC(_el_14);
    _RequiredValidator_14_5 = import4.RequiredValidator();
    _NgValidators_14_6 = [_RequiredValidator_14_5];
    _DefaultValueAccessor_14_7 = import5.DefaultValueAccessor(_el_14);
    _NgValueAccessor_14_8 = [_DefaultValueAccessor_14_7];
    _NgModel_14_9 = import7.NgModel(_NgValidators_14_6, _NgValueAccessor_14_8);
    final _el_15 = import15.appendDiv(doc, _el_5);
    this.updateChildClass(_el_15, 'form-group');
    addShimC(_el_15);
    final _el_16 = import15.appendElement(doc, _el_15, 'label');
    import15.setAttribute(_el_16, 'for', 'senha');
    addShimE(_el_16);
    final _text_17 = import15.appendText(_el_16, 'Senha');
    final _text_18 = import15.appendText(_el_15, ' ');
    _el_19 = import15.appendElement(doc, _el_15, 'input');
    this.updateChildClass(_el_19, 'form-control');
    import15.setAttribute(_el_19, 'id', 'senhaId');
    import15.setAttribute(_el_19, 'required', '');
    import15.setAttribute(_el_19, 'type', 'password');
    addShimC(_el_19);
    _RequiredValidator_19_5 = import4.RequiredValidator();
    _NgValidators_19_6 = [_RequiredValidator_19_5];
    _DefaultValueAccessor_19_7 = import5.DefaultValueAccessor(_el_19);
    _NgValueAccessor_19_8 = [_DefaultValueAccessor_19_7];
    _NgModel_19_9 = import7.NgModel(_NgValidators_19_6, _NgValueAccessor_19_8);
    final _el_20 = import15.appendDiv(doc, _el_5);
    this.updateChildClass(_el_20, 'form-group');
    addShimC(_el_20);
    final _el_21 = import15.appendDiv(doc, _el_20);
    this.updateChildClass(_el_21, 'form-check');
    addShimC(_el_21);
    final _el_22 = import15.appendElement(doc, _el_21, 'input');
    this.updateChildClass(_el_22, 'form-check-input');
    import15.setAttribute(_el_22, 'id', 'switchConectadoId');
    import15.setAttribute(_el_22, 'type', 'checkbox');
    addShimC(_el_22);
    _CheckboxControlValueAccessor_22_5 = import8.CheckboxControlValueAccessor(_el_22);
    _NgValueAccessor_22_6 = [_CheckboxControlValueAccessor_22_5];
    _NgModel_22_7 = import7.NgModel(null, _NgValueAccessor_22_6);
    final _text_23 = import15.appendText(_el_21, ' ');
    final _el_24 = import15.appendElement(doc, _el_21, 'label');
    this.updateChildClass(_el_24, 'form-check-label');
    import15.setAttribute(_el_24, 'for', 'dropdownCheck2');
    addShimE(_el_24);
    final _text_25 = import15.appendText(_el_24, 'Mantenha-me Conectado');
    final _el_26 = import15.appendElement(doc, _el_5, 'button');
    this.updateChildClass(_el_26, 'btn btn-dark');
    import15.setAttribute(_el_26, 'type', 'submit');
    addShimC(_el_26);
    final _text_27 = import15.appendText(_el_26, 'Acessar');
    final _el_28 = import15.appendDiv(doc, _el_5);
    this.updateChildClass(_el_28, 'alert alert-danger mt-3 col-12');
    import15.setAttribute(_el_28, 'id', 'alert-erro');
    import15.setAttribute(_el_28, 'role', 'alert');
    addShimC(_el_28);
    import16.appViewUtils.eventManager.addEventListener(_el_5, 'submit', eventHandler1(_NgForm_5_5.onSubmit));
    _el_5.addEventListener('reset', eventHandler1(_NgForm_5_5.onReset));
    final subscription_0 = _NgForm_5_5.ngSubmit.listen(eventHandler0(_ctx.Logar));
    _el_14.addEventListener('blur', eventHandler0(_DefaultValueAccessor_14_7.touchHandler));
    _el_14.addEventListener('input', eventHandler1(_handle_input_14_2));
    final subscription_1 = _NgModel_14_9.update.listen(eventHandler1(_handle_ngModelChange_14_0));
    _el_19.addEventListener('blur', eventHandler0(_DefaultValueAccessor_19_7.touchHandler));
    _el_19.addEventListener('input', eventHandler1(_handle_input_19_2));
    final subscription_2 = _NgModel_19_9.update.listen(eventHandler1(_handle_ngModelChange_19_0));
    _el_22.addEventListener('blur', eventHandler0(_CheckboxControlValueAccessor_22_5.touchHandler));
    _el_22.addEventListener('change', eventHandler1(_handle_change_22_2));
    final subscription_3 = _NgModel_22_7.update.listen(eventHandler1(_handle_ngModelChange_22_0));
    init(const [], [subscription_0, subscription_1, subscription_2, subscription_3]);
  }

  @override
  dynamic injectorGetInternal(dynamic token, int nodeIndex, dynamic notFoundResult) {
    if (((5 <= nodeIndex) && (nodeIndex <= 28))) {
      if ((14 == nodeIndex)) {
        if (identical(token, const import17.MultiToken<dynamic>('NgValidators'))) {
          return _NgValidators_14_6;
        }
        if (identical(token, const import17.MultiToken<import18.ControlValueAccessor<dynamic>>('NgValueAccessor'))) {
          return _NgValueAccessor_14_8;
        }
        if ((identical(token, import7.NgModel) || identical(token, import19.NgControl))) {
          return _NgModel_14_9;
        }
      }
      if ((19 == nodeIndex)) {
        if (identical(token, const import17.MultiToken<dynamic>('NgValidators'))) {
          return _NgValidators_19_6;
        }
        if (identical(token, const import17.MultiToken<import18.ControlValueAccessor<dynamic>>('NgValueAccessor'))) {
          return _NgValueAccessor_19_8;
        }
        if ((identical(token, import7.NgModel) || identical(token, import19.NgControl))) {
          return _NgModel_19_9;
        }
      }
      if ((22 == nodeIndex)) {
        if (identical(token, const import17.MultiToken<import18.ControlValueAccessor<dynamic>>('NgValueAccessor'))) {
          return _NgValueAccessor_22_6;
        }
        if ((identical(token, import7.NgModel) || identical(token, import19.NgControl))) {
          return _NgModel_22_7;
        }
      }
      if ((identical(token, import3.NgForm) || identical(token, import20.ControlContainer))) {
        return _NgForm_5_5;
      }
    }
    return notFoundResult;
  }

  @override
  void detectChangesInternal() {
    final _ctx = ctx;
    bool changed = false;
    bool firstCheck = (this.cdState == 0);
    final import7.NgModel local_usuarioId = _NgModel_14_9;
    final import7.NgModel local_senhaId = _NgModel_19_9;
    if (firstCheck) {
      (_RequiredValidator_14_5.required = true);
    }
    changed = false;
    _NgModel_14_9.model = _ctx.usuario;
    _NgModel_14_9.ngAfterChanges();
    if (((!import16.AppViewUtils.throwOnChanges) && firstCheck)) {
      _NgModel_14_9.ngOnInit();
    }
    if (firstCheck) {
      (_RequiredValidator_19_5.required = true);
    }
    changed = false;
    _NgModel_19_9.model = _ctx.senha;
    _NgModel_19_9.ngAfterChanges();
    if (((!import16.AppViewUtils.throwOnChanges) && firstCheck)) {
      _NgModel_19_9.ngOnInit();
    }
    changed = false;
    _NgModel_22_7.model = _ctx.switchConectado;
    _NgModel_22_7.ngAfterChanges();
    if (((!import16.AppViewUtils.throwOnChanges) && firstCheck)) {
      _NgModel_22_7.ngOnInit();
    }
    final currVal_0 = local_usuarioId.valid;
    if (import16.checkBinding(_expr_0, currVal_0)) {
      import15.updateClassBinding(_el_14, 'is-valid', currVal_0);
      _expr_0 = currVal_0;
    }
    final currVal_3 = local_senhaId.valid;
    if (import16.checkBinding(_expr_3, currVal_3)) {
      import15.updateClassBinding(_el_19, 'is-valid', currVal_3);
      _expr_3 = currVal_3;
    }
  }

  void _handle_ngModelChange_14_0($event) {
    final _ctx = ctx;
    _ctx.usuario = $event;
  }

  void _handle_input_14_2($event) {
    _DefaultValueAccessor_14_7.handleChange($event.target.value);
  }

  void _handle_ngModelChange_19_0($event) {
    final _ctx = ctx;
    _ctx.senha = $event;
  }

  void _handle_input_19_2($event) {
    _DefaultValueAccessor_19_7.handleChange($event.target.value);
  }

  void _handle_ngModelChange_22_0($event) {
    final _ctx = ctx;
    _ctx.switchConectado = $event;
  }

  void _handle_change_22_2($event) {
    _CheckboxControlValueAccessor_22_5.handleChange($event.target.checked);
  }

  @override
  void initComponentStyles() {
    var styles = _componentStyles;
    if (identical(styles, null)) {
      (_componentStyles = (styles = (_componentStyles = import10.ComponentStyles.scoped(styles$LoginComponent, _debugComponentUrl))));
    }
    componentStyles = styles;
  }
}

const ComponentFactory<import2.LoginComponent> _LoginComponentNgFactory = const ComponentFactory('login', viewFactory_LoginComponentHost0);
ComponentFactory<import2.LoginComponent> get LoginComponentNgFactory {
  return _LoginComponentNgFactory;
}

final List<dynamic> styles$LoginComponentHost = const [];

class _ViewLoginComponentHost0 extends AppView<import2.LoginComponent> {
  ViewLoginComponent0 _compView_0;
  import2.LoginComponent _LoginComponent_0_5;
  _ViewLoginComponentHost0(AppView<dynamic> parentView, int parentIndex) : super(import11.ViewType.host, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways);
  @override
  ComponentRef<import2.LoginComponent> build() {
    _compView_0 = ViewLoginComponent0(this, 0);
    rootEl = _compView_0.rootEl;
    _LoginComponent_0_5 = (import13.isDevMode
        ? import21.debugInjectorWrap(import2.LoginComponent, () {
            return import2.LoginComponent(this.injectorGet(import22.Router, viewData.parentIndex), this.injectorGet(import23.LoginService, viewData.parentIndex));
          })
        : import2.LoginComponent(this.injectorGet(import22.Router, viewData.parentIndex), this.injectorGet(import23.LoginService, viewData.parentIndex)));
    _compView_0.create(_LoginComponent_0_5, projectedNodes);
    init1(rootEl);
    return ComponentRef(0, this, rootEl, _LoginComponent_0_5);
  }

  @override
  void detectChangesInternal() {
    bool firstCheck = (this.cdState == 0);
    if (((!import16.AppViewUtils.throwOnChanges) && firstCheck)) {
      _LoginComponent_0_5.ngOnInit();
    }
    _compView_0.detectChanges();
  }

  @override
  void destroyInternal() {
    _compView_0.destroyInternalState();
  }
}

AppView<import2.LoginComponent> viewFactory_LoginComponentHost0(AppView<dynamic> parentView, int parentIndex) {
  return _ViewLoginComponentHost0(parentView, parentIndex);
}

var _visited = false;
void initReflector() {
  if (_visited) {
    return;
  }
  _visited = true;

  _ngRef.registerComponent(LoginComponent, LoginComponentNgFactory);
  _ref0.initReflector();
  _ref1.initReflector();
  _ref2.initReflector();
  _ref3.initReflector();
  _ref4.initReflector();
}
