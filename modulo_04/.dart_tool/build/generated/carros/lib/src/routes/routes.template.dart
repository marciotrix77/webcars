// **************************************************************************
// Generator: AngularDart Compiler
// **************************************************************************

import 'routes.dart';
export 'routes.dart';
import '../components/login_component/login_component.template.dart' as _ref0;
import '../components/not_found_component/not_found_component.template.dart' as _ref1;
import '../components/painel_component/painel_component.template.dart' as _ref2;
import 'package:angular_router/angular_router.template.dart' as _ref3;
import 'route_paths.template.dart' as _ref4;
import 'route_paths.template.dart' as _ref5;

var _visited = false;
void initReflector() {
  if (_visited) {
    return;
  }
  _visited = true;

  _ref0.initReflector();
  _ref1.initReflector();
  _ref2.initReflector();
  _ref3.initReflector();
  _ref4.initReflector();
  _ref5.initReflector();
}
