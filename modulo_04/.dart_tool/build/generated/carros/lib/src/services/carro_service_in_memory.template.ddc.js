define(['dart_sdk', 'packages/carros/src/components/cadastro_component/cadastro_component.css.shim'], (function load__packages__carros__src__services__carro_service_in_memory_template(dart_sdk, packages__carros__src__components__cadastro_component__cadastro_component$46css$46shim) {
  'use strict';
  const core = dart_sdk.core;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const carro$46template = packages__carros__src__components__cadastro_component__cadastro_component$46css$46shim.src__model__carro$46template;
  var carro_service_in_memory$46template = Object.create(dart.library);
  const CT = Object.create(null);
  carro_service_in_memory$46template.initReflector = function initReflector() {
    if (dart.test(carro_service_in_memory$46template._visited)) {
      return;
    }
    carro_service_in_memory$46template._visited = true;
    carro$46template.initReflector();
  };
  dart.defineLazy(carro_service_in_memory$46template, {
    /*carro_service_in_memory$46template._visited*/get _visited() {
      return false;
    },
    set _visited(_) {}
  }, true);
  dart.trackLibraries("packages/carros/src/services/carro_service_in_memory.template", {
    "package:carros/src/services/carro_service_in_memory.template.dart": carro_service_in_memory$46template
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["carro_service_in_memory.template.dart"],"names":[],"mappings":";;;;;;;;;AAUE,kBAAI;AACF;;AAEa,IAAf,8CAAW;AAEU,IAArB;EACF;;MARI,2CAAQ;YAAG","file":"carro_service_in_memory.template.ddc.js"}');
  // Exports:
  return {
    src__services__carro_service_in_memory$46template: carro_service_in_memory$46template
  };
}));

//# sourceMappingURL=carro_service_in_memory.template.ddc.js.map
