// **************************************************************************
// Generator: AngularDart Compiler
// **************************************************************************

import 'app_component.dart';
export 'app_component.dart';
import 'package:angular/src/di/reflector.dart' as _ngRef;
import 'package:angular/angular.template.dart' as _ref0;
import 'package:angular_router/angular_router.template.dart' as _ref1;
import 'package:carros/src/components/cadastro_component/cadastro_component.template.dart' as _ref2;
import 'package:carros/src/services/carro_service.template.dart' as _ref3;
import 'package:carros/src/services/login_service.template.dart' as _ref4;
import 'src/components/login_component/login_component.template.dart' as _ref5;
import 'src/components/navbar_component/navbar_component.template.dart' as _ref6;
import 'src/components/tabela_component/tabela_component.template.dart' as _ref7;
import 'src/routes/routes.template.dart' as _ref8;
import 'package:carros/app_component.css.shim.dart' as import0;
import 'package:angular/src/core/linker/app_view.dart';
import 'app_component.dart' as import2;
import 'package:angular/src/core/linker/view_container.dart';
import 'package:angular_router/src/directives/router_outlet_directive.dart' as import4;
import 'package:angular/src/core/linker/style_encapsulation.dart' as import5;
import 'package:angular/src/core/linker/view_type.dart' as import6;
import 'package:angular/src/core/change_detection/change_detection.dart';
import 'dart:html' as import8;
import 'package:angular/src/runtime.dart' as import9;
import 'package:angular/angular.dart';
import 'package:angular/src/runtime/dom_helpers.dart' as import11;
import 'package:angular/src/di/errors.dart' as import12;
import 'package:angular_router/src/router/router_outlet_token.dart' as import13;
import 'package:angular_router/src/router/router.dart' as import14;
import 'package:angular_router/src/router_hook.dart' as import15;
import 'src/routes/routes.dart' as import16;
import 'package:angular/src/core/linker/app_view_utils.dart' as import17;
import 'src/services/login_service.dart' as import18;
import 'src/services/carro_service.dart' as import19;
import 'package:http/src/client.dart' as import20;

final List<dynamic> styles$AppComponent = [import0.styles];

class ViewAppComponent0 extends AppView<import2.AppComponent> {
  ViewContainer _appEl_0;
  import4.RouterOutlet _RouterOutlet_0_8;
  static import5.ComponentStyles _componentStyles;
  ViewAppComponent0(AppView<dynamic> parentView, int parentIndex) : super(import6.ViewType.component, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways) {
    initComponentStyles();
    rootEl = import8.document.createElement('my-app');
  }
  static String get _debugComponentUrl {
    return (import9.isDevMode ? 'asset:carros/lib/app_component.dart' : null);
  }

  @override
  ComponentRef<import2.AppComponent> build() {
    final _rootEl = rootEl;
    final import8.HtmlElement parentRenderNode = initViewRoot(_rootEl);
    final doc = import8.document;
    final _el_0 = import11.appendElement(doc, parentRenderNode, 'router-outlet');
    addShimE(_el_0);
    _appEl_0 = ViewContainer(0, null, this, _el_0);
    _RouterOutlet_0_8 = (import9.isDevMode
        ? import12.debugInjectorWrap(import4.RouterOutlet, () {
            return import4.RouterOutlet(parentView.injectorGetOptional(import13.RouterOutletToken, viewData.parentIndex), _appEl_0, parentView.injectorGet(import14.Router, viewData.parentIndex), parentView.injectorGetOptional(import15.RouterHook, viewData.parentIndex));
          })
        : import4.RouterOutlet(parentView.injectorGetOptional(import13.RouterOutletToken, viewData.parentIndex), _appEl_0, parentView.injectorGet(import14.Router, viewData.parentIndex), parentView.injectorGetOptional(import15.RouterHook, viewData.parentIndex)));
    init0();
  }

  @override
  void detectChangesInternal() {
    bool firstCheck = (this.cdState == 0);
    if (firstCheck) {
      if (!identical(import16.Routes.all, null)) {
        (_RouterOutlet_0_8.routes = import16.Routes.all);
      }
    }
    if (((!import17.AppViewUtils.throwOnChanges) && firstCheck)) {
      _RouterOutlet_0_8.ngOnInit();
    }
    _appEl_0.detectChangesInNestedViews();
  }

  @override
  void destroyInternal() {
    _appEl_0.destroyNestedViews();
    _RouterOutlet_0_8.ngOnDestroy();
  }

  @override
  void initComponentStyles() {
    var styles = _componentStyles;
    if (identical(styles, null)) {
      (_componentStyles = (styles = (_componentStyles = import5.ComponentStyles.scoped(styles$AppComponent, _debugComponentUrl))));
    }
    componentStyles = styles;
  }
}

const ComponentFactory<import2.AppComponent> _AppComponentNgFactory = const ComponentFactory('my-app', viewFactory_AppComponentHost0);
ComponentFactory<import2.AppComponent> get AppComponentNgFactory {
  return _AppComponentNgFactory;
}

final List<dynamic> styles$AppComponentHost = const [];

class _ViewAppComponentHost0 extends AppView<import2.AppComponent> {
  ViewAppComponent0 _compView_0;
  import2.AppComponent _AppComponent_0_5;
  dynamic __CarroService_0_6;
  import18.LoginService __LoginService_0_7;
  _ViewAppComponentHost0(AppView<dynamic> parentView, int parentIndex) : super(import6.ViewType.host, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways);
  dynamic get _CarroService_0_6 {
    if ((__CarroService_0_6 == null)) {
      (__CarroService_0_6 = (import9.isDevMode
          ? import12.debugInjectorWrap(import19.CarroService, () {
              return import19.CarroService(this.injectorGet(import20.Client, viewData.parentIndex));
            })
          : import19.CarroService(this.injectorGet(import20.Client, viewData.parentIndex))));
    }
    return __CarroService_0_6;
  }

  import18.LoginService get _LoginService_0_7 {
    if ((__LoginService_0_7 == null)) {
      (__LoginService_0_7 = import18.LoginService());
    }
    return __LoginService_0_7;
  }

  @override
  ComponentRef<import2.AppComponent> build() {
    _compView_0 = ViewAppComponent0(this, 0);
    rootEl = _compView_0.rootEl;
    _AppComponent_0_5 = import2.AppComponent();
    _compView_0.create(_AppComponent_0_5, projectedNodes);
    init1(rootEl);
    return ComponentRef(0, this, rootEl, _AppComponent_0_5);
  }

  @override
  dynamic injectorGetInternal(dynamic token, int nodeIndex, dynamic notFoundResult) {
    if ((0 == nodeIndex)) {
      if (identical(token, import19.CarroService)) {
        return _CarroService_0_6;
      }
      if (identical(token, import18.LoginService)) {
        return _LoginService_0_7;
      }
    }
    return notFoundResult;
  }

  @override
  void detectChangesInternal() {
    _compView_0.detectChanges();
  }

  @override
  void destroyInternal() {
    _compView_0.destroyInternalState();
  }
}

AppView<import2.AppComponent> viewFactory_AppComponentHost0(AppView<dynamic> parentView, int parentIndex) {
  return _ViewAppComponentHost0(parentView, parentIndex);
}

var _visited = false;
void initReflector() {
  if (_visited) {
    return;
  }
  _visited = true;

  _ngRef.registerComponent(AppComponent, AppComponentNgFactory);
  _ref0.initReflector();
  _ref1.initReflector();
  _ref2.initReflector();
  _ref3.initReflector();
  _ref4.initReflector();
  _ref5.initReflector();
  _ref6.initReflector();
  _ref7.initReflector();
  _ref8.initReflector();
}
