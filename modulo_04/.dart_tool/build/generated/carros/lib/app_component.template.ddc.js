define(['dart_sdk', 'packages/angular/src/bootstrap/modules', 'packages/angular/src/core/change_detection/change_detection', 'packages/angular_router/src/directives/router_outlet_directive', 'packages/carros/src/components/cadastro_component/cadastro_component.css.shim', 'packages/carros/app_component', 'packages/http/src/base_client', 'packages/angular/angular.template', 'packages/angular_router/angular_router.template', 'packages/carros/app_component.css.shim'], (function load__packages__carros__app_component_template(dart_sdk, packages__angular__src__bootstrap__modules, packages__angular__src__core__change_detection__change_detection, packages__angular_router__src__directives__router_outlet_directive, packages__carros__src__components__cadastro_component__cadastro_component$46css$46shim, packages__carros__app_component, packages__http__src__base_client, packages__angular__angular$46template, packages__angular_router__angular_router$46template, packages__carros__app_component$46css$46shim) {
  'use strict';
  const core = dart_sdk.core;
  const html = dart_sdk.html;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const view_type = packages__angular__src__bootstrap__modules.src__core__linker__view_type;
  const dom_helpers = packages__angular__src__bootstrap__modules.src__runtime__dom_helpers;
  const view_container = packages__angular__src__bootstrap__modules.src__core__linker__view_container;
  const app_view_utils = packages__angular__src__bootstrap__modules.src__core__linker__app_view_utils;
  const style_encapsulation = packages__angular__src__bootstrap__modules.src__core__linker__style_encapsulation;
  const app_view = packages__angular__src__bootstrap__modules.src__core__linker__app_view;
  const component_factory = packages__angular__src__bootstrap__modules.src__core__linker__component_factory;
  const optimizations = packages__angular__src__core__change_detection__change_detection.src__runtime__optimizations;
  const errors = packages__angular__src__core__change_detection__change_detection.src__di__errors;
  const reflector = packages__angular__src__core__change_detection__change_detection.src__di__reflector;
  const router_outlet_directive = packages__angular_router__src__directives__router_outlet_directive.src__directives__router_outlet_directive;
  const router_outlet_token = packages__angular_router__src__directives__router_outlet_directive.src__router__router_outlet_token;
  const router = packages__angular_router__src__directives__router_outlet_directive.src__router__router;
  const router_hook = packages__angular_router__src__directives__router_outlet_directive.src__router_hook;
  const routes = packages__carros__src__components__cadastro_component__cadastro_component$46css$46shim.src__routes__routes;
  const carro_service = packages__carros__src__components__cadastro_component__cadastro_component$46css$46shim.src__services__carro_service;
  const login_service = packages__carros__src__components__cadastro_component__cadastro_component$46css$46shim.src__services__login_service;
  const cadastro_component$46template = packages__carros__src__components__cadastro_component__cadastro_component$46css$46shim.src__components__cadastro_component__cadastro_component$46template;
  const carro_service$46template = packages__carros__src__components__cadastro_component__cadastro_component$46css$46shim.src__services__carro_service$46template;
  const login_service$46template = packages__carros__src__components__cadastro_component__cadastro_component$46css$46shim.src__services__login_service$46template;
  const login_component$46template = packages__carros__src__components__cadastro_component__cadastro_component$46css$46shim.src__components__login_component__login_component$46template;
  const navbar_component$46template = packages__carros__src__components__cadastro_component__cadastro_component$46css$46shim.src__components__navbar_component__navbar_component$46template;
  const tabela_component$46template = packages__carros__src__components__cadastro_component__cadastro_component$46css$46shim.src__components__tabela_component__tabela_component$46template;
  const routes$46template = packages__carros__src__components__cadastro_component__cadastro_component$46css$46shim.src__routes__routes$46template;
  const app_component = packages__carros__app_component.app_component;
  const client = packages__http__src__base_client.src__client;
  const angular$46template = packages__angular__angular$46template.angular$46template;
  const angular_router$46template = packages__angular_router__angular_router$46template.angular_router$46template;
  const app_component$46css$46shim = packages__carros__app_component$46css$46shim.app_component$46css$46shim;
  var app_component$46template = Object.create(dart.library);
  var $createElement = dartx.createElement;
  var HtmlElementL = () => (HtmlElementL = dart.constFn(dart.legacy(html.HtmlElement)))();
  var RouterOutletL = () => (RouterOutletL = dart.constFn(dart.legacy(router_outlet_directive.RouterOutlet)))();
  var RouterOutletTokenL = () => (RouterOutletTokenL = dart.constFn(dart.legacy(router_outlet_token.RouterOutletToken)))();
  var RouterL = () => (RouterL = dart.constFn(dart.legacy(router.Router)))();
  var RouterHookL = () => (RouterHookL = dart.constFn(dart.legacy(router_hook.RouterHook)))();
  var VoidToRouterOutletL = () => (VoidToRouterOutletL = dart.constFn(dart.fnType(RouterOutletL(), [])))();
  var CarroServiceL = () => (CarroServiceL = dart.constFn(dart.legacy(carro_service.CarroService)))();
  var ClientL = () => (ClientL = dart.constFn(dart.legacy(client.Client)))();
  var VoidToCarroServiceL = () => (VoidToCarroServiceL = dart.constFn(dart.fnType(CarroServiceL(), [])))();
  var AppComponentL = () => (AppComponentL = dart.constFn(dart.legacy(app_component.AppComponent)))();
  var ComponentRefOfAppComponentL = () => (ComponentRefOfAppComponentL = dart.constFn(component_factory.ComponentRef$(AppComponentL())))();
  var LoginServiceL = () => (LoginServiceL = dart.constFn(dart.legacy(login_service.LoginService)))();
  var ComponentFactoryOfAppComponentL = () => (ComponentFactoryOfAppComponentL = dart.constFn(component_factory.ComponentFactory$(AppComponentL())))();
  var AppViewOfAppComponentL = () => (AppViewOfAppComponentL = dart.constFn(app_view.AppView$(AppComponentL())))();
  var AppViewLOfAppComponentL = () => (AppViewLOfAppComponentL = dart.constFn(dart.legacy(AppViewOfAppComponentL())))();
  var AppViewL = () => (AppViewL = dart.constFn(dart.legacy(app_view.AppView)))();
  var intL = () => (intL = dart.constFn(dart.legacy(core.int)))();
  var AppViewLAndintLToAppViewLOfAppComponentL = () => (AppViewLAndintLToAppViewLOfAppComponentL = dart.constFn(dart.fnType(AppViewLOfAppComponentL(), [AppViewL(), intL()])))();
  const CT = Object.create(null);
  var L0 = "package:carros/app_component.template.dart";
  dart.defineLazy(CT, {
    get C1() {
      return C1 = dart.fn(app_component$46template.viewFactory_AppComponentHost0, AppViewLAndintLToAppViewLOfAppComponentL());
    },
    get C0() {
      return C0 = dart.const({
        __proto__: ComponentFactoryOfAppComponentL().prototype,
        [ComponentFactory__viewFactory]: C1 || CT.C1,
        [ComponentFactory_selector]: "my-app"
      });
    },
    get C2() {
      return C2 = dart.constList([], dart.dynamic);
    }
  }, false);
  var _appEl_0 = dart.privateName(app_component$46template, "_appEl_0");
  var _RouterOutlet_0_8 = dart.privateName(app_component$46template, "_RouterOutlet_0_8");
  app_component$46template.ViewAppComponent0 = class ViewAppComponent0 extends app_view.AppView$(dart.legacy(app_component.AppComponent)) {
    static get _debugComponentUrl() {
      return dart.test(optimizations.isDevMode) ? "asset:carros/lib/app_component.dart" : null;
    }
    build() {
      let _rootEl = this.rootEl;
      let parentRenderNode = this.initViewRoot(_rootEl);
      let doc = html.document;
      let _el_0 = dom_helpers.appendElement(doc, parentRenderNode, "router-outlet");
      this.addShimE(_el_0);
      this[_appEl_0] = new view_container.ViewContainer.new(0, null, this, _el_0);
      this[_RouterOutlet_0_8] = dart.test(optimizations.isDevMode) ? errors.debugInjectorWrap(RouterOutletL(), dart.wrapType(RouterOutletL()), dart.fn(() => new router_outlet_directive.RouterOutlet.new(RouterOutletTokenL().as(this.parentView.injectorGetOptional(dart.wrapType(RouterOutletTokenL()), this.viewData.parentIndex)), this[_appEl_0], RouterL().as(this.parentView.injectorGet(dart.wrapType(RouterL()), this.viewData.parentIndex)), RouterHookL().as(this.parentView.injectorGetOptional(dart.wrapType(RouterHookL()), this.viewData.parentIndex))), VoidToRouterOutletL())) : new router_outlet_directive.RouterOutlet.new(RouterOutletTokenL().as(this.parentView.injectorGetOptional(dart.wrapType(RouterOutletTokenL()), this.viewData.parentIndex)), this[_appEl_0], RouterL().as(this.parentView.injectorGet(dart.wrapType(RouterL()), this.viewData.parentIndex)), RouterHookL().as(this.parentView.injectorGetOptional(dart.wrapType(RouterHookL()), this.viewData.parentIndex)));
      this.init0();
    }
    detectChangesInternal() {
      let firstCheck = this.cdState === 0;
      if (firstCheck) {
        if (routes.Routes.all != null) {
          this[_RouterOutlet_0_8].routes = routes.Routes.all;
        }
      }
      if (!dart.test(app_view_utils.AppViewUtils.throwOnChanges) && firstCheck) {
        this[_RouterOutlet_0_8].ngOnInit();
      }
      this[_appEl_0].detectChangesInNestedViews();
    }
    destroyInternal() {
      this[_appEl_0].destroyNestedViews();
      this[_RouterOutlet_0_8].ngOnDestroy();
    }
    initComponentStyles() {
      let styles = app_component$46template.ViewAppComponent0._componentStyles;
      if (styles == null) {
        app_component$46template.ViewAppComponent0._componentStyles = styles = app_component$46template.ViewAppComponent0._componentStyles = style_encapsulation.ComponentStyles.scoped(app_component$46template.styles$AppComponent, app_component$46template.ViewAppComponent0._debugComponentUrl);
      }
      this.componentStyles = styles;
    }
  };
  (app_component$46template.ViewAppComponent0.new = function(parentView, parentIndex) {
    this[_appEl_0] = null;
    this[_RouterOutlet_0_8] = null;
    app_component$46template.ViewAppComponent0.__proto__.new.call(this, view_type.ViewType.component, parentView, parentIndex, 3);
    this.initComponentStyles();
    this.rootEl = HtmlElementL().as(html.document[$createElement]("my-app"));
  }).prototype = app_component$46template.ViewAppComponent0.prototype;
  dart.addTypeTests(app_component$46template.ViewAppComponent0);
  dart.addTypeCaches(app_component$46template.ViewAppComponent0);
  dart.setLibraryUri(app_component$46template.ViewAppComponent0, L0);
  dart.setFieldSignature(app_component$46template.ViewAppComponent0, () => ({
    __proto__: dart.getFields(app_component$46template.ViewAppComponent0.__proto__),
    [_appEl_0]: dart.fieldType(dart.legacy(view_container.ViewContainer)),
    [_RouterOutlet_0_8]: dart.fieldType(dart.legacy(router_outlet_directive.RouterOutlet))
  }));
  dart.defineLazy(app_component$46template.ViewAppComponent0, {
    /*app_component$46template.ViewAppComponent0._componentStyles*/get _componentStyles() {
      return null;
    },
    set _componentStyles(_) {}
  }, true);
  var _compView_0 = dart.privateName(app_component$46template, "_compView_0");
  var _AppComponent_0_5 = dart.privateName(app_component$46template, "_AppComponent_0_5");
  var __CarroService_0_6 = dart.privateName(app_component$46template, "__CarroService_0_6");
  var __LoginService_0_7 = dart.privateName(app_component$46template, "__LoginService_0_7");
  var _CarroService_0_6 = dart.privateName(app_component$46template, "_CarroService_0_6");
  var _LoginService_0_7 = dart.privateName(app_component$46template, "_LoginService_0_7");
  app_component$46template._ViewAppComponentHost0 = class _ViewAppComponentHost0 extends app_view.AppView$(dart.legacy(app_component.AppComponent)) {
    get [_CarroService_0_6]() {
      if (this[__CarroService_0_6] == null) {
        this[__CarroService_0_6] = dart.test(optimizations.isDevMode) ? errors.debugInjectorWrap(CarroServiceL(), dart.wrapType(CarroServiceL()), dart.fn(() => new carro_service.CarroService.new(ClientL().as(this.injectorGet(dart.wrapType(ClientL()), this.viewData.parentIndex))), VoidToCarroServiceL())) : new carro_service.CarroService.new(ClientL().as(this.injectorGet(dart.wrapType(ClientL()), this.viewData.parentIndex)));
      }
      return this[__CarroService_0_6];
    }
    get [_LoginService_0_7]() {
      if (this[__LoginService_0_7] == null) {
        this[__LoginService_0_7] = new login_service.LoginService.new();
      }
      return this[__LoginService_0_7];
    }
    build() {
      this[_compView_0] = new app_component$46template.ViewAppComponent0.new(this, 0);
      this.rootEl = this[_compView_0].rootEl;
      this[_AppComponent_0_5] = new app_component.AppComponent.new();
      this[_compView_0].create(this[_AppComponent_0_5], this.projectedNodes);
      this.init1(this.rootEl);
      return new (ComponentRefOfAppComponentL()).new(0, this, this.rootEl, this[_AppComponent_0_5]);
    }
    injectorGetInternal(token, nodeIndex, notFoundResult) {
      if (0 === nodeIndex) {
        if (token === dart.wrapType(CarroServiceL())) {
          return this[_CarroService_0_6];
        }
        if (token === dart.wrapType(LoginServiceL())) {
          return this[_LoginService_0_7];
        }
      }
      return notFoundResult;
    }
    detectChangesInternal() {
      this[_compView_0].detectChanges();
    }
    destroyInternal() {
      this[_compView_0].destroyInternalState();
    }
  };
  (app_component$46template._ViewAppComponentHost0.new = function(parentView, parentIndex) {
    this[_compView_0] = null;
    this[_AppComponent_0_5] = null;
    this[__CarroService_0_6] = null;
    this[__LoginService_0_7] = null;
    app_component$46template._ViewAppComponentHost0.__proto__.new.call(this, view_type.ViewType.host, parentView, parentIndex, 3);
    ;
  }).prototype = app_component$46template._ViewAppComponentHost0.prototype;
  dart.addTypeTests(app_component$46template._ViewAppComponentHost0);
  dart.addTypeCaches(app_component$46template._ViewAppComponentHost0);
  dart.setMethodSignature(app_component$46template._ViewAppComponentHost0, () => ({
    __proto__: dart.getMethods(app_component$46template._ViewAppComponentHost0.__proto__),
    injectorGetInternal: dart.fnType(dart.dynamic, [dart.dynamic, dart.legacy(core.int), dart.dynamic])
  }));
  dart.setGetterSignature(app_component$46template._ViewAppComponentHost0, () => ({
    __proto__: dart.getGetters(app_component$46template._ViewAppComponentHost0.__proto__),
    [_CarroService_0_6]: dart.dynamic,
    [_LoginService_0_7]: dart.legacy(login_service.LoginService)
  }));
  dart.setLibraryUri(app_component$46template._ViewAppComponentHost0, L0);
  dart.setFieldSignature(app_component$46template._ViewAppComponentHost0, () => ({
    __proto__: dart.getFields(app_component$46template._ViewAppComponentHost0.__proto__),
    [_compView_0]: dart.fieldType(dart.legacy(app_component$46template.ViewAppComponent0)),
    [_AppComponent_0_5]: dart.fieldType(dart.legacy(app_component.AppComponent)),
    [__CarroService_0_6]: dart.fieldType(dart.dynamic),
    [__LoginService_0_7]: dart.fieldType(dart.legacy(login_service.LoginService))
  }));
  app_component$46template.viewFactory_AppComponentHost0 = function viewFactory_AppComponentHost0(parentView, parentIndex) {
    return new app_component$46template._ViewAppComponentHost0.new(parentView, parentIndex);
  };
  app_component$46template.initReflector = function initReflector() {
    if (dart.test(app_component$46template._visited)) {
      return;
    }
    app_component$46template._visited = true;
    reflector.registerComponent(dart.wrapType(AppComponentL()), app_component$46template.AppComponentNgFactory);
    angular$46template.initReflector();
    angular_router$46template.initReflector();
    cadastro_component$46template.initReflector();
    carro_service$46template.initReflector();
    login_service$46template.initReflector();
    login_component$46template.initReflector();
    navbar_component$46template.initReflector();
    tabela_component$46template.initReflector();
    routes$46template.initReflector();
  };
  dart.copyProperties(app_component$46template, {
    get AppComponentNgFactory() {
      return app_component$46template._AppComponentNgFactory;
    }
  });
  var C1;
  var ComponentFactory__viewFactory = dart.privateName(component_factory, "ComponentFactory._viewFactory");
  var ComponentFactory_selector = dart.privateName(component_factory, "ComponentFactory.selector");
  var C0;
  var C2;
  dart.defineLazy(app_component$46template, {
    /*app_component$46template.styles$AppComponent*/get styles$AppComponent() {
      return [app_component$46css$46shim.styles];
    },
    /*app_component$46template._AppComponentNgFactory*/get _AppComponentNgFactory() {
      return C0 || CT.C0;
    },
    /*app_component$46template.styles$AppComponentHost*/get styles$AppComponentHost() {
      return C2 || CT.C2;
    },
    /*app_component$46template._visited*/get _visited() {
      return false;
    },
    set _visited(_) {}
  }, true);
  dart.trackLibraries("packages/carros/app_component.template", {
    "package:carros/app_component.template.dart": app_component$46template
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["app_component.template.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;AAiDI,uBAAgB,2BAAY,wCAAwC;IACtE;;AAIQ,oBAAU;AACU,6BAAmB,kBAAa,OAAO;AAC3D,gBAAc;AACd,kBAAQ,0BAAuB,GAAG,EAAE,gBAAgB,EAAE;AAC7C,MAAf,cAAS,KAAK;AACgC,MAA9C,iBAAW,qCAAc,GAAG,MAAM,MAAM,KAAK;AAKoN,MAJjQ,oCAA6B,2BACvB,0CAAmC,gCAAc,cAChC,qEAAa,AAAW,oCAA6B,qCAAmB,AAAS,6BAAc,6BAAU,AAAW,4BAAqB,0BAAQ,AAAS,8CAAc,AAAW,oCAA6B,8BAAY,AAAS,wDAE9O,qEAAa,AAAW,oCAA6B,qCAAmB,AAAS,6BAAc,6BAAU,AAAW,4BAAqB,0BAAQ,AAAS,8CAAc,AAAW,oCAA6B,8BAAY,AAAS;AAC5O,MAAP;IACF;;AAIO,uBAAmB,AAAQ,iBAAG;AACnC,UAAI,UAAU;AACZ,YAA+B,qBAAK;AACc,UAA/C,AAAkB,iCAAyB;;;AAGhD,qBAA6B,+CAAmB,UAAU;AAC5B,QAA5B,AAAkB;;AAEiB,MAArC,AAAS;IACX;;AAI+B,MAA7B,AAAS;AACsB,MAA/B,AAAkB;IACpB;;AAIM,mBAAS;AACb,UAAI,AAAU,MAAM,IAAE;AACwG,QAA3H,8DAAoB,SAAU,8DAA2C,2CAAO,8CAAqB;;AAEhF,MAAxB,uBAAkB,MAAM;IAC1B;;6DAnDmC,YAAgB;IAHrC;IACO;AAE6C,wEAAuB,8BAAW,UAAU,EAAE,WAAW;AACpG,IAArB;AACiD,kBAAjD,kBAAiB,AAAS,8BAAc;EAC1C;;;;;;;;;;MAJ+B,2DAAgB;;;;;;;;;;;;;AAqE7C,UAAK,AAAmB,4BAAG;AAK6D,QAJrF,qCAA8B,2BACzB,0CAAoC,gCAAc,cAChC,gDAAa,AAAK,iBAAqB,0BAAQ,AAAS,wDAEjE,gDAAa,AAAK,iBAAqB,0BAAQ,AAAS;;AAEzE,YAAO;IACT;;AAGE,UAAK,AAAmB,4BAAG;AACqB,QAA7C,2BAA8B;;AAEjC,YAAO;IACT;;AAI0C,MAAxC,oBAAc,mDAAkB,MAAM;AACX,MAA3B,cAAS,AAAY;AACqB,MAA1C,0BAA4B;AACyB,MAArD,AAAY,yBAAO,yBAAmB;AACzB,MAAb,WAAM;AACN,YAAO,yCAAa,GAAG,MAAM,aAAQ;IACvC;wBAGoC,OAAW,WAAmB;AAChE,UAAK,AAAE,MAAG,SAAS;AACjB,YAAI,AAAU,KAAK,KAAW;AAC5B,gBAAO;;AAET,YAAI,AAAU,KAAK,KAAW;AAC5B,gBAAO;;;AAGX,YAAO,eAAc;IACvB;;AAI6B,MAA3B,AAAY;IACd;;AAIoC,MAAlC,AAAY;IACd;;kEAlDwC,YAAgB;IAJtC;IACG;IACb;IACc;AACiD,6EAAuB,yBAAM,UAAU,EAAE,WAAW;;EAAsC;;;;;;;;;;;;;;;;;;;;kGAqDtF,YAAgB;AAC3F,UAAO,yDAAuB,UAAU,EAAE,WAAW;EACvD;;AAIE,kBAAI;AACF;;AAEa,IAAf,oCAAW;AAEkD,IAA7D,4BAAyB,gCAAc;AAClB,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;EACF;;;AApFE,YAAO;IACT;;;;;;;;MA/DoB,4CAAmB;YAAG,EAAS;;MA4DN,+CAAsB;;;MAK/C,gDAAuB;;;MAgEvC,iCAAQ;YAAG","file":"app_component.template.ddc.js"}');
  // Exports:
  return {
    app_component$46template: app_component$46template
  };
}));

//# sourceMappingURL=app_component.template.ddc.js.map
