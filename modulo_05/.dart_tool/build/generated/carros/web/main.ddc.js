define(['dart_sdk', 'packages/angular/src/core/change_detection/change_detection', 'packages/carros/src/services/carro_service_in_memory', 'packages/angular_router/src/directives/router_outlet_directive', 'packages/http/src/base_client', 'packages/angular/angular.template', 'packages/angular_router/angular_router.template', 'packages/carros/app_component.template', 'packages/carros/src/services/carro_service_in_memory.template', 'packages/angular/src/bootstrap/modules', 'packages/carros/app_component'], (function load__web__main(dart_sdk, packages__angular__src__core__change_detection__change_detection, packages__carros__src__services__carro_service_in_memory, packages__angular_router__src__directives__router_outlet_directive, packages__http__src__base_client, packages__angular__angular$46template, packages__angular_router__angular_router$46template, packages__carros__app_component$46template, packages__carros__src__services__carro_service_in_memory$46template, packages__angular__src__bootstrap__modules, packages__carros__app_component) {
  'use strict';
  const core = dart_sdk.core;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const hierarchical = packages__angular__src__core__change_detection__change_detection.src__di__injector__hierarchical;
  const opaque_token = packages__angular__src__core__change_detection__change_detection.src__core__di__opaque_token;
  const injector = packages__angular__src__core__change_detection__change_detection.src__di__injector__injector;
  const carro_service_in_memory = packages__carros__src__services__carro_service_in_memory.src__services__carro_service_in_memory;
  const router_impl = packages__angular_router__src__directives__router_outlet_directive.src__router__router_impl;
  const location = packages__angular_router__src__directives__router_outlet_directive.src__location__location;
  const router_hook = packages__angular_router__src__directives__router_outlet_directive.src__router_hook;
  const location_strategy = packages__angular_router__src__directives__router_outlet_directive.src__location__location_strategy;
  const browser_platform_location = packages__angular_router__src__directives__router_outlet_directive.src__location__browser_platform_location;
  const hash_location_strategy = packages__angular_router__src__directives__router_outlet_directive.src__location__hash_location_strategy;
  const platform_location = packages__angular_router__src__directives__router_outlet_directive.src__location__platform_location;
  const router = packages__angular_router__src__directives__router_outlet_directive.src__router__router;
  const client = packages__http__src__base_client.src__client;
  const angular$46template = packages__angular__angular$46template.angular$46template;
  const angular_router$46template = packages__angular_router__angular_router$46template.angular_router$46template;
  const app_component$46template = packages__carros__app_component$46template.app_component$46template;
  const carro_service_in_memory$46template = packages__carros__src__services__carro_service_in_memory$46template.src__services__carro_service_in_memory$46template;
  const run = packages__angular__src__bootstrap__modules.src__bootstrap__run;
  const app_component = packages__carros__app_component.app_component;
  var main$46template = Object.create(dart.library);
  var main = Object.create(dart.library);
  var HierarchicalInjectorL = () => (HierarchicalInjectorL = dart.constFn(dart.legacy(hierarchical.HierarchicalInjector)))();
  var LocationL = () => (LocationL = dart.constFn(dart.legacy(location.Location)))();
  var RouterHookL = () => (RouterHookL = dart.constFn(dart.legacy(router_hook.RouterHook)))();
  var LocationStrategyL = () => (LocationStrategyL = dart.constFn(dart.legacy(location_strategy.LocationStrategy)))();
  var PlatformLocationL = () => (PlatformLocationL = dart.constFn(dart.legacy(platform_location.PlatformLocation)))();
  var StringL = () => (StringL = dart.constFn(dart.legacy(core.String)))();
  var OpaqueTokenOfStringL = () => (OpaqueTokenOfStringL = dart.constFn(opaque_token.OpaqueToken$(StringL())))();
  var ClientL = () => (ClientL = dart.constFn(dart.legacy(client.Client)))();
  var RouterL = () => (RouterL = dart.constFn(dart.legacy(router.Router)))();
  var InjectorL = () => (InjectorL = dart.constFn(dart.legacy(injector.Injector)))();
  var AppComponentL = () => (AppComponentL = dart.constFn(dart.legacy(app_component.AppComponent)))();
  var InjectorLToInjectorL = () => (InjectorLToInjectorL = dart.constFn(dart.fnType(InjectorL(), [], [InjectorL()])))();
  const CT = Object.create(null);
  var L0 = "org-dartlang-app:///web/main.template.dart";
  dart.defineLazy(CT, {
    get C0() {
      return C0 = dart.const({
        __proto__: OpaqueTokenOfStringL().prototype,
        [OpaqueToken__uniqueName]: "appBaseHref"
      });
    },
    get C1() {
      return C1 = dart.const({
        __proto__: core.Object.prototype
      });
    },
    get C2() {
      return C2 = dart.fn(main$46template.injector$Injector, InjectorLToInjectorL());
    }
  }, false);
  var _field0 = dart.privateName(main$46template, "_field0");
  var _field1 = dart.privateName(main$46template, "_field1");
  var _field2 = dart.privateName(main$46template, "_field2");
  var _field3 = dart.privateName(main$46template, "_field3");
  var _field4 = dart.privateName(main$46template, "_field4");
  var _getInMemoryDataService$0 = dart.privateName(main$46template, "_getInMemoryDataService$0");
  var _getRouterImpl$1 = dart.privateName(main$46template, "_getRouterImpl$1");
  var _getLocation$2 = dart.privateName(main$46template, "_getLocation$2");
  var _getBrowserPlatformLocation$3 = dart.privateName(main$46template, "_getBrowserPlatformLocation$3");
  var OpaqueToken__uniqueName = dart.privateName(opaque_token, "OpaqueToken._uniqueName");
  var C0;
  var _getHashLocationStrategy$4 = dart.privateName(main$46template, "_getHashLocationStrategy$4");
  var _getInjector$5 = dart.privateName(main$46template, "_getInjector$5");
  var C1;
  main$46template._Injector$injector = class _Injector$36injector extends hierarchical.HierarchicalInjector {
    [_getInMemoryDataService$0]() {
      let t0;
      t0 = this[_field0];
      return t0 == null ? this[_field0] = new carro_service_in_memory.InMemoryDataService.new() : t0;
    }
    [_getRouterImpl$1]() {
      let t0;
      t0 = this[_field1];
      return t0 == null ? this[_field1] = new router_impl.RouterImpl.new(LocationL().as(this.get(dart.wrapType(LocationL()))), RouterHookL().as(this.provideUntyped(dart.wrapType(RouterHookL()), null))) : t0;
    }
    [_getLocation$2]() {
      let t0;
      t0 = this[_field2];
      return t0 == null ? this[_field2] = new location.Location.new(LocationStrategyL().as(this.get(dart.wrapType(LocationStrategyL())))) : t0;
    }
    [_getBrowserPlatformLocation$3]() {
      let t0;
      t0 = this[_field3];
      return t0 == null ? this[_field3] = new browser_platform_location.BrowserPlatformLocation.new() : t0;
    }
    [_getHashLocationStrategy$4]() {
      let t0;
      t0 = this[_field4];
      return t0 == null ? this[_field4] = new hash_location_strategy.HashLocationStrategy.new(PlatformLocationL().as(this.get(dart.wrapType(PlatformLocationL()))), StringL().as(this.provideUntyped(C0 || CT.C0, null))) : t0;
    }
    [_getInjector$5]() {
      return this;
    }
    injectFromSelfOptional(token, orElse = C1 || CT.C1) {
      if (token === dart.wrapType(ClientL())) {
        return this[_getInMemoryDataService$0]();
      }
      if (token === dart.wrapType(RouterL())) {
        return this[_getRouterImpl$1]();
      }
      if (token === dart.wrapType(LocationL())) {
        return this[_getLocation$2]();
      }
      if (token === dart.wrapType(PlatformLocationL())) {
        return this[_getBrowserPlatformLocation$3]();
      }
      if (token === dart.wrapType(LocationStrategyL())) {
        return this[_getHashLocationStrategy$4]();
      }
      if (token === dart.wrapType(InjectorL())) {
        return this[_getInjector$5]();
      }
      return orElse;
    }
  };
  (main$46template._Injector$injector.__ = function(parent = null) {
    this[_field0] = null;
    this[_field1] = null;
    this[_field2] = null;
    this[_field3] = null;
    this[_field4] = null;
    main$46template._Injector$injector.__proto__.new.call(this, HierarchicalInjectorL().as(parent));
    ;
  }).prototype = main$46template._Injector$injector.prototype;
  dart.addTypeTests(main$46template._Injector$injector);
  dart.addTypeCaches(main$46template._Injector$injector);
  dart.setMethodSignature(main$46template._Injector$injector, () => ({
    __proto__: dart.getMethods(main$46template._Injector$injector.__proto__),
    [_getInMemoryDataService$0]: dart.fnType(dart.legacy(carro_service_in_memory.InMemoryDataService), []),
    [_getRouterImpl$1]: dart.fnType(dart.legacy(router_impl.RouterImpl), []),
    [_getLocation$2]: dart.fnType(dart.legacy(location.Location), []),
    [_getBrowserPlatformLocation$3]: dart.fnType(dart.legacy(browser_platform_location.BrowserPlatformLocation), []),
    [_getHashLocationStrategy$4]: dart.fnType(dart.legacy(hash_location_strategy.HashLocationStrategy), []),
    [_getInjector$5]: dart.fnType(dart.legacy(injector.Injector), []),
    injectFromSelfOptional: dart.fnType(dart.legacy(core.Object), [dart.legacy(core.Object)], [dart.legacy(core.Object)])
  }));
  dart.setLibraryUri(main$46template._Injector$injector, L0);
  dart.setFieldSignature(main$46template._Injector$injector, () => ({
    __proto__: dart.getFields(main$46template._Injector$injector.__proto__),
    [_field0]: dart.fieldType(dart.legacy(carro_service_in_memory.InMemoryDataService)),
    [_field1]: dart.fieldType(dart.legacy(router_impl.RouterImpl)),
    [_field2]: dart.fieldType(dart.legacy(location.Location)),
    [_field3]: dart.fieldType(dart.legacy(browser_platform_location.BrowserPlatformLocation)),
    [_field4]: dart.fieldType(dart.legacy(hash_location_strategy.HashLocationStrategy))
  }));
  main$46template.injector$Injector = function injector$36Injector(parent = null) {
    return new main$46template._Injector$injector.__(parent);
  };
  main$46template.initReflector = function initReflector() {
    if (dart.test(main$46template._visited)) {
      return;
    }
    main$46template._visited = true;
    main$46template.initReflector();
    angular$46template.initReflector();
    angular_router$46template.initReflector();
    app_component$46template.initReflector();
    carro_service_in_memory$46template.initReflector();
  };
  dart.defineLazy(main$46template, {
    /*main$46template._visited*/get _visited() {
      return false;
    },
    set _visited(_) {}
  }, true);
  main.main = function main$() {
    run.runApp(AppComponentL(), app_component$46template.AppComponentNgFactory, {createInjector: main.injector});
  };
  var C2;
  dart.defineLazy(main, {
    /*main.injector*/get injector() {
      return C2 || CT.C2;
    }
  }, true);
  main$46template.main = main.main;
  dart.trackLibraries("web/main", {
    "org-dartlang-app:///web/main.template.dart": main$46template,
    "org-dartlang-app:///web/main.dart": main
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["main.template.dart","main.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;AAwCyD;mBAAQ,OAAR,gBAAgB;IAAqB;;;AACvD;mBAAQ,OAAR,gBAAgB,8CAAW,AAAK,SAAQ,+CAAW,oBAAmB,8BAAY;IAAM;;;AAC5F;mBAAQ,OAAR,gBAAgB,iDAAS,AAAK,SAAQ;IAAkB;;;AAC1B;mBAAQ,OAAR,gBAAgB;IAAyB;;;AAC/C;mBAAQ,OAAR,gBAAgB,2EAAqB,AAAK,SAAS,mDAAmB,iCAA8D;IAAM;;AAClK;IAAI;2BAEA,OAAe;AAClD,UAAI,AAAU,KAAK,KAAO;AACxB,cAAO;;AAET,UAAI,AAAU,KAAK,KAAO;AACxB,cAAO;;AAET,UAAI,AAAU,KAAK,KAAM;AACvB,cAAO;;AAET,UAAI,AAAU,KAAK,KAAO;AACxB,cAAO;;AAET,UAAI,AAAU,KAAK,KAAM;AACvB,cAAO;;AAET,UAAI,AAAU,KAAK,KAAM;AACvB,cAAO;;AAET,YAAO,OAAM;IACf;;oDAvCmC;IAEX;IAET;IAEF;IAEe;IAEH;AAVqB,2FAAM,MAAM;;EAAC;;;;;;;;;;;;;;;;;;;;;;mEAHhB;AAAY,UAAmB,2CAAE,MAAM;EAAC;;AA+CnF,kBAAI;AACF;;AAEa,IAAf,2BAAW;AAEU,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;AACqB,IAArB;EACF;;MAZI,wBAAQ;YAAG;;;;;ACvD6C,IAA1D,4BAAU,iEAAuC;EACnD;;;MAJsB,aAAQ","file":"main.ddc.js"}');
  // Exports:
  return {
    web__main$46template: main$46template,
    web__main: main
  };
}));

//# sourceMappingURL=main.ddc.js.map
