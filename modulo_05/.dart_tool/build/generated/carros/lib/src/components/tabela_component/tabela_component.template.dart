// **************************************************************************
// Generator: AngularDart Compiler
// **************************************************************************

import 'tabela_component.dart';
export 'tabela_component.dart';
import 'package:angular/src/di/reflector.dart' as _ngRef;
import 'package:angular/angular.template.dart' as _ref0;
import 'package:carros/src/model/carro.template.dart' as _ref1;
import 'package:carros/src/services/carro_service.template.dart' as _ref2;
import 'package:carros/src/components/tabela_component/tabela_component.css.shim.dart' as import0;
import 'package:angular/src/core/linker/app_view.dart';
import 'tabela_component.dart' as import2;
import 'package:angular/src/core/linker/view_container.dart';
import 'package:angular/src/common/directives/ng_for.dart' as import4;
import 'package:angular/src/core/linker/style_encapsulation.dart' as import5;
import 'package:angular/src/core/linker/view_type.dart' as import6;
import 'package:angular/src/core/change_detection/change_detection.dart';
import 'dart:html' as import8;
import 'package:angular/src/runtime.dart' as import9;
import 'package:angular/angular.dart';
import 'package:angular/src/runtime/dom_helpers.dart' as import11;
import 'package:angular/src/core/linker/template_ref.dart';
import 'package:angular/src/core/linker/app_view_utils.dart' as import13;
import 'package:angular/src/runtime/text_binding.dart' as import14;
import '../../model/carro.dart' as import15;
import 'package:angular/src/runtime/interpolate.dart' as import16;
import 'package:angular/src/di/errors.dart' as import17;
import '../../services/carro_service.dart' as import18;

final List<dynamic> styles$TabelaComponent = [import0.styles];

class ViewTabelaComponent0 extends AppView<import2.TabelaComponent> {
  ViewContainer _appEl_18;
  import4.NgFor _NgFor_18_9;
  var _expr_0;
  static import5.ComponentStyles _componentStyles;
  ViewTabelaComponent0(AppView<dynamic> parentView, int parentIndex) : super(import6.ViewType.component, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways) {
    initComponentStyles();
    rootEl = import8.document.createElement('tabela');
  }
  static String get _debugComponentUrl {
    return (import9.isDevMode ? 'asset:carros/lib/src/components/tabela_component/tabela_component.dart' : null);
  }

  @override
  ComponentRef<import2.TabelaComponent> build() {
    final _rootEl = rootEl;
    final import8.HtmlElement parentRenderNode = initViewRoot(_rootEl);
    final doc = import8.document;
    final _el_0 = import11.appendDiv(doc, parentRenderNode);
    this.updateChildClass(_el_0, 'row');
    import11.setAttribute(_el_0, 'id', 'tabela-carro');
    addShimC(_el_0);
    final _el_1 = import11.appendElement(doc, _el_0, 'table');
    this.updateChildClass(_el_1, 'table');
    addShimC(_el_1);
    final _el_2 = import11.appendElement(doc, _el_1, 'thead');
    this.updateChildClass(_el_2, 'thead-dark');
    addShimE(_el_2);
    final _el_3 = import11.appendElement(doc, _el_2, 'tr');
    addShimE(_el_3);
    final _el_4 = import11.appendElement(doc, _el_3, 'th');
    import11.setAttribute(_el_4, 'scope', 'col');
    addShimE(_el_4);
    final _text_5 = import11.appendText(_el_4, 'Código');
    final _el_6 = import11.appendElement(doc, _el_3, 'th');
    import11.setAttribute(_el_6, 'scope', 'col');
    addShimE(_el_6);
    final _text_7 = import11.appendText(_el_6, 'Nome do Carro');
    final _el_8 = import11.appendElement(doc, _el_3, 'th');
    import11.setAttribute(_el_8, 'scope', 'col');
    addShimE(_el_8);
    final _text_9 = import11.appendText(_el_8, 'Fabricante');
    final _el_10 = import11.appendElement(doc, _el_3, 'th');
    import11.setAttribute(_el_10, 'scope', 'col');
    addShimE(_el_10);
    final _text_11 = import11.appendText(_el_10, 'Ano');
    final _el_12 = import11.appendElement(doc, _el_3, 'th');
    import11.setAttribute(_el_12, 'scope', 'col');
    addShimE(_el_12);
    final _text_13 = import11.appendText(_el_12, 'Preço');
    final _el_14 = import11.appendElement(doc, _el_3, 'th');
    import11.setAttribute(_el_14, 'scope', 'col');
    addShimE(_el_14);
    final _text_15 = import11.appendText(_el_14, 'Imagem');
    final _el_16 = import11.appendElement(doc, _el_3, 'th');
    import11.setAttribute(_el_16, 'scope', 'col');
    addShimE(_el_16);
    final _el_17 = import11.appendElement(doc, _el_1, 'tbody');
    addShimE(_el_17);
    final _anchor_18 = import11.appendAnchor(_el_17);
    _appEl_18 = ViewContainer(18, 17, this, _anchor_18);
    TemplateRef _TemplateRef_18_8 = TemplateRef(_appEl_18, viewFactory_TabelaComponent1);
    _NgFor_18_9 = import4.NgFor(_appEl_18, _TemplateRef_18_8);
    init0();
  }

  @override
  void detectChangesInternal() {
    final _ctx = ctx;
    final currVal_0 = _ctx.listaCarros;
    if (import13.checkBinding(_expr_0, currVal_0)) {
      _NgFor_18_9.ngForOf = currVal_0;
      _expr_0 = currVal_0;
    }
    if ((!import13.AppViewUtils.throwOnChanges)) {
      _NgFor_18_9.ngDoCheck();
    }
    _appEl_18.detectChangesInNestedViews();
  }

  @override
  void destroyInternal() {
    _appEl_18.destroyNestedViews();
  }

  @override
  void initComponentStyles() {
    var styles = _componentStyles;
    if (identical(styles, null)) {
      (_componentStyles = (styles = (_componentStyles = import5.ComponentStyles.scoped(styles$TabelaComponent, _debugComponentUrl))));
    }
    componentStyles = styles;
  }
}

const ComponentFactory<import2.TabelaComponent> _TabelaComponentNgFactory = const ComponentFactory('tabela', viewFactory_TabelaComponentHost0);
ComponentFactory<import2.TabelaComponent> get TabelaComponentNgFactory {
  return _TabelaComponentNgFactory;
}

class _ViewTabelaComponent1 extends AppView<import2.TabelaComponent> {
  final import14.TextBinding _textBinding_2 = import14.TextBinding();
  final import14.TextBinding _textBinding_4 = import14.TextBinding();
  final import14.TextBinding _textBinding_6 = import14.TextBinding();
  final import14.TextBinding _textBinding_8 = import14.TextBinding();
  final import14.TextBinding _textBinding_10 = import14.TextBinding();
  var _expr_0;
  import8.Element _el_12;
  _ViewTabelaComponent1(AppView<dynamic> parentView, int parentIndex) : super(import6.ViewType.embedded, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways) {
    initComponentStyles();
  }
  @override
  ComponentRef<import2.TabelaComponent> build() {
    final doc = import8.document;
    final _el_0 = doc.createElement('tr');
    addShimE(_el_0);
    final _el_1 = import11.appendElement(doc, _el_0, 'th');
    import11.setAttribute(_el_1, 'scope', 'row');
    addShimE(_el_1);
    _el_1.append(_textBinding_2.element);
    final _el_3 = import11.appendElement(doc, _el_0, 'td');
    addShimE(_el_3);
    _el_3.append(_textBinding_4.element);
    final _el_5 = import11.appendElement(doc, _el_0, 'td');
    addShimE(_el_5);
    _el_5.append(_textBinding_6.element);
    final _el_7 = import11.appendElement(doc, _el_0, 'td');
    addShimE(_el_7);
    _el_7.append(_textBinding_8.element);
    final _el_9 = import11.appendElement(doc, _el_0, 'td');
    addShimE(_el_9);
    _el_9.append(_textBinding_10.element);
    final _el_11 = import11.appendElement(doc, _el_0, 'td');
    addShimE(_el_11);
    _el_12 = import11.appendElement(doc, _el_11, 'img');
    import11.setAttribute(_el_12, 'alt', 'Responsive image');
    this.updateChildClass(_el_12, 'img-fluid img-carro');
    addShimE(_el_12);
    final _el_13 = import11.appendElement(doc, _el_0, 'td');
    addShimE(_el_13);
    final _el_14 = import11.appendElement(doc, _el_13, 'i');
    import11.setAttribute(_el_14, 'aria-hidden', 'true');
    this.updateChildClass(_el_14, 'fa fa-trash');
    addShimE(_el_14);
    _el_1.addEventListener('click', eventHandler1(_handle_click_1_0));
    _el_3.addEventListener('click', eventHandler1(_handle_click_3_0));
    _el_5.addEventListener('click', eventHandler1(_handle_click_5_0));
    _el_7.addEventListener('click', eventHandler1(_handle_click_7_0));
    _el_9.addEventListener('click', eventHandler1(_handle_click_9_0));
    _el_11.addEventListener('click', eventHandler1(_handle_click_11_0));
    _el_14.addEventListener('click', eventHandler1(_handle_click_14_0));
    init1(_el_0);
  }

  @override
  void detectChangesInternal() {
    final local_carro = import9.unsafeCast<import15.Carro>(locals['\$implicit']);
    _textBinding_2.updateText(import16.interpolate0(local_carro.codigoCarro));
    _textBinding_4.updateText(import16.interpolate0(local_carro.nomeCarro));
    _textBinding_6.updateText(import16.interpolate0(local_carro.nomeFabricante));
    _textBinding_8.updateText(import16.interpolate0(local_carro.anoFabricacao));
    _textBinding_10.updateText(import16.interpolate0(local_carro.preco));
    final currVal_0 = import16.interpolate0(local_carro.imagem);
    if (import13.checkBinding(_expr_0, currVal_0)) {
      import11.setProperty(_el_12, 'src', import13.appViewUtils.sanitizer.sanitizeUrl(currVal_0));
      _expr_0 = currVal_0;
    }
  }

  void _handle_click_1_0($event) {
    final local_carro = import9.unsafeCast<import15.Carro>(locals['\$implicit']);
    final _ctx = ctx;
    _ctx.SelecionarCarro(local_carro);
  }

  void _handle_click_3_0($event) {
    final local_carro = import9.unsafeCast<import15.Carro>(locals['\$implicit']);
    final _ctx = ctx;
    _ctx.SelecionarCarro(local_carro);
  }

  void _handle_click_5_0($event) {
    final local_carro = import9.unsafeCast<import15.Carro>(locals['\$implicit']);
    final _ctx = ctx;
    _ctx.SelecionarCarro(local_carro);
  }

  void _handle_click_7_0($event) {
    final local_carro = import9.unsafeCast<import15.Carro>(locals['\$implicit']);
    final _ctx = ctx;
    _ctx.SelecionarCarro(local_carro);
  }

  void _handle_click_9_0($event) {
    final local_carro = import9.unsafeCast<import15.Carro>(locals['\$implicit']);
    final _ctx = ctx;
    _ctx.SelecionarCarro(local_carro);
  }

  void _handle_click_11_0($event) {
    final local_carro = import9.unsafeCast<import15.Carro>(locals['\$implicit']);
    final _ctx = ctx;
    _ctx.SelecionarCarro(local_carro);
  }

  void _handle_click_14_0($event) {
    final local_carro = import9.unsafeCast<import15.Carro>(locals['\$implicit']);
    final _ctx = ctx;
    _ctx.DeletarCarro(local_carro);
  }
}

AppView<void> viewFactory_TabelaComponent1(AppView<dynamic> parentView, int parentIndex) {
  return _ViewTabelaComponent1(parentView, parentIndex);
}

final List<dynamic> styles$TabelaComponentHost = const [];

class _ViewTabelaComponentHost0 extends AppView<import2.TabelaComponent> {
  ViewTabelaComponent0 _compView_0;
  import2.TabelaComponent _TabelaComponent_0_5;
  _ViewTabelaComponentHost0(AppView<dynamic> parentView, int parentIndex) : super(import6.ViewType.host, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways);
  @override
  ComponentRef<import2.TabelaComponent> build() {
    _compView_0 = ViewTabelaComponent0(this, 0);
    rootEl = _compView_0.rootEl;
    _TabelaComponent_0_5 = (import9.isDevMode
        ? import17.debugInjectorWrap(import2.TabelaComponent, () {
            return import2.TabelaComponent(this.injectorGet(import18.CarroService, viewData.parentIndex));
          })
        : import2.TabelaComponent(this.injectorGet(import18.CarroService, viewData.parentIndex)));
    _compView_0.create(_TabelaComponent_0_5, projectedNodes);
    init1(rootEl);
    return ComponentRef(0, this, rootEl, _TabelaComponent_0_5);
  }

  @override
  void detectChangesInternal() {
    bool firstCheck = (this.cdState == 0);
    if (((!import13.AppViewUtils.throwOnChanges) && firstCheck)) {
      _TabelaComponent_0_5.ngOnInit();
    }
    _compView_0.detectChanges();
  }

  @override
  void destroyInternal() {
    _compView_0.destroyInternalState();
  }
}

AppView<import2.TabelaComponent> viewFactory_TabelaComponentHost0(AppView<dynamic> parentView, int parentIndex) {
  return _ViewTabelaComponentHost0(parentView, parentIndex);
}

var _visited = false;
void initReflector() {
  if (_visited) {
    return;
  }
  _visited = true;

  _ngRef.registerComponent(TabelaComponent, TabelaComponentNgFactory);
  _ref0.initReflector();
  _ref1.initReflector();
  _ref2.initReflector();
}
