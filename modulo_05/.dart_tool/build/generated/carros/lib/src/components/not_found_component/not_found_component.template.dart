// **************************************************************************
// Generator: AngularDart Compiler
// **************************************************************************

import 'not_found_component.dart';
export 'not_found_component.dart';
import 'package:angular/src/di/reflector.dart' as _ngRef;
import 'package:angular/angular.template.dart' as _ref0;
import 'package:carros/src/components/not_found_component/not_found_component.css.shim.dart' as import0;
import 'package:angular/src/core/linker/app_view.dart';
import 'not_found_component.dart' as import2;
import 'package:angular/src/core/linker/style_encapsulation.dart' as import3;
import 'package:angular/src/core/linker/view_type.dart' as import4;
import 'package:angular/src/core/change_detection/change_detection.dart';
import 'dart:html' as import6;
import 'package:angular/src/runtime.dart' as import7;
import 'package:angular/angular.dart';
import 'package:angular/src/runtime/dom_helpers.dart' as import9;

final List<dynamic> styles$NotFoundComponent = [import0.styles];

class ViewNotFoundComponent0 extends AppView<import2.NotFoundComponent> {
  static import3.ComponentStyles _componentStyles;
  ViewNotFoundComponent0(AppView<dynamic> parentView, int parentIndex) : super(import4.ViewType.component, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways) {
    initComponentStyles();
    rootEl = import6.document.createElement('not-found');
  }
  static String get _debugComponentUrl {
    return (import7.isDevMode ? 'asset:carros/lib/src/components/not_found_component/not_found_component.dart' : null);
  }

  @override
  ComponentRef<import2.NotFoundComponent> build() {
    final _rootEl = rootEl;
    final import6.HtmlElement parentRenderNode = initViewRoot(_rootEl);
    final doc = import6.document;
    final _el_0 = import9.appendElement(doc, parentRenderNode, 'h1');
    addShimE(_el_0);
    final _text_1 = import9.appendText(_el_0, 'Página Não Existe');
    init0();
  }

  @override
  void initComponentStyles() {
    var styles = _componentStyles;
    if (identical(styles, null)) {
      (_componentStyles = (styles = (_componentStyles = import3.ComponentStyles.scoped(styles$NotFoundComponent, _debugComponentUrl))));
    }
    componentStyles = styles;
  }
}

const ComponentFactory<import2.NotFoundComponent> _NotFoundComponentNgFactory = const ComponentFactory('not-found', viewFactory_NotFoundComponentHost0);
ComponentFactory<import2.NotFoundComponent> get NotFoundComponentNgFactory {
  return _NotFoundComponentNgFactory;
}

final List<dynamic> styles$NotFoundComponentHost = const [];

class _ViewNotFoundComponentHost0 extends AppView<import2.NotFoundComponent> {
  ViewNotFoundComponent0 _compView_0;
  import2.NotFoundComponent _NotFoundComponent_0_5;
  _ViewNotFoundComponentHost0(AppView<dynamic> parentView, int parentIndex) : super(import4.ViewType.host, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways);
  @override
  ComponentRef<import2.NotFoundComponent> build() {
    _compView_0 = ViewNotFoundComponent0(this, 0);
    rootEl = _compView_0.rootEl;
    _NotFoundComponent_0_5 = import2.NotFoundComponent();
    _compView_0.create(_NotFoundComponent_0_5, projectedNodes);
    init1(rootEl);
    return ComponentRef(0, this, rootEl, _NotFoundComponent_0_5);
  }

  @override
  void detectChangesInternal() {
    _compView_0.detectChanges();
  }

  @override
  void destroyInternal() {
    _compView_0.destroyInternalState();
  }
}

AppView<import2.NotFoundComponent> viewFactory_NotFoundComponentHost0(AppView<dynamic> parentView, int parentIndex) {
  return _ViewNotFoundComponentHost0(parentView, parentIndex);
}

var _visited = false;
void initReflector() {
  if (_visited) {
    return;
  }
  _visited = true;

  _ngRef.registerComponent(NotFoundComponent, NotFoundComponentNgFactory);
  _ref0.initReflector();
}
