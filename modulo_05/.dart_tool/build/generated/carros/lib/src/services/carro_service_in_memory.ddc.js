define(['dart_sdk', 'packages/http/src/base_client', 'packages/carros/src/bloc/login_component/login_component_bloc', 'packages/http/src/mock_client'], (function load__packages__carros__src__services__carro_service_in_memory(dart_sdk, packages__http__src__base_client, packages__carros__src__bloc__login_component__login_component_bloc, packages__http__src__mock_client) {
  'use strict';
  const core = dart_sdk.core;
  const async = dart_sdk.async;
  const convert = dart_sdk.convert;
  const _js_helper = dart_sdk._js_helper;
  const math = dart_sdk.math;
  const _interceptors = dart_sdk._interceptors;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const response = packages__http__src__base_client.src__response;
  const request = packages__http__src__base_client.src__request;
  const carro = packages__carros__src__bloc__login_component__login_component_bloc.src__model__carro;
  const mock_client = packages__http__src__mock_client.src__mock_client;
  var carro_service_in_memory = Object.create(dart.library);
  var $last = dartx.last;
  var $firstWhere = dartx.firstWhere;
  var $_get = dartx._get;
  var $where = dartx.where;
  var $toList = dartx.toList;
  var $add = dartx.add;
  var $remove = dartx.remove;
  var $map = dartx.map;
  var $fold = dartx.fold;
  var ResponseL = () => (ResponseL = dart.constFn(dart.legacy(response.Response)))();
  var FutureOfResponseL = () => (FutureOfResponseL = dart.constFn(async.Future$(ResponseL())))();
  var FutureLOfResponseL = () => (FutureLOfResponseL = dart.constFn(dart.legacy(FutureOfResponseL())))();
  var RequestL = () => (RequestL = dart.constFn(dart.legacy(request.Request)))();
  var RequestLToFutureLOfResponseL = () => (RequestLToFutureLOfResponseL = dart.constFn(dart.fnType(FutureLOfResponseL(), [RequestL()])))();
  var boolL = () => (boolL = dart.constFn(dart.legacy(core.bool)))();
  var CarroL = () => (CarroL = dart.constFn(dart.legacy(carro.Carro)))();
  var CarroLToboolL = () => (CarroLToboolL = dart.constFn(dart.fnType(boolL(), [CarroL()])))();
  var StringL = () => (StringL = dart.constFn(dart.legacy(core.String)))();
  var MapOfStringL$dynamic = () => (MapOfStringL$dynamic = dart.constFn(core.Map$(StringL(), dart.dynamic)))();
  var MapLOfStringL$dynamic = () => (MapLOfStringL$dynamic = dart.constFn(dart.legacy(MapOfStringL$dynamic())))();
  var IdentityMapOfStringL$dynamic = () => (IdentityMapOfStringL$dynamic = dart.constFn(_js_helper.IdentityMap$(StringL(), dart.dynamic)))();
  var IdentityMapOfStringL$StringL = () => (IdentityMapOfStringL$StringL = dart.constFn(_js_helper.IdentityMap$(StringL(), StringL())))();
  var ObjectL = () => (ObjectL = dart.constFn(dart.legacy(core.Object)))();
  var MapOfStringL$ObjectL = () => (MapOfStringL$ObjectL = dart.constFn(core.Map$(StringL(), ObjectL())))();
  var MapLOfStringL$ObjectL = () => (MapLOfStringL$ObjectL = dart.constFn(dart.legacy(MapOfStringL$ObjectL())))();
  var MapLOfStringL$ObjectLToCarroL = () => (MapLOfStringL$ObjectLToCarroL = dart.constFn(dart.fnType(CarroL(), [MapLOfStringL$ObjectL()])))();
  var intL = () => (intL = dart.constFn(dart.legacy(core.int)))();
  var CarroLTointL = () => (CarroLTointL = dart.constFn(dart.fnType(intL(), [CarroL()])))();
  var TAndTToT = () => (TAndTToT = dart.constFn(dart.gFnType(T => [T, [T, T]], T => [core.num])))();
  var IdentityMapOfStringL$ObjectL = () => (IdentityMapOfStringL$ObjectL = dart.constFn(_js_helper.IdentityMap$(StringL(), ObjectL())))();
  var JSArrayOfMapLOfStringL$ObjectL = () => (JSArrayOfMapLOfStringL$ObjectL = dart.constFn(_interceptors.JSArray$(MapLOfStringL$ObjectL())))();
  const CT = Object.create(null);
  var L0 = "package:carros/src/services/carro_service_in_memory.dart";
  dart.defineLazy(CT, {
    get C0() {
      return C0 = dart.fn(carro_service_in_memory.InMemoryDataService._handler, RequestLToFutureLOfResponseL());
    },
    get C1() {
      return C1 = dart.fn(math.max, TAndTToT());
    }
  }, false);
  var C0;
  var C1;
  carro_service_in_memory.InMemoryDataService = class InMemoryDataService extends mock_client.MockClient {
    static _handler(request) {
      return async.async(ResponseL(), function* _handler() {
        let t0, t0$;
        if (carro_service_in_memory.InMemoryDataService._carroDb == null) carro_service_in_memory.InMemoryDataService.resetLista();
        let data = null;
        switch (request.method) {
          case "GET":
          {
            let codigo = core.int.tryParse(request.url.pathSegments[$last]);
            if (codigo != null) {
              data = carro_service_in_memory.InMemoryDataService._carroDb[$firstWhere](dart.fn(carro => dart.equals(carro.codigoCarro, codigo), CarroLToboolL()));
            } else {
              let prefix = (t0 = request.url.queryParameters[$_get]("nomeCarro"), t0 == null ? "" : t0);
              let regExp = core.RegExp.new(prefix, {caseSensitive: false});
              data = carro_service_in_memory.InMemoryDataService._carroDb[$where](dart.fn(carro => boolL().as(dart.dsend(carro.nomeCarro, 'contains', [regExp])), CarroLToboolL()))[$toList]();
            }
            break;
          }
          case "POST":
          {
            let carroMap = dart.dsend(convert.json.decode(request.body), '_get', ["carro"]);
            let novoCarro = carro.Carro.fromMap(MapLOfStringL$dynamic().as(carroMap));
            novoCarro.codigoCarro = (t0$ = carro_service_in_memory.InMemoryDataService._nextCodigo, carro_service_in_memory.InMemoryDataService._nextCodigo = dart.notNull(t0$) + 1, t0$);
            carro_service_in_memory.InMemoryDataService._carroDb[$add](novoCarro);
            data = carro_service_in_memory.InMemoryDataService._carroDb;
            break;
          }
          case "PUT":
          {
            let carroAlteracaoMap = convert.json.decode(request.body);
            let carroAlteracao = carro.Carro.fromMap(MapLOfStringL$dynamic().as(carroAlteracaoMap));
            let carroTarget = carro_service_in_memory.InMemoryDataService._carroDb[$firstWhere](dart.fn(carro => dart.equals(carro.codigoCarro, carroAlteracao.codigoCarro), CarroLToboolL()));
            carroTarget.CloneCarro(carroAlteracao);
            data = carro_service_in_memory.InMemoryDataService._carroDb;
            break;
          }
          case "DELETE":
          {
            let codigoCarroDeletar = core.int.tryParse(request.url.pathSegments[$last]);
            let carroDeletar = carro_service_in_memory.InMemoryDataService._carroDb[$firstWhere](dart.fn(carro => dart.equals(carro.codigoCarro, codigoCarroDeletar), CarroLToboolL()));
            carro_service_in_memory.InMemoryDataService._carroDb[$remove](carroDeletar);
            data = carro_service_in_memory.InMemoryDataService._carroDb;
            break;
          }
          default:
          {
            dart.throw("Método HTTP não implementado " + dart.str(request.method));
          }
        }
        return new response.Response.new(convert.json.encode(new (IdentityMapOfStringL$dynamic()).from(["data", data])), 200, {headers: new (IdentityMapOfStringL$StringL()).from(["content-type", "application/json"])});
      });
    }
    static resetLista() {
      carro_service_in_memory.InMemoryDataService._carroDb = carro_service_in_memory.InMemoryDataService._initialCarros[$map](CarroL(), dart.fn(carroJson => carro.Carro.fromMap(carroJson), MapLOfStringL$ObjectLToCarroL()))[$toList]();
      carro_service_in_memory.InMemoryDataService._nextCodigo = dart.notNull(carro_service_in_memory.InMemoryDataService._carroDb[$map](intL(), dart.fn(carro => intL().as(carro.codigoCarro), CarroLTointL()))[$fold](intL(), 0, dart.gbind(C1 || CT.C1, intL()))) + 1;
    }
  };
  (carro_service_in_memory.InMemoryDataService.new = function() {
    carro_service_in_memory.InMemoryDataService.__proto__.new.call(this, C0 || CT.C0);
    ;
  }).prototype = carro_service_in_memory.InMemoryDataService.prototype;
  dart.addTypeTests(carro_service_in_memory.InMemoryDataService);
  dart.addTypeCaches(carro_service_in_memory.InMemoryDataService);
  dart.setLibraryUri(carro_service_in_memory.InMemoryDataService, L0);
  dart.defineLazy(carro_service_in_memory.InMemoryDataService, {
    /*carro_service_in_memory.InMemoryDataService._initialCarros*/get _initialCarros() {
      return JSArrayOfMapLOfStringL$ObjectL().of([new (IdentityMapOfStringL$ObjectL()).from(["codigoCarro", 1, "nomeCarro", "Gol GTI", "nomeFabricante", "Volksvagem", "anoFabricacao", 1994, "preco", 15000.2, "imagem", "https://quatrorodas.abril.com.br/wp-content/uploads/2018/11/gol-gti-modelo-1989-da-volkswagen-de-propriedade-do-publicitc3a1rio-rafael-carmin_1.jpg?quality=70&strip=info"]), new (IdentityMapOfStringL$ObjectL()).from(["codigoCarro", 2, "nomeCarro", "Scort XR3", "nomeFabricante", "Ford", "anoFabricacao", 1995, "preco", 18000.2, "imagem", "https://quatrorodas.abril.com.br/wp-content/uploads/2019/06/qr-721-classicos-xr3-01.tif_-e1559850002131.jpg?quality=70&strip=info"]), new (IdentityMapOfStringL$ObjectL()).from(["codigoCarro", 3, "nomeCarro", "Apolo", "nomeFabricante", "Volksvagem", "anoFabricacao", 1997, "preco", 17200.2, "imagem", "https://quatrorodas.abril.com.br/wp-content/uploads/2016/11/56be66f80e21630a3e127f80qr-667-grandes-brasileiros-02-psd.jpeg?quality=70&strip=all&strip=all"]), new (IdentityMapOfStringL$ObjectL()).from(["codigoCarro", 4, "nomeCarro", "Verona", "nomeFabricante", "Ford", "anoFabricacao", 1998, "preco", 18300.2, "imagem", "https://combustivel.app/imgs/t650/consumo-verona-lx-1-6.jpg"])]);
    },
    /*carro_service_in_memory.InMemoryDataService._carroDb*/get _carroDb() {
      return null;
    },
    set _carroDb(_) {},
    /*carro_service_in_memory.InMemoryDataService._nextCodigo*/get _nextCodigo() {
      return null;
    },
    set _nextCodigo(_) {}
  }, true);
  dart.trackLibraries("packages/carros/src/services/carro_service_in_memory", {
    "package:carros/src/services/carro_service_in_memory.dart": carro_service_in_memory
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["carro_service_in_memory.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;oBAkB2C;AAAT;;AAC9B,YAAI,AAAS,wDAAG,MAAM,AAAY;AAC9B;AACJ,gBAAQ,AAAQ,OAAD;;;AAEL,yBAAa,kBAAS,AAAQ,AAAI,AAAa,OAAlB;AACnC,gBAAI,MAAM,IAAI;AAE2C,cADvD,OAAO,AACF,kEAAW,QAAC,SAA4B,YAAlB,AAAM,KAAD,cAAgB,MAAM;;AAElD,4BAAkD,KAAzC,AAAQ,AAAI,AAAe,OAApB,4BAAqB,oBAAD,OAAiB;AACnD,2BAAS,gBAAO,MAAM,kBAAiB;AAC8B,cAA3E,OAAO,AAAS,AAAmD,6DAA7C,QAAC,SAAU,WAAgB,WAAhB,AAAM,KAAD,yBAAoB,MAAM;;AAElE;;;;AAEI,2BAAoC,WAAzB,AAAK,oBAAO,AAAQ,OAAD,iBAAO;AACrC,4BAAkB,+CAAQ,QAAQ;AACD,YAArC,AAAU,SAAD,gBAA0B,MAAX,uIAAW;AACZ,YAAvB,AAAS,2DAAI,SAAS;AACP,YAAf,OAAO;AACP;;;;AAEI,oCAAoB,AAAK,oBAAO,AAAQ,OAAD;AACvC,iCAAuB,+CAAQ,iBAAiB;AAChD,8BAAc,AAAS,kEAAW,QAAC,SAA4B,YAAlB,AAAM,KAAD,cAAgB,AAAe,cAAD;AAC9C,YAAtC,AAAY,WAAD,YAAY,cAAc;AACtB,YAAf,OAAO;AACP;;;;AAEM,qCAAyB,kBAAS,AAAQ,AAAI,AAAa,OAAlB;AAC3C,+BAAe,AAAS,kEAAW,QAAC,SAA4B,YAAlB,AAAM,KAAD,cAAgB,kBAAkB;AAC5D,YAA7B,AAAS,8DAAO,YAAY;AACb,YAAf,OAAO;AACP;;;;AAEsD,YAAtD,WAAM,AAAgD,2CAAhB,AAAQ,OAAD;;;AAEjD,cAAO,2BAAS,AAAK,oBAAO,2CAAC,QAAQ,IAAI,KAAI,eAChC,2CAAC,gBAAgB;MAChC;;;AAGiF,MAA/E,uDAAW,AAAe,AAA6C,2EAAzC,QAAC,aAAoB,oBAAQ,SAAS;AACY,MAAhF,0DAA6E,aAA/D,AAAS,AAAyC,mEAArC,QAAC,SAA4B,UAAlB,AAAM,KAAD,+CAA0B,GAAG,oCAAO;IACjF;;;AAEwB;;EAAe;;;;;MAxD5B,0DAAc;YAAG,sCAC1B,2CAAC,eAAc,GAAE,aAAY,WAAU,kBAAiB,cAAa,iBAAgB,MAAK,SAAQ,SAAS,UAAS,+KACpH,2CAAC,eAAc,GAAE,aAAY,aAAY,kBAAiB,QAAO,iBAAgB,MAAK,SAAQ,SAAS,UAAS,uIAChH,2CAAC,eAAc,GAAE,aAAY,SAAQ,kBAAiB,cAAa,iBAAgB,MAAK,SAAQ,SAAS,UAAS,+JAClH,2CAAC,eAAc,GAAE,aAAY,UAAS,kBAAiB,QAAO,iBAAgB,MAAK,SAAQ,SAAS,UAAS;;MAE5F,oDAAQ;;;;MAChB,uDAAW","file":"carro_service_in_memory.ddc.js"}');
  // Exports:
  return {
    src__services__carro_service_in_memory: carro_service_in_memory
  };
}));

//# sourceMappingURL=carro_service_in_memory.ddc.js.map
