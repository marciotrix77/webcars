
import 'dart:html';

import 'package:angular/angular.dart';

import 'package:angular_forms/angular_forms.dart';
import 'package:angular_router/angular_router.dart';
import 'package:carros/src/bloc/login_component/login_component_bloc.dart';
import 'package:carros/src/routes/routes.dart';
import 'package:carros/src/services/login_service.dart';

@Component(
  selector: 'login',
  styleUrls: ['login_component.css'],
  templateUrl: 'login_component.html',
  directives: [coreDirectives, formDirectives]
)
class LoginComponent extends OnInit{

  Router _router;

  LoginService _loginService;

  var usuario = '';

  var senha = '';

  var switchConectado = false;

  LoginComponent(this._router, this._loginService);

  void Logar()async{
    var resposta = await _loginService.Logar(usuario, senha);
    if (resposta['codigo'] == 0){
      if (switchConectado == true){
        window.localStorage['USR_WEBCARS']=_loginService.LoginString();
        print (window.localStorage['USR_WEBCARS']);
      }
      await _router.navigate(RoutePaths.painel.toUrl());
    }else{
      var elementoAlerta = document.getElementById('alert-erro');
      MostraMensagemAlerta(elementoAlerta,resposta['mensagem']);
    }    
  }

  void ativaDesativaSpinner (String acao) async{
    var btnLogar = document.getElementById('btn-logar');
    var btnLogando = document.getElementById('btn-logando');
    if (acao == 'ativar'){
      btnLogar.style.display = 'none';
      btnLogando.style.display = 'block';
    } else {
      btnLogar.style.display = 'block';
      btnLogando.style.display = 'none';      
    }
    await Future.delayed(const Duration(seconds : 3));
  }

  void LogarBloc()async{
    
    var loginBloc = LoginComponentBloc(usuario, senha, _loginService);

    loginBloc.add(LoginComponentEventLogar(usuario, senha));

    loginBloc.listen((state) async {
      if (state is LoginComponentLogado){
        if (switchConectado == true){
          window.localStorage['USR_WEBCARS']=_loginService.LoginString();
          print (window.localStorage['USR_WEBCARS']);
        }
        ativaDesativaSpinner('desativar');
        await _router.navigate(RoutePaths.painel.toUrl());        
      } else if (state is LoginComponentNaoLogado){
        var elementoAlerta = document.getElementById('alert-erro');
        var mensagem = state.props[2];
        ativaDesativaSpinner('desativar');
        MostraMensagemAlerta(elementoAlerta,mensagem);        
      } else if (state is LoginComponentLogando){
        ativaDesativaSpinner('ativar');
      }

     });
  
  }

  void MostraMensagemAlerta(Element element, String mensagem) async{
    element.style.display = 'block';
    element.text = mensagem;
    await Future.delayed(Duration(seconds: 3));
    element.style.display = 'none';
  }

  @override
  void ngOnInit() {
    // TODO: implement ngOnInit
    if(_loginService.ManterConectado() == true) _router.navigate(RoutePaths.painel.toUrl());    
  } 
}