import 'dart:convert';
import 'dart:html';

import 'package:angular/core.dart';
import 'package:carros/src/model/login.dart';
import 'package:http/http.dart';

class LoginService{

  Login _loginUsuario = Login();
  
  LoginService();

  var headers = {'content-type':'application/json'};

  bool EstaLogado(){
    if (_loginUsuario.token == null){
      return false;
    }
    return true;
  }

  bool ManterConectado(){
    return window.localStorage['USR_WEBCARS'] == null ? false : true; 
  }

  void Desconectar(){
    window.localStorage.remove('USR_WEBCARS');
  }

  String LoginString(){
    return _loginUsuario.toJson();
  }

  Future<Map<String, dynamic>> Logar(String usuario, String senha)async {
  var http2 = Client();
    try{
      var urlLogin = 'http://carros-springboot.herokuapp.com/api/v2/login';

      var body = {'username':usuario, 'password':senha};

      var response = await http2.post(urlLogin, headers: headers, body: json.encode(body));

      if (response.statusCode == 200){
        _loginUsuario = Login.fromJson(response.body);
        return {'codigo':0, 'mensagem': 'Login realizado com sucesso'};   
      } else{
        return {'codigo':1, 'mensagem': 'Usuário e/ou senha inválidos.'};   
      }

    } catch(e){
      return {'codigo':1, 'mensagem': 'Falha ao tentar acessar o sistema.'};   
    }
  }
}