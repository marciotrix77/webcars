define(['dart_sdk', 'packages/collection/src/canonicalized_map'], (function load__packages__test_api__src__util__iterable_set(dart_sdk, packages__collection__src__canonicalized_map) {
  'use strict';
  const core = dart_sdk.core;
  const collection = dart_sdk.collection;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const unmodifiable_wrappers = packages__collection__src__canonicalized_map.src__unmodifiable_wrappers;
  var iterable_set = Object.create(dart.library);
  var $length = dartx.length;
  var $iterator = dartx.iterator;
  var $contains = dartx.contains;
  var $firstWhere = dartx.firstWhere;
  var $toSet = dartx.toSet;
  var boolL = () => (boolL = dart.constFn(dart.legacy(core.bool)))();
  var VoidToNullN = () => (VoidToNullN = dart.constFn(dart.fnType(core.Null, [])))();
  const CT = Object.create(null);
  var L0 = "package:test_api/src/util/iterable_set.dart";
  var _base$ = dart.privateName(iterable_set, "_base");
  const _is_IterableSet_default = Symbol('_is_IterableSet_default');
  iterable_set.IterableSet$ = dart.generic(E => {
    var EL = () => (EL = dart.constFn(dart.legacy(E)))();
    var ELToboolL = () => (ELToboolL = dart.constFn(dart.fnType(boolL(), [EL()])))();
    const SetMixin_UnmodifiableSetMixin$36 = class SetMixin_UnmodifiableSetMixin extends collection.SetMixin$(dart.legacy(E)) {};
    (SetMixin_UnmodifiableSetMixin$36.new = function() {
    }).prototype = SetMixin_UnmodifiableSetMixin$36.prototype;
    dart.applyMixin(SetMixin_UnmodifiableSetMixin$36, unmodifiable_wrappers.UnmodifiableSetMixin$(dart.legacy(E)));
    class IterableSet extends SetMixin_UnmodifiableSetMixin$36 {
      get length() {
        return this[_base$][$length];
      }
      get iterator() {
        return this[_base$][$iterator];
      }
      contains(element) {
        return this[_base$][$contains](element);
      }
      lookup(needle) {
        return this[_base$][$firstWhere](dart.fn(element => dart.equals(element, needle), ELToboolL()), {orElse: dart.fn(() => null, VoidToNullN())});
      }
      toSet() {
        return this[_base$][$toSet]();
      }
    }
    (IterableSet.new = function(_base) {
      this[_base$] = _base;
      ;
    }).prototype = IterableSet.prototype;
    dart.addTypeTests(IterableSet);
    IterableSet.prototype[_is_IterableSet_default] = true;
    dart.addTypeCaches(IterableSet);
    dart.setMethodSignature(IterableSet, () => ({
      __proto__: dart.getMethods(IterableSet.__proto__),
      contains: dart.fnType(dart.legacy(core.bool), [dart.legacy(core.Object)]),
      [$contains]: dart.fnType(dart.legacy(core.bool), [dart.legacy(core.Object)]),
      lookup: dart.fnType(dart.legacy(E), [dart.legacy(core.Object)]),
      toSet: dart.fnType(dart.legacy(core.Set$(dart.legacy(E))), []),
      [$toSet]: dart.fnType(dart.legacy(core.Set$(dart.legacy(E))), [])
    }));
    dart.setGetterSignature(IterableSet, () => ({
      __proto__: dart.getGetters(IterableSet.__proto__),
      length: dart.legacy(core.int),
      [$length]: dart.legacy(core.int),
      iterator: dart.legacy(core.Iterator$(dart.legacy(E))),
      [$iterator]: dart.legacy(core.Iterator$(dart.legacy(E)))
    }));
    dart.setLibraryUri(IterableSet, L0);
    dart.setFieldSignature(IterableSet, () => ({
      __proto__: dart.getFields(IterableSet.__proto__),
      [_base$]: dart.finalFieldType(dart.legacy(core.Iterable$(dart.legacy(E))))
    }));
    dart.defineExtensionMethods(IterableSet, ['contains', 'toSet']);
    dart.defineExtensionAccessors(IterableSet, ['length', 'iterator']);
    return IterableSet;
  });
  iterable_set.IterableSet = iterable_set.IterableSet$();
  dart.addTypeTests(iterable_set.IterableSet, _is_IterableSet_default);
  dart.trackLibraries("packages/test_api/src/util/iterable_set", {
    "package:test_api/src/util/iterable_set.dart": iterable_set
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["iterable_set.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;AAqBoB,cAAA,AAAM;MAAM;;AAEF,cAAA,AAAM;MAAQ;eAKrB;AAAY,cAAA,AAAM,yBAAS,OAAO;MAAC;aAExC;AACZ,cAAA,AAAM,2BAAW,QAAC,WAAoB,YAAR,OAAO,EAAI,MAAM,0BAAU,cAAM;MAAK;;AAEtD,cAAA,AAAM;MAAO;;;MAPd;;IAAM","file":"iterable_set.ddc.js"}');
  // Exports:
  return {
    src__util__iterable_set: iterable_set
  };
}));

//# sourceMappingURL=iterable_set.ddc.js.map
