define(['dart_sdk', 'packages/source_maps/builder', 'packages/source_map_stack_trace/source_map_stack_trace', 'packages/package_resolver/package_resolver', 'packages/test_api/src/backend/closed_exception'], (function load__packages__test_core__src__util__stack_trace_mapper(dart_sdk, packages__source_maps__builder, packages__source_map_stack_trace__source_map_stack_trace, packages__package_resolver__package_resolver, packages__test_api__src__backend__closed_exception) {
  'use strict';
  const core = dart_sdk.core;
  const _js_helper = dart_sdk._js_helper;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const parser = packages__source_maps__builder.parser;
  const source_map_stack_trace = packages__source_map_stack_trace__source_map_stack_trace.source_map_stack_trace;
  const package_root_resolver = packages__package_resolver__package_resolver.src__package_root_resolver;
  const package_config_resolver = packages__package_resolver__package_resolver.src__package_config_resolver;
  const sync_package_resolver = packages__package_resolver__package_resolver.src__sync_package_resolver;
  const stack_trace_mapper = packages__test_api__src__backend__closed_exception.src__util__stack_trace_mapper;
  var stack_trace_mapper$ = Object.create(dart.library);
  var $_get = dartx._get;
  var $isNotEmpty = dartx.isNotEmpty;
  var $cast = dartx.cast;
  var $map = dartx.map;
  var StringL = () => (StringL = dart.constFn(dart.legacy(core.String)))();
  var IdentityMapOfStringL$dynamic = () => (IdentityMapOfStringL$dynamic = dart.constFn(_js_helper.IdentityMap$(StringL(), dart.dynamic)))();
  var MapL = () => (MapL = dart.constFn(dart.legacy(core.Map)))();
  var MapEntryOfStringL$StringL = () => (MapEntryOfStringL$StringL = dart.constFn(core.MapEntry$(StringL(), StringL())))();
  var MapEntryLOfStringL$StringL = () => (MapEntryLOfStringL$StringL = dart.constFn(dart.legacy(MapEntryOfStringL$StringL())))();
  var UriL = () => (UriL = dart.constFn(dart.legacy(core.Uri)))();
  var StringLAndUriLToMapEntryLOfStringL$StringL = () => (StringLAndUriLToMapEntryLOfStringL$StringL = dart.constFn(dart.fnType(MapEntryLOfStringL$StringL(), [StringL(), UriL()])))();
  var MapEntryOfStringL$UriL = () => (MapEntryOfStringL$UriL = dart.constFn(core.MapEntry$(StringL(), UriL())))();
  var MapEntryLOfStringL$UriL = () => (MapEntryLOfStringL$UriL = dart.constFn(dart.legacy(MapEntryOfStringL$UriL())))();
  var StringLAndStringLToMapEntryLOfStringL$UriL = () => (StringLAndStringLToMapEntryLOfStringL$UriL = dart.constFn(dart.fnType(MapEntryLOfStringL$UriL(), [StringL(), StringL()])))();
  const CT = Object.create(null);
  var L0 = "package:test_core/src/util/stack_trace_mapper.dart";
  var _mapping = dart.privateName(stack_trace_mapper$, "_mapping");
  var _mapContents$ = dart.privateName(stack_trace_mapper$, "_mapContents");
  var _mapUrl = dart.privateName(stack_trace_mapper$, "_mapUrl");
  var _packageResolver = dart.privateName(stack_trace_mapper$, "_packageResolver");
  var _sdkRoot = dart.privateName(stack_trace_mapper$, "_sdkRoot");
  stack_trace_mapper$.JSStackTraceMapper = class JSStackTraceMapper extends stack_trace_mapper.StackTraceMapper {
    mapStackTrace(trace) {
      this[_mapping] == null ? this[_mapping] = parser.parseExtended(this[_mapContents$], {mapUrl: this[_mapUrl]}) : null;
      return source_map_stack_trace.mapStackTrace(this[_mapping], trace, {packageResolver: this[_packageResolver], sdkRoot: this[_sdkRoot]});
    }
    serialize() {
      let t0, t0$, t0$0;
      return new (IdentityMapOfStringL$dynamic()).from(["mapContents", this[_mapContents$], "sdkRoot", (t0 = this[_sdkRoot], t0 == null ? null : dart.toString(t0)), "packageConfigMap", stack_trace_mapper$.JSStackTraceMapper._serializePackageConfigMap(this[_packageResolver].packageConfigMap), "packageRoot", (t0$ = this[_packageResolver].packageRoot, t0$ == null ? null : dart.toString(t0$)), "mapUrl", (t0$0 = this[_mapUrl], t0$0 == null ? null : dart.toString(t0$0))]);
    }
    static deserialize(serialized) {
      let t0;
      if (serialized == null) return null;
      let packageRoot = (t0 = StringL().as(serialized[$_get]("packageRoot")), t0 == null ? "" : t0);
      return new stack_trace_mapper$.JSStackTraceMapper.new(StringL().as(serialized[$_get]("mapContents")), {sdkRoot: core.Uri.parse(StringL().as(serialized[$_get]("sdkRoot"))), packageResolver: packageRoot[$isNotEmpty] ? new package_root_resolver.PackageRootResolver.new(core.Uri.parse(StringL().as(serialized[$_get]("packageRoot")))) : new package_config_resolver.PackageConfigResolver.new(stack_trace_mapper$.JSStackTraceMapper._deserializePackageConfigMap(MapL().as(serialized[$_get]("packageConfigMap"))[$cast](StringL(), StringL()))), mapUrl: core.Uri.parse(StringL().as(serialized[$_get]("mapUrl")))});
    }
    static _serializePackageConfigMap(packageConfigMap) {
      if (packageConfigMap == null) return null;
      return packageConfigMap[$map](StringL(), StringL(), dart.fn((key, value) => new (MapEntryOfStringL$StringL()).__(key, dart.str(value)), StringLAndUriLToMapEntryLOfStringL$StringL()));
    }
    static _deserializePackageConfigMap(serialized) {
      if (serialized == null) return null;
      return serialized[$map](StringL(), UriL(), dart.fn((key, value) => new (MapEntryOfStringL$UriL()).__(key, core.Uri.parse(value)), StringLAndStringLToMapEntryLOfStringL$UriL()));
    }
  };
  (stack_trace_mapper$.JSStackTraceMapper.new = function(_mapContents, opts) {
    let mapUrl = opts && 'mapUrl' in opts ? opts.mapUrl : null;
    let packageResolver = opts && 'packageResolver' in opts ? opts.packageResolver : null;
    let sdkRoot = opts && 'sdkRoot' in opts ? opts.sdkRoot : null;
    this[_mapping] = null;
    this[_mapContents$] = _mapContents;
    this[_mapUrl] = mapUrl;
    this[_packageResolver] = packageResolver;
    this[_sdkRoot] = sdkRoot;
    ;
  }).prototype = stack_trace_mapper$.JSStackTraceMapper.prototype;
  dart.addTypeTests(stack_trace_mapper$.JSStackTraceMapper);
  dart.addTypeCaches(stack_trace_mapper$.JSStackTraceMapper);
  dart.setMethodSignature(stack_trace_mapper$.JSStackTraceMapper, () => ({
    __proto__: dart.getMethods(stack_trace_mapper$.JSStackTraceMapper.__proto__),
    mapStackTrace: dart.fnType(dart.legacy(core.StackTrace), [dart.legacy(core.StackTrace)]),
    serialize: dart.fnType(dart.legacy(core.Map$(dart.legacy(core.String), dart.dynamic)), [])
  }));
  dart.setLibraryUri(stack_trace_mapper$.JSStackTraceMapper, L0);
  dart.setFieldSignature(stack_trace_mapper$.JSStackTraceMapper, () => ({
    __proto__: dart.getFields(stack_trace_mapper$.JSStackTraceMapper.__proto__),
    [_mapping]: dart.fieldType(dart.legacy(parser.Mapping)),
    [_packageResolver]: dart.finalFieldType(dart.legacy(sync_package_resolver.SyncPackageResolver)),
    [_sdkRoot]: dart.finalFieldType(dart.legacy(core.Uri)),
    [_mapContents$]: dart.finalFieldType(dart.legacy(core.String)),
    [_mapUrl]: dart.finalFieldType(dart.legacy(core.Uri))
  }));
  dart.trackLibraries("packages/test_core/src/util/stack_trace_mapper", {
    "package:test_core/src/util/stack_trace_mapper.dart": stack_trace_mapper$
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["stack_trace_mapper.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;kBAmCsC;AACuB,MAAzD,AAAS,kBAAA,OAAT,iBAAa,qBAAc,8BAAsB,kBAAxC;AACT,YAAO,sCAAqB,gBAAU,KAAK,oBACtB,iCAA2B;IAClD;;;AAIE,YAAO,4CACL,eAAe,qBACf,8CAAW,OAAU,oBACrB,oBACI,kEAA2B,AAAiB,0CAChD,sBAAe,AAAiB,kDAAA,OAAa,qBAC7C,gDAAU,OAAS;IAEvB;uBAIwC;;AACtC,UAAI,AAAW,UAAD,IAAI,MAAM,MAAO;AACxB,yBAAkD,KAAV,aAA1B,AAAU,UAAA,QAAC,uBAAe,OAAa;AAC5D,YAAO,gDAA6C,aAA1B,AAAU,UAAA,QAAC,2BACpB,eAA4B,aAAtB,AAAU,UAAA,QAAC,+BACb,AAAY,WAAD,gBACF,kDACd,eAAgC,aAA1B,AAAU,UAAA,QAAC,oBACH,sDAAO,oEACO,AAC3B,UADJ,AAAU,UAAA,QAAC,6DAER,eAA2B,aAArB,AAAU,UAAA,QAAC;IACnC;sCAKqB;AACnB,UAAI,AAAiB,gBAAD,IAAI,MAAM,MAAO;AACrC,YAAO,AAAiB,iBAAD,6BAAK,SAAC,KAAK,UAAU,qCAAS,GAAG,EAAU,SAAN,KAAK;IACnE;wCAKwB;AACtB,UAAI,AAAW,UAAD,IAAI,MAAM,MAAO;AAC/B,YAAO,AAAW,WAAD,0BAAK,SAAC,KAAK,UAAU,kCAAS,GAAG,EAAM,eAAM,KAAK;IACrE;;yDAvDwB;QACf;QAA4B;QAAqB;IAflD;IAcgB;IAEV,gBAAE,MAAM;IACC,yBAAE,eAAe;IACzB,iBAAE,OAAO","file":"stack_trace_mapper.ddc.js"}');
  // Exports:
  return {
    src__util__stack_trace_mapper: stack_trace_mapper$
  };
}));

//# sourceMappingURL=stack_trace_mapper.ddc.js.map
