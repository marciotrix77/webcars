define(['dart_sdk', 'packages/test_api/src/backend/closed_exception', 'packages/collection/collection', 'packages/stream_channel/stream_channel', 'packages/async/async', 'packages/collection/src/canonicalized_map', 'packages/boolean_selector/boolean_selector', 'packages/test_api/src/backend/metadata', 'packages/source_span/source_span'], (function load__packages__test_core__src__runner__environment(dart_sdk, packages__test_api__src__backend__closed_exception, packages__collection__collection, packages__stream_channel__stream_channel, packages__async__async, packages__collection__src__canonicalized_map, packages__boolean_selector__boolean_selector, packages__test_api__src__backend__metadata, packages__source_span__source_span) {
  'use strict';
  const core = dart_sdk.core;
  const _interceptors = dart_sdk._interceptors;
  const collection = dart_sdk.collection;
  const async = dart_sdk.async;
  const _js_helper = dart_sdk._js_helper;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const live_test = packages__test_api__src__backend__closed_exception.src__backend__live_test;
  const group = packages__test_api__src__backend__closed_exception.src__backend__group;
  const group_entry = packages__test_api__src__backend__closed_exception.src__backend__group_entry;
  const suite = packages__test_api__src__backend__closed_exception.src__backend__suite;
  const test = packages__test_api__src__backend__closed_exception.src__backend__test;
  const state$ = packages__test_api__src__backend__closed_exception.src__backend__state;
  const union_set = packages__collection__collection.src__union_set;
  const functions = packages__collection__collection.src__functions;
  const stream_channel = packages__stream_channel__stream_channel.stream_channel;
  const multi_channel = packages__stream_channel__stream_channel.src__multi_channel;
  const async_memoizer = packages__async__async.src__async_memoizer;
  const cancelable_operation = packages__async__async.src__cancelable_operation;
  const future_group = packages__async__async.src__future_group;
  const unmodifiable_wrappers = packages__collection__src__canonicalized_map.src__unmodifiable_wrappers;
  const boolean_selector = packages__boolean_selector__boolean_selector.boolean_selector;
  const platform_selector = packages__test_api__src__backend__metadata.src__backend__platform_selector;
  const metadata$ = packages__test_api__src__backend__metadata.src__backend__metadata;
  const runtime = packages__test_api__src__backend__metadata.src__backend__runtime;
  const timeout = packages__test_api__src__backend__metadata.src__frontend__timeout;
  const suite_platform = packages__test_api__src__backend__metadata.src__backend__suite_platform;
  const utils = packages__test_api__src__backend__metadata.src__utils;
  const span_exception = packages__source_span__source_span.src__span_exception;
  const span = packages__source_span__source_span.src__span;
  var live_suite = Object.create(dart.library);
  var runner_suite = Object.create(dart.library);
  var environment = Object.create(dart.library);
  var suite$ = Object.create(dart.library);
  var runtime_selection = Object.create(dart.library);
  var reporter = Object.create(dart.library);
  var live_suite_controller = Object.create(dart.library);
  var io_stub = Object.create(dart.library);
  var load_exception = Object.create(dart.library);
  var environment$ = Object.create(dart.library);
  var $add = dartx.add;
  var $toSet = dartx.toSet;
  var $map = dartx.map;
  var $isEmpty = dartx.isEmpty;
  var $keys = dartx.keys;
  var $values = dartx.values;
  var $toList = dartx.toList;
  var $addAll = dartx.addAll;
  var $any = dartx.any;
  var $forEach = dartx.forEach;
  var $remove = dartx.remove;
  var $fold = dartx.fold;
  var $hashCode = dartx.hashCode;
  var $_equals = dartx._equals;
  var $replaceFirst = dartx.replaceFirst;
  var $contains = dartx.contains;
  var $toString = dartx.toString;
  var LiveTestL = () => (LiveTestL = dart.constFn(dart.legacy(live_test.LiveTest)))();
  var SetOfLiveTestL = () => (SetOfLiveTestL = dart.constFn(core.Set$(LiveTestL())))();
  var SetLOfLiveTestL = () => (SetLOfLiveTestL = dart.constFn(dart.legacy(SetOfLiveTestL())))();
  var JSArrayOfSetLOfLiveTestL = () => (JSArrayOfSetLOfLiveTestL = dart.constFn(_interceptors.JSArray$(SetLOfLiveTestL())))();
  var LinkedHashSetOfLiveTestL = () => (LinkedHashSetOfLiveTestL = dart.constFn(collection.LinkedHashSet$(LiveTestL())))();
  var UnionSetOfLiveTestL = () => (UnionSetOfLiveTestL = dart.constFn(union_set.UnionSet$(LiveTestL())))();
  var RunnerSuiteL = () => (RunnerSuiteL = dart.constFn(dart.legacy(runner_suite.RunnerSuite)))();
  var FutureOfRunnerSuiteL = () => (FutureOfRunnerSuiteL = dart.constFn(async.Future$(RunnerSuiteL())))();
  var GroupEntryL = () => (GroupEntryL = dart.constFn(dart.legacy(group_entry.GroupEntry)))();
  var JSArrayOfGroupEntryL = () => (JSArrayOfGroupEntryL = dart.constFn(_interceptors.JSArray$(GroupEntryL())))();
  var boolL = () => (boolL = dart.constFn(dart.legacy(core.bool)))();
  var StreamControllerOfboolL = () => (StreamControllerOfboolL = dart.constFn(async.StreamController$(boolL())))();
  var StringL = () => (StringL = dart.constFn(dart.legacy(core.String)))();
  var _IdentityHashSetOfStringL = () => (_IdentityHashSetOfStringL = dart.constFn(collection._IdentityHashSet$(StringL())))();
  var GroupL = () => (GroupL = dart.constFn(dart.legacy(group.Group)))();
  var GroupLToRunnerSuiteL = () => (GroupLToRunnerSuiteL = dart.constFn(dart.fnType(RunnerSuiteL(), [GroupL()])))();
  var ObjectL = () => (ObjectL = dart.constFn(dart.legacy(core.Object)))();
  var IdentityMapOfStringL$ObjectL = () => (IdentityMapOfStringL$ObjectL = dart.constFn(_js_helper.IdentityMap$(StringL(), ObjectL())))();
  var FutureOfNullN = () => (FutureOfNullN = dart.constFn(async.Future$(core.Null)))();
  var FutureLOfNullN = () => (FutureLOfNullN = dart.constFn(dart.legacy(FutureOfNullN())))();
  var VoidToFutureLOfNullN = () => (VoidToFutureLOfNullN = dart.constFn(dart.fnType(FutureLOfNullN(), [])))();
  var PatternL = () => (PatternL = dart.constFn(dart.legacy(core.Pattern)))();
  var UnmodifiableSetViewOfPatternL = () => (UnmodifiableSetViewOfPatternL = dart.constFn(unmodifiable_wrappers.UnmodifiableSetView$(PatternL())))();
  var _HashSetOfPatternL = () => (_HashSetOfPatternL = dart.constFn(collection._HashSet$(PatternL())))();
  var RuntimeSelectionL = () => (RuntimeSelectionL = dart.constFn(dart.legacy(runtime_selection.RuntimeSelection)))();
  var BooleanSelectorL = () => (BooleanSelectorL = dart.constFn(dart.legacy(boolean_selector.BooleanSelector)))();
  var SuiteConfigurationL = () => (SuiteConfigurationL = dart.constFn(dart.legacy(suite$.SuiteConfiguration)))();
  var PlatformSelectorL = () => (PlatformSelectorL = dart.constFn(dart.legacy(platform_selector.PlatformSelector)))();
  var ListOfStringL = () => (ListOfStringL = dart.constFn(core.List$(StringL())))();
  var RuntimeSelectionLToStringL = () => (RuntimeSelectionLToStringL = dart.constFn(dart.fnType(StringL(), [RuntimeSelectionL()])))();
  var MetadataL = () => (MetadataL = dart.constFn(dart.legacy(metadata$.Metadata)))();
  var MapEntryOfBooleanSelectorL$MetadataL = () => (MapEntryOfBooleanSelectorL$MetadataL = dart.constFn(core.MapEntry$(BooleanSelectorL(), MetadataL())))();
  var MapEntryLOfBooleanSelectorL$MetadataL = () => (MapEntryLOfBooleanSelectorL$MetadataL = dart.constFn(dart.legacy(MapEntryOfBooleanSelectorL$MetadataL())))();
  var BooleanSelectorLAndSuiteConfigurationLToMapEntryLOfBooleanSelectorL$MetadataL = () => (BooleanSelectorLAndSuiteConfigurationLToMapEntryLOfBooleanSelectorL$MetadataL = dart.constFn(dart.fnType(MapEntryLOfBooleanSelectorL$MetadataL(), [BooleanSelectorL(), SuiteConfigurationL()])))();
  var MapEntryOfPlatformSelectorL$MetadataL = () => (MapEntryOfPlatformSelectorL$MetadataL = dart.constFn(core.MapEntry$(PlatformSelectorL(), MetadataL())))();
  var MapEntryLOfPlatformSelectorL$MetadataL = () => (MapEntryLOfPlatformSelectorL$MetadataL = dart.constFn(dart.legacy(MapEntryOfPlatformSelectorL$MetadataL())))();
  var PlatformSelectorLAndSuiteConfigurationLToMapEntryLOfPlatformSelectorL$MetadataL = () => (PlatformSelectorLAndSuiteConfigurationLToMapEntryLOfPlatformSelectorL$MetadataL = dart.constFn(dart.fnType(MapEntryLOfPlatformSelectorL$MetadataL(), [PlatformSelectorL(), SuiteConfigurationL()])))();
  var UnmodifiableSetViewOfStringL = () => (UnmodifiableSetViewOfStringL = dart.constFn(unmodifiable_wrappers.UnmodifiableSetView$(StringL())))();
  var SyncIterableOfSuiteConfigurationL = () => (SyncIterableOfSuiteConfigurationL = dart.constFn(_js_helper.SyncIterable$(SuiteConfigurationL())))();
  var MapEntryOfBooleanSelectorL$SuiteConfigurationL = () => (MapEntryOfBooleanSelectorL$SuiteConfigurationL = dart.constFn(core.MapEntry$(BooleanSelectorL(), SuiteConfigurationL())))();
  var MapEntryLOfBooleanSelectorL$SuiteConfigurationL = () => (MapEntryLOfBooleanSelectorL$SuiteConfigurationL = dart.constFn(dart.legacy(MapEntryOfBooleanSelectorL$SuiteConfigurationL())))();
  var BooleanSelectorLAndMetadataLToMapEntryLOfBooleanSelectorL$SuiteConfigurationL = () => (BooleanSelectorLAndMetadataLToMapEntryLOfBooleanSelectorL$SuiteConfigurationL = dart.constFn(dart.fnType(MapEntryLOfBooleanSelectorL$SuiteConfigurationL(), [BooleanSelectorL(), MetadataL()])))();
  var MapEntryOfPlatformSelectorL$SuiteConfigurationL = () => (MapEntryOfPlatformSelectorL$SuiteConfigurationL = dart.constFn(core.MapEntry$(PlatformSelectorL(), SuiteConfigurationL())))();
  var MapEntryLOfPlatformSelectorL$SuiteConfigurationL = () => (MapEntryLOfPlatformSelectorL$SuiteConfigurationL = dart.constFn(dart.legacy(MapEntryOfPlatformSelectorL$SuiteConfigurationL())))();
  var PlatformSelectorLAndMetadataLToMapEntryLOfPlatformSelectorL$SuiteConfigurationL = () => (PlatformSelectorLAndMetadataLToMapEntryLOfPlatformSelectorL$SuiteConfigurationL = dart.constFn(dart.fnType(MapEntryLOfPlatformSelectorL$SuiteConfigurationL(), [PlatformSelectorL(), MetadataL()])))();
  var LinkedMapOfBooleanSelectorL$MetadataL = () => (LinkedMapOfBooleanSelectorL$MetadataL = dart.constFn(_js_helper.LinkedMap$(BooleanSelectorL(), MetadataL())))();
  var LinkedMapOfPlatformSelectorL$MetadataL = () => (LinkedMapOfPlatformSelectorL$MetadataL = dart.constFn(_js_helper.LinkedMap$(PlatformSelectorL(), MetadataL())))();
  var RuntimeL = () => (RuntimeL = dart.constFn(dart.legacy(runtime.Runtime)))();
  var RuntimeLToStringL = () => (RuntimeLToStringL = dart.constFn(dart.fnType(StringL(), [RuntimeL()])))();
  var RuntimeLToboolL = () => (RuntimeLToboolL = dart.constFn(dart.fnType(boolL(), [RuntimeL()])))();
  var PlatformSelectorLAndSuiteConfigurationLToNullN = () => (PlatformSelectorLAndSuiteConfigurationLToNullN = dart.constFn(dart.fnType(core.Null, [PlatformSelectorL(), SuiteConfigurationL()])))();
  var LinkedMapOfPlatformSelectorL$SuiteConfigurationL = () => (LinkedMapOfPlatformSelectorL$SuiteConfigurationL = dart.constFn(_js_helper.LinkedMap$(PlatformSelectorL(), SuiteConfigurationL())))();
  var SuiteConfigurationLAndSuiteConfigurationLToSuiteConfigurationL = () => (SuiteConfigurationLAndSuiteConfigurationLToSuiteConfigurationL = dart.constFn(dart.fnType(SuiteConfigurationL(), [SuiteConfigurationL(), SuiteConfigurationL()])))();
  var LinkedHashMapOfBooleanSelectorL$SuiteConfigurationL = () => (LinkedHashMapOfBooleanSelectorL$SuiteConfigurationL = dart.constFn(collection.LinkedHashMap$(BooleanSelectorL(), SuiteConfigurationL())))();
  var SuiteConfigurationLAndBooleanSelectorLToSuiteConfigurationL = () => (SuiteConfigurationLAndBooleanSelectorLToSuiteConfigurationL = dart.constFn(dart.fnType(SuiteConfigurationL(), [SuiteConfigurationL(), BooleanSelectorL()])))();
  var UnmodifiableSetViewOfLiveTestL = () => (UnmodifiableSetViewOfLiveTestL = dart.constFn(unmodifiable_wrappers.UnmodifiableSetView$(LiveTestL())))();
  var StreamControllerOfLiveTestL = () => (StreamControllerOfLiveTestL = dart.constFn(async.StreamController$(LiveTestL())))();
  var _HashSetOfLiveTestL = () => (_HashSetOfLiveTestL = dart.constFn(collection._HashSet$(LiveTestL())))();
  var ListL = () => (ListL = dart.constFn(dart.legacy(core.List)))();
  var ListLToNullN = () => (ListLToNullN = dart.constFn(dart.fnType(core.Null, [ListL()])))();
  var dynamicToNullN = () => (dynamicToNullN = dart.constFn(dart.fnType(core.Null, [dart.dynamic])))();
  var StateL = () => (StateL = dart.constFn(dart.legacy(state$.State)))();
  var StateLToNullN = () => (StateLToNullN = dart.constFn(dart.fnType(core.Null, [StateL()])))();
  var SourceSpanExceptionL = () => (SourceSpanExceptionL = dart.constFn(dart.legacy(span_exception.SourceSpanException)))();
  const CT = Object.create(null);
  var L6 = "package:test_core/src/runner/live_suite_controller.dart";
  var L2 = "package:test_core/src/runner/environment.dart";
  var L7 = "file:///tmp/scratch_spaceZAGYHZ/packages/test_core/src/runner/live_suite_controller.dart";
  var L0 = "package:test_core/src/runner/live_suite.dart";
  var L4 = "package:test_core/src/runner/runtime_selection.dart";
  var L9 = "package:test_core/src/runner/plugin/environment.dart";
  var L1 = "package:test_core/src/runner/runner_suite.dart";
  var L3 = "package:test_core/src/runner/suite.dart";
  var L5 = "package:test_core/src/runner/reporter.dart";
  var L8 = "package:test_core/src/runner/load_exception.dart";
  dart.defineLazy(CT, {
    get C0() {
      return C0 = dart.constList([], StringL());
    },
    get C1() {
      return C1 = dart.constList(["vm"], StringL());
    },
    get C2() {
      return C2 = dart.constMap(core.Null, core.Null, []);
    }
  }, false);
  live_suite.LiveSuite = class LiveSuite extends core.Object {
    get liveTests() {
      let sets = JSArrayOfSetLOfLiveTestL().of([this.passed, this.skipped, this.failed]);
      if (this.active != null) sets[$add](LinkedHashSetOfLiveTestL().from([this.active]));
      return new (UnionSetOfLiveTestL()).from(sets);
    }
  };
  (live_suite.LiveSuite.new = function() {
    ;
  }).prototype = live_suite.LiveSuite.prototype;
  dart.addTypeTests(live_suite.LiveSuite);
  dart.addTypeCaches(live_suite.LiveSuite);
  dart.setGetterSignature(live_suite.LiveSuite, () => ({
    __proto__: dart.getGetters(live_suite.LiveSuite.__proto__),
    liveTests: dart.legacy(core.Set$(dart.legacy(live_test.LiveTest)))
  }));
  dart.setLibraryUri(live_suite.LiveSuite, L0);
  var _controller$ = dart.privateName(runner_suite, "_controller");
  var _environment$ = dart.privateName(runner_suite, "_environment");
  var _config$ = dart.privateName(runner_suite, "_config");
  var _isDebugging = dart.privateName(runner_suite, "_isDebugging");
  var _onDebuggingController = dart.privateName(runner_suite, "_onDebuggingController");
  var _suite = dart.privateName(runner_suite, "_suite");
  var _close = dart.privateName(runner_suite, "_close");
  runner_suite.RunnerSuite = class RunnerSuite extends suite.Suite {
    get environment() {
      return this[_controller$][_environment$];
    }
    get config() {
      return this[_controller$][_config$];
    }
    get isDebugging() {
      return this[_controller$][_isDebugging];
    }
    get onDebugging() {
      return this[_controller$][_onDebuggingController].stream;
    }
    channel(name) {
      return this[_controller$].channel(name);
    }
    static new(environment, config, group, platform, opts) {
      let path = opts && 'path' in opts ? opts.path : null;
      let onClose = opts && 'onClose' in opts ? opts.onClose : null;
      let controller = new runner_suite.RunnerSuiteController._local(environment, config, {onClose: onClose});
      let suite = new runner_suite.RunnerSuite.__(controller, group, path, platform);
      controller[_suite] = FutureOfRunnerSuiteL().value(suite);
      return suite;
    }
    filter(callback) {
      let filtered = this.group.filter(callback);
      filtered == null ? filtered = new group.Group.root(JSArrayOfGroupEntryL().of([]), {metadata: this.metadata}) : null;
      return new runner_suite.RunnerSuite.__(this[_controller$], filtered, this.path, this.platform);
    }
    close() {
      return this[_controller$][_close]();
    }
  };
  (runner_suite.RunnerSuite.__ = function(_controller, group, path, platform) {
    this[_controller$] = _controller;
    runner_suite.RunnerSuite.__proto__.new.call(this, group, platform, {path: path});
    ;
  }).prototype = runner_suite.RunnerSuite.prototype;
  dart.addTypeTests(runner_suite.RunnerSuite);
  dart.addTypeCaches(runner_suite.RunnerSuite);
  dart.setMethodSignature(runner_suite.RunnerSuite, () => ({
    __proto__: dart.getMethods(runner_suite.RunnerSuite.__proto__),
    channel: dart.fnType(dart.legacy(stream_channel.StreamChannel), [dart.legacy(core.String)]),
    filter: dart.fnType(dart.legacy(runner_suite.RunnerSuite), [dart.legacy(dart.fnType(dart.legacy(core.bool), [dart.legacy(test.Test)]))]),
    close: dart.fnType(dart.legacy(async.Future), [])
  }));
  dart.setGetterSignature(runner_suite.RunnerSuite, () => ({
    __proto__: dart.getGetters(runner_suite.RunnerSuite.__proto__),
    environment: dart.legacy(environment.Environment),
    config: dart.legacy(suite$.SuiteConfiguration),
    isDebugging: dart.legacy(core.bool),
    onDebugging: dart.legacy(async.Stream$(dart.legacy(core.bool)))
  }));
  dart.setLibraryUri(runner_suite.RunnerSuite, L1);
  dart.setFieldSignature(runner_suite.RunnerSuite, () => ({
    __proto__: dart.getFields(runner_suite.RunnerSuite.__proto__),
    [_controller$]: dart.finalFieldType(dart.legacy(runner_suite.RunnerSuiteController))
  }));
  var _channelNames = dart.privateName(runner_suite, "_channelNames");
  var _closeMemo = dart.privateName(runner_suite, "_closeMemo");
  var _suiteChannel$ = dart.privateName(runner_suite, "_suiteChannel");
  var _onClose = dart.privateName(runner_suite, "_onClose");
  runner_suite.RunnerSuiteController = class RunnerSuiteController extends core.Object {
    get suite() {
      return this[_suite];
    }
    setDebugging(debugging) {
      if (dart.equals(debugging, this[_isDebugging])) return;
      this[_isDebugging] = debugging;
      this[_onDebuggingController].add(debugging);
    }
    channel(name) {
      if (!dart.test(this[_channelNames].add(name))) {
        dart.throw(new core.StateError.new("Duplicate RunnerSuite.channel() connection \"" + dart.str(name) + "\"."));
      }
      let channel = this[_suiteChannel$].virtualChannel();
      this[_suiteChannel$].sink.add(new (IdentityMapOfStringL$ObjectL()).from(["type", "suiteChannel", "name", name, "id", channel.id]));
      return channel;
    }
    [_close]() {
      return this[_closeMemo].runOnce(dart.fn(() => async.async(core.Null, (function*() {
        yield this[_onDebuggingController].close();
        if (this[_onClose] != null) yield this[_onClose]();
      }).bind(this)), VoidToFutureLOfNullN()));
    }
  };
  (runner_suite.RunnerSuiteController.new = function(_environment, _config, _suiteChannel, groupFuture, platform, opts) {
    let path = opts && 'path' in opts ? opts.path : null;
    let onClose = opts && 'onClose' in opts ? opts.onClose : null;
    this[_suite] = null;
    this[_isDebugging] = false;
    this[_onDebuggingController] = StreamControllerOfboolL().broadcast();
    this[_channelNames] = new (_IdentityHashSetOfStringL()).new();
    this[_closeMemo] = new async_memoizer.AsyncMemoizer.new();
    this[_environment$] = _environment;
    this[_config$] = _config;
    this[_suiteChannel$] = _suiteChannel;
    this[_onClose] = onClose;
    this[_suite] = groupFuture.then(RunnerSuiteL(), dart.fn(group => new runner_suite.RunnerSuite.__(this, group, path, platform), GroupLToRunnerSuiteL()));
  }).prototype = runner_suite.RunnerSuiteController.prototype;
  (runner_suite.RunnerSuiteController._local = function(_environment, _config, opts) {
    let onClose = opts && 'onClose' in opts ? opts.onClose : null;
    this[_suite] = null;
    this[_isDebugging] = false;
    this[_onDebuggingController] = StreamControllerOfboolL().broadcast();
    this[_channelNames] = new (_IdentityHashSetOfStringL()).new();
    this[_closeMemo] = new async_memoizer.AsyncMemoizer.new();
    this[_environment$] = _environment;
    this[_config$] = _config;
    this[_suiteChannel$] = null;
    this[_onClose] = onClose;
    ;
  }).prototype = runner_suite.RunnerSuiteController.prototype;
  dart.addTypeTests(runner_suite.RunnerSuiteController);
  dart.addTypeCaches(runner_suite.RunnerSuiteController);
  dart.setMethodSignature(runner_suite.RunnerSuiteController, () => ({
    __proto__: dart.getMethods(runner_suite.RunnerSuiteController.__proto__),
    setDebugging: dart.fnType(dart.void, [dart.legacy(core.bool)]),
    channel: dart.fnType(dart.legacy(stream_channel.StreamChannel), [dart.legacy(core.String)]),
    [_close]: dart.fnType(dart.legacy(async.Future), [])
  }));
  dart.setGetterSignature(runner_suite.RunnerSuiteController, () => ({
    __proto__: dart.getGetters(runner_suite.RunnerSuiteController.__proto__),
    suite: dart.legacy(async.Future$(dart.legacy(runner_suite.RunnerSuite)))
  }));
  dart.setLibraryUri(runner_suite.RunnerSuiteController, L1);
  dart.setFieldSignature(runner_suite.RunnerSuiteController, () => ({
    __proto__: dart.getFields(runner_suite.RunnerSuiteController.__proto__),
    [_suite]: dart.fieldType(dart.legacy(async.Future$(dart.legacy(runner_suite.RunnerSuite)))),
    [_environment$]: dart.finalFieldType(dart.legacy(environment.Environment)),
    [_config$]: dart.finalFieldType(dart.legacy(suite$.SuiteConfiguration)),
    [_suiteChannel$]: dart.finalFieldType(dart.legacy(multi_channel.MultiChannel)),
    [_onClose]: dart.finalFieldType(dart.legacy(dart.fnType(dart.dynamic, []))),
    [_isDebugging]: dart.fieldType(dart.legacy(core.bool)),
    [_onDebuggingController]: dart.finalFieldType(dart.legacy(async.StreamController$(dart.legacy(core.bool)))),
    [_channelNames]: dart.finalFieldType(dart.legacy(core.Set$(dart.legacy(core.String)))),
    [_closeMemo]: dart.finalFieldType(dart.legacy(async_memoizer.AsyncMemoizer))
  }));
  environment.Environment = class Environment extends core.Object {};
  (environment.Environment.new = function() {
    ;
  }).prototype = environment.Environment.prototype;
  dart.addTypeTests(environment.Environment);
  dart.addTypeCaches(environment.Environment);
  dart.setLibraryUri(environment.Environment, L2);
  var supportsDebugging = dart.privateName(environment, "PluginEnvironment.supportsDebugging");
  environment.PluginEnvironment = class PluginEnvironment extends core.Object {
    get supportsDebugging() {
      return this[supportsDebugging];
    }
    set supportsDebugging(value) {
      super.supportsDebugging = value;
    }
    get onRestart() {
      return async.StreamController.broadcast().stream;
    }
    get observatoryUrl() {
      return null;
    }
    get remoteDebuggerUrl() {
      return null;
    }
    displayPause() {
      return dart.throw(new core.UnsupportedError.new("PluginEnvironment.displayPause is not supported."));
    }
  };
  (environment.PluginEnvironment.new = function() {
    this[supportsDebugging] = false;
    ;
  }).prototype = environment.PluginEnvironment.prototype;
  dart.addTypeTests(environment.PluginEnvironment);
  dart.addTypeCaches(environment.PluginEnvironment);
  environment.PluginEnvironment[dart.implements] = () => [environment.Environment];
  dart.setMethodSignature(environment.PluginEnvironment, () => ({
    __proto__: dart.getMethods(environment.PluginEnvironment.__proto__),
    displayPause: dart.fnType(dart.legacy(cancelable_operation.CancelableOperation), [])
  }));
  dart.setGetterSignature(environment.PluginEnvironment, () => ({
    __proto__: dart.getGetters(environment.PluginEnvironment.__proto__),
    onRestart: dart.legacy(async.Stream),
    observatoryUrl: dart.legacy(core.Uri),
    remoteDebuggerUrl: dart.legacy(core.Uri)
  }));
  dart.setLibraryUri(environment.PluginEnvironment, L2);
  dart.setFieldSignature(environment.PluginEnvironment, () => ({
    __proto__: dart.getFields(environment.PluginEnvironment.__proto__),
    supportsDebugging: dart.finalFieldType(dart.legacy(core.bool))
  }));
  var _knownTags = dart.privateName(suite$, "_knownTags");
  var _jsTrace = dart.privateName(suite$, "_jsTrace");
  var _runSkipped = dart.privateName(suite$, "_runSkipped");
  var C0;
  var _runtimes = dart.privateName(suite$, "_runtimes");
  var _metadata = dart.privateName(suite$, "_metadata");
  var C1;
  var _children = dart.privateName(suite$, "_children");
  var _resolveTags = dart.privateName(suite$, "_resolveTags");
  var C2;
  var _mergeConfigMaps = dart.privateName(suite$, "_mergeConfigMaps");
  var precompiledPath$ = dart.privateName(suite$, "SuiteConfiguration.precompiledPath");
  var dart2jsArgs$ = dart.privateName(suite$, "SuiteConfiguration.dart2jsArgs");
  var patterns$ = dart.privateName(suite$, "SuiteConfiguration.patterns");
  var includeTags$ = dart.privateName(suite$, "SuiteConfiguration.includeTags");
  var excludeTags$ = dart.privateName(suite$, "SuiteConfiguration.excludeTags");
  var tags$ = dart.privateName(suite$, "SuiteConfiguration.tags");
  var onPlatform$ = dart.privateName(suite$, "SuiteConfiguration.onPlatform");
  suite$.SuiteConfiguration = class SuiteConfiguration extends core.Object {
    get precompiledPath() {
      return this[precompiledPath$];
    }
    set precompiledPath(value) {
      super.precompiledPath = value;
    }
    get dart2jsArgs() {
      return this[dart2jsArgs$];
    }
    set dart2jsArgs(value) {
      super.dart2jsArgs = value;
    }
    get patterns() {
      return this[patterns$];
    }
    set patterns(value) {
      super.patterns = value;
    }
    get includeTags() {
      return this[includeTags$];
    }
    set includeTags(value) {
      super.includeTags = value;
    }
    get excludeTags() {
      return this[excludeTags$];
    }
    set excludeTags(value) {
      super.excludeTags = value;
    }
    get tags() {
      return this[tags$];
    }
    set tags(value) {
      super.tags = value;
    }
    get onPlatform() {
      return this[onPlatform$];
    }
    set onPlatform(value) {
      super.onPlatform = value;
    }
    get jsTrace() {
      let t0;
      t0 = this[_jsTrace];
      return t0 == null ? false : t0;
    }
    get runSkipped() {
      let t0;
      t0 = this[_runSkipped];
      return t0 == null ? false : t0;
    }
    get runtimes() {
      return this[_runtimes] == null ? C1 || CT.C1 : ListOfStringL().unmodifiable(this[_runtimes][$map](dart.dynamic, dart.fn(runtime => runtime.name, RuntimeSelectionLToStringL())));
    }
    get metadata() {
      if (dart.test(this.tags[$isEmpty]) && dart.test(this.onPlatform[$isEmpty])) return this[_metadata];
      return this[_metadata].change({forTag: this.tags[$map](BooleanSelectorL(), MetadataL(), dart.fn((key, config) => new (MapEntryOfBooleanSelectorL$MetadataL()).__(key, config.metadata), BooleanSelectorLAndSuiteConfigurationLToMapEntryLOfBooleanSelectorL$MetadataL())), onPlatform: this.onPlatform[$map](PlatformSelectorL(), MetadataL(), dart.fn((key, config) => new (MapEntryOfPlatformSelectorL$MetadataL()).__(key, config.metadata), PlatformSelectorLAndSuiteConfigurationLToMapEntryLOfPlatformSelectorL$MetadataL()))});
    }
    get knownTags() {
      let t0;
      if (this[_knownTags] != null) return this[_knownTags];
      let known = (t0 = this.includeTags.variables[$toSet](), (() => {
        t0.addAll(this.excludeTags.variables);
        t0.addAll(this[_metadata].tags);
        return t0;
      })());
      for (let selector of this.tags[$keys]) {
        known.addAll(selector.variables);
      }
      for (let configuration of this[_children]) {
        known.addAll(configuration.knownTags);
      }
      this[_knownTags] = new (UnmodifiableSetViewOfStringL()).new(known);
      return this[_knownTags];
    }
    get [_children]() {
      return new (SyncIterableOfSuiteConfigurationL()).new((function* _children() {
        yield* this.tags[$values];
        yield* this.onPlatform[$values];
      }).bind(this));
    }
    static new(opts) {
      let jsTrace = opts && 'jsTrace' in opts ? opts.jsTrace : null;
      let runSkipped = opts && 'runSkipped' in opts ? opts.runSkipped : null;
      let dart2jsArgs = opts && 'dart2jsArgs' in opts ? opts.dart2jsArgs : null;
      let precompiledPath = opts && 'precompiledPath' in opts ? opts.precompiledPath : null;
      let patterns = opts && 'patterns' in opts ? opts.patterns : null;
      let runtimes = opts && 'runtimes' in opts ? opts.runtimes : null;
      let includeTags = opts && 'includeTags' in opts ? opts.includeTags : null;
      let excludeTags = opts && 'excludeTags' in opts ? opts.excludeTags : null;
      let tags = opts && 'tags' in opts ? opts.tags : null;
      let onPlatform = opts && 'onPlatform' in opts ? opts.onPlatform : null;
      let timeout = opts && 'timeout' in opts ? opts.timeout : null;
      let verboseTrace = opts && 'verboseTrace' in opts ? opts.verboseTrace : null;
      let chainStackTraces = opts && 'chainStackTraces' in opts ? opts.chainStackTraces : null;
      let skip = opts && 'skip' in opts ? opts.skip : null;
      let retry = opts && 'retry' in opts ? opts.retry : null;
      let skipReason = opts && 'skipReason' in opts ? opts.skipReason : null;
      let testOn = opts && 'testOn' in opts ? opts.testOn : null;
      let addTags = opts && 'addTags' in opts ? opts.addTags : null;
      let config = new suite$.SuiteConfiguration.__({jsTrace: jsTrace, runSkipped: runSkipped, dart2jsArgs: dart2jsArgs, precompiledPath: precompiledPath, patterns: patterns, runtimes: runtimes, includeTags: includeTags, excludeTags: excludeTags, tags: tags, onPlatform: onPlatform, metadata: metadata$.Metadata.new({timeout: timeout, verboseTrace: verboseTrace, chainStackTraces: chainStackTraces, skip: skip, retry: retry, skipReason: skipReason, testOn: testOn, tags: addTags})});
      return config[_resolveTags]();
    }
    static fromMetadata(metadata) {
      return new suite$.SuiteConfiguration.__({tags: metadata.forTag[$map](BooleanSelectorL(), SuiteConfigurationL(), dart.fn((key, child) => new (MapEntryOfBooleanSelectorL$SuiteConfigurationL()).__(key, suite$.SuiteConfiguration.fromMetadata(child)), BooleanSelectorLAndMetadataLToMapEntryLOfBooleanSelectorL$SuiteConfigurationL())), onPlatform: metadata.onPlatform[$map](PlatformSelectorL(), SuiteConfigurationL(), dart.fn((key, child) => new (MapEntryOfPlatformSelectorL$SuiteConfigurationL()).__(key, suite$.SuiteConfiguration.fromMetadata(child)), PlatformSelectorLAndMetadataLToMapEntryLOfPlatformSelectorL$SuiteConfigurationL())), metadata: metadata.change({forTag: new (LinkedMapOfBooleanSelectorL$MetadataL()).new(), onPlatform: new (LinkedMapOfPlatformSelectorL$MetadataL()).new()})});
    }
    static _list(T, input) {
      if (input == null) return null;
      let list = core.List$(dart.legacy(T)).unmodifiable(input);
      if (dart.test(list[$isEmpty])) return null;
      return list;
    }
    static _map(K, V, input) {
      if (input == null || dart.test(input[$isEmpty])) return C2 || CT.C2;
      return core.Map$(dart.legacy(K), dart.legacy(V)).unmodifiable(input);
    }
    merge(other) {
      let t0, t0$, t0$0, t0$1, t0$2;
      if (this._equals(suite$.SuiteConfiguration.empty)) return other;
      if (dart.equals(other, suite$.SuiteConfiguration.empty)) return this;
      let config = new suite$.SuiteConfiguration.__({jsTrace: (t0 = other[_jsTrace], t0 == null ? this[_jsTrace] : t0), runSkipped: (t0$ = other[_runSkipped], t0$ == null ? this[_runSkipped] : t0$), dart2jsArgs: (t0$0 = this.dart2jsArgs[$toList](), (() => {
          t0$0[$addAll](other.dart2jsArgs);
          return t0$0;
        })()), precompiledPath: (t0$1 = other.precompiledPath, t0$1 == null ? this.precompiledPath : t0$1), patterns: this.patterns.union(other.patterns), runtimes: (t0$2 = other[_runtimes], t0$2 == null ? this[_runtimes] : t0$2), includeTags: this.includeTags.intersection(other.includeTags), excludeTags: this.excludeTags.union(other.excludeTags), tags: this[_mergeConfigMaps](BooleanSelectorL(), this.tags, other.tags), onPlatform: this[_mergeConfigMaps](PlatformSelectorL(), this.onPlatform, other.onPlatform), metadata: this.metadata.merge(other.metadata)});
      return config[_resolveTags]();
    }
    change(opts) {
      let t0, t0$, t0$0, t0$1, t0$2, t0$3, t0$4, t0$5, t0$6, t0$7, t0$8, t0$9;
      let jsTrace = opts && 'jsTrace' in opts ? opts.jsTrace : null;
      let runSkipped = opts && 'runSkipped' in opts ? opts.runSkipped : null;
      let dart2jsArgs = opts && 'dart2jsArgs' in opts ? opts.dart2jsArgs : null;
      let precompiledPath = opts && 'precompiledPath' in opts ? opts.precompiledPath : null;
      let patterns = opts && 'patterns' in opts ? opts.patterns : null;
      let runtimes = opts && 'runtimes' in opts ? opts.runtimes : null;
      let includeTags = opts && 'includeTags' in opts ? opts.includeTags : null;
      let excludeTags = opts && 'excludeTags' in opts ? opts.excludeTags : null;
      let tags = opts && 'tags' in opts ? opts.tags : null;
      let onPlatform = opts && 'onPlatform' in opts ? opts.onPlatform : null;
      let timeout = opts && 'timeout' in opts ? opts.timeout : null;
      let verboseTrace = opts && 'verboseTrace' in opts ? opts.verboseTrace : null;
      let chainStackTraces = opts && 'chainStackTraces' in opts ? opts.chainStackTraces : null;
      let skip = opts && 'skip' in opts ? opts.skip : null;
      let retry = opts && 'retry' in opts ? opts.retry : null;
      let skipReason = opts && 'skipReason' in opts ? opts.skipReason : null;
      let testOn = opts && 'testOn' in opts ? opts.testOn : null;
      let addTags = opts && 'addTags' in opts ? opts.addTags : null;
      let config = new suite$.SuiteConfiguration.__({jsTrace: (t0 = jsTrace, t0 == null ? this[_jsTrace] : t0), runSkipped: (t0$ = runSkipped, t0$ == null ? this[_runSkipped] : t0$), dart2jsArgs: (t0$1 = (t0$0 = dart2jsArgs, t0$0 == null ? null : t0$0[$toList]()), t0$1 == null ? this.dart2jsArgs : t0$1), precompiledPath: (t0$2 = precompiledPath, t0$2 == null ? this.precompiledPath : t0$2), patterns: (t0$3 = patterns, t0$3 == null ? this.patterns : t0$3), runtimes: (t0$4 = runtimes, t0$4 == null ? this[_runtimes] : t0$4), includeTags: (t0$5 = includeTags, t0$5 == null ? this.includeTags : t0$5), excludeTags: (t0$6 = excludeTags, t0$6 == null ? this.excludeTags : t0$6), tags: (t0$7 = tags, t0$7 == null ? this.tags : t0$7), onPlatform: (t0$8 = onPlatform, t0$8 == null ? this.onPlatform : t0$8), metadata: this[_metadata].change({timeout: timeout, verboseTrace: verboseTrace, chainStackTraces: chainStackTraces, skip: skip, retry: retry, skipReason: skipReason, testOn: testOn, tags: (t0$9 = addTags, t0$9 == null ? null : t0$9[$toSet]())})});
      return config[_resolveTags]();
    }
    validateRuntimes(allRuntimes) {
      let validVariables = allRuntimes[$map](StringL(), dart.fn(runtime => runtime.identifier, RuntimeLToStringL()))[$toSet]();
      this[_metadata].validatePlatformSelectors(validVariables);
      if (this[_runtimes] != null) {
        for (let selection of this[_runtimes]) {
          if (!dart.test(allRuntimes[$any](dart.fn(runtime => runtime.identifier == selection.name, RuntimeLToboolL())))) {
            if (selection.span != null) {
              dart.throw(new span_exception.SourceSpanFormatException.new("Unknown platform \"" + dart.str(selection.name) + "\".", selection.span));
            } else {
              dart.throw(new core.FormatException.new("Unknown platform \"" + dart.str(selection.name) + "\"."));
            }
          }
        }
      }
      this.onPlatform[$forEach](dart.fn((selector, config) => {
        selector.validate(validVariables);
        config.validateRuntimes(allRuntimes);
      }, PlatformSelectorLAndSuiteConfigurationLToNullN()));
    }
    forPlatform(platform) {
      if (dart.test(this.onPlatform[$isEmpty])) return this;
      let config = this;
      this.onPlatform[$forEach](dart.fn((platformSelector, platformConfig) => {
        if (!dart.test(platformSelector.evaluate(platform))) return;
        config = config.merge(platformConfig);
      }, PlatformSelectorLAndSuiteConfigurationLToNullN()));
      return config.change({onPlatform: new (LinkedMapOfPlatformSelectorL$SuiteConfigurationL()).new()});
    }
    [_mergeConfigMaps](T, map1, map2) {
      return functions.mergeMaps(dart.legacy(T), SuiteConfigurationL(), map1, map2, {value: dart.fn((config1, config2) => config1.merge(config2), SuiteConfigurationLAndSuiteConfigurationLToSuiteConfigurationL())});
    }
    [_resolveTags]() {
      if (dart.test(this[_metadata].tags[$isEmpty]) || dart.test(this.tags[$isEmpty])) return this;
      let newTags = LinkedHashMapOfBooleanSelectorL$SuiteConfigurationL().from(this.tags);
      let merged = this.tags[$keys][$fold](SuiteConfigurationL(), suite$.SuiteConfiguration.empty, dart.fn((merged, selector) => {
        if (!dart.test(selector.evaluate(this[_metadata].tags))) return merged;
        return merged.merge(newTags[$remove](selector));
      }, SuiteConfigurationLAndBooleanSelectorLToSuiteConfigurationL()));
      if (dart.equals(merged, suite$.SuiteConfiguration.empty)) return this;
      return this.change({tags: newTags}).merge(merged);
    }
  };
  (suite$.SuiteConfiguration.__ = function(opts) {
    let t0, t0$, t0$0, t0$1, t0$2, t0$3;
    let jsTrace = opts && 'jsTrace' in opts ? opts.jsTrace : null;
    let runSkipped = opts && 'runSkipped' in opts ? opts.runSkipped : null;
    let dart2jsArgs = opts && 'dart2jsArgs' in opts ? opts.dart2jsArgs : null;
    let precompiledPath = opts && 'precompiledPath' in opts ? opts.precompiledPath : null;
    let patterns = opts && 'patterns' in opts ? opts.patterns : null;
    let runtimes = opts && 'runtimes' in opts ? opts.runtimes : null;
    let includeTags = opts && 'includeTags' in opts ? opts.includeTags : null;
    let excludeTags = opts && 'excludeTags' in opts ? opts.excludeTags : null;
    let tags = opts && 'tags' in opts ? opts.tags : null;
    let onPlatform = opts && 'onPlatform' in opts ? opts.onPlatform : null;
    let metadata = opts && 'metadata' in opts ? opts.metadata : null;
    this[_knownTags] = null;
    this[precompiledPath$] = precompiledPath;
    this[_jsTrace] = jsTrace;
    this[_runSkipped] = runSkipped;
    this[dart2jsArgs$] = (t0 = suite$.SuiteConfiguration._list(StringL(), dart2jsArgs), t0 == null ? C0 || CT.C0 : t0);
    this[patterns$] = new (UnmodifiableSetViewOfPatternL()).new((t0$0 = (t0$ = patterns, t0$ == null ? null : t0$[$toSet]()), t0$0 == null ? new (_HashSetOfPatternL()).new() : t0$0));
    this[_runtimes] = suite$.SuiteConfiguration._list(RuntimeSelectionL(), runtimes);
    this[includeTags$] = (t0$1 = includeTags, t0$1 == null ? boolean_selector.BooleanSelector.all : t0$1);
    this[excludeTags$] = (t0$2 = excludeTags, t0$2 == null ? boolean_selector.BooleanSelector.none : t0$2);
    this[tags$] = suite$.SuiteConfiguration._map(BooleanSelectorL(), SuiteConfigurationL(), tags);
    this[onPlatform$] = suite$.SuiteConfiguration._map(PlatformSelectorL(), SuiteConfigurationL(), onPlatform);
    this[_metadata] = (t0$3 = metadata, t0$3 == null ? metadata$.Metadata.empty : t0$3);
    ;
  }).prototype = suite$.SuiteConfiguration.prototype;
  dart.addTypeTests(suite$.SuiteConfiguration);
  dart.addTypeCaches(suite$.SuiteConfiguration);
  dart.setMethodSignature(suite$.SuiteConfiguration, () => ({
    __proto__: dart.getMethods(suite$.SuiteConfiguration.__proto__),
    merge: dart.fnType(dart.legacy(suite$.SuiteConfiguration), [dart.legacy(suite$.SuiteConfiguration)]),
    change: dart.fnType(dart.legacy(suite$.SuiteConfiguration), [], {addTags: dart.legacy(core.Iterable$(dart.legacy(core.String))), chainStackTraces: dart.legacy(core.bool), dart2jsArgs: dart.legacy(core.Iterable$(dart.legacy(core.String))), excludeTags: dart.legacy(boolean_selector.BooleanSelector), includeTags: dart.legacy(boolean_selector.BooleanSelector), jsTrace: dart.legacy(core.bool), onPlatform: dart.legacy(core.Map$(dart.legacy(platform_selector.PlatformSelector), dart.legacy(suite$.SuiteConfiguration))), patterns: dart.legacy(core.Iterable$(dart.legacy(core.Pattern))), precompiledPath: dart.legacy(core.String), retry: dart.legacy(core.int), runSkipped: dart.legacy(core.bool), runtimes: dart.legacy(core.Iterable$(dart.legacy(runtime_selection.RuntimeSelection))), skip: dart.legacy(core.bool), skipReason: dart.legacy(core.String), tags: dart.legacy(core.Map$(dart.legacy(boolean_selector.BooleanSelector), dart.legacy(suite$.SuiteConfiguration))), testOn: dart.legacy(platform_selector.PlatformSelector), timeout: dart.legacy(timeout.Timeout), verboseTrace: dart.legacy(core.bool)}, {}),
    validateRuntimes: dart.fnType(dart.void, [dart.legacy(core.List$(dart.legacy(runtime.Runtime)))]),
    forPlatform: dart.fnType(dart.legacy(suite$.SuiteConfiguration), [dart.legacy(suite_platform.SuitePlatform)]),
    [_mergeConfigMaps]: dart.gFnType(T => [dart.legacy(core.Map$(dart.legacy(T), dart.legacy(suite$.SuiteConfiguration))), [dart.legacy(core.Map$(dart.legacy(T), dart.legacy(suite$.SuiteConfiguration))), dart.legacy(core.Map$(dart.legacy(T), dart.legacy(suite$.SuiteConfiguration)))]]),
    [_resolveTags]: dart.fnType(dart.legacy(suite$.SuiteConfiguration), [])
  }));
  dart.setGetterSignature(suite$.SuiteConfiguration, () => ({
    __proto__: dart.getGetters(suite$.SuiteConfiguration.__proto__),
    jsTrace: dart.legacy(core.bool),
    runSkipped: dart.legacy(core.bool),
    runtimes: dart.legacy(core.List$(dart.legacy(core.String))),
    metadata: dart.legacy(metadata$.Metadata),
    knownTags: dart.legacy(core.Set$(dart.legacy(core.String))),
    [_children]: dart.legacy(core.Iterable$(dart.legacy(suite$.SuiteConfiguration)))
  }));
  dart.setLibraryUri(suite$.SuiteConfiguration, L3);
  dart.setFieldSignature(suite$.SuiteConfiguration, () => ({
    __proto__: dart.getFields(suite$.SuiteConfiguration.__proto__),
    [_jsTrace]: dart.finalFieldType(dart.legacy(core.bool)),
    [_runSkipped]: dart.finalFieldType(dart.legacy(core.bool)),
    precompiledPath: dart.finalFieldType(dart.legacy(core.String)),
    dart2jsArgs: dart.finalFieldType(dart.legacy(core.List$(dart.legacy(core.String)))),
    patterns: dart.finalFieldType(dart.legacy(core.Set$(dart.legacy(core.Pattern)))),
    [_runtimes]: dart.finalFieldType(dart.legacy(core.List$(dart.legacy(runtime_selection.RuntimeSelection)))),
    includeTags: dart.finalFieldType(dart.legacy(boolean_selector.BooleanSelector)),
    excludeTags: dart.finalFieldType(dart.legacy(boolean_selector.BooleanSelector)),
    tags: dart.finalFieldType(dart.legacy(core.Map$(dart.legacy(boolean_selector.BooleanSelector), dart.legacy(suite$.SuiteConfiguration)))),
    onPlatform: dart.finalFieldType(dart.legacy(core.Map$(dart.legacy(platform_selector.PlatformSelector), dart.legacy(suite$.SuiteConfiguration)))),
    [_metadata]: dart.finalFieldType(dart.legacy(metadata$.Metadata)),
    [_knownTags]: dart.fieldType(dart.legacy(core.Set$(dart.legacy(core.String))))
  }));
  dart.defineLazy(suite$.SuiteConfiguration, {
    /*suite$.SuiteConfiguration.empty*/get empty() {
      return new suite$.SuiteConfiguration.__();
    }
  }, true);
  var name$ = dart.privateName(runtime_selection, "RuntimeSelection.name");
  var span$ = dart.privateName(runtime_selection, "RuntimeSelection.span");
  runtime_selection.RuntimeSelection = class RuntimeSelection extends core.Object {
    get name() {
      return this[name$];
    }
    set name(value) {
      super.name = value;
    }
    get span() {
      return this[span$];
    }
    set span(value) {
      super.span = value;
    }
    _equals(other) {
      if (other == null) return false;
      return RuntimeSelectionL().is(other) && other.name == this.name;
    }
    get hashCode() {
      return dart.hashCode(this.name);
    }
  };
  (runtime_selection.RuntimeSelection.new = function(name, span = null) {
    this[name$] = name;
    this[span$] = span;
    ;
  }).prototype = runtime_selection.RuntimeSelection.prototype;
  dart.addTypeTests(runtime_selection.RuntimeSelection);
  dart.addTypeCaches(runtime_selection.RuntimeSelection);
  dart.setMethodSignature(runtime_selection.RuntimeSelection, () => ({
    __proto__: dart.getMethods(runtime_selection.RuntimeSelection.__proto__),
    _equals: dart.fnType(dart.legacy(core.bool), [dart.dynamic]),
    [$_equals]: dart.fnType(dart.legacy(core.bool), [dart.dynamic])
  }));
  dart.setGetterSignature(runtime_selection.RuntimeSelection, () => ({
    __proto__: dart.getGetters(runtime_selection.RuntimeSelection.__proto__),
    hashCode: dart.legacy(core.int),
    [$hashCode]: dart.legacy(core.int)
  }));
  dart.setLibraryUri(runtime_selection.RuntimeSelection, L4);
  dart.setFieldSignature(runtime_selection.RuntimeSelection, () => ({
    __proto__: dart.getFields(runtime_selection.RuntimeSelection.__proto__),
    name: dart.finalFieldType(dart.legacy(core.String)),
    span: dart.finalFieldType(dart.legacy(span.SourceSpan))
  }));
  dart.defineExtensionMethods(runtime_selection.RuntimeSelection, ['_equals']);
  dart.defineExtensionAccessors(runtime_selection.RuntimeSelection, ['hashCode']);
  reporter.Reporter = class Reporter extends core.Object {};
  (reporter.Reporter.new = function() {
    ;
  }).prototype = reporter.Reporter.prototype;
  dart.addTypeTests(reporter.Reporter);
  dart.addTypeCaches(reporter.Reporter);
  dart.setLibraryUri(reporter.Reporter, L5);
  var _controller$0 = dart.privateName(live_suite_controller, "_controller");
  var _suite$ = dart.privateName(live_suite_controller, "_suite");
  var _isComplete = dart.privateName(live_suite_controller, "_isComplete");
  var _onCompleteGroup = dart.privateName(live_suite_controller, "_onCompleteGroup");
  var _onCloseCompleter = dart.privateName(live_suite_controller, "_onCloseCompleter");
  var _onTestStartedController = dart.privateName(live_suite_controller, "_onTestStartedController");
  var _passed = dart.privateName(live_suite_controller, "_passed");
  var _skipped = dart.privateName(live_suite_controller, "_skipped");
  var _failed = dart.privateName(live_suite_controller, "_failed");
  var _active = dart.privateName(live_suite_controller, "_active");
  live_suite_controller._LiveSuite = class _LiveSuite extends live_suite.LiveSuite {
    get suite() {
      return this[_controller$0][_suite$];
    }
    get isComplete() {
      return this[_controller$0][_isComplete];
    }
    get onComplete() {
      return this[_controller$0][_onCompleteGroup].future;
    }
    get isClosed() {
      return this[_controller$0][_onCloseCompleter].isCompleted;
    }
    get onClose() {
      return this[_controller$0][_onCloseCompleter].future;
    }
    get onTestStarted() {
      return this[_controller$0][_onTestStartedController].stream;
    }
    get passed() {
      return new (UnmodifiableSetViewOfLiveTestL()).new(this[_controller$0][_passed]);
    }
    get skipped() {
      return new (UnmodifiableSetViewOfLiveTestL()).new(this[_controller$0][_skipped]);
    }
    get failed() {
      return new (UnmodifiableSetViewOfLiveTestL()).new(this[_controller$0][_failed]);
    }
    get active() {
      return this[_controller$0][_active];
    }
  };
  (live_suite_controller._LiveSuite.new = function(_controller) {
    this[_controller$0] = _controller;
    ;
  }).prototype = live_suite_controller._LiveSuite.prototype;
  dart.addTypeTests(live_suite_controller._LiveSuite);
  dart.addTypeCaches(live_suite_controller._LiveSuite);
  dart.setGetterSignature(live_suite_controller._LiveSuite, () => ({
    __proto__: dart.getGetters(live_suite_controller._LiveSuite.__proto__),
    suite: dart.legacy(runner_suite.RunnerSuite),
    isComplete: dart.legacy(core.bool),
    onComplete: dart.legacy(async.Future),
    isClosed: dart.legacy(core.bool),
    onClose: dart.legacy(async.Future),
    onTestStarted: dart.legacy(async.Stream$(dart.legacy(live_test.LiveTest))),
    passed: dart.legacy(core.Set$(dart.legacy(live_test.LiveTest))),
    skipped: dart.legacy(core.Set$(dart.legacy(live_test.LiveTest))),
    failed: dart.legacy(core.Set$(dart.legacy(live_test.LiveTest))),
    active: dart.legacy(live_test.LiveTest)
  }));
  dart.setLibraryUri(live_suite_controller._LiveSuite, L6);
  dart.setFieldSignature(live_suite_controller._LiveSuite, () => ({
    __proto__: dart.getFields(live_suite_controller._LiveSuite.__proto__),
    [_controller$0]: dart.finalFieldType(dart.legacy(live_suite_controller.LiveSuiteController))
  }));
  var _liveSuite = dart.privateName(live_suite_controller, "_liveSuite");
  var _closeMemo$ = dart.privateName(live_suite_controller, "_closeMemo");
  live_suite_controller.LiveSuiteController = class LiveSuiteController extends core.Object {
    get liveSuite() {
      return this[_liveSuite];
    }
    reportLiveTest(liveTest, opts) {
      let countSuccess = opts && 'countSuccess' in opts ? opts.countSuccess : true;
      if (dart.test(this[_onTestStartedController].isClosed)) {
        dart.throw(new core.StateError.new("Can't call reportLiveTest() after noMoreTests()."));
      }
      if (!dart.equals(liveTest.suite, this[_suite$])) dart.assertFailed(null, L7, 116, 12, "liveTest.suite == _suite");
      if (!(this[_active] == null)) dart.assertFailed(null, L7, 117, 12, "_active == null");
      this[_active] = liveTest;
      liveTest.onStateChange.listen(dart.fn(state => {
        if (!dart.equals(state.status, state$.Status.complete)) return;
        this[_active] = null;
        if (dart.equals(state.result, state$.Result.skipped)) {
          this[_skipped].add(liveTest);
        } else if (!dart.equals(state.result, state$.Result.success)) {
          this[_passed].remove(liveTest);
          this[_failed].add(liveTest);
        } else if (dart.test(countSuccess)) {
          this[_passed].add(liveTest);
          this[_failed].remove(liveTest);
        }
      }, StateLToNullN()));
      this[_onTestStartedController].add(liveTest);
      this[_onCompleteGroup].add(liveTest.onComplete);
    }
    noMoreLiveTests() {
      this[_onTestStartedController].close();
      this[_onCompleteGroup].close();
    }
    close() {
      return this[_closeMemo$].runOnce(dart.fn(() => async.async(core.Null, (function*() {
        try {
          yield this[_suite$].close();
        } finally {
          this[_onCloseCompleter].complete();
        }
      }).bind(this)), VoidToFutureLOfNullN()));
    }
  };
  (live_suite_controller.LiveSuiteController.new = function(_suite) {
    this[_liveSuite] = null;
    this[_onCompleteGroup] = new future_group.FutureGroup.new();
    this[_isComplete] = false;
    this[_onCloseCompleter] = async.Completer.new();
    this[_onTestStartedController] = StreamControllerOfLiveTestL().broadcast({sync: true});
    this[_passed] = new (_HashSetOfLiveTestL()).new();
    this[_skipped] = new (_HashSetOfLiveTestL()).new();
    this[_failed] = new (_HashSetOfLiveTestL()).new();
    this[_active] = null;
    this[_closeMemo$] = new async_memoizer.AsyncMemoizer.new();
    this[_suite$] = _suite;
    this[_liveSuite] = new live_suite_controller._LiveSuite.new(this);
    this[_onCompleteGroup].future.then(core.Null, dart.fn(_ => {
      this[_isComplete] = true;
    }, ListLToNullN()), {onError: dart.fn(_ => {
      }, dynamicToNullN())});
  }).prototype = live_suite_controller.LiveSuiteController.prototype;
  dart.addTypeTests(live_suite_controller.LiveSuiteController);
  dart.addTypeCaches(live_suite_controller.LiveSuiteController);
  dart.setMethodSignature(live_suite_controller.LiveSuiteController, () => ({
    __proto__: dart.getMethods(live_suite_controller.LiveSuiteController.__proto__),
    reportLiveTest: dart.fnType(dart.void, [dart.legacy(live_test.LiveTest)], {countSuccess: dart.legacy(core.bool)}, {}),
    noMoreLiveTests: dart.fnType(dart.void, []),
    close: dart.fnType(dart.legacy(async.Future), [])
  }));
  dart.setGetterSignature(live_suite_controller.LiveSuiteController, () => ({
    __proto__: dart.getGetters(live_suite_controller.LiveSuiteController.__proto__),
    liveSuite: dart.legacy(live_suite.LiveSuite)
  }));
  dart.setLibraryUri(live_suite_controller.LiveSuiteController, L6);
  dart.setFieldSignature(live_suite_controller.LiveSuiteController, () => ({
    __proto__: dart.getFields(live_suite_controller.LiveSuiteController.__proto__),
    [_liveSuite]: dart.fieldType(dart.legacy(live_suite.LiveSuite)),
    [_suite$]: dart.finalFieldType(dart.legacy(runner_suite.RunnerSuite)),
    [_onCompleteGroup]: dart.finalFieldType(dart.legacy(future_group.FutureGroup)),
    [_isComplete]: dart.fieldType(dart.legacy(core.bool)),
    [_onCloseCompleter]: dart.finalFieldType(dart.legacy(async.Completer)),
    [_onTestStartedController]: dart.finalFieldType(dart.legacy(async.StreamController$(dart.legacy(live_test.LiveTest)))),
    [_passed]: dart.finalFieldType(dart.legacy(core.Set$(dart.legacy(live_test.LiveTest)))),
    [_skipped]: dart.finalFieldType(dart.legacy(core.Set$(dart.legacy(live_test.LiveTest)))),
    [_failed]: dart.finalFieldType(dart.legacy(core.Set$(dart.legacy(live_test.LiveTest)))),
    [_active]: dart.fieldType(dart.legacy(live_test.LiveTest)),
    [_closeMemo$]: dart.finalFieldType(dart.legacy(async_memoizer.AsyncMemoizer))
  }));
  io_stub.currentPlatform = function currentPlatform(runtime) {
    return dart.throw(new core.UnsupportedError.new("Getting the current platform is only supported where dart:io exists"));
  };
  var path$ = dart.privateName(load_exception, "LoadException.path");
  var innerError$ = dart.privateName(load_exception, "LoadException.innerError");
  load_exception.LoadException = class LoadException extends core.Object {
    get path() {
      return this[path$];
    }
    set path(value) {
      super.path = value;
    }
    get innerError() {
      return this[innerError$];
    }
    set innerError(value) {
      super.innerError = value;
    }
    toString(opts) {
      let color = opts && 'color' in opts ? opts.color : false;
      let buffer = new core.StringBuffer.new();
      if (dart.test(color)) buffer.write("[31m");
      buffer.write("Failed to load \"" + dart.str(this.path) + "\":");
      if (dart.test(color)) buffer.write("[0m");
      let innerString = utils.getErrorMessage(this.innerError);
      if (SourceSpanExceptionL().is(this.innerError)) {
        innerString = SourceSpanExceptionL().as(this.innerError).toString({color: color})[$replaceFirst](" of " + dart.str(this.path), "");
      }
      buffer.write(innerString[$contains]("\n") ? "\n" : " ");
      buffer.write(innerString);
      return buffer.toString();
    }
  };
  (load_exception.LoadException.new = function(path, innerError) {
    this[path$] = path;
    this[innerError$] = innerError;
    ;
  }).prototype = load_exception.LoadException.prototype;
  dart.addTypeTests(load_exception.LoadException);
  dart.addTypeCaches(load_exception.LoadException);
  load_exception.LoadException[dart.implements] = () => [core.Exception];
  dart.setMethodSignature(load_exception.LoadException, () => ({
    __proto__: dart.getMethods(load_exception.LoadException.__proto__),
    toString: dart.fnType(dart.legacy(core.String), [], {color: dart.legacy(core.bool)}, {}),
    [$toString]: dart.fnType(dart.legacy(core.String), [], {color: dart.legacy(core.bool)}, {})
  }));
  dart.setLibraryUri(load_exception.LoadException, L8);
  dart.setFieldSignature(load_exception.LoadException, () => ({
    __proto__: dart.getFields(load_exception.LoadException.__proto__),
    path: dart.finalFieldType(dart.legacy(core.String)),
    innerError: dart.finalFieldType(dart.legacy(core.Object))
  }));
  dart.defineExtensionMethods(load_exception.LoadException, ['toString']);
  var supportsDebugging$ = dart.privateName(environment$, "PluginEnvironment.supportsDebugging");
  environment$.PluginEnvironment = class PluginEnvironment extends core.Object {
    get supportsDebugging() {
      return this[supportsDebugging$];
    }
    set supportsDebugging(value) {
      super.supportsDebugging = value;
    }
    get onRestart() {
      return async.StreamController.broadcast().stream;
    }
    get observatoryUrl() {
      return null;
    }
    get remoteDebuggerUrl() {
      return null;
    }
    displayPause() {
      return dart.throw(new core.UnsupportedError.new("PluginEnvironment.displayPause is not supported."));
    }
  };
  (environment$.PluginEnvironment.new = function() {
    this[supportsDebugging$] = false;
    ;
  }).prototype = environment$.PluginEnvironment.prototype;
  dart.addTypeTests(environment$.PluginEnvironment);
  dart.addTypeCaches(environment$.PluginEnvironment);
  environment$.PluginEnvironment[dart.implements] = () => [environment.Environment];
  dart.setMethodSignature(environment$.PluginEnvironment, () => ({
    __proto__: dart.getMethods(environment$.PluginEnvironment.__proto__),
    displayPause: dart.fnType(dart.legacy(cancelable_operation.CancelableOperation), [])
  }));
  dart.setGetterSignature(environment$.PluginEnvironment, () => ({
    __proto__: dart.getGetters(environment$.PluginEnvironment.__proto__),
    onRestart: dart.legacy(async.Stream),
    observatoryUrl: dart.legacy(core.Uri),
    remoteDebuggerUrl: dart.legacy(core.Uri)
  }));
  dart.setLibraryUri(environment$.PluginEnvironment, L9);
  dart.setFieldSignature(environment$.PluginEnvironment, () => ({
    __proto__: dart.getFields(environment$.PluginEnvironment.__proto__),
    supportsDebugging: dart.finalFieldType(dart.legacy(core.bool))
  }));
  dart.trackLibraries("packages/test_core/src/runner/environment", {
    "package:test_core/src/runner/live_suite.dart": live_suite,
    "package:test_core/src/runner/runner_suite.dart": runner_suite,
    "package:test_core/src/runner/environment.dart": environment,
    "package:test_core/src/runner/suite.dart": suite$,
    "package:test_core/src/runner/runtime_selection.dart": runtime_selection,
    "package:test_core/src/runner/reporter.dart": reporter,
    "package:test_core/src/runner/live_suite_controller.dart": live_suite_controller,
    "package:test_core/src/util/io_stub.dart": io_stub,
    "package:test_core/src/runner/load_exception.dart": load_exception,
    "package:test_core/src/runner/plugin/environment.dart": environment$
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["live_suite.dart","runner_suite.dart","environment.dart","suite.dart","runtime_selection.dart","reporter.dart","live_suite_controller.dart","../util/io_stub.dart","load_exception.dart","plugin/environment.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;AA8DQ,iBAAO,+BAAC,aAAQ,cAAS;AAC7B,UAAI,eAAU,MAAM,AAAK,AAAuB,IAAxB,OAAS,gCAAK,CAAC;AACvC,YAAgB,kCAAK,IAAI;IAC3B;;;;EAuBF;;;;;;;;;;;;;;;;;AC1DiC,YAAA,AAAY;IAAY;;AAGtB,YAAA,AAAY;IAAO;;AAM5B,YAAA,AAAY;IAAY;;AAMhB,YAAA,AAAY,AAAuB;IAAM;YAO5C;AAAS,YAAA,AAAY,4BAAQ,IAAI;IAAC;eAI/B,aAAgC,QACtD,OAAqB;UACnB;UAAiB;AACvB,uBACsB,8CAAO,WAAW,EAAE,MAAM,YAAW,OAAO;AAClE,kBAAoB,gCAAE,UAAU,EAAE,KAAK,EAAE,IAAI,EAAE,QAAQ;AACpB,MAAvC,AAAW,UAAD,WAAiB,6BAAM,KAAK;AACtC,YAAO,MAAK;IACd;WAMwB;AAClB,qBAAW,AAAM,kBAAO,QAAQ;AACW,MAA/C,AAAS,QAAD,IAAC,OAAT,WAAmB,qBAAK,0CAAc,kBAA7B;AACT,YAAmB,iCAAE,oBAAa,QAAQ,EAAE,WAAM;IACpD;;AAGkB,YAAA,AAAY;IAAQ;;0CAV7B,aAAmB,OAAc,MAAoB;IAArD;AACH,sDAAM,KAAK,EAAE,QAAQ,SAAQ,IAAI;;EAAC;;;;;;;;;;;;;;;;;;;;;;;;;;;AAeP;IAAM;iBA2ChB;AACrB,UAAc,YAAV,SAAS,EAAI,qBAAc;AACP,MAAxB,qBAAe,SAAS;AACa,MAArC,AAAuB,iCAAI,SAAS;IACtC;YAW6B;AAC3B,qBAAK,AAAc,wBAAI,IAAI;AAC8C,QAAvE,WAAM,wBAAW,AAAqD,2DAAP,IAAI;;AAGjE,oBAAU,AAAc;AAEsC,MADlE,AAAc,AACT,8BAAI,2CAAC,QAAQ,gBAAgB,QAAQ,IAAI,EAAE,MAAM,AAAQ,OAAD;AAC7D,YAAO,QAAO;IAChB;;AAGmB,YAAA,AAAW,0BAAQ;AACI,QAApC,MAAM,AAAuB;AAC7B,YAAI,kBAAY,MAAM,AAAgB,MAAV,AAAQ;MACrC;IAAC;;qDAjDqB,cAAmB,SAAc,eAC1C,aAA2B;QACjC;QAAiB;IAzBT;IAef,qBAAe;IAGd,+BAAyB;IAGzB,sBAAgB;IAoDhB,mBAAa;IAlDQ;IAAmB;IAAc;IAG7C,iBAAE,OAAO;AAEqD,IAD3E,eACI,AAAY,WAAD,sBAAM,QAAC,SAAsB,gCAAE,MAAM,KAAK,EAAE,IAAI,EAAE,QAAQ;EAC3E;wDAIkC,cAAmB;QACrC;IAlCI;IAef,qBAAe;IAGd,+BAAyB;IAGzB,sBAAgB;IAoDhB,mBAAa;IAxCe;IAAmB;IAEjC,uBAAE;IACP,iBAAE,OAAO;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ECtF1B;;;;;;IAIQ;;;;;;;AACkB,YAAiB,AAAY;IAAM;;AAIjC;IAAI;;AAED;IAAI;;AAEK,wBAAM,8BACxC;IAAmD;;;IAVjD,0BAAoB;;EAGD;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ICDZ;;;;;;IAOM;;;;;;IAMA;;;;;;IAYG;;;;;;IAMA;;;;;;IAMyB;;;;;;IAOC;;;;;;;;AAxD5B;0BAAY;IAAK;;;AAId;0BAAe;IAAK;;AAwBd,YAAA,AAAU,oBAAG,qBAE/B,6BAAa,AAAU,oCAAI,QAAC,WAAY,AAAQ,OAAD;IAAO;;AA8B/D,oBAAI,AAAK,kCAAW,AAAW,4BAAS,MAAO;AAC/C,YAAO,AAAU,iCACL,AAAK,iDAAI,SAAC,KAAK,WAAW,gDAAS,GAAG,EAAE,AAAO,MAAD,2GAElD,AAAW,wDAAI,SAAC,KAAK,WAAW,iDAAS,GAAG,EAAE,AAAO,MAAD;IAC9D;;;AAME,UAAI,oBAAc,MAAM,MAAO;AAE3B,wBAAQ,AAAY,AAAU,sCAAA;AAC9B,kBAAO,AAAY;AACnB,kBAAO,AAAU;;;AAErB,eAAS,WAAY,AAAK;AACQ,QAAhC,AAAM,KAAD,QAAQ,AAAS,QAAD;;AAGvB,eAAS,gBAAiB;AACa,QAArC,AAAM,KAAD,QAAQ,AAAc,aAAD;;AAGW,MAAvC,mBAAa,yCAAoB,KAAK;AACtC,YAAO;IACT;;AAM2C;AACzC,eAAO,AAAK;AACZ,eAAO,AAAW;MACpB;;;UAGU;UACD;UACY;UACV;UACW;UACS;UACX;UACA;UACyB;UACC;UAGlC;UACH;UACA;UACA;UACD;UACG;UACU;UACA;AACf,mBAA4B,2CACnB,OAAO,cACJ,UAAU,eACT,WAAW,mBACP,eAAe,YACtB,QAAQ,YACR,QAAQ,eACL,WAAW,eACX,WAAW,QAClB,IAAI,cACE,UAAU,YACZ,iCACG,OAAO,gBACF,YAAY,oBACR,gBAAgB,QAC5B,IAAI,SACH,KAAK,cACA,UAAU,UACd,MAAM,QACR,OAAO;AACrB,YAAO,AAAO,OAAD;IACf;wBA+BiD;AAC7C,YAAmB,yCACT,AAAS,AAAO,QAAR,yDAAY,SAAC,KAAK,UAC5B,0DAAS,GAAG,EAAqB,uCAAa,KAAK,mGAC3C,AAAS,AAAW,QAAZ,8DAAgB,SAAC,KAAK,UACtC,2DAAS,GAAG,EAAqB,uCAAa,KAAK,mGAC7C,AAAS,QAAD,iBAAgB,iEAAgB;IAAI;oBAK1B;AAClC,UAAI,AAAM,KAAD,IAAI,MAAM,MAAO;AACtB,iBAAO,wCAAqB,KAAK;AACrC,oBAAI,AAAK,IAAD,aAAU,MAAO;AACzB,YAAO,KAAI;IACb;sBAGsC;AACpC,UAAI,AAAM,KAAD,IAAI,kBAAQ,AAAM,KAAD,aAAU;AACpC,YAAW,wDAAa,KAAK;IAC/B;UAO4C;;AAC1C,UAAI,AAAK,aAAsB,kCAAO,MAAO,MAAK;AAClD,UAAU,YAAN,KAAK,EAAuB,kCAAO,MAAO;AAE1C,mBAA4B,4CACJ,KAAf,AAAM,KAAD,kBAAC,OAAY,mCACG,MAAlB,AAAM,KAAD,sBAAC,OAAe,+CACpB,AAAY,6BAAA;AAAU,wBAAO,AAAM,KAAD;;iCACR,OAAtB,AAAM,KAAD,0BAAC,OAAmB,wCAChC,AAAS,oBAAM,AAAM,KAAD,uBACJ,OAAhB,AAAM,KAAD,qBAAC,OAAa,sCAChB,AAAY,8BAAa,AAAM,KAAD,4BAC9B,AAAY,uBAAM,AAAM,KAAD,qBAC9B,2CAAiB,WAAM,AAAM,KAAD,oBACtB,4CAAiB,iBAAY,AAAM,KAAD,wBACpC,AAAS,oBAAM,AAAM,KAAD;AAClC,YAAO,AAAO,OAAD;IACf;;;UAOU;UACD;UACY;UACV;UACW;UACS;UACX;UACA;UACyB;UACC;UAGlC;UACH;UACA;UACA;UACD;UACG;UACU;UACA;AACf,mBAA4B,4CACX,KAAR,OAAO,QAAP,OAAW,mCACG,MAAX,UAAU,SAAV,OAAc,wCACS,eAAtB,WAAW,iBAAX,OAAa,0BAAb,OAA8B,4CACV,OAAhB,eAAe,UAAf,OAAwB,yCACtB,OAAT,QAAQ,UAAR,OAAiB,kCACR,OAAT,QAAQ,UAAR,OAAY,uCACG,OAAZ,WAAW,UAAX,OAAoB,wCACR,OAAZ,WAAW,UAAX,OAAoB,iCACtB,OAAL,IAAI,UAAJ,OAAa,gCACI,OAAX,UAAU,UAAV,OAAmB,mCACrB,AAAU,iCACP,OAAO,gBACF,YAAY,oBACR,gBAAgB,QAC5B,IAAI,SACH,KAAK,cACA,UAAU,UACd,MAAM,gBACR,OAAO,iBAAP,OAAS;AACvB,YAAO,AAAO,OAAD;IACf;qBAGoC;AAC9B,2BACA,AAAY,AAAqC,WAAtC,kBAAK,QAAC,WAAY,AAAQ,OAAD;AACW,MAAnD,AAAU,0CAA0B,cAAc;AAElD,UAAI,mBAAa;AACf,iBAAS,YAAa;AACpB,yBAAK,AACA,WADW,OACP,QAAC,WAAY,AAAQ,AAAW,OAAZ,eAAe,AAAU,SAAD;AACnD,gBAAI,AAAU,SAAD,SAAS;AAEwC,cAD5D,WAAM,iDACF,AAAuC,iCAAlB,AAAU,SAAD,SAAM,OAAK,AAAU,SAAD;;AAEQ,cAA9D,WAAM,6BAAgB,AAAuC,iCAAlB,AAAU,SAAD,SAAM;;;;;AAShE,MAHF,AAAW,0BAAQ,SAAC,UAAU;AACK,QAAjC,AAAS,QAAD,UAAU,cAAc;AACI,QAApC,AAAO,MAAD,kBAAkB,WAAW;;IAEvC;gBAI6C;AAC3C,oBAAI,AAAW,4BAAS,MAAO;AAE3B,mBAAS;AAIX,MAHF,AAAW,0BAAQ,SAAC,kBAAkB;AACpC,uBAAK,AAAiB,gBAAD,UAAU,QAAQ,IAAG;AACL,QAArC,SAAS,AAAO,MAAD,OAAO,cAAc;;AAEtC,YAAO,AAAO,OAAD,qBAAoB;IACnC;0BAOmC,MAAiC;AAChE,wEAAU,IAAI,EAAE,IAAI,UACT,SAAC,SAAS,YAAY,AAAQ,OAAD,OAAO,OAAO;IAAE;;AAK1D,oBAAI,AAAU,AAAK,6CAAW,AAAK,sBAAS,MAAO;AAG/C,oBAAU,2DAA8C;AACxD,mBAAS,AAAK,AAAK,+CAAK,iCAAO,SAAoB,QAAQ;AAC7D,uBAAK,AAAS,QAAD,UAAU,AAAU,wBAAO,MAAO,OAAM;AACrD,cAAO,AAAO,OAAD,OAAO,AAAQ,OAAD,UAAQ,QAAQ;;AAG7C,UAAW,YAAP,MAAM,EAAI,kCAAO,MAAO;AAC5B,YAAO,AAAK,AAAsB,oBAAT,OAAO,SAAQ,MAAM;IAChD;;;;QAtLU;QACD;QACY;QACZ;QACa;QACS;QACX;QACA;QACyB;QACC;QACjC;IApED;IA6DH;IAQM,iBAAE,OAAO;IACN,oBAAE,UAAU;IACZ,sBAAqB,KAAnB,2CAAM,WAAW,SAAjB;IACL,kBAAE,2CAAsC,cAAlB,QAAQ,gBAAR,OAAU,wBAAV,OAAqB;IAC1C,kBAAE,qDAAM,QAAQ;IACd,sBAAc,OAAZ,WAAW,UAAX,OAA+B;IACjC,sBAAc,OAAZ,WAAW,UAAX,OAA+B;IACxC,cAAE,0EAAK,IAAI;IACL,oBAAE,2EAAK,UAAU;IAClB,mBAAW,OAAT,QAAQ,UAAR,OAAqB;;EAAK;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MA3K/B,+BAAK;YAAsB;;;;;;ICf3B;;;;;;IAKI;;;;;;;UAIA;AAAU,YAAM,AAAoB,wBAA1B,KAAK,KAAwB,AAAM,AAAK,KAAN,SAAS;IAAI;;AAEtD,YAAK,eAAL;IAAa;;qDAJX,MAAY;IAAZ;IAAY;;EAAM;;;;;;;;;;;;;;;;;;;;;;;;ECQ1C;;;;;;;;;;;;;;;;ACJ2B,YAAA,AAAY;IAAM;;AAEpB,YAAA,AAAY;IAAW;;AAErB,YAAA,AAAY,AAAiB;IAAM;;AAEvC,YAAA,AAAY,AAAkB;IAAW;;AAExC,YAAA,AAAY,AAAkB;IAAM;;AAGtD,YAAA,AAAY,AAAyB;IAAM;;AAEnB,wDAAoB,AAAY;IAAQ;;AAEvC,wDAAoB,AAAY;IAAS;;AAE1C,wDAAoB,AAAY;IAAQ;;AAE7C,YAAA,AAAY;IAAO;;;IAE1B;;EAAY;;;;;;;;;;;;;;;;;;;;;;;;;AAaD;IAAU;mBAwDR;UAAgB;AAC3C,oBAAI,AAAyB;AACyC,QAApE,WAAM,wBAAW;;AAGnB,WAAsB,YAAf,AAAS,QAAD,QAAU;AACzB,YAAO,AAAQ,iBAAG;AAEA,MAAlB,gBAAU,QAAQ;AAgBhB,MAdF,AAAS,AAAc,QAAf,sBAAsB,QAAC;AAC7B,yBAAI,AAAM,KAAD,SAAkB,yBAAU;AACvB,QAAd,gBAAU;AAEV,YAAiB,YAAb,AAAM,KAAD,SAAkB;AACH,UAAtB,AAAS,mBAAI,QAAQ;cAChB,kBAAI,AAAM,KAAD,SAAkB;AACR,UAAxB,AAAQ,qBAAO,QAAQ;AACF,UAArB,AAAQ,kBAAI,QAAQ;cACf,eAAI,YAAY;AACA,UAArB,AAAQ,kBAAI,QAAQ;AAEI,UAAxB,AAAQ,qBAAO,QAAQ;;;AAIW,MAAtC,AAAyB,mCAAI,QAAQ;AAEI,MAAzC,AAAiB,2BAAI,AAAS,QAAD;IAC/B;;AAKkC,MAAhC,AAAyB;AACD,MAAxB,AAAiB;IACnB;;AAGkB,YAAA,AAAW,2BAAQ;AAC/B;AACsB,UAApB,MAAM,AAAO;;AAEe,UAA5B,AAAkB;;MAErB;IAAC;;4DA5DmB;IAxCf;IAQJ,yBAAmB;IAGrB,oBAAc;IAKZ,0BAAoB;IAGpB,iCACF,+CAA2C;IAGzC,gBAAU;IAGV,iBAAW;IAGX,gBAAU;IAGP;IAqEH,oBAAa;IA7DM;AACM,IAA7B,mBAAa,yCAAW;AAIL,IAFnB,AAAiB,AAAO,8CAAK,QAAC;AACV,MAAlB,oBAAc;kCACJ,QAAC;;EACf;;;;;;;;;;;;;;;;;;;;;;;;;;;;qDC9FoC;AAAY,sBAAM,8BACpD;EAAsE;;;;ICA3D;;;;;;IAEA;;;;;;;UAIS;AAChB,mBAAS;AACb,oBAAI,KAAK,GAAE,AAAO,AAAmB,MAApB,OAAO;AACe,MAAvC,AAAO,MAAD,OAAO,AAAyB,+BAAP,aAAI;AACnC,oBAAI,KAAK,GAAE,AAAO,AAAkB,MAAnB,OAAO;AAEpB,wBAAc,sBAAgB;AAClC,UAAe,0BAAX;AAGgC,QAFlC,cAA0B,AACrB,AACA,0BAFU,kCACM,KAAK,kBACR,AAAW,kBAAL,YAAO;;AAGoB,MAArD,AAAO,MAAD,OAAO,AAAY,WAAD,YAAU,QAAQ,OAAO;AACxB,MAAzB,AAAO,MAAD,OAAO,WAAW;AACxB,YAAO,AAAO,OAAD;IACf;;+CAlBmB,MAAW;IAAX;IAAW;;EAAW;;;;;;;;;;;;;;;;;;ICAnC;;;;;;;AACkB,YAAiB,AAAY;IAAM;;AAIjC;IAAI;;AAED;IAAI;;AAEK,wBAAM,8BACxC;IAAmD;;;IAVjD,2BAAoB;;EAGD","file":"environment.ddc.js"}');
  // Exports:
  return {
    src__runner__live_suite: live_suite,
    src__runner__runner_suite: runner_suite,
    src__runner__environment: environment,
    src__runner__suite: suite$,
    src__runner__runtime_selection: runtime_selection,
    src__runner__reporter: reporter,
    src__runner__live_suite_controller: live_suite_controller,
    src__util__io_stub: io_stub,
    src__runner__load_exception: load_exception,
    src__runner__plugin__environment: environment$
  };
}));

//# sourceMappingURL=environment.ddc.js.map
