define(['dart_sdk', 'packages/angular/src/core/change_detection/change_detection', 'packages/angular/src/bootstrap/modules', 'packages/collection/src/priority_queue', 'packages/pedantic/pedantic'], (function load__packages__angular_test__src__bootstrap(dart_sdk, packages__angular__src__core__change_detection__change_detection, packages__angular__src__bootstrap__modules, packages__collection__src__priority_queue, packages__pedantic__pedantic) {
  'use strict';
  const core = dart_sdk.core;
  const html = dart_sdk.html;
  const _interceptors = dart_sdk._interceptors;
  const async = dart_sdk.async;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const injector = packages__angular__src__core__change_detection__change_detection.src__di__injector__injector;
  const empty = packages__angular__src__core__change_detection__change_detection.src__di__injector__empty;
  const hierarchical = packages__angular__src__core__change_detection__change_detection.src__di__injector__hierarchical;
  const ng_zone = packages__angular__src__core__change_detection__change_detection.src__core__zone__ng_zone;
  const runtime = packages__angular__src__core__change_detection__change_detection.src__di__injector__runtime;
  const dynamic_component_loader = packages__angular__src__bootstrap__modules.src__core__linker__dynamic_component_loader;
  const component_resolver = packages__angular__src__bootstrap__modules.src__core__linker__component_resolver;
  const component_factory = packages__angular__src__bootstrap__modules.src__core__linker__component_factory;
  const application_ref = packages__angular__src__bootstrap__modules.src__core__application_ref;
  const modules = packages__angular__src__bootstrap__modules.src__bootstrap__modules;
  const run = packages__angular__src__bootstrap__modules.src__bootstrap__run;
  const app_view = packages__angular__src__bootstrap__modules.src__core__linker__app_view;
  const priority_queue = packages__collection__src__priority_queue.src__priority_queue;
  const pedantic = packages__pedantic__pedantic.pedantic;
  var bed = Object.create(dart.library);
  var stabilizer = Object.create(dart.library);
  var timer_hook_zone = Object.create(dart.library);
  var errors = Object.create(dart.library);
  var will_never_stabilize = Object.create(dart.library);
  var test_already_running = Object.create(dart.library);
  var generic_type_missing = Object.create(dart.library);
  var real_time_stabilizer = Object.create(dart.library);
  var base_stabilizer = Object.create(dart.library);
  var fixture = Object.create(dart.library);
  var bootstrap = Object.create(dart.library);
  var $toList = dartx.toList;
  var $append = dartx.append;
  var $_equals = dartx._equals;
  var $addAll = dartx.addAll;
  var $isNotEmpty = dartx.isNotEmpty;
  var $every = dartx.every;
  var $isEmpty = dartx.isEmpty;
  var $map = dartx.map;
  var $toString = dartx.toString;
  var $last = dartx.last;
  var $join = dartx.join;
  var $compareTo = dartx.compareTo;
  var $parent = dartx.parent;
  var $remove = dartx.remove;
  var $text = dartx.text;
  var InjectorL = () => (InjectorL = dart.constFn(dart.legacy(injector.Injector)))();
  var InjectorLToInjectorL = () => (InjectorLToInjectorL = dart.constFn(dart.fnType(InjectorL(), [], [InjectorL()])))();
  var ObjectL = () => (ObjectL = dart.constFn(dart.legacy(core.Object)))();
  var NgTestStabilizerL = () => (NgTestStabilizerL = dart.constFn(dart.legacy(stabilizer.NgTestStabilizer)))();
  var TimerHookZoneL = () => (TimerHookZoneL = dart.constFn(dart.legacy(timer_hook_zone.TimerHookZone)))();
  var InjectorLAndTimerHookZoneLToNgTestStabilizerL = () => (InjectorLAndTimerHookZoneLToNgTestStabilizerL = dart.constFn(dart.fnType(NgTestStabilizerL(), [InjectorL()], [TimerHookZoneL()])))();
  var dynamicToNgTestStabilizerL = () => (dynamicToNgTestStabilizerL = dart.constFn(dart.fnType(NgTestStabilizerL(), [dart.dynamic])))();
  var HierarchicalInjectorL = () => (HierarchicalInjectorL = dart.constFn(dart.legacy(hierarchical.HierarchicalInjector)))();
  var NgZoneL = () => (NgZoneL = dart.constFn(dart.legacy(ng_zone.NgZone)))();
  var SlowComponentLoaderL = () => (SlowComponentLoaderL = dart.constFn(dart.legacy(dynamic_component_loader.SlowComponentLoader)))();
  var InjectorLToNgTestStabilizerL = () => (InjectorLToNgTestStabilizerL = dart.constFn(dart.fnType(NgTestStabilizerL(), [InjectorL()])))();
  var InjectorLToLNgTestStabilizerL = () => (InjectorLToLNgTestStabilizerL = dart.constFn(dart.legacy(InjectorLToNgTestStabilizerL())))();
  var JSArrayOfInjectorLToLNgTestStabilizerL = () => (JSArrayOfInjectorLToLNgTestStabilizerL = dart.constFn(_interceptors.JSArray$(InjectorLToLNgTestStabilizerL())))();
  var ReflectiveInjectorL = () => (ReflectiveInjectorL = dart.constFn(dart.legacy(runtime.ReflectiveInjector)))();
  var InjectorLToReflectiveInjectorL = () => (InjectorLToReflectiveInjectorL = dart.constFn(dart.fnType(ReflectiveInjectorL(), [], [InjectorL()])))();
  var VoidToNgZoneL = () => (VoidToNgZoneL = dart.constFn(dart.fnType(NgZoneL(), [])))();
  var InjectorLAndTimerHookZoneLToLNgTestStabilizerL = () => (InjectorLAndTimerHookZoneLToLNgTestStabilizerL = dart.constFn(dart.legacy(InjectorLAndTimerHookZoneLToNgTestStabilizerL())))();
  var CompleterOfvoid = () => (CompleterOfvoid = dart.constFn(async.Completer$(dart.void)))();
  var FutureOfNullN = () => (FutureOfNullN = dart.constFn(async.Future$(core.Null)))();
  var FutureLOfNullN = () => (FutureLOfNullN = dart.constFn(dart.legacy(FutureOfNullN())))();
  var VoidToFutureLOfNullN = () => (VoidToFutureLOfNullN = dart.constFn(dart.fnType(FutureLOfNullN(), [])))();
  var boolL = () => (boolL = dart.constFn(dart.legacy(core.bool)))();
  var FutureOfboolL = () => (FutureOfboolL = dart.constFn(async.Future$(boolL())))();
  var FutureLOfboolL = () => (FutureLOfboolL = dart.constFn(dart.legacy(FutureOfboolL())))();
  var VoidToFutureLOfboolL = () => (VoidToFutureLOfboolL = dart.constFn(dart.fnType(FutureLOfboolL(), [])))();
  var FutureOfvoid = () => (FutureOfvoid = dart.constFn(async.Future$(dart.void)))();
  var FutureLOfvoid = () => (FutureLOfvoid = dart.constFn(dart.legacy(FutureOfvoid())))();
  var InjectorLToFutureLOfvoid = () => (InjectorLToFutureLOfvoid = dart.constFn(dart.fnType(FutureLOfvoid(), [InjectorL()])))();
  var ApplicationRefL = () => (ApplicationRefL = dart.constFn(dart.legacy(application_ref.ApplicationRef)))();
  var VoidToboolL = () => (VoidToboolL = dart.constFn(dart.fnType(boolL(), [])))();
  var NgTestStabilizerLToboolL = () => (NgTestStabilizerLToboolL = dart.constFn(dart.fnType(boolL(), [NgTestStabilizerL()])))();
  var FnToNgTestStabilizerL = () => (FnToNgTestStabilizerL = dart.constFn(dart.fnType(NgTestStabilizerL(), [InjectorLToLNgTestStabilizerL()])))();
  var _DelegatingNgTestStabilizerL = () => (_DelegatingNgTestStabilizerL = dart.constFn(dart.legacy(stabilizer._DelegatingNgTestStabilizer)))();
  var InjectorLAndTimerHookZoneLTo_DelegatingNgTestStabilizerL = () => (InjectorLAndTimerHookZoneLTo_DelegatingNgTestStabilizerL = dart.constFn(dart.fnType(_DelegatingNgTestStabilizerL(), [InjectorL()], [TimerHookZoneL()])))();
  var TimerL = () => (TimerL = dart.constFn(dart.legacy(async.Timer)))();
  var ZoneL = () => (ZoneL = dart.constFn(dart.legacy(async.Zone)))();
  var ZoneDelegateL = () => (ZoneDelegateL = dart.constFn(dart.legacy(async.ZoneDelegate)))();
  var DurationL = () => (DurationL = dart.constFn(dart.legacy(core.Duration)))();
  var VoidTovoid = () => (VoidTovoid = dart.constFn(dart.fnType(dart.void, [])))();
  var VoidToLvoid = () => (VoidToLvoid = dart.constFn(dart.legacy(VoidTovoid())))();
  var ZoneLAndZoneDelegateLAndZoneL__ToTimerL = () => (ZoneLAndZoneDelegateLAndZoneL__ToTimerL = dart.constFn(dart.fnType(TimerL(), [ZoneL(), ZoneDelegateL(), ZoneL(), DurationL(), VoidToLvoid()])))();
  var TimerLTovoid = () => (TimerLTovoid = dart.constFn(dart.fnType(dart.void, [TimerL()])))();
  var TimerLToLvoid = () => (TimerLToLvoid = dart.constFn(dart.legacy(TimerLTovoid())))();
  var ZoneLAndZoneDelegateLAndZoneL__ToTimerL$ = () => (ZoneLAndZoneDelegateLAndZoneL__ToTimerL$ = dart.constFn(dart.fnType(TimerL(), [ZoneL(), ZoneDelegateL(), ZoneL(), DurationL(), TimerLToLvoid()])))();
  var _ObservedTimerL = () => (_ObservedTimerL = dart.constFn(dart.legacy(real_time_stabilizer._ObservedTimer)))();
  var HeapPriorityQueueOf_ObservedTimerL = () => (HeapPriorityQueueOf_ObservedTimerL = dart.constFn(priority_queue.HeapPriorityQueue$(_ObservedTimerL())))();
  var VoidToNullN = () => (VoidToNullN = dart.constFn(dart.fnType(core.Null, [])))();
  var VoidToFutureLOfvoid = () => (VoidToFutureLOfvoid = dart.constFn(dart.fnType(FutureLOfvoid(), [])))();
  var voidToboolL = () => (voidToboolL = dart.constFn(dart.fnType(boolL(), [dart.void])))();
  var NgZoneErrorL = () => (NgZoneErrorL = dart.constFn(dart.legacy(ng_zone.NgZoneError)))();
  var NgZoneErrorLToNullN = () => (NgZoneErrorLToNullN = dart.constFn(dart.fnType(core.Null, [NgZoneErrorL()])))();
  var JSArrayOfObjectL = () => (JSArrayOfObjectL = dart.constFn(_interceptors.JSArray$(ObjectL())))();
  var AppViewOfvoid = () => (AppViewOfvoid = dart.constFn(app_view.AppView$(dart.void)))();
  var AppViewLOfvoid = () => (AppViewLOfvoid = dart.constFn(dart.legacy(AppViewOfvoid())))();
  const CT = Object.create(null);
  var L3 = "package:angular_test/src/errors/will_never_stabilize.dart";
  var L7 = "package:angular_test/src/frontend/ng_zone/real_time_stabilizer.dart";
  var L8 = "package:angular_test/src/frontend/fixture.dart";
  var L6 = "package:angular_test/src/frontend/ng_zone/base_stabilizer.dart";
  var L2 = "package:angular_test/src/frontend/ng_zone/timer_hook_zone.dart";
  var L0 = "package:angular_test/src/frontend/bed.dart";
  var L5 = "package:angular_test/src/errors/generic_type_missing.dart";
  var L4 = "package:angular_test/src/errors/test_already_running.dart";
  var L1 = "package:angular_test/src/frontend/stabilizer.dart";
  dart.defineLazy(CT, {
    get C0() {
      return C0 = dart.fn(bed.NgTestBed._defaultRootInjector, InjectorLToInjectorL());
    },
    get C1() {
      return C1 = dart.constList([], ObjectL());
    },
    get C2() {
      return C2 = dart.fn(bed.NgTestBed._defaultStabilizers, InjectorLAndTimerHookZoneLToNgTestStabilizerL());
    },
    get C3() {
      return C3 = dart.fn(bed.NgTestBed._alwaysStable, dynamicToNgTestStabilizerL());
    },
    get C5() {
      return C5 = dart.wrapType(SlowComponentLoaderL());
    },
    get C4() {
      return C4 = dart.constList([C5 || CT.C5], ObjectL());
    },
    get C6() {
      return C6 = dart.const({
        __proto__: stabilizer._AlwaysStableNgTestStabilizer.prototype
      });
    },
    get C7() {
      return C7 = dart.fn(base_stabilizer.BaseNgZoneStabilizer._noSideEffects, VoidTovoid());
    },
    get C8() {
      return C8 = dart.fn(modules.createNgZone, VoidToNgZoneL());
    }
  }, false);
  var _host = dart.privateName(bed, "_host");
  var _providers = dart.privateName(bed, "_providers");
  var _createStabilizer = dart.privateName(bed, "_createStabilizer");
  var _rootInjector = dart.privateName(bed, "_rootInjector");
  var C0;
  var _componentFactory = dart.privateName(bed, "_componentFactory");
  var C1;
  var C2;
  var C3;
  var C5;
  var C4;
  var _usesComponentFactory = dart.privateName(bed, "_usesComponentFactory");
  var _createDynamic = dart.privateName(bed, "_createDynamic");
  var _createRootInjectorFactory = dart.privateName(bed, "_createRootInjectorFactory");
  const _is_NgTestBed_default = Symbol('_is_NgTestBed_default');
  bed.NgTestBed$ = dart.generic(T => {
    var TL = () => (TL = dart.constFn(dart.legacy(T)))();
    var NgTestFixtureOfTL = () => (NgTestFixtureOfTL = dart.constFn(fixture.NgTestFixture$(TL())))();
    var NgTestFixtureLOfTL = () => (NgTestFixtureLOfTL = dart.constFn(dart.legacy(NgTestFixtureOfTL())))();
    var FutureOfNgTestFixtureLOfTL = () => (FutureOfNgTestFixtureLOfTL = dart.constFn(async.Future$(NgTestFixtureLOfTL())))();
    var ComponentFactoryOfTL = () => (ComponentFactoryOfTL = dart.constFn(component_factory.ComponentFactory$(TL())))();
    var ComponentFactoryLOfTL = () => (ComponentFactoryLOfTL = dart.constFn(dart.legacy(ComponentFactoryOfTL())))();
    var FutureLOfNgTestFixtureLOfTL = () => (FutureLOfNgTestFixtureLOfTL = dart.constFn(dart.legacy(FutureOfNgTestFixtureLOfTL())))();
    var ComponentRefOfTL = () => (ComponentRefOfTL = dart.constFn(component_factory.ComponentRef$(TL())))();
    var ComponentRefLOfTL = () => (ComponentRefLOfTL = dart.constFn(dart.legacy(ComponentRefOfTL())))();
    var ComponentRefLOfTLToFutureLOfNgTestFixtureLOfTL = () => (ComponentRefLOfTLToFutureLOfNgTestFixtureLOfTL = dart.constFn(dart.fnType(FutureLOfNgTestFixtureLOfTL(), [ComponentRefLOfTL()])))();
    var VoidToFutureLOfNgTestFixtureLOfTL = () => (VoidToFutureLOfNgTestFixtureLOfTL = dart.constFn(dart.fnType(FutureLOfNgTestFixtureLOfTL(), [])))();
    class NgTestBed extends core.Object {
      static _defaultHost() {
        let host = html.Element.tag("ng-test-bed");
        html.document.body[$append](host);
        return host;
      }
      static _defaultRootInjector(parent = null) {
        return new empty.EmptyInjector.new(HierarchicalInjectorL().as(parent));
      }
      static _alwaysStable(_) {
        return stabilizer.NgTestStabilizer.alwaysStable;
      }
      static _defaultStabilizers(injector, timerZone = null) {
        return real_time_stabilizer.RealTimeNgZoneStabilizer.new(timerZone, injector.provideType(NgZoneL(), dart.wrapType(NgZoneL())));
      }
      static forComponent(T, component, opts) {
        let host = opts && 'host' in opts ? opts.host : null;
        let rootInjector = opts && 'rootInjector' in opts ? opts.rootInjector : C0 || CT.C0;
        let watchAngularLifecycle = opts && 'watchAngularLifecycle' in opts ? opts.watchAngularLifecycle : true;
        if (dart.wrapType(dart.legacy(T))[$_equals](dart.wrapType(dart.dynamic))) {
          dart.throw(new generic_type_missing.GenericTypeMissingError.new());
        }
        if (component == null) {
          dart.throw(new core.ArgumentError.notNull("component"));
        }
        return new (bed.NgTestBed$(dart.legacy(T)))._useComponentFactory({component: component, rootInjector: rootInjector, host: host, watchAngularLifecycle: watchAngularLifecycle});
      }
      static new(opts) {
        let host = opts && 'host' in opts ? opts.host : null;
        let rootInjector = opts && 'rootInjector' in opts ? opts.rootInjector : null;
        let watchAngularLifecycle = opts && 'watchAngularLifecycle' in opts ? opts.watchAngularLifecycle : true;
        if (dart.wrapType(dart.legacy(T))[$_equals](dart.wrapType(dart.dynamic))) {
          dart.throw(new generic_type_missing.GenericTypeMissingError.new());
        }
        return bed.NgTestBed$(dart.legacy(T))._allowDynamicType({host: host, rootInjector: rootInjector, watchAngularLifecycle: watchAngularLifecycle});
      }
      static _allowDynamicType(opts) {
        let host = opts && 'host' in opts ? opts.host : null;
        let rootInjector = opts && 'rootInjector' in opts ? opts.rootInjector : null;
        let watchAngularLifecycle = opts && 'watchAngularLifecycle' in opts ? opts.watchAngularLifecycle : true;
        return new (bed.NgTestBed$(dart.legacy(T))).__({host: host, providers: C4 || CT.C4, stabilizer: dart.test(watchAngularLifecycle) ? C2 || CT.C2 : C3 || CT.C3, rootInjector: rootInjector});
      }
      get [_usesComponentFactory]() {
        return this[_componentFactory] != null;
      }
      addProviders(providers) {
        if (dart.test(this[_usesComponentFactory])) {
          dart.throw(new core.UnsupportedError.new("Use \"addInjector\" instead"));
        }
        return this.fork(TL(), {providers: bed._concat(ObjectL(), this[_providers], providers)});
      }
      addInjector(factory) {
        return this.fork(TL(), {rootInjector: dart.fn((parent = null) => {
            let t0;
            t0 = factory(parent);
            return this[_rootInjector](t0);
          }, InjectorLToInjectorL())});
      }
      addStabilizers(stabilizers) {
        let t0;
        return this.fork(TL(), {stabilizer: stabilizer.composeStabilizers((t0 = JSArrayOfInjectorLToLNgTestStabilizerL().of([this[_createStabilizer]]), (() => {
            t0[$addAll](stabilizers);
            return t0;
          })()))});
      }
      create(opts) {
        let beforeComponentCreated = opts && 'beforeComponentCreated' in opts ? opts.beforeComponentCreated : null;
        let beforeChangeDetection = opts && 'beforeChangeDetection' in opts ? opts.beforeChangeDetection : null;
        return this[_createDynamic](dart.wrapType(TL()), {beforeComponentCreated: beforeComponentCreated, beforeChangeDetection: beforeChangeDetection});
      }
      static _checkForActiveTest() {
        if (bed.activeTest != null) {
          dart.throw(new test_already_running.TestAlreadyRunningError.new());
        }
      }
      [_createRootInjectorFactory]() {
        let rootInjector = this[_rootInjector];
        if (dart.test(this[_providers][$isNotEmpty])) {
          rootInjector = dart.fn((parent = null) => {
            let t0;
            return runtime.ReflectiveInjector.resolveAndCreate(this[_providers], (t0 = parent, this[_rootInjector](t0)));
          }, InjectorLToReflectiveInjectorL());
        }
        return rootInjector;
      }
      [_createDynamic](type, opts) {
        let beforeComponentCreated = opts && 'beforeComponentCreated' in opts ? opts.beforeComponentCreated : null;
        let beforeChangeDetection = opts && 'beforeChangeDetection' in opts ? opts.beforeChangeDetection : null;
        bed.NgTestBed._checkForActiveTest();
        return FutureOfNgTestFixtureLOfTL().sync(dart.fn(() => {
          let t1, t1$;
          bed.NgTestBed._checkForActiveTest();
          let timerHookZone = new timer_hook_zone.TimerHookZone.new();
          let ngZoneInstance = null;
          function ngZoneFactory() {
            return timerHookZone.run(NgZoneL(), dart.fn(() => ngZoneInstance = new ng_zone.NgZone.new({enableLongStackTrace: true}), VoidToNgZoneL()));
          }
          dart.fn(ngZoneFactory, VoidToNgZoneL());
          let allStabilizers = null;
          const createStabilizersAndRunUserHook = injector => {
            return async.async(dart.void, (function* createStabilizersAndRunUserHook() {
              let createStabilizer = this[_createStabilizer];
              allStabilizers = InjectorLAndTimerHookZoneLToLNgTestStabilizerL().is(createStabilizer) ? createStabilizer(injector, timerHookZone) : createStabilizer(injector);
              if (beforeComponentCreated == null) {
                return null;
              }
              let completer = CompleterOfvoid().new();
              ngZoneInstance.runGuarded(dart.fn(() => async.async(core.Null, function*() {
                try {
                  yield beforeComponentCreated(injector);
                  completer.complete();
                } catch (e$) {
                  let e = dart.getThrown(e$);
                  let s = dart.stackTrace(e$);
                  completer.completeError(e, s);
                }
              }), VoidToFutureLOfNullN()));
              return completer.future.whenComplete(dart.fn(() => allStabilizers.update(), VoidToFutureLOfboolL()));
            }).bind(this));
          };
          dart.fn(createStabilizersAndRunUserHook, InjectorLToFutureLOfvoid());
          return bootstrap.bootstrapForTest(TL(), ComponentFactoryLOfTL().as((t1 = this[_componentFactory], t1 == null ? component_resolver.typeToFactory(type) : t1)), (t1$ = this[_host], t1$ == null ? bed.NgTestBed._defaultHost() : t1$), this[_createRootInjectorFactory](), {beforeComponentCreated: createStabilizersAndRunUserHook, beforeChangeDetection: beforeChangeDetection, createNgZone: ngZoneFactory}).then(NgTestFixtureLOfTL(), dart.fn(componentRef => async.async(NgTestFixtureLOfTL(), function*() {
            bed.NgTestBed._checkForActiveTest();
            yield allStabilizers.stabilize();
            let testFixture = new (NgTestFixtureOfTL()).__(ApplicationRefL().as(componentRef.injector.get(dart.wrapType(ApplicationRefL()))), componentRef, allStabilizers);
            bed.activeTest = testFixture;
            return testFixture;
          }), ComponentRefLOfTLToFutureLOfNgTestFixtureLOfTL()));
        }, VoidToFutureLOfNgTestFixtureLOfTL()));
      }
      fork(E, opts) {
        let t1, t1$, t1$0, t1$1, t1$2;
        dart.checkTypeBound(E, TL(), 'E');
        let host = opts && 'host' in opts ? opts.host : null;
        let component = opts && 'component' in opts ? opts.component : null;
        let providers = opts && 'providers' in opts ? opts.providers : null;
        let rootInjector = opts && 'rootInjector' in opts ? opts.rootInjector : null;
        let stabilizer = opts && 'stabilizer' in opts ? opts.stabilizer : null;
        return new (bed.NgTestBed$(dart.legacy(E))).__({host: (t1 = host, t1 == null ? this[_host] : t1), providers: (t1$ = providers, t1$ == null ? this[_providers] : t1$), stabilizer: (t1$0 = stabilizer, t1$0 == null ? this[_createStabilizer] : t1$0), rootInjector: (t1$1 = rootInjector, t1$1 == null ? this[_rootInjector] : t1$1), component: dart.legacy(component_factory.ComponentFactory$(dart.legacy(E))).as((t1$2 = component, t1$2 == null ? this[_componentFactory] : t1$2))});
      }
      setComponent(E, component) {
        dart.checkTypeBound(E, TL(), 'E');
        return this.fork(dart.legacy(E), {component: component});
      }
      setHost(host) {
        return this.fork(TL(), {host: host});
      }
    }
    (NgTestBed.__ = function(opts) {
      let t0;
      let host = opts && 'host' in opts ? opts.host : null;
      let providers = opts && 'providers' in opts ? opts.providers : null;
      let stabilizer = opts && 'stabilizer' in opts ? opts.stabilizer : null;
      let rootInjector = opts && 'rootInjector' in opts ? opts.rootInjector : null;
      let component = opts && 'component' in opts ? opts.component : null;
      this[_host] = host;
      this[_providers] = providers[$toList]();
      this[_createStabilizer] = stabilizer;
      this[_rootInjector] = (t0 = rootInjector, t0 == null ? C0 || CT.C0 : t0);
      this[_componentFactory] = component;
      ;
    }).prototype = NgTestBed.prototype;
    (NgTestBed._useComponentFactory = function(opts) {
      let host = opts && 'host' in opts ? opts.host : null;
      let component = opts && 'component' in opts ? opts.component : null;
      let rootInjector = opts && 'rootInjector' in opts ? opts.rootInjector : null;
      let watchAngularLifecycle = opts && 'watchAngularLifecycle' in opts ? opts.watchAngularLifecycle : null;
      this[_host] = host;
      this[_providers] = C1 || CT.C1;
      this[_createStabilizer] = dart.test(watchAngularLifecycle) ? C2 || CT.C2 : C3 || CT.C3;
      this[_rootInjector] = rootInjector;
      this[_componentFactory] = component;
      ;
    }).prototype = NgTestBed.prototype;
    dart.addTypeTests(NgTestBed);
    NgTestBed.prototype[_is_NgTestBed_default] = true;
    dart.addTypeCaches(NgTestBed);
    dart.setMethodSignature(NgTestBed, () => ({
      __proto__: dart.getMethods(NgTestBed.__proto__),
      addProviders: dart.fnType(dart.legacy(bed.NgTestBed$(dart.legacy(T))), [dart.legacy(core.Iterable$(dart.legacy(core.Object)))]),
      addInjector: dart.fnType(dart.legacy(bed.NgTestBed$(dart.legacy(T))), [dart.legacy(dart.fnType(dart.legacy(injector.Injector), [], [dart.legacy(injector.Injector)]))]),
      addStabilizers: dart.fnType(dart.legacy(bed.NgTestBed$(dart.legacy(T))), [dart.legacy(core.Iterable$(dart.legacy(dart.fnType(dart.legacy(stabilizer.NgTestStabilizer), [dart.legacy(injector.Injector)]))))]),
      create: dart.fnType(dart.legacy(async.Future$(dart.legacy(fixture.NgTestFixture$(dart.legacy(T))))), [], {beforeChangeDetection: dart.legacy(dart.fnType(dart.void, [dart.legacy(T)])), beforeComponentCreated: dart.legacy(dart.fnType(dart.void, [dart.legacy(injector.Injector)]))}, {}),
      [_createRootInjectorFactory]: dart.fnType(dart.legacy(dart.fnType(dart.legacy(injector.Injector), [], [dart.legacy(injector.Injector)])), []),
      [_createDynamic]: dart.fnType(dart.legacy(async.Future$(dart.legacy(fixture.NgTestFixture$(dart.legacy(T))))), [dart.legacy(core.Type)], {beforeChangeDetection: dart.legacy(dart.fnType(dart.void, [dart.legacy(T)])), beforeComponentCreated: dart.legacy(dart.fnType(dart.void, [dart.legacy(injector.Injector)]))}, {}),
      fork: dart.gFnType(E => [dart.legacy(bed.NgTestBed$(dart.legacy(E))), [], {component: dart.legacy(component_factory.ComponentFactory$(dart.legacy(E))), host: dart.legacy(html.Element), providers: dart.legacy(core.Iterable$(dart.legacy(core.Object))), rootInjector: dart.legacy(dart.fnType(dart.legacy(injector.Injector), [], [dart.legacy(injector.Injector)])), stabilizer: dart.legacy(dart.fnType(dart.legacy(stabilizer.NgTestStabilizer), [dart.legacy(injector.Injector)]))}, {}], E => [dart.legacy(T)]),
      setComponent: dart.gFnType(E => [dart.legacy(bed.NgTestBed$(dart.legacy(E))), [dart.legacy(component_factory.ComponentFactory$(dart.legacy(E)))]], E => [dart.legacy(T)]),
      setHost: dart.fnType(dart.legacy(bed.NgTestBed$(dart.legacy(T))), [dart.legacy(html.Element)])
    }));
    dart.setGetterSignature(NgTestBed, () => ({
      __proto__: dart.getGetters(NgTestBed.__proto__),
      [_usesComponentFactory]: dart.legacy(core.bool)
    }));
    dart.setLibraryUri(NgTestBed, L0);
    dart.setFieldSignature(NgTestBed, () => ({
      __proto__: dart.getFields(NgTestBed.__proto__),
      [_host]: dart.finalFieldType(dart.legacy(html.Element)),
      [_providers]: dart.finalFieldType(dart.legacy(core.List$(dart.legacy(core.Object)))),
      [_createStabilizer]: dart.finalFieldType(dart.legacy(dart.fnType(dart.legacy(stabilizer.NgTestStabilizer), [dart.legacy(injector.Injector)]))),
      [_componentFactory]: dart.finalFieldType(dart.legacy(component_factory.ComponentFactory$(dart.legacy(T)))),
      [_rootInjector]: dart.finalFieldType(dart.legacy(dart.fnType(dart.legacy(injector.Injector), [], [dart.legacy(injector.Injector)])))
    }));
    return NgTestBed;
  });
  bed.NgTestBed = bed.NgTestBed$();
  dart.addTypeTests(bed.NgTestBed, _is_NgTestBed_default);
  bed._concat = function _concat(E, a, b) {
    let t1;
    t1 = a[$toList]();
    return (() => {
      t1[$addAll](b);
      return t1;
    })();
  };
  bed.disposeAnyRunningTest = function disposeAnyRunningTest() {
    return async.async(dart.void, function* disposeAnyRunningTest() {
      let t1;
      t1 = bed.activeTest;
      return t1 == null ? null : t1.dispose();
    });
  };
  bed.createDynamicFixture = function createDynamicFixture(T, bed, type, opts) {
    let beforeComponentCreated = opts && 'beforeComponentCreated' in opts ? opts.beforeComponentCreated : null;
    let beforeChangeDetection = opts && 'beforeChangeDetection' in opts ? opts.beforeChangeDetection : null;
    return bed[_createDynamic](type, {beforeComponentCreated: beforeComponentCreated, beforeChangeDetection: beforeChangeDetection});
  };
  bed.createDynamicTestBed = function createDynamicTestBed(T, opts) {
    let host = opts && 'host' in opts ? opts.host : null;
    let rootInjector = opts && 'rootInjector' in opts ? opts.rootInjector : null;
    let watchAngularLifecycle = opts && 'watchAngularLifecycle' in opts ? opts.watchAngularLifecycle : true;
    return bed.NgTestBed$(dart.legacy(T))._allowDynamicType({host: host, rootInjector: rootInjector, watchAngularLifecycle: watchAngularLifecycle});
  };
  dart.defineLazy(bed, {
    /*bed.activeTest*/get activeTest() {
      return null;
    },
    set activeTest(_) {}
  }, true);
  var C6;
  stabilizer.NgTestStabilizer = class NgTestStabilizer extends core.Object {
    get isStable() {
      return false;
    }
    update(runAndTrackSideEffects = null) {
      return FutureOfboolL().sync(dart.fn(() => {
        if (runAndTrackSideEffects != null) {
          runAndTrackSideEffects();
        }
        return false;
      }, VoidToboolL()));
    }
    stabilize(opts) {
      let runAndTrackSideEffects = opts && 'runAndTrackSideEffects' in opts ? opts.runAndTrackSideEffects : null;
      let threshold = opts && 'threshold' in opts ? opts.threshold : 100;
      return async.async(dart.void, (function* stabilize() {
        if (threshold == null) {
          dart.throw(new core.ArgumentError.notNull("threshold"));
        }
        if (runAndTrackSideEffects != null) {
          yield this.update(runAndTrackSideEffects);
        }
        return this.stabilizeWithThreshold(threshold);
      }).bind(this));
    }
    stabilizeWithThreshold(threshold) {
      return async.async(dart.void, (function* stabilizeWithThreshold() {
        if (dart.notNull(threshold) < 1) {
          dart.throw(new core.ArgumentError.value(threshold, "threshold", "Must be >= 1"));
        }
        let count = 0;
        function thresholdExceeded() {
          let t1;
          return (t1 = count, count = t1 + 1, t1) > dart.notNull(threshold);
        }
        dart.fn(thresholdExceeded, VoidToboolL());
        while (!dart.test(yield this.update())) {
          if (dart.test(thresholdExceeded())) {
            dart.throw(new will_never_stabilize.WillNeverStabilizeError.new(threshold));
          }
        }
      }).bind(this));
    }
  };
  (stabilizer.NgTestStabilizer.new = function() {
    ;
  }).prototype = stabilizer.NgTestStabilizer.prototype;
  dart.addTypeTests(stabilizer.NgTestStabilizer);
  dart.addTypeCaches(stabilizer.NgTestStabilizer);
  dart.setMethodSignature(stabilizer.NgTestStabilizer, () => ({
    __proto__: dart.getMethods(stabilizer.NgTestStabilizer.__proto__),
    update: dart.fnType(dart.legacy(async.Future$(dart.legacy(core.bool))), [], [dart.legacy(dart.fnType(dart.void, []))]),
    stabilize: dart.fnType(dart.legacy(async.Future$(dart.void)), [], {runAndTrackSideEffects: dart.legacy(dart.fnType(dart.void, [])), threshold: dart.legacy(core.int)}, {}),
    stabilizeWithThreshold: dart.fnType(dart.legacy(async.Future$(dart.void)), [dart.legacy(core.int)])
  }));
  dart.setGetterSignature(stabilizer.NgTestStabilizer, () => ({
    __proto__: dart.getGetters(stabilizer.NgTestStabilizer.__proto__),
    isStable: dart.legacy(core.bool)
  }));
  dart.setLibraryUri(stabilizer.NgTestStabilizer, L1);
  dart.defineLazy(stabilizer.NgTestStabilizer, {
    /*stabilizer.NgTestStabilizer.alwaysStable*/get alwaysStable() {
      return C6 || CT.C6;
    }
  }, true);
  stabilizer._AlwaysStableNgTestStabilizer = class _AlwaysStableNgTestStabilizer extends stabilizer.NgTestStabilizer {
    get isStable() {
      return true;
    }
  };
  (stabilizer._AlwaysStableNgTestStabilizer.new = function() {
    stabilizer._AlwaysStableNgTestStabilizer.__proto__.new.call(this);
    ;
  }).prototype = stabilizer._AlwaysStableNgTestStabilizer.prototype;
  dart.addTypeTests(stabilizer._AlwaysStableNgTestStabilizer);
  dart.addTypeCaches(stabilizer._AlwaysStableNgTestStabilizer);
  dart.setLibraryUri(stabilizer._AlwaysStableNgTestStabilizer, L1);
  var _updatedAtLeastOnce = dart.privateName(stabilizer, "_updatedAtLeastOnce");
  var _delegates = dart.privateName(stabilizer, "_delegates");
  var _updateAll = dart.privateName(stabilizer, "_updateAll");
  const stabilizeWithThreshold = Symbol("stabilizeWithThreshold");
  stabilizer._DelegatingNgTestStabilizer = class _DelegatingNgTestStabilizer extends stabilizer.NgTestStabilizer {
    get isStable() {
      return this[_delegates][$every](dart.fn(delegate => delegate.isStable, NgTestStabilizerLToboolL()));
    }
    update(runAndTrackSideEffects = null) {
      return async.async(boolL(), (function* update() {
        if (dart.test(this[_delegates][$isEmpty])) {
          return false;
        }
        if (runAndTrackSideEffects == null && dart.test(this[_updatedAtLeastOnce])) {
          yield this[_updateAll](runAndTrackSideEffects, dart.fn(d => !dart.test(d.isStable), NgTestStabilizerLToboolL()));
        } else {
          yield this[_updateAll](runAndTrackSideEffects);
        }
        this[_updatedAtLeastOnce] = true;
        return this.isStable;
      }).bind(this));
    }
    [_updateAll](runAndTrackSideEffects, test = null) {
      return async.async(dart.void, (function* _updateAll() {
        for (let delegate of this[_delegates]) {
          if (test == null || dart.test(test(delegate))) {
            yield delegate.update(runAndTrackSideEffects);
          }
        }
      }).bind(this));
    }
    stabilizeWithThreshold(threshold) {
      return async.async(dart.void, (function* stabilizeWithThreshold$() {
        try {
          this[_updatedAtLeastOnce] = false;
          return this[stabilizeWithThreshold](threshold);
        } finally {
          this[_updatedAtLeastOnce] = false;
        }
      }).bind(this));
    }
    [stabilizeWithThreshold](threshold) {
      return super.stabilizeWithThreshold(threshold);
    }
  };
  (stabilizer._DelegatingNgTestStabilizer.new = function(stabilizers) {
    this[_updatedAtLeastOnce] = false;
    this[_delegates] = stabilizers[$toList]({growable: false});
    stabilizer._DelegatingNgTestStabilizer.__proto__.new.call(this);
    ;
  }).prototype = stabilizer._DelegatingNgTestStabilizer.prototype;
  dart.addTypeTests(stabilizer._DelegatingNgTestStabilizer);
  dart.addTypeCaches(stabilizer._DelegatingNgTestStabilizer);
  dart.setMethodSignature(stabilizer._DelegatingNgTestStabilizer, () => ({
    __proto__: dart.getMethods(stabilizer._DelegatingNgTestStabilizer.__proto__),
    [_updateAll]: dart.fnType(dart.legacy(async.Future$(dart.void)), [dart.legacy(dart.fnType(dart.void, []))], [dart.legacy(dart.fnType(dart.legacy(core.bool), [dart.legacy(stabilizer.NgTestStabilizer)]))])
  }));
  dart.setLibraryUri(stabilizer._DelegatingNgTestStabilizer, L1);
  dart.setFieldSignature(stabilizer._DelegatingNgTestStabilizer, () => ({
    __proto__: dart.getFields(stabilizer._DelegatingNgTestStabilizer.__proto__),
    [_delegates]: dart.finalFieldType(dart.legacy(core.List$(dart.legacy(stabilizer.NgTestStabilizer)))),
    [_updatedAtLeastOnce]: dart.fieldType(dart.legacy(core.bool))
  }));
  stabilizer.composeStabilizers = function composeStabilizers(factories) {
    return dart.fn((injector, zone = null) => new stabilizer._DelegatingNgTestStabilizer.new(factories[$map](NgTestStabilizerL(), dart.fn(f => {
      if (InjectorLAndTimerHookZoneLToLNgTestStabilizerL().is(f)) {
        return f(injector, zone);
      }
      if (InjectorLToLNgTestStabilizerL().is(f)) {
        return f(injector);
      }
      dart.throw(new core.ArgumentError.new("Invalid stabilizer factory: " + dart.str(f)));
    }, FnToNgTestStabilizerL()))), InjectorLAndTimerHookZoneLTo_DelegatingNgTestStabilizerL());
  };
  var _timerZone = dart.privateName(timer_hook_zone, "_timerZone");
  var createTimer = dart.privateName(timer_hook_zone, "TimerHookZone.createTimer");
  var createPeriodicTimer = dart.privateName(timer_hook_zone, "TimerHookZone.createPeriodicTimer");
  timer_hook_zone.TimerHookZone = class TimerHookZone extends core.Object {
    get createTimer() {
      return this[createTimer];
    }
    set createTimer(value) {
      this[createTimer] = value;
    }
    get createPeriodicTimer() {
      return this[createPeriodicTimer];
    }
    set createPeriodicTimer(value) {
      this[createPeriodicTimer] = value;
    }
    run(T, context) {
      return this[_timerZone].run(dart.legacy(T), context);
    }
  };
  (timer_hook_zone.TimerHookZone.new = function() {
    this[_timerZone] = null;
    this[createTimer] = dart.fn((self, parent, zone, duration, callback) => parent.createTimer(zone, duration, callback), ZoneLAndZoneDelegateLAndZoneL__ToTimerL());
    this[createPeriodicTimer] = dart.fn((self, parent, zone, duration, callback) => parent.createPeriodicTimer(zone, duration, callback), ZoneLAndZoneDelegateLAndZoneL__ToTimerL$());
    this[_timerZone] = async.Zone.current.fork({specification: new async._ZoneSpecification.new({createTimer: dart.fn((self, parent, zone, duration, callback) => {
          let t5, t4, t3, t2, t1;
          t1 = self;
          t2 = parent;
          t3 = zone;
          t4 = duration;
          t5 = callback;
          return this.createTimer(t1, t2, t3, t4, t5);
        }, ZoneLAndZoneDelegateLAndZoneL__ToTimerL()), createPeriodicTimer: dart.fn((self, parent, zone, duration, callback) => {
          let t5, t4, t3, t2, t1;
          t1 = self;
          t2 = parent;
          t3 = zone;
          t4 = duration;
          t5 = callback;
          return this.createPeriodicTimer(t1, t2, t3, t4, t5);
        }, ZoneLAndZoneDelegateLAndZoneL__ToTimerL$())})});
  }).prototype = timer_hook_zone.TimerHookZone.prototype;
  dart.addTypeTests(timer_hook_zone.TimerHookZone);
  dart.addTypeCaches(timer_hook_zone.TimerHookZone);
  dart.setMethodSignature(timer_hook_zone.TimerHookZone, () => ({
    __proto__: dart.getMethods(timer_hook_zone.TimerHookZone.__proto__),
    run: dart.gFnType(T => [dart.legacy(T), [dart.legacy(dart.fnType(dart.legacy(T), []))]])
  }));
  dart.setLibraryUri(timer_hook_zone.TimerHookZone, L2);
  dart.setFieldSignature(timer_hook_zone.TimerHookZone, () => ({
    __proto__: dart.getFields(timer_hook_zone.TimerHookZone.__proto__),
    [_timerZone]: dart.fieldType(dart.legacy(async.Zone)),
    createTimer: dart.fieldType(dart.legacy(dart.fnType(async.Timer, [async.Zone, async.ZoneDelegate, async.Zone, core.Duration, dart.fnType(dart.void, [])]))),
    createPeriodicTimer: dart.fieldType(dart.legacy(dart.fnType(async.Timer, [async.Zone, async.ZoneDelegate, async.Zone, core.Duration, dart.fnType(dart.void, [async.Timer])])))
  }));
  var threshold$ = dart.privateName(will_never_stabilize, "WillNeverStabilizeError.threshold");
  will_never_stabilize.WillNeverStabilizeError = class WillNeverStabilizeError extends core.Error {
    get threshold() {
      return this[threshold$];
    }
    set threshold(value) {
      super.threshold = value;
    }
    toString() {
      return "Failed to stabilize after " + dart.str(this.threshold) + " attempts";
    }
  };
  (will_never_stabilize.WillNeverStabilizeError.new = function(threshold) {
    this[threshold$] = threshold;
    will_never_stabilize.WillNeverStabilizeError.__proto__.new.call(this);
    ;
  }).prototype = will_never_stabilize.WillNeverStabilizeError.prototype;
  dart.addTypeTests(will_never_stabilize.WillNeverStabilizeError);
  dart.addTypeCaches(will_never_stabilize.WillNeverStabilizeError);
  dart.setMethodSignature(will_never_stabilize.WillNeverStabilizeError, () => ({
    __proto__: dart.getMethods(will_never_stabilize.WillNeverStabilizeError.__proto__),
    toString: dart.fnType(dart.legacy(core.String), []),
    [$toString]: dart.fnType(dart.legacy(core.String), [])
  }));
  dart.setLibraryUri(will_never_stabilize.WillNeverStabilizeError, L3);
  dart.setFieldSignature(will_never_stabilize.WillNeverStabilizeError, () => ({
    __proto__: dart.getFields(will_never_stabilize.WillNeverStabilizeError.__proto__),
    threshold: dart.finalFieldType(dart.legacy(core.int))
  }));
  dart.defineExtensionMethods(will_never_stabilize.WillNeverStabilizeError, ['toString']);
  test_already_running.TestAlreadyRunningError = class TestAlreadyRunningError extends core.Error {
    toString() {
      return "Another instance of an `NgTestFixture` is still executing!\n\n" + "NgTestBed supports *one* test executing at a time to avoid timing " + "conflicts or stability issues related to sharing a browser DOM.\n\n" + "When you are done with a test, make sure to dispose fixtures:\n" + "  tearDown(() => disposeAnyRunningTest())\n\n" + "NOTE: `disposeAnyRunningTest` returns a Future that must complete " + "*before* executing another test - `tearDown` handles this for you " + "if returned like the example above.";
    }
  };
  (test_already_running.TestAlreadyRunningError.new = function() {
    test_already_running.TestAlreadyRunningError.__proto__.new.call(this);
    ;
  }).prototype = test_already_running.TestAlreadyRunningError.prototype;
  dart.addTypeTests(test_already_running.TestAlreadyRunningError);
  dart.addTypeCaches(test_already_running.TestAlreadyRunningError);
  dart.setMethodSignature(test_already_running.TestAlreadyRunningError, () => ({
    __proto__: dart.getMethods(test_already_running.TestAlreadyRunningError.__proto__),
    toString: dart.fnType(dart.legacy(core.String), []),
    [$toString]: dart.fnType(dart.legacy(core.String), [])
  }));
  dart.setLibraryUri(test_already_running.TestAlreadyRunningError, L4);
  dart.defineExtensionMethods(test_already_running.TestAlreadyRunningError, ['toString']);
  var message$ = dart.privateName(generic_type_missing, "GenericTypeMissingError.message");
  generic_type_missing.GenericTypeMissingError = class GenericTypeMissingError extends core.Error {
    get message() {
      return this[message$];
    }
    set message(value) {
      super.message = value;
    }
    toString() {
      if (this.message == null) {
        return "Generic type required";
      }
      return "Generic type required: " + dart.str(this.message);
    }
  };
  (generic_type_missing.GenericTypeMissingError.new = function(message = null) {
    this[message$] = message;
    generic_type_missing.GenericTypeMissingError.__proto__.new.call(this);
    ;
  }).prototype = generic_type_missing.GenericTypeMissingError.prototype;
  dart.addTypeTests(generic_type_missing.GenericTypeMissingError);
  dart.addTypeCaches(generic_type_missing.GenericTypeMissingError);
  dart.setMethodSignature(generic_type_missing.GenericTypeMissingError, () => ({
    __proto__: dart.getMethods(generic_type_missing.GenericTypeMissingError.__proto__),
    toString: dart.fnType(dart.legacy(core.String), []),
    [$toString]: dart.fnType(dart.legacy(core.String), [])
  }));
  dart.setLibraryUri(generic_type_missing.GenericTypeMissingError, L5);
  dart.setFieldSignature(generic_type_missing.GenericTypeMissingError, () => ({
    __proto__: dart.getFields(generic_type_missing.GenericTypeMissingError.__proto__),
    message: dart.finalFieldType(dart.legacy(core.String))
  }));
  dart.defineExtensionMethods(generic_type_missing.GenericTypeMissingError, ['toString']);
  var _minimumDurationForAllPendingTimers = dart.privateName(real_time_stabilizer, "_minimumDurationForAllPendingTimers");
  var _duration = dart.privateName(real_time_stabilizer, "_duration");
  var C7;
  var _triggerSideEffects = dart.privateName(base_stabilizer, "_triggerSideEffects");
  var _waitForAsyncEventsOrErrors = dart.privateName(base_stabilizer, "_waitForAsyncEventsOrErrors");
  const _is_BaseNgZoneStabilizer_default = Symbol('_is_BaseNgZoneStabilizer_default');
  var ngZone$ = dart.privateName(base_stabilizer, "BaseNgZoneStabilizer.ngZone");
  var pendingTimers$ = dart.privateName(base_stabilizer, "BaseNgZoneStabilizer.pendingTimers");
  base_stabilizer.BaseNgZoneStabilizer$ = dart.generic(T => {
    class BaseNgZoneStabilizer extends stabilizer.NgTestStabilizer {
      get ngZone() {
        return this[ngZone$];
      }
      set ngZone(value) {
        super.ngZone = value;
      }
      get pendingTimers() {
        return this[pendingTimers$];
      }
      set pendingTimers(value) {
        super.pendingTimers = value;
      }
      get isStable() {
        return !dart.test(this.ngZone.hasPendingMicrotasks);
      }
      static _noSideEffects() {
      }
      update(runAndTrackSideEffects = null) {
        return FutureOfvoid().sync(dart.fn(() => {
          let t1;
          this[_triggerSideEffects]((t1 = runAndTrackSideEffects, t1 == null ? C7 || CT.C7 : t1));
          return this[_waitForAsyncEventsOrErrors]();
        }, VoidToFutureLOfvoid())).then(boolL(), dart.fn(_ => this.isStable, voidToboolL()));
      }
      [_triggerSideEffects](withCallback) {
        async.scheduleMicrotask(dart.fn(() => this.ngZone.runGuarded(dart.fn(() => withCallback(), VoidTovoid())), VoidTovoid()));
      }
      waitForAsyncEvents() {
        return this.ngZone.onTurnDone.first;
      }
      static rebuildStackTrace(traceOrChain) {
        return new core._StringStackTrace.new(traceOrChain[$join]("\n"));
      }
      [_waitForAsyncEventsOrErrors]() {
        return async.async(dart.void, (function* _waitForAsyncEventsOrErrors() {
          let uncaughtError = null;
          let uncaughtStack = null;
          let onErrorSub = null;
          onErrorSub = this.ngZone.onError.listen(dart.fn(e => {
            uncaughtError = e.error;
            uncaughtStack = base_stabilizer.BaseNgZoneStabilizer.rebuildStackTrace(e.stackTrace);
            onErrorSub.cancel();
          }, NgZoneErrorLToNullN()));
          yield this.waitForAsyncEvents();
          yield FutureOfNullN().new(dart.fn(() => {
          }, VoidToNullN()));
          pedantic.unawaited(onErrorSub.cancel());
          return uncaughtError != null ? FutureOfvoid().error(uncaughtError, uncaughtStack) : FutureOfvoid().value();
        }).bind(this));
      }
    }
    (BaseNgZoneStabilizer.new = function(ngZone, pendingTimers) {
      this[ngZone$] = ngZone;
      this[pendingTimers$] = pendingTimers;
      BaseNgZoneStabilizer.__proto__.new.call(this);
      ;
    }).prototype = BaseNgZoneStabilizer.prototype;
    dart.addTypeTests(BaseNgZoneStabilizer);
    BaseNgZoneStabilizer.prototype[_is_BaseNgZoneStabilizer_default] = true;
    dart.addTypeCaches(BaseNgZoneStabilizer);
    dart.setMethodSignature(BaseNgZoneStabilizer, () => ({
      __proto__: dart.getMethods(BaseNgZoneStabilizer.__proto__),
      [_triggerSideEffects]: dart.fnType(dart.void, [dart.legacy(dart.fnType(dart.void, []))]),
      waitForAsyncEvents: dart.fnType(dart.legacy(async.Future$(dart.void)), []),
      [_waitForAsyncEventsOrErrors]: dart.fnType(dart.legacy(async.Future$(dart.void)), [])
    }));
    dart.setLibraryUri(BaseNgZoneStabilizer, L6);
    dart.setFieldSignature(BaseNgZoneStabilizer, () => ({
      __proto__: dart.getFields(BaseNgZoneStabilizer.__proto__),
      ngZone: dart.finalFieldType(dart.legacy(ng_zone.NgZone)),
      pendingTimers: dart.finalFieldType(dart.legacy(priority_queue.PriorityQueue$(dart.legacy(T))))
    }));
    return BaseNgZoneStabilizer;
  });
  base_stabilizer.BaseNgZoneStabilizer = base_stabilizer.BaseNgZoneStabilizer$();
  dart.addTypeTests(base_stabilizer.BaseNgZoneStabilizer, _is_BaseNgZoneStabilizer_default);
  var _delegate = dart.privateName(real_time_stabilizer, "_delegate");
  var _onCancel = dart.privateName(real_time_stabilizer, "_onCancel");
  var _delegate$ = dart.privateName(real_time_stabilizer, "_ObservedTimer._delegate");
  var _duration$ = dart.privateName(real_time_stabilizer, "_ObservedTimer._duration");
  var _onCancel$ = dart.privateName(real_time_stabilizer, "_ObservedTimer._onCancel");
  real_time_stabilizer._ObservedTimer = class _ObservedTimer extends core.Object {
    get [_delegate]() {
      return this[_delegate$];
    }
    set [_delegate](value) {
      super[_delegate] = value;
    }
    get [_duration]() {
      return this[_duration$];
    }
    set [_duration](value) {
      super[_duration] = value;
    }
    get [_onCancel]() {
      return this[_onCancel$];
    }
    set [_onCancel](value) {
      super[_onCancel] = value;
    }
    cancel() {
      this[_onCancel]();
      this[_delegate].cancel();
    }
    compareTo(b) {
      _ObservedTimerL().as(b);
      return this[_duration].compareTo(b[_duration]);
    }
    get tick() {
      return this[_delegate].tick;
    }
    get isActive() {
      return this[_delegate].isActive;
    }
  };
  (real_time_stabilizer._ObservedTimer.new = function(_delegate, _duration, _onCancel) {
    this[_delegate$] = _delegate;
    this[_duration$] = _duration;
    this[_onCancel$] = _onCancel;
    ;
  }).prototype = real_time_stabilizer._ObservedTimer.prototype;
  dart.addTypeTests(real_time_stabilizer._ObservedTimer);
  dart.addTypeCaches(real_time_stabilizer._ObservedTimer);
  real_time_stabilizer._ObservedTimer[dart.implements] = () => [async.Timer, core.Comparable$(dart.legacy(real_time_stabilizer._ObservedTimer))];
  dart.setMethodSignature(real_time_stabilizer._ObservedTimer, () => ({
    __proto__: dart.getMethods(real_time_stabilizer._ObservedTimer.__proto__),
    cancel: dart.fnType(dart.void, []),
    compareTo: dart.fnType(dart.legacy(core.int), [dart.legacy(core.Object)]),
    [$compareTo]: dart.fnType(dart.legacy(core.int), [dart.legacy(core.Object)])
  }));
  dart.setGetterSignature(real_time_stabilizer._ObservedTimer, () => ({
    __proto__: dart.getGetters(real_time_stabilizer._ObservedTimer.__proto__),
    tick: dart.legacy(core.int),
    isActive: dart.legacy(core.bool)
  }));
  dart.setLibraryUri(real_time_stabilizer._ObservedTimer, L7);
  dart.setFieldSignature(real_time_stabilizer._ObservedTimer, () => ({
    __proto__: dart.getFields(real_time_stabilizer._ObservedTimer.__proto__),
    [_delegate]: dart.finalFieldType(dart.legacy(async.Timer)),
    [_duration]: dart.finalFieldType(dart.legacy(core.Duration)),
    [_onCancel]: dart.finalFieldType(dart.legacy(dart.fnType(dart.void, [])))
  }));
  dart.defineExtensionMethods(real_time_stabilizer._ObservedTimer, ['compareTo']);
  const waitForAsyncEvents = Symbol("waitForAsyncEvents");
  real_time_stabilizer.RealTimeNgZoneStabilizer = class RealTimeNgZoneStabilizer extends base_stabilizer.BaseNgZoneStabilizer$(dart.legacy(real_time_stabilizer._ObservedTimer)) {
    static new(timerZone, ngZone) {
      let pendingTimers = new (HeapPriorityQueueOf_ObservedTimerL()).new();
      timerZone.createTimer = dart.fn((self, parent, zone, duration, callback) => {
        if (!dart.test(ng_zone.inAngularZone(ngZone, zone))) {
          return parent.createTimer(zone, duration, callback);
        }
        let instance = null;
        let wrappedCallback = dart.fn(() => {
          try {
            callback();
          } finally {
            pendingTimers.remove(instance);
          }
        }, VoidToNullN());
        let delegate = parent.createTimer(zone, duration, wrappedCallback);
        instance = new real_time_stabilizer._ObservedTimer.new(delegate, duration, dart.fn(() => pendingTimers.remove(instance), VoidToboolL()));
        pendingTimers.add(instance);
        return instance;
      }, ZoneLAndZoneDelegateLAndZoneL__ToTimerL());
      return new real_time_stabilizer.RealTimeNgZoneStabilizer.__(ngZone, pendingTimers);
    }
    get isStable() {
      return dart.test(super.isStable) && dart.test(this.pendingTimers.isEmpty);
    }
    waitForAsyncEvents() {
      return async.async(dart.void, (function* waitForAsyncEvents$() {
        yield this[waitForAsyncEvents]();
        if (dart.test(this.pendingTimers.isNotEmpty)) {
          yield FutureOfvoid().delayed(this[_minimumDurationForAllPendingTimers]());
        }
      }).bind(this));
    }
    [_minimumDurationForAllPendingTimers]() {
      return this.pendingTimers.toList()[$last][_duration];
    }
    [waitForAsyncEvents]() {
      return super.waitForAsyncEvents();
    }
  };
  (real_time_stabilizer.RealTimeNgZoneStabilizer.__ = function(ngZone, pendingTimers) {
    real_time_stabilizer.RealTimeNgZoneStabilizer.__proto__.new.call(this, ngZone, pendingTimers);
    ;
  }).prototype = real_time_stabilizer.RealTimeNgZoneStabilizer.prototype;
  dart.addTypeTests(real_time_stabilizer.RealTimeNgZoneStabilizer);
  dart.addTypeCaches(real_time_stabilizer.RealTimeNgZoneStabilizer);
  dart.setMethodSignature(real_time_stabilizer.RealTimeNgZoneStabilizer, () => ({
    __proto__: dart.getMethods(real_time_stabilizer.RealTimeNgZoneStabilizer.__proto__),
    [_minimumDurationForAllPendingTimers]: dart.fnType(dart.legacy(core.Duration), [])
  }));
  dart.setLibraryUri(real_time_stabilizer.RealTimeNgZoneStabilizer, L7);
  var _applicationRef$ = dart.privateName(fixture, "_applicationRef");
  var _rootComponentRef$ = dart.privateName(fixture, "_rootComponentRef");
  var _testStabilizer$ = dart.privateName(fixture, "_testStabilizer");
  const _is_NgTestFixture_default = Symbol('_is_NgTestFixture_default');
  fixture.NgTestFixture$ = dart.generic(T => {
    class NgTestFixture extends core.Object {
      dispose() {
        return async.async(dart.void, (function* dispose() {
          yield this.update();
          this[_rootComponentRef$].destroy();
          this[_rootComponentRef$].location[$parent][$remove]();
          this[_applicationRef$].dispose();
          bed.activeTest = null;
        }).bind(this));
      }
      get rootElement() {
        return this[_rootComponentRef$].location;
      }
      update(run = null) {
        return this[_testStabilizer$].stabilize({runAndTrackSideEffects: dart.fn(() => {
            if (run != null) {
              FutureOfvoid().sync(dart.fn(() => {
                run(this[_rootComponentRef$].instance);
              }, VoidToNullN()));
            }
          }, VoidToNullN())});
      }
      get text() {
        return this.rootElement[$text];
      }
      get assertOnlyInstance() {
        return this[_rootComponentRef$].instance;
      }
    }
    (NgTestFixture.__ = function(_applicationRef, _rootComponentRef, _testStabilizer) {
      this[_applicationRef$] = _applicationRef;
      this[_rootComponentRef$] = _rootComponentRef;
      this[_testStabilizer$] = _testStabilizer;
      ;
    }).prototype = NgTestFixture.prototype;
    dart.addTypeTests(NgTestFixture);
    NgTestFixture.prototype[_is_NgTestFixture_default] = true;
    dart.addTypeCaches(NgTestFixture);
    dart.setMethodSignature(NgTestFixture, () => ({
      __proto__: dart.getMethods(NgTestFixture.__proto__),
      dispose: dart.fnType(dart.legacy(async.Future$(dart.void)), []),
      update: dart.fnType(dart.legacy(async.Future$(dart.void)), [], [dart.legacy(dart.fnType(dart.void, [dart.legacy(T)]))])
    }));
    dart.setGetterSignature(NgTestFixture, () => ({
      __proto__: dart.getGetters(NgTestFixture.__proto__),
      rootElement: dart.legacy(html.Element),
      text: dart.legacy(core.String),
      assertOnlyInstance: dart.legacy(T)
    }));
    dart.setLibraryUri(NgTestFixture, L8);
    dart.setFieldSignature(NgTestFixture, () => ({
      __proto__: dart.getFields(NgTestFixture.__proto__),
      [_applicationRef$]: dart.finalFieldType(dart.legacy(application_ref.ApplicationRef)),
      [_rootComponentRef$]: dart.finalFieldType(dart.legacy(component_factory.ComponentRef$(dart.legacy(T)))),
      [_testStabilizer$]: dart.finalFieldType(dart.legacy(stabilizer.NgTestStabilizer))
    }));
    return NgTestFixture;
  });
  fixture.NgTestFixture = fixture.NgTestFixture$();
  dart.addTypeTests(fixture.NgTestFixture, _is_NgTestFixture_default);
  fixture.injectFromFixture = function injectFromFixture(T, fixture, tokenOrType) {
    return dart.legacy(T).as(fixture[_rootComponentRef$].injector.get(tokenOrType));
  };
  var C8;
  bootstrap.testInjectorFactory = function testInjectorFactory(providers) {
    if (dart.test(providers[$isEmpty])) {
      return dart.fn((parent = null) => parent, InjectorLToInjectorL());
    }
    return dart.fn((parent = null) => runtime.ReflectiveInjector.resolveAndCreate(JSArrayOfObjectL().of([providers]), parent), InjectorLToReflectiveInjectorL());
  };
  bootstrap.bootstrapForTest = function bootstrapForTest(E, componentFactory, hostElement, userInjector, opts) {
    let beforeComponentCreated = opts && 'beforeComponentCreated' in opts ? opts.beforeComponentCreated : null;
    let beforeChangeDetection = opts && 'beforeChangeDetection' in opts ? opts.beforeChangeDetection : null;
    let createNgZone = opts && 'createNgZone' in opts ? opts.createNgZone : C8 || CT.C8;
    return async.async(dart.legacy(component_factory.ComponentRef$(dart.legacy(E))), function* bootstrapForTest() {
      if (componentFactory == null) {
        dart.throw(new core.ArgumentError.notNull("componentFactory"));
      }
      if (hostElement == null) {
        dart.throw(new core.ArgumentError.notNull("hostElement"));
      }
      if (userInjector == null) {
        dart.throw(new core.ArgumentError.notNull("userInjector"));
      }
      let injector = run.appInjector(userInjector, {createNgZone: createNgZone});
      let appRef = ApplicationRefL().as(injector.get(dart.wrapType(ApplicationRefL())));
      let caughtError = null;
      let ngZone = NgZoneL().as(injector.get(dart.wrapType(NgZoneL())));
      let onErrorSub = ngZone.onError.listen(dart.fn(e => {
        caughtError = e;
      }, NgZoneErrorLToNullN()));
      if (beforeComponentCreated != null) {
        yield beforeComponentCreated(injector);
      }
      return appRef.run(dart.legacy(component_factory.ComponentRef$(dart.legacy(E))), dart.fn(() => bootstrap._runAndLoadComponent(dart.legacy(E), appRef, componentFactory, hostElement, injector, {beforeChangeDetection: beforeChangeDetection}).then(dart.legacy(component_factory.ComponentRef$(dart.legacy(E))), dart.fn(componentRef => async.async(dart.legacy(component_factory.ComponentRef$(dart.legacy(E))), function*() {
        yield ngZone.onTurnDone.first;
        yield FutureOfvoid().value();
        yield onErrorSub.cancel();
        if (caughtError != null) {
          return async.Future$(dart.legacy(component_factory.ComponentRef$(dart.legacy(E)))).error(caughtError.error, new core._StringStackTrace.new(caughtError.stackTrace[$join]("\n")));
        }
        return componentRef;
      }), dart.fnType(dart.legacy(async.Future$(dart.legacy(component_factory.ComponentRef$(dart.legacy(E))))), [dart.legacy(component_factory.ComponentRef$(dart.legacy(E)))]))), dart.fnType(dart.legacy(async.Future$(dart.legacy(component_factory.ComponentRef$(dart.legacy(E))))), [])));
    });
  };
  bootstrap._runAndLoadComponent = function _runAndLoadComponent(E, appRef, componentFactory, hostElement, injector, opts) {
    let beforeChangeDetection = opts && 'beforeChangeDetection' in opts ? opts.beforeChangeDetection : null;
    let componentRef = componentFactory.create(injector);
    let cdMode = AppViewLOfvoid().as(componentRef.hostView).cdMode;
    if (cdMode !== 0 && cdMode !== 3) {
      dart.throw(new core.UnsupportedError.new("The root component in an Angular test or application must use the " + "default form of change detection (ChangeDetectionStrategy.Default). " + "Instead got " + dart.str(cdMode) + " on component " + dart.str(dart.wrapType(dart.legacy(E))) + "."));
    }
    function loadComponent() {
      hostElement[$append](componentRef.location);
      appRef.registerChangeDetector(componentRef.changeDetectorRef);
      componentRef.onDestroy(dart.fn(() => {
        appRef.unregisterChangeDetector(componentRef.changeDetectorRef);
      }, VoidToNullN()));
      appRef.tick();
      return async.Future$(dart.legacy(component_factory.ComponentRef$(dart.legacy(E)))).value(componentRef);
    }
    dart.fn(loadComponent, dart.fnType(dart.legacy(async.Future$(dart.legacy(component_factory.ComponentRef$(dart.legacy(E))))), []));
    let beforeChangeDetectionReturn = null;
    if (beforeChangeDetection != null) {
      beforeChangeDetectionReturn = beforeChangeDetection(componentRef.instance);
    }
    if (FutureLOfvoid().is(beforeChangeDetectionReturn)) {
      return beforeChangeDetectionReturn.then(dart.legacy(component_factory.ComponentRef$(dart.legacy(E))), dart.fn(_ => loadComponent(), dart.fnType(dart.legacy(async.Future$(dart.legacy(component_factory.ComponentRef$(dart.legacy(E))))), [dart.void])));
    } else {
      return loadComponent();
    }
  };
  dart.trackLibraries("packages/angular_test/src/bootstrap", {
    "package:angular_test/src/frontend/bed.dart": bed,
    "package:angular_test/src/frontend/stabilizer.dart": stabilizer,
    "package:angular_test/src/frontend/ng_zone/timer_hook_zone.dart": timer_hook_zone,
    "package:angular_test/src/errors.dart": errors,
    "package:angular_test/src/errors/will_never_stabilize.dart": will_never_stabilize,
    "package:angular_test/src/errors/test_already_running.dart": test_already_running,
    "package:angular_test/src/errors/generic_type_missing.dart": generic_type_missing,
    "package:angular_test/src/frontend/ng_zone/real_time_stabilizer.dart": real_time_stabilizer,
    "package:angular_test/src/frontend/ng_zone/base_stabilizer.dart": base_stabilizer,
    "package:angular_test/src/frontend/fixture.dart": fixture,
    "package:angular_test/src/bootstrap.dart": bootstrap
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["frontend/bed.dart","frontend/stabilizer.dart","frontend/ng_zone/timer_hook_zone.dart","errors/will_never_stabilize.dart","errors/test_already_running.dart","errors/generic_type_missing.dart","frontend/ng_zone/base_stabilizer.dart","frontend/ng_zone/real_time_stabilizer.dart","frontend/fixture.dart","bootstrap.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;AA6GU,mBAAe,iBAAI;AACC,QAA1B,AAAS,AAAK,4BAAO,IAAI;AACzB,cAAO,KAAI;MACb;kCAE+C;AAC7C,cAAgB,wDAAM,MAAM;MAC9B;2BAEsC;AAAM,cAAiB;MAAY;iCAG9D,UACK;AAEd,cAAO,mDAAyB,SAAS,EAAE,AAAS,QAAD,wBAAa;MAClE;6BAgCsB;YACZ;YACQ;YACX;AAEL,YAAI,AAAE,wCAAG;AACwB,UAA/B,WAAM;;AAER,YAAI,AAAU,SAAD,IAAI;AACyB,UAAxC,WAAoB,+BAAQ;;AAE9B,cAAO,uEACM,SAAS,gBACN,YAAY,QACpB,IAAI,yBACa,qBAAqB;MAEhD;;YAUU;YACQ;YACX;AAEL,YAAI,AAAE,wCAAG;AACwB,UAA/B,WAAM;;AAER,cAAO,yDACC,IAAI,gBACI,YAAY,yBACH,qBAAqB;MAEhD;;YAIU;YACQ;YACX;AAEL,cAAO,gDACC,IAAI,gDAGE,qBAAqB,6CACnB,YAAY;MAE9B;;AA2BkC,cAAA,AAAkB,4BAAG;MAAI;mBAGhB;AACzC,sBAAI;AACiD,UAAnD,WAAM,8BAAiB;;AAEzB,cAAO,6BAAgB,uBAAQ,kBAAY,SAAS;MACtD;kBAOyC;AACvC,cAAO,gCACS,SAAW;;AAAY,iBAAc,AAAO,OAAA,CAAC,MAAM;kBAA5B,AAAa;;MAEtD;qBAG8D;;AAC5D,cAAO,8BACO,oCAAmB,6CAAC,2BAAD;AAAqB,wBAAO,WAAW;;;MAE1E;;YAUoC;YACE;AAEpC,cAAO,sBACL,8CACwB,sBAAsB,yBACvB,qBAAqB;MAEhD;;AAGE,YAAI,kBAAc;AACe,UAA/B,WAAM;;MAEV;;AAIM,2BAAe;AACnB,sBAAI,AAAW;AAMZ,UALD,eAAe,SAAE;;AACf,kBAA0B,6CACxB,wBACc,MAAM,EAApB,AAAa;;;AAInB,cAAO,aAAY;MACrB;uBAIO;YAC6B;YACE;AAKf,QAArB;AAGA,cAAO,mCAA8B;;AAEd,UAArB;AAGM,8BAAgB;AACf;AACP,mBAAO;AACL,kBAAO,AAAc,cAAD,gBAAK,cAChB,iBAAiB,8CAA6B;;;AAKxC;AAEjB,gBAAa,kCAAyC;AAAV;AAGpC,qCAAmB;AAGO,cAFhC,iBAAkC,oDAAjB,gBAAgB,IAC3B,AAAgB,gBAAA,CAAC,QAAQ,EAAE,aAAa,IACxC,AAAgB,gBAAA,CAAC,QAAQ;AAG/B,kBAAI,AAAuB,sBAAD,IAAI;AAC5B,sBAAO;;AAIH,8BAAY;AAQhB,cAPF,AAAe,cAAD,YAAY;AACxB;AACwC,kBAAtC,MAAM,AAAsB,sBAAA,CAAC,QAAQ;AACjB,kBAApB,AAAU,SAAD;;sBACF;sBAAG;AACmB,kBAA7B,AAAU,SAAD,eAAe,CAAC,EAAE,CAAC;;cAE/B;AACD,oBAAO,AAAU,AAAO,UAAR,qBAAqB,cAAM,AAAe,cAAD;YAC3D;;;AAEA,gBAAO,AAOL,8DANkB,oCAAlB,OAAqB,iCAAc,IAAI,WACjC,0BAAN,OAAS,qCACT,6DACwB,+BAA+B,yBAChC,qBAAqB,gBAC9B,aAAa,8BACtB,QAAC;AACe,YAArB;AACgC,YAAhC,MAAM,AAAe,cAAD;AACd,8BAAc,kDAClB,AAAa,AAAS,YAAV,cAAc,oCAC1B,YAAY,EACZ,cAAc;AAGQ,YAAxB,iBAAa,WAAW;AACxB,kBAAO,YAAW;UACnB;;MAEL;;;;YAMU;YACY;YACH;YACD;YACQ;AAExB,cAAO,iDACM,KAAL,IAAI,QAAJ,OAAQ,+BACO,MAAV,SAAS,SAAT,OAAa,sCACD,OAAX,UAAU,UAAV,OAAc,gDACC,OAAb,YAAY,UAAZ,OAAgB,6GACT,OAAV,SAAS,UAAT,OAAa;MAE5B;sBAG2D;;AACzD,cAAO,uCAAgB,SAAS;MAClC;cAG6B;AAAS,sCAAW,IAAI;MAAC;;;;UA/L5C;UACS;UACO;UACR;UACI;MACV,cAAE,IAAI;MACD,mBAAE,AAAU,SAAD;MACJ,0BAAE,UAAU;MAChB,uBAAe,KAAb,YAAY,QAAZ;MACE,0BAAE,SAAS;;;;UAGf;UACY;UACJ;UACX;MACL,cAAE,IAAI;MACD;MACO,oCACd,qBAAqB;MACX,sBAAE,YAAY;MACV,0BAAE,SAAS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;oCArNN,GAAe;;AAC5C,SAAO,AAAE,CAAD;UAAC;AAAU,kBAAO,CAAC;;;EAC7B;;AAWkC;;AAAY;iCAAY;IAAS;;8DAMpD,KACR;QAC6B;QACP;AAE3B,UAAO,AAAI,IAAD,iBAAgB,IAAI,2BACF,sBAAsB,yBACvB,qBAAqB;EAClD;;QAMU;QACQ;QACX;AAEL,UAAO,yDACC,IAAI,gBACI,YAAY,yBACH,qBAAqB;EAEhD;;MA7CsB,cAAU;;;;;;;;AC4DT;IAAK;WAmBW;AACnC,YAAO,sBAAkB;AACvB,YAAI,sBAAsB,IAAI;AACJ,UAAxB,AAAsB,sBAAA;;AAExB,cAAO;;IAEX;;UAMkB;UACZ;AAFgB;AAIpB,YAAI,AAAU,SAAD,IAAI;AACyB,UAAxC,WAAoB,+BAAQ;;AAE9B,YAAI,sBAAsB,IAAI;AACQ,UAApC,MAAM,YAAO,sBAAsB;;AAErC,cAAO,6BAAuB,SAAS;MACzC;;2BAMwC;AAAL;AACjC,YAAc,aAAV,SAAS,IAAG;AACmD,UAAjE,WAAoB,6BAAM,SAAS,EAAE,aAAa;;AAGhD,oBAAQ;AACZ,iBAAK;;AAAuB,gBAAQ,EAAH,KAAL,KAAK,qCAAK,SAAS;;;AAG/C,0BAAQ,MAAM;AACZ,wBAAI,AAAiB,iBAAA;AACqB,YAAxC,WAAM,qDAAwB,SAAS;;;MAG7C;;;;;EAjEwB;;;;;;;;;;;;;;;MAHM,wCAAY;;;;;;AA2ErB;IAAI;;;AAHnB;;EAA+B;;;;;;;;;;AAehB,YAAA,AAAW,0BAAM,QAAC,YAAa,AAAS,QAAD;IAAU;WAGjC;AAAlB;AACjB,sBAAI,AAAW;AACb,gBAAO;;AAGT,YAAI,AAAuB,sBAAD,IAAI,kBAAQ;AAOwB,UAA5D,MAAM,iBAAW,sBAAsB,EAAE,QAAC,KAAM,WAAC,AAAE,CAAD;;AAEV,UAAxC,MAAM,iBAAW,sBAAsB;;AAGf,QAA1B,4BAAsB;AACtB,cAAO;MACT;;iBAIkB,wBACgB;AAFX;AAIrB,iBAAW,WAAY;AACrB,cAAI,AAAK,IAAD,IAAI,kBAAQ,AAAI,IAAA,CAAC,QAAQ;AACc,YAA7C,MAAM,AAAS,QAAD,QAAQ,sBAAsB;;;MAGlD;;2BAGwC;AAAL;AACjC;AAC6B,UAA3B,4BAAsB;AACtB,gBAAa,8BAAuB,SAAS;;AAElB,UAA3B,4BAAsB;;MAE1B;;;;;;yDAhDuD;IAFlD,4BAAsB;IAGV,mBAAE,AAAY,WAAD,qBAAkB;AADhD;;EACsD;;;;;;;;;;;;;8DAnIpB;AAElC,UAAO,UAAU,UAAyB,gBACjC,+CAA4B,AAAU,SAAD,4BAAK,QAAC;AAIhD,UAAM,oDAAF,CAAC;AACH,cAAO,AAAC,EAAA,CAAC,QAAQ,EAAE,IAAI;;AAGzB,UAAM,mCAAF,CAAC;AACH,cAAO,AAAC,EAAA,CAAC,QAAQ;;AAGkC,MAArD,WAAM,2BAAc,AAAgC,0CAAF,CAAC;;EAGzD;;;;;ICbqB;;;;;;IAWQ;;;;;;WAWL;AAAY,YAAA,AAAW,sCAAI,OAAO;IAAC;;;IAjDpD;IA2Bc,oBAAc,SAC/B,MACA,QACA,MACA,UACA,aAEO,AAAO,MAAD,aAAa,IAAI,EAAE,QAAQ,EAAE,QAAQ;IAIzB,4BAAsB,SAC/C,MACA,QACA,MACA,UACA,aAEO,AAAO,MAAD,qBAAqB,IAAI,EAAE,QAAQ,EAAE,QAAQ;AAtBzD,IApBD,mBAAkB,AAAQ,wCACT,+CAA+B,SAC5C,MACA,QACA,MACA,UACA;;AAGA,eAAmB,IAAI;eAAE,MAAM;eAAE,IAAI;eAAE,QAAQ;eAAE,QAAQ;gBAAlD,AAAW;4EACI,SACtB,MACA,QACA,MACA,UACA;;AAGA,eAA2B,IAAI;eAAE,MAAM;eAAE,IAAI;eAAE,QAAQ;eAAE,QAAQ;gBAA1D,AAAmB;;EAGhC;;;;;;;;;;;;;;;;ICtBU;;;;;;;AAKW,YAAA,AAA+C,yCAAnB,kBAAS;IAAU;;;IAHvC;AAA7B;;EAAuC;;;;;;;;;;;;;;;;ACDrC,YACI,oEACA,uEACA,wEACA,oEACA,kDACA,uEACA,uEACA;IACN;;;;;EACF;;;;;;;;;;;;ICXe;;;;;;;AAMX,UAAI,AAAQ,gBAAG;AACb,cAAO;;AAET,YAAO,AAAiC,sCAAR;IAClC;;;IAR8B;AAA9B;;EAAuC;;;;;;;;;;;;;;;;;;;;;;;;MCI1B;;;;;;MAIU;;;;;;;AAUF,0BAAC,AAAO;MAAoB;;MAEnB;aAIZ;AAEhB,cAAO,AAGJ,qBAHsB;;AACsC,UAA7D,2BAA2C,KAAvB,sBAAsB,QAAtB;AACpB,gBAAO;iDACD,QAAC,KAAM;MACjB;4BAEyC;AACyB,QAAhE,wBAAkB,cAAM,AAAO,uBAAW,cAAM,AAAY,YAAA;MAC9D;;AAMqC,cAAA,AAAO,AAAW;MAAK;+BAGX;AAC/C,cAAkB,gCAAW,AAAa,YAAD,QAAM;MACjD;;AAEwC;AAC/B;AACI;AACc;AAOvB,UAJF,aAAa,AAAO,AAAQ,2BAAO,QAAC;AACX,YAAvB,gBAAgB,AAAE,CAAD;AAC8B,YAA/C,gBAAgB,uDAAkB,AAAE,CAAD;AAChB,YAAnB,AAAW,UAAD;;AAIc,UAA1B,MAAM;AAGa,UAAnB,MAAM,oBAAO;;AACiB,UAA9B,mBAAU,AAAW,UAAD;AAGpB,gBAAO,AAAc,cAAD,IAAI,OACX,qBAAM,aAAa,EAAE,aAAa,IAClC;QACf;;;yCAxD0B,QAAa;MAAb;MAAa;AAAvC;;IAAqD;;;;;;;;;;;;;;;;;;;;;;;;;;ICiEzC;;;;;;IAGG;;;;;;IAGO;;;;;;;AAMT,MAAX,AAAS;AACS,MAAlB,AAAU;IACZ;;2BAG6B;AAAM,YAAA,AAAU,2BAAU,AAAE,CAAD;IAAW;;AAGnD,YAAA,AAAU;IAAI;;AAGT,YAAA,AAAU;IAAQ;;sDAfb,WAAgB,WAAgB;IAAhC;IAAgB;IAAgB;;EAAU;;;;;;;;;;;;;;;;;;;;;;;;;eA3ErB,WAAkB;AAEzD,0BAAgB;AAkCrB,MAjCD,AAAU,SAAD,eAAe,SACtB,MACA,QACA,MACA,UACA;AAIA,uBAAK,sBAAc,MAAM,EAAE,IAAI;AAC7B,gBAAO,AAAO,OAAD,aAAa,IAAI,EAAE,QAAQ,EAAE,QAAQ;;AAGrC;AACT,8BAAkB;AACtB;AACY,YAAV,AAAQ,QAAA;;AAEsB,YAA9B,AAAc,aAAD,QAAQ,QAAQ;;;AAG3B,uBAAW,AAAO,MAAD,aACrB,IAAI,EACJ,QAAQ,EACR,eAAe;AAMhB,QAJD,WAAW,4CACT,QAAQ,EACR,QAAQ,EACR,cAAM,AAAc,aAAD,QAAQ,QAAQ;AAEV,QAA3B,AAAc,aAAD,KAAK,QAAQ;AAC1B,cAAO,SAAQ;;AAEjB,YAAgC,sDAC9B,MAAM,EACN,aAAa;IAEjB;;AAQqB,YAAe,WAAT,6BAAY,AAAc;IAAO;;AAG7B;AACG,QAAhC,MAAY;AACZ,sBAAI,AAAc;AACiD,UAAjE,MAAM,uBAAqB;;MAE/B;;;AAGE,YAAO,AAAc,AAAS,AAAK;IACrC;;;;;+DAjBS,QACuB;AAC5B,2EAAM,MAAM,EAAE,aAAa;;EAAC;;;;;;;;;;;;;;;AC3BZ;AACJ,UAAd,MAAM;AACqB,UAA3B,AAAkB;AACwB,UAA1C,AAAkB,AAAS,AAAO;AACT,UAAzB,AAAgB;AACC,UAAjB,iBAAa;QACf;;;AAG2B,cAAA,AAAkB;MAAQ;aAoBN;AAC7C,cAAO,AAAgB,2DAAkC;AACvD,gBAAI,GAAG,IAAI;AAGP,cAFF,oBAAkB;AACe,gBAA/B,AAAG,GAAA,CAAC,AAAkB;;;;MAI9B;;AAKmB,cAAA,AAAY;MAAI;;AAkBP,cAAA,AAAkB;MAAQ;;iCApE/C,iBACA,mBACA;MAFA;MACA;MACA;;IACN;;;;;;;;;;;;;;;;;;;;;;;;;;4DAnBwC,SAAgB;AACzD,6BAAO,AAAQ,AAAkB,AAAS,OAA5B,kCAAgC,WAAW;EAC3D;;+DCFkD;AAEhD,kBAAI,AAAU,SAAD;AACX,YAAO,UAAE,kBAAY,MAAM;;AAE7B,UAAO,UAAE,kBACmB,4CAAiB,uBACzC,SAAS,IACR,MAAM;EAEb;4DAWsB,kBACZ,aACQ;QACkB;QACP;QACT;AANuB;AAQzC,UAAI,AAAiB,gBAAD,IAAI;AACyB,QAA/C,WAAoB,+BAAQ;;AAE9B,UAAI,AAAY,WAAD,IAAI;AACyB,QAA1C,WAAoB,+BAAQ;;AAE9B,UAAI,AAAa,YAAD,IAAI;AACyB,QAA3C,WAAoB,+BAAQ;;AAGxB,qBAAW,gBAAY,YAAY,iBAAgB,YAAY;AAChD,wCAAS,AAAS,QAAD,KAAK;AAC/B;AACC,gCAAS,AAAS,QAAD,KAAK;AAC7B,uBAAa,AAAO,AAAQ,MAAT,gBAAgB,QAAC;AACzB,QAAf,cAAc,CAAC;;AAGjB,UAAI,sBAAsB,IAAI;AACU,QAAtC,MAAM,AAAsB,sBAAA,CAAC,QAAQ;;AAKvC,YAAO,AAAO,OAAD,mEAAsB,cAC1B,AAML,+CALA,MAAM,EACN,gBAAgB,EAChB,WAAW,EACX,QAAQ,0BACe,qBAAqB,sEACvC,QAAiB;AAGO,QAA7B,MAAM,AAAO,AAAW,MAAZ;AAQc,QAA1B,MAAM;AACmB,QAAzB,MAAM,AAAW,UAAD;AAChB,YAAI,WAAW,IAAI;AACjB,gBAAc,mFACZ,AAAY,WAAD,QACA,+BAAW,AAAY,AAAW,WAAZ,mBAAiB;;AAGtD,cAAO,aAAY;MACpB;IAEL;;oEAGiB,QACK,kBACZ,aACC;QACM;AAET,uBAAe,AAAiB,gBAAD,QAAQ,QAAQ;AAC/C,iBAAgC,AAAkB,oBAAxC,AAAa,YAAD;AAC5B,QAAI,MAAM,UACN,MAAM;AAImC,MAH3C,WAAM,8BAAgB,AAClB,uEACA,yEACA,0BAAc,MAAM,gCAAe,iCAAC;;AAG1C,aAAwB;AACmB,MAAzC,AAAY,WAAD,UAAQ,AAAa,YAAD;AAC8B,MAA7D,AAAO,MAAD,wBAAwB,AAAa,YAAD;AAGxC,MAFF,AAAa,YAAD,WAAW;AAC0C,QAA/D,AAAO,MAAD,0BAA0B,AAAa,YAAD;;AAEjC,MAAb,AAAO,MAAD;AACN,YAAc,mFAAM,YAAY;;;AAGnB;AACf,QAAI,qBAAqB,IAAI;AAC+C,MAA1E,8BAA8B,AAAqB,qBAAA,CAAC,AAAa,YAAD;;AAGlE,QAAgC,mBAA5B,2BAA2B;AAC7B,YAAO,AAA4B,4BAAD,oEAAM,QAAC,KAAM,AAAa,aAAA;;AAE5D,YAAO,AAAa,cAAA;;EAExB","file":"bootstrap.ddc.js"}');
  // Exports:
  return {
    src__frontend__bed: bed,
    src__frontend__stabilizer: stabilizer,
    src__frontend__ng_zone__timer_hook_zone: timer_hook_zone,
    src__errors: errors,
    src__errors__will_never_stabilize: will_never_stabilize,
    src__errors__test_already_running: test_already_running,
    src__errors__generic_type_missing: generic_type_missing,
    src__frontend__ng_zone__real_time_stabilizer: real_time_stabilizer,
    src__frontend__ng_zone__base_stabilizer: base_stabilizer,
    src__frontend__fixture: fixture,
    src__bootstrap: bootstrap
  };
}));

//# sourceMappingURL=bootstrap.ddc.js.map
