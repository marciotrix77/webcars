define(['dart_sdk'], (function load__packages__package_config__src__errors(dart_sdk) {
  'use strict';
  const core = dart_sdk.core;
  const _interceptors = dart_sdk._interceptors;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  var util = Object.create(dart.library);
  var errors = Object.create(dart.library);
  var $codeUnitAt = dartx.codeUnitAt;
  var $startsWith = dartx.startsWith;
  var $indexOf = dartx.indexOf;
  var $substring = dartx.substring;
  var $isEmpty = dartx.isEmpty;
  var $toRadixString = dartx.toRadixString;
  var $padLeft = dartx.padLeft;
  var $_get = dartx._get;
  var $endsWith = dartx.endsWith;
  var $length = dartx.length;
  var $toLowerCase = dartx.toLowerCase;
  var $add = dartx.add;
  var $isNotEmpty = dartx.isNotEmpty;
  var $removeLast = dartx.removeLast;
  var $last = dartx.last;
  var $skip = dartx.skip;
  var $join = dartx.join;
  var $invalidValue = dartx.invalidValue;
  var $name = dartx.name;
  var $message = dartx.message;
  var StringL = () => (StringL = dart.constFn(dart.legacy(core.String)))();
  var JSArrayOfStringL = () => (JSArrayOfStringL = dart.constFn(_interceptors.JSArray$(StringL())))();
  const CT = Object.create(null);
  var L1 = "package:package_config/src/errors.dart";
  var L0 = "file:///tmp/scratch_spaceZAGYHZ/packages/package_config/src/util.dart";
  util.isValidPackageName = function isValidPackageName(string) {
    return dart.notNull(util.checkPackageName(string)) < 0;
  };
  util.checkPackageName = function checkPackageName(string) {
    let nonDot = 0;
    for (let i = 0; i < string.length; i = i + 1) {
      let c = string[$codeUnitAt](i);
      if (c > 127 || util._validPackageNameCharacters[$codeUnitAt](c) <= 32) {
        return i;
      }
      nonDot = nonDot + ((c ^ 46) >>> 0);
    }
    if (nonDot === 0) return string.length;
    return -1;
  };
  util.checkValidPackageUri = function checkValidPackageUri(packageUri, name) {
    if (packageUri.scheme !== "package") {
      dart.throw(new errors.PackageConfigArgumentError.new(packageUri, name, "Not a package: URI"));
    }
    if (dart.test(packageUri.hasAuthority)) {
      dart.throw(new errors.PackageConfigArgumentError.new(packageUri, name, "Package URIs must not have a host part"));
    }
    if (dart.test(packageUri.hasQuery)) {
      dart.throw(new errors.PackageConfigArgumentError.new(packageUri, name, "Package URIs must not have a query part"));
    }
    if (dart.test(packageUri.hasFragment)) {
      dart.throw(new errors.PackageConfigArgumentError.new(packageUri, name, "Package URIs must not have a fragment part"));
    }
    if (packageUri.path[$startsWith]("/")) {
      dart.throw(new errors.PackageConfigArgumentError.new(packageUri, name, "Package URIs must not start with a '/'"));
    }
    let firstSlash = packageUri.path[$indexOf]("/");
    if (firstSlash === -1) {
      dart.throw(new errors.PackageConfigArgumentError.new(packageUri, name, "Package URIs must start with the package name followed by a '/'"));
    }
    let packageName = packageUri.path[$substring](0, firstSlash);
    let badIndex = util.checkPackageName(packageName);
    if (dart.notNull(badIndex) >= 0) {
      if (packageName[$isEmpty]) {
        dart.throw(new errors.PackageConfigArgumentError.new(packageUri, name, "Package names mus be non-empty"));
      }
      if (badIndex === packageName.length) {
        dart.throw(new errors.PackageConfigArgumentError.new(packageUri, name, "Package names must contain at least one non-'.' character"));
      }
      if (!(dart.notNull(badIndex) < packageName.length)) dart.assertFailed(null, L0, 90, 12, "badIndex < packageName.length");
      let badCharCode = packageName[$codeUnitAt](badIndex);
      let badChar = "U+" + badCharCode[$toRadixString](16)[$padLeft](4, "0");
      if (badCharCode >= 32 && badCharCode <= 126) {
        badChar = "'" + dart.str(packageName[$_get](badIndex)) + "' (" + badChar + ")";
      }
      dart.throw(new errors.PackageConfigArgumentError.new(packageUri, name, "Package names must not contain " + badChar));
    }
    return packageName;
  };
  util.isAbsoluteDirectoryUri = function isAbsoluteDirectoryUri(uri) {
    if (dart.test(uri.hasQuery)) return false;
    if (dart.test(uri.hasFragment)) return false;
    if (!dart.test(uri.hasScheme)) return false;
    let path = uri.path;
    if (!path[$endsWith]("/")) return false;
    return true;
  };
  util.isUriPrefix = function isUriPrefix(prefix, path) {
    if (!!dart.test(prefix.hasFragment)) dart.assertFailed(null, L0, 119, 10, "!prefix.hasFragment");
    if (!!dart.test(prefix.hasQuery)) dart.assertFailed(null, L0, 120, 10, "!prefix.hasQuery");
    if (!!dart.test(path.hasQuery)) dart.assertFailed(null, L0, 121, 10, "!path.hasQuery");
    if (!!dart.test(path.hasFragment)) dart.assertFailed(null, L0, 122, 10, "!path.hasFragment");
    if (!prefix.path[$endsWith]("/")) dart.assertFailed(null, L0, 123, 10, "prefix.path.endsWith('/')");
    return dart.toString(path)[$startsWith](dart.toString(prefix));
  };
  util.firstNonWhitespaceChar = function firstNonWhitespaceChar(bytes) {
    for (let i = 0; i < dart.notNull(bytes[$length]); i = i + 1) {
      let char = bytes[$_get](i);
      if (char !== 32 && char !== 9 && char !== 10 && char !== 13) {
        return char;
      }
    }
    return -1;
  };
  util.relativizeUri = function relativizeUri(uri, baseUri) {
    if (baseUri == null) return uri;
    if (!dart.test(baseUri.isAbsolute)) dart.assertFailed(null, L0, 158, 10, "baseUri.isAbsolute");
    if (dart.test(uri.hasQuery) || dart.test(uri.hasFragment)) {
      uri = core._Uri.new({scheme: uri.scheme, userInfo: dart.test(uri.hasAuthority) ? uri.userInfo : null, host: dart.test(uri.hasAuthority) ? uri.host : null, port: dart.test(uri.hasAuthority) ? uri.port : null, path: uri.path});
    }
    if (!dart.test(uri.isAbsolute)) return uri;
    if (baseUri.scheme != uri.scheme) {
      return uri;
    }
    if (!dart.equals(uri.hasAuthority, baseUri.hasAuthority)) return uri;
    if (dart.test(uri.hasAuthority)) {
      if (uri.userInfo != baseUri.userInfo || uri.host[$toLowerCase]() !== baseUri.host[$toLowerCase]() || uri.port != baseUri.port) {
        return uri;
      }
    }
    baseUri = baseUri.normalizePath();
    let base = (() => {
      let t0 = JSArrayOfStringL().of([]);
      for (let t1 of baseUri.pathSegments)
        t0[$add](t1);
      return t0;
    })();
    if (dart.test(base[$isNotEmpty])) base[$removeLast]();
    uri = uri.normalizePath();
    let target = (() => {
      let t2 = JSArrayOfStringL().of([]);
      for (let t3 of uri.pathSegments)
        t2[$add](t3);
      return t2;
    })();
    if (dart.test(target[$isNotEmpty]) && target[$last][$isEmpty]) target[$removeLast]();
    let index = 0;
    while (index < dart.notNull(base[$length]) && index < dart.notNull(target[$length])) {
      if (base[$_get](index) != target[$_get](index)) {
        break;
      }
      index = index + 1;
    }
    if (index === base[$length]) {
      if (index === target[$length]) {
        return core._Uri.new({path: "./"});
      }
      return core._Uri.new({path: target[$skip](index)[$join]("/")});
    } else if (index > 0) {
      let buffer = new core.StringBuffer.new();
      for (let n = dart.notNull(base[$length]) - index; n > 0; n = n - 1) {
        buffer.write("../");
      }
      buffer.writeAll(target[$skip](index), "/");
      return core._Uri.new({path: buffer.toString()});
    } else {
      return uri;
    }
  };
  dart.defineLazy(util, {
    /*util._validPackageNameCharacters*/get _validPackageNameCharacters() {
      return "                                 !  $ &'()*+,-. 0123456789 ; =  @ABCDEFGHIJKLMNOPQRSTUVWXYZ    _ abcdefghijklmnopqrstuvwxyz   ~ ";
    },
    /*util.$lf*/get $lf() {
      return 10;
    },
    /*util.$cr*/get $cr() {
      return 13;
    },
    /*util.$space*/get $space() {
      return 32;
    },
    /*util.$hash*/get $hash() {
      return 35;
    },
    /*util.$dot*/get $dot() {
      return 46;
    },
    /*util.$colon*/get $colon() {
      return 58;
    },
    /*util.$question*/get $question() {
      return 63;
    },
    /*util.$lbrace*/get $lbrace() {
      return 123;
    }
  }, true);
  errors.PackageConfigError = class PackageConfigError extends core.Object {};
  (errors.PackageConfigError.__ = function() {
    ;
  }).prototype = errors.PackageConfigError.prototype;
  dart.addTypeTests(errors.PackageConfigError);
  dart.addTypeCaches(errors.PackageConfigError);
  dart.setLibraryUri(errors.PackageConfigError, L1);
  errors.PackageConfigArgumentError = class PackageConfigArgumentError extends core.ArgumentError {};
  (errors.PackageConfigArgumentError.new = function(value, name, message) {
    errors.PackageConfigArgumentError.__proto__.value.call(this, value, name, message);
    ;
  }).prototype = errors.PackageConfigArgumentError.prototype;
  (errors.PackageConfigArgumentError.from = function(error) {
    errors.PackageConfigArgumentError.__proto__.value.call(this, error[$invalidValue], error[$name], error[$message]);
    ;
  }).prototype = errors.PackageConfigArgumentError.prototype;
  dart.addTypeTests(errors.PackageConfigArgumentError);
  dart.addTypeCaches(errors.PackageConfigArgumentError);
  errors.PackageConfigArgumentError[dart.implements] = () => [errors.PackageConfigError];
  dart.setLibraryUri(errors.PackageConfigArgumentError, L1);
  errors.PackageConfigFormatException = class PackageConfigFormatException extends core.FormatException {};
  (errors.PackageConfigFormatException.new = function(message, source, offset = null) {
    errors.PackageConfigFormatException.__proto__.new.call(this, message, source, offset);
    ;
  }).prototype = errors.PackageConfigFormatException.prototype;
  (errors.PackageConfigFormatException.from = function(exception) {
    errors.PackageConfigFormatException.__proto__.new.call(this, exception.message, exception.source, exception.offset);
    ;
  }).prototype = errors.PackageConfigFormatException.prototype;
  dart.addTypeTests(errors.PackageConfigFormatException);
  dart.addTypeCaches(errors.PackageConfigFormatException);
  errors.PackageConfigFormatException[dart.implements] = () => [errors.PackageConfigError];
  dart.setLibraryUri(errors.PackageConfigFormatException, L1);
  errors.throwError = function throwError(error) {
    return dart.throw(error);
  };
  dart.trackLibraries("packages/package_config/src/errors", {
    "package:package_config/src/util.dart": util,
    "package:package_config/src/errors.dart": errors
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["util.dart","errors.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;wDAgB+B;AAC7B,UAAgC,cAAzB,sBAAiB,MAAM,KAAI;EACpC;oDAU4B;AAEtB,iBAAS;AACb,aAAS,IAAI,GAAG,AAAE,CAAD,GAAG,AAAO,MAAD,SAAS,IAAA,AAAC,CAAA;AAC9B,cAAI,AAAO,MAAD,cAAY,CAAC;AAC3B,UAAI,AAAE,CAAD,GAAG,OAAQ,AAA4B,AAAc,8CAAH,CAAC;AACtD,cAAO,EAAC;;AAEQ,MAAlB,SAAA,AAAO,MAAD,IAAM,CAAF,CAAC;;AAEb,QAAI,AAAO,MAAD,KAAI,GAAG,MAAO,AAAO,OAAD;AAC9B,UAAO,EAAC;EACV;4DAQgC,YAAmB;AACjD,QAAI,AAAW,UAAD,YAAW;AACiD,MAAxE,WAAM,0CAA2B,UAAU,EAAE,IAAI,EAAE;;AAErD,kBAAI,AAAW,UAAD;AAEmD,MAD/D,WAAM,0CACF,UAAU,EAAE,IAAI,EAAE;;AAExB,kBAAI,AAAW,UAAD;AAGoD,MADhE,WAAM,0CACF,UAAU,EAAE,IAAI,EAAE;;AAExB,kBAAI,AAAW,UAAD;AAMuD,MADnE,WAAM,0CACF,UAAU,EAAE,IAAI,EAAE;;AAExB,QAAI,AAAW,AAAK,UAAN,mBAAiB;AAEkC,MAD/D,WAAM,0CACF,UAAU,EAAE,IAAI,EAAE;;AAEpB,qBAAa,AAAW,AAAK,UAAN,gBAAc;AACzC,QAAI,AAAW,UAAD,KAAI,CAAC;AAEqD,MADtE,WAAM,0CAA2B,UAAU,EAAE,IAAI,EAC7C;;AAEF,sBAAc,AAAW,AAAK,UAAN,kBAAgB,GAAG,UAAU;AACrD,mBAAW,sBAAiB,WAAW;AAC3C,QAAa,aAAT,QAAQ,KAAI;AACd,UAAI,AAAY,WAAD;AAE0C,QADvD,WAAM,0CACF,UAAU,EAAE,IAAI,EAAE;;AAExB,UAAI,AAAS,QAAD,KAAI,AAAY,WAAD;AAEuC,QADhE,WAAM,0CAA2B,UAAU,EAAE,IAAI,EAC7C;;AAEN,YAAgB,aAAT,QAAQ,IAAG,AAAY,WAAD;AACzB,wBAAc,AAAY,WAAD,cAAY,QAAQ;AAC7C,oBAAU,AAAK,OAAE,AAAY,AAAkB,WAAnB,iBAAe,cAAY,GAAG;AAC9D,UAAI,AAAY,WAAD,IAAI,MAAQ,AAAY,WAAD,IAAI;AAES,QAAjD,UAAU,AAAuC,eAAnC,AAAW,WAAA,QAAC,QAAQ,KAAE,QAAI,OAAO;;AAGe,MADhE,WAAM,0CACF,UAAU,EAAE,IAAI,EAAE,AAAyC,oCAAR,OAAO;;AAEhE,UAAO,YAAW;EACpB;gEAOgC;AAC9B,kBAAI,AAAI,GAAD,YAAW,MAAO;AACzB,kBAAI,AAAI,GAAD,eAAc,MAAO;AAC5B,mBAAK,AAAI,GAAD,aAAY,MAAO;AACvB,eAAO,AAAI,GAAD;AACd,SAAK,AAAK,IAAD,YAAU,MAAM,MAAO;AAChC,UAAO;EACT;0CAGqB,QAAY;AAC/B,SAAO,WAAC,AAAO,MAAD;AACd,SAAO,WAAC,AAAO,MAAD;AACd,SAAO,WAAC,AAAK,IAAD;AACZ,SAAO,WAAC,AAAK,IAAD;AACZ,SAAO,AAAO,AAAK,MAAN,iBAAe;AAC5B,UAAY,AAAW,eAAhB,IAAI,eAA8B,cAAP,MAAM;EAC1C;gEAKqC;AACnC,aAAS,IAAI,GAAG,AAAE,CAAD,gBAAG,AAAM,KAAD,YAAS,IAAA,AAAC,CAAA;AAC7B,iBAAO,AAAK,KAAA,QAAC,CAAC;AAClB,UAAI,IAAI,KAAI,MAAQ,IAAI,KAAI,KAAQ,IAAI,KAAI,MAAQ,IAAI,KAAI;AAC1D,cAAO,KAAI;;;AAGf,UAAO,EAAC;EACV;8CAkBsB,KAAe;AACnC,QAAI,AAAQ,OAAD,IAAI,MAAM,MAAO,IAAG;AAC/B,mBAAO,AAAQ,OAAD;AACd,kBAAI,AAAI,GAAD,wBAAa,AAAI,GAAD;AAMF,MALnB,MAAM,uBACM,AAAI,GAAD,6BACD,AAAI,GAAD,iBAAgB,AAAI,GAAD,YAAY,sBACtC,AAAI,GAAD,iBAAgB,AAAI,GAAD,QAAQ,sBAC9B,AAAI,GAAD,iBAAgB,AAAI,GAAD,QAAQ,YAC9B,AAAI,GAAD;;AAIf,mBAAK,AAAI,GAAD,cAAa,MAAO,IAAG;AAE/B,QAAI,AAAQ,OAAD,WAAW,AAAI,GAAD;AACvB,YAAO,IAAG;;AAIZ,qBAAI,AAAI,GAAD,eAAiB,AAAQ,OAAD,gBAAe,MAAO,IAAG;AACxD,kBAAI,AAAI,GAAD;AACL,UAAI,AAAI,GAAD,aAAa,AAAQ,OAAD,aACvB,AAAI,AAAK,GAAN,0BAAuB,AAAQ,AAAK,OAAN,yBACjC,AAAI,GAAD,SAAS,AAAQ,OAAD;AACrB,cAAO,IAAG;;;AAImB,IAAjC,UAAU,AAAQ,OAAD;AACb,eAAO;;AAAC,oBAAG,AAAQ,QAAD;AAAV;;;AACZ,kBAAI,AAAK,IAAD,gBAAa,AAAK,AAAY,IAAb;AACA,IAAzB,MAAM,AAAI,GAAD;AACL,iBAAS;;AAAC,oBAAG,AAAI,IAAD;AAAN;;;AACd,kBAAI,AAAO,MAAD,kBAAe,AAAO,AAAK,MAAN,mBAAe,AAAO,AAAY,MAAb;AAChD,gBAAQ;AACZ,WAAO,AAAM,KAAD,gBAAG,AAAK,IAAD,cAAW,AAAM,KAAD,gBAAG,AAAO,MAAD;AAC1C,UAAI,AAAI,IAAA,QAAC,KAAK,KAAK,AAAM,MAAA,QAAC,KAAK;AAC7B;;AAEK,MAAP,QAAA,AAAK,KAAA;;AAEP,QAAI,AAAM,KAAD,KAAI,AAAK,IAAD;AACf,UAAI,AAAM,KAAD,KAAI,AAAO,MAAD;AACjB,cAAO,sBAAU;;AAEnB,YAAO,sBAAU,AAAO,AAAY,MAAb,QAAM,KAAK,SAAO;UACpC,KAAI,AAAM,KAAD,GAAG;AACb,mBAAS;AACb,eAAS,IAAgB,aAAZ,AAAK,IAAD,aAAU,KAAK,EAAE,AAAE,CAAD,GAAG,GAAK,IAAF,AAAE,CAAC,GAAH;AACpB,QAAnB,AAAO,MAAD,OAAO;;AAEyB,MAAxC,AAAO,MAAD,UAAU,AAAO,MAAD,QAAM,KAAK,GAAG;AACpC,YAAO,sBAAU,AAAO,MAAD;;AAEvB,YAAO,IAAG;;EAEd;;MAzMa,gCAA2B;;;MA6M9B,QAAG;;;MAGH,QAAG;;;MAGH,WAAM;;;MAGN,UAAK;;;MAGL,SAAI;;;MAGJ,WAAM;;;MAGN,cAAS;;;MAGT,YAAO;;;;;;;ECpOO;;;;;oDAKkB,OAAc,MAAa;AACvD,iEAAM,KAAK,EAAE,IAAI,EAAE,OAAO;;EAAC;qDAEO;AAClC,iEAAM,AAAM,KAAD,iBAAe,AAAM,KAAD,SAAO,AAAM,KAAD;;EAAS;;;;;;sDAK5B,SAAsB,QAC3C;AACT,iEAAM,OAAO,EAAE,MAAM,EAAE,MAAM;;EAAC;uDAEc;AAC5C,iEAAM,AAAU,SAAD,UAAU,AAAU,SAAD,SAAS,AAAU,SAAD;;EAAQ;;;;;0CAInC;AAAU,sBAAM,KAAK","file":"errors.ddc.js"}');
  // Exports:
  return {
    src__util: util,
    src__errors: errors
  };
}));

//# sourceMappingURL=errors.ddc.js.map
