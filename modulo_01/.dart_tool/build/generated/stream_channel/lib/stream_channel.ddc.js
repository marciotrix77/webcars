define(['dart_sdk', 'packages/async/async'], (function load__packages__stream_channel__stream_channel(dart_sdk, packages__async__async) {
  'use strict';
  const core = dart_sdk.core;
  const async = dart_sdk.async;
  const _js_helper = dart_sdk._js_helper;
  const collection = dart_sdk.collection;
  const _interceptors = dart_sdk._interceptors;
  const convert = dart_sdk.convert;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const single_subscription_transformer = packages__async__async.src__single_subscription_transformer;
  const stream_sink_transformer = packages__async__async.src__stream_sink_transformer;
  const stream = packages__async__async.src__delegate__stream;
  const stream_sink = packages__async__async.src__delegate__stream_sink;
  const stream_transformer_wrapper = packages__async__async.src__stream_sink_transformer__stream_transformer_wrapper;
  const stream_completer = packages__async__async.src__stream_completer;
  const stream_sink_completer = packages__async__async.src__stream_sink_completer;
  const null_stream_sink = packages__async__async.src__null_stream_sink;
  const async_memoizer = packages__async__async.src__async_memoizer;
  var guarantee_channel = Object.create(dart.library);
  var stream_channel = Object.create(dart.library);
  var stream_channel_transformer = Object.create(dart.library);
  var stream_channel_controller = Object.create(dart.library);
  var stream_channel_completer = Object.create(dart.library);
  var multi_channel = Object.create(dart.library);
  var json_document_transformer = Object.create(dart.library);
  var disconnector = Object.create(dart.library);
  var delegating_stream_channel = Object.create(dart.library);
  var close_guarantee_channel = Object.create(dart.library);
  var $_set = dartx._set;
  var $putIfAbsent = dartx.putIfAbsent;
  var $_get = dartx._get;
  var $containsKey = dartx.containsKey;
  var $remove = dartx.remove;
  var $isEmpty = dartx.isEmpty;
  var $values = dartx.values;
  var $clear = dartx.clear;
  var $map = dartx.map;
  var $toList = dartx.toList;
  var $add = dartx.add;
  var VoidToNullN = () => (VoidToNullN = dart.constFn(dart.fnType(core.Null, [])))();
  var dynamicToNullN = () => (dynamicToNullN = dart.constFn(dart.fnType(core.Null, [dart.dynamic])))();
  var FutureOfvoid = () => (FutureOfvoid = dart.constFn(async.Future$(dart.void)))();
  var intL = () => (intL = dart.constFn(dart.legacy(core.int)))();
  var _IdentityHashSetOfintL = () => (_IdentityHashSetOfintL = dart.constFn(collection._IdentityHashSet$(intL())))();
  var ObjectL = () => (ObjectL = dart.constFn(dart.legacy(core.Object)))();
  var JSArrayOfObjectL = () => (JSArrayOfObjectL = dart.constFn(_interceptors.JSArray$(ObjectL())))();
  var VoidTovoid = () => (VoidTovoid = dart.constFn(dart.fnType(dart.void, [])))();
  var JSArrayOfintL = () => (JSArrayOfintL = dart.constFn(_interceptors.JSArray$(intL())))();
  var StringL = () => (StringL = dart.constFn(dart.legacy(core.String)))();
  var StreamChannelOfStringL = () => (StreamChannelOfStringL = dart.constFn(stream_channel.StreamChannel$(StringL())))();
  var StreamChannelLOfStringL = () => (StreamChannelLOfStringL = dart.constFn(dart.legacy(StreamChannelOfStringL())))();
  var ObjectN = () => (ObjectN = dart.constFn(dart.nullable(core.Object)))();
  var ObjectNAndObjectNToObjectN = () => (ObjectNAndObjectNToObjectN = dart.constFn(dart.fnType(ObjectN(), [ObjectN(), ObjectN()])))();
  var ObjectNAndObjectNToNObjectN = () => (ObjectNAndObjectNToNObjectN = dart.constFn(dart.nullable(ObjectNAndObjectNToObjectN())))();
  var String__Todynamic = () => (String__Todynamic = dart.constFn(dart.fnType(dart.dynamic, [core.String], {reviver: ObjectNAndObjectNToNObjectN()}, {})))();
  var StreamSinkTransformerOfObjectL$StringL = () => (StreamSinkTransformerOfObjectL$StringL = dart.constFn(stream_sink_transformer.StreamSinkTransformer$(ObjectL(), StringL())))();
  var EventSinkOfStringL = () => (EventSinkOfStringL = dart.constFn(async.EventSink$(StringL())))();
  var EventSinkLOfStringL = () => (EventSinkLOfStringL = dart.constFn(dart.legacy(EventSinkOfStringL())))();
  var ObjectLAndEventSinkLOfStringLToNullN = () => (ObjectLAndEventSinkLOfStringLToNullN = dart.constFn(dart.fnType(core.Null, [ObjectL(), EventSinkLOfStringL()])))();
  var StreamChannelOfObjectL = () => (StreamChannelOfObjectL = dart.constFn(stream_channel.StreamChannel$(ObjectL())))();
  var StreamSinkTransformerLOfObjectL$StringL = () => (StreamSinkTransformerLOfObjectL$StringL = dart.constFn(dart.legacy(StreamSinkTransformerOfObjectL$StringL())))();
  var StreamTransformerOfStringL$ObjectL = () => (StreamTransformerOfStringL$ObjectL = dart.constFn(async.StreamTransformer$(StringL(), ObjectL())))();
  var StreamTransformerLOfStringL$ObjectL = () => (StreamTransformerLOfStringL$ObjectL = dart.constFn(dart.legacy(StreamTransformerOfStringL$ObjectL())))();
  var FutureLOfvoid = () => (FutureLOfvoid = dart.constFn(dart.legacy(FutureOfvoid())))();
  var ListOfvoid = () => (ListOfvoid = dart.constFn(core.List$(dart.void)))();
  var ListLOfvoid = () => (ListLOfvoid = dart.constFn(dart.legacy(ListOfvoid())))();
  var FutureOfListLOfvoid = () => (FutureOfListLOfvoid = dart.constFn(async.Future$(ListLOfvoid())))();
  var FutureLOfListLOfvoid = () => (FutureLOfListLOfvoid = dart.constFn(dart.legacy(FutureOfListLOfvoid())))();
  var VoidToFutureLOfListLOfvoid = () => (VoidToFutureLOfListLOfvoid = dart.constFn(dart.fnType(FutureLOfListLOfvoid(), [])))();
  const CT = Object.create(null);
  var L3 = "package:stream_channel/src/stream_channel_controller.dart";
  var L2 = "package:stream_channel/src/stream_channel_transformer.dart";
  var L5 = "package:stream_channel/src/multi_channel.dart";
  var L7 = "package:stream_channel/src/disconnector.dart";
  var L9 = "package:stream_channel/src/close_guarantee_channel.dart";
  var L4 = "package:stream_channel/src/stream_channel_completer.dart";
  var L0 = "package:stream_channel/stream_channel.dart";
  var L1 = "package:stream_channel/src/guarantee_channel.dart";
  var L6 = "package:stream_channel/src/json_document_transformer.dart";
  var L8 = "package:stream_channel/src/delegating_stream_channel.dart";
  dart.defineLazy(CT, {
    get C0() {
      return C0 = dart.fn(convert.jsonDecode, String__Todynamic());
    },
    get C1() {
      return C1 = dart.const(new _js_helper.PrivateSymbol.new('_sinkTransformer', _sinkTransformer$0));
    },
    get C2() {
      return C2 = dart.const(new _js_helper.PrivateSymbol.new('_streamTransformer', _streamTransformer$0));
    },
    get C3() {
      return C3 = dart.const({
        __proto__: json_document_transformer._JsonDocument.prototype
      });
    },
    get C4() {
      return C4 = dart.const(new _js_helper.PrivateSymbol.new('_sinkTransformer', _sinkTransformer$1));
    },
    get C5() {
      return C5 = dart.const(new _js_helper.PrivateSymbol.new('_streamTransformer', _streamTransformer$1));
    }
  }, false);
  var _sink = dart.privateName(guarantee_channel, "_sink");
  var _streamController = dart.privateName(guarantee_channel, "_streamController");
  var _subscription = dart.privateName(guarantee_channel, "_subscription");
  var _disconnected = dart.privateName(guarantee_channel, "_disconnected");
  var _onStreamDisconnected = dart.privateName(guarantee_channel, "_onStreamDisconnected");
  var _onSinkDisconnected = dart.privateName(guarantee_channel, "_onSinkDisconnected");
  const _is_StreamChannelMixin_default = Symbol('_is_StreamChannelMixin_default');
  stream_channel.StreamChannelMixin$ = dart.generic(T => {
    var TL = () => (TL = dart.constFn(dart.legacy(T)))();
    var StreamChannelOfTL = () => (StreamChannelOfTL = dart.constFn(stream_channel.StreamChannel$(TL())))();
    var StreamChannelLOfTL = () => (StreamChannelLOfTL = dart.constFn(dart.legacy(StreamChannelOfTL())))();
    var StreamTransformerOfTL$TL = () => (StreamTransformerOfTL$TL = dart.constFn(async.StreamTransformer$(TL(), TL())))();
    var StreamTransformerLOfTL$TL = () => (StreamTransformerLOfTL$TL = dart.constFn(dart.legacy(StreamTransformerOfTL$TL())))();
    var StreamSinkTransformerOfTL$TL = () => (StreamSinkTransformerOfTL$TL = dart.constFn(stream_sink_transformer.StreamSinkTransformer$(TL(), TL())))();
    var StreamSinkTransformerLOfTL$TL = () => (StreamSinkTransformerLOfTL$TL = dart.constFn(dart.legacy(StreamSinkTransformerOfTL$TL())))();
    var StreamOfTL = () => (StreamOfTL = dart.constFn(async.Stream$(TL())))();
    var StreamLOfTL = () => (StreamLOfTL = dart.constFn(dart.legacy(StreamOfTL())))();
    var StreamLOfTLToStreamLOfTL = () => (StreamLOfTLToStreamLOfTL = dart.constFn(dart.fnType(StreamLOfTL(), [StreamLOfTL()])))();
    var StreamLOfTLToLStreamLOfTL = () => (StreamLOfTLToLStreamLOfTL = dart.constFn(dart.legacy(StreamLOfTLToStreamLOfTL())))();
    var StreamSinkOfTL = () => (StreamSinkOfTL = dart.constFn(async.StreamSink$(TL())))();
    var StreamSinkLOfTL = () => (StreamSinkLOfTL = dart.constFn(dart.legacy(StreamSinkOfTL())))();
    var StreamSinkLOfTLToStreamSinkLOfTL = () => (StreamSinkLOfTLToStreamSinkLOfTL = dart.constFn(dart.fnType(StreamSinkLOfTL(), [StreamSinkLOfTL()])))();
    var StreamSinkLOfTLToLStreamSinkLOfTL = () => (StreamSinkLOfTLToLStreamSinkLOfTL = dart.constFn(dart.legacy(StreamSinkLOfTLToStreamSinkLOfTL())))();
    class StreamChannelMixin extends core.Object {
      pipe(other) {
        StreamChannelLOfTL().as(other);
        this.stream.pipe(other.sink);
        other.stream.pipe(this.sink);
      }
      transform(S, transformer) {
        dart.legacy(stream_channel_transformer.StreamChannelTransformer$(dart.legacy(S), TL())).as(transformer);
        return transformer.bind(this);
      }
      transformStream(transformer) {
        StreamTransformerLOfTL$TL().as(transformer);
        return this.changeStream(dart.bind(transformer, 'bind'));
      }
      transformSink(transformer) {
        StreamSinkTransformerLOfTL$TL().as(transformer);
        return this.changeSink(dart.bind(transformer, 'bind'));
      }
      changeStream(change) {
        StreamLOfTLToLStreamLOfTL().as(change);
        return StreamChannelOfTL().withCloseGuarantee(change(this.stream), this.sink);
      }
      changeSink(change) {
        StreamSinkLOfTLToLStreamSinkLOfTL().as(change);
        return StreamChannelOfTL().withCloseGuarantee(this.stream, change(this.sink));
      }
      cast(S) {
        return stream_channel.StreamChannel$(dart.legacy(S)).new(stream.DelegatingStream.typed(dart.legacy(S), this.stream), stream_sink.DelegatingStreamSink.typed(dart.legacy(S), this.sink));
      }
    }
    (StreamChannelMixin.new = function() {
      ;
    }).prototype = StreamChannelMixin.prototype;
    dart.addTypeTests(StreamChannelMixin);
    StreamChannelMixin.prototype[_is_StreamChannelMixin_default] = true;
    dart.addTypeCaches(StreamChannelMixin);
    StreamChannelMixin[dart.implements] = () => [stream_channel.StreamChannel$(dart.legacy(T))];
    dart.setMethodSignature(StreamChannelMixin, () => ({
      __proto__: dart.getMethods(StreamChannelMixin.__proto__),
      pipe: dart.fnType(dart.void, [dart.legacy(core.Object)]),
      transform: dart.gFnType(S => [dart.legacy(stream_channel.StreamChannel$(dart.legacy(S))), [dart.legacy(core.Object)]]),
      transformStream: dart.fnType(dart.legacy(stream_channel.StreamChannel$(dart.legacy(T))), [dart.legacy(core.Object)]),
      transformSink: dart.fnType(dart.legacy(stream_channel.StreamChannel$(dart.legacy(T))), [dart.legacy(core.Object)]),
      changeStream: dart.fnType(dart.legacy(stream_channel.StreamChannel$(dart.legacy(T))), [dart.legacy(core.Object)]),
      changeSink: dart.fnType(dart.legacy(stream_channel.StreamChannel$(dart.legacy(T))), [dart.legacy(core.Object)]),
      cast: dart.gFnType(S => [dart.legacy(stream_channel.StreamChannel$(dart.legacy(S))), []])
    }));
    dart.setLibraryUri(StreamChannelMixin, L0);
    return StreamChannelMixin;
  });
  stream_channel.StreamChannelMixin = stream_channel.StreamChannelMixin$();
  dart.addTypeTests(stream_channel.StreamChannelMixin, _is_StreamChannelMixin_default);
  const _is_GuaranteeChannel_default = Symbol('_is_GuaranteeChannel_default');
  guarantee_channel.GuaranteeChannel$ = dart.generic(T => {
    var TL = () => (TL = dart.constFn(dart.legacy(T)))();
    var _GuaranteeSinkOfTL = () => (_GuaranteeSinkOfTL = dart.constFn(guarantee_channel._GuaranteeSink$(TL())))();
    var SingleSubscriptionTransformerOfTL$TL = () => (SingleSubscriptionTransformerOfTL$TL = dart.constFn(single_subscription_transformer.SingleSubscriptionTransformer$(TL(), TL())))();
    var StreamControllerOfTL = () => (StreamControllerOfTL = dart.constFn(async.StreamController$(TL())))();
    class GuaranteeChannel extends stream_channel.StreamChannelMixin$(dart.legacy(T)) {
      get stream() {
        return this[_streamController].stream;
      }
      get sink() {
        return this[_sink];
      }
      [_onSinkDisconnected]() {
        this[_disconnected] = true;
        if (this[_subscription] != null) this[_subscription].cancel();
        this[_streamController].close();
      }
    }
    (GuaranteeChannel.new = function(innerStream, innerSink, opts) {
      let allowSinkErrors = opts && 'allowSinkErrors' in opts ? opts.allowSinkErrors : true;
      this[_sink] = null;
      this[_streamController] = null;
      this[_subscription] = null;
      this[_disconnected] = false;
      this[_sink] = new (_GuaranteeSinkOfTL()).new(innerSink, this, {allowErrors: allowSinkErrors});
      if (dart.test(innerStream.isBroadcast)) {
        innerStream = innerStream.transform(TL(), new (SingleSubscriptionTransformerOfTL$TL()).new());
      }
      this[_streamController] = StreamControllerOfTL().new({onListen: dart.fn(() => {
          if (dart.test(this[_disconnected])) return;
          this[_subscription] = innerStream.listen(dart.bind(this[_streamController], 'add'), {onError: dart.bind(this[_streamController], 'addError'), onDone: dart.fn(() => {
              this[_sink][_onStreamDisconnected]();
              this[_streamController].close();
            }, VoidToNullN())});
        }, VoidToNullN()), sync: true});
    }).prototype = GuaranteeChannel.prototype;
    dart.addTypeTests(GuaranteeChannel);
    GuaranteeChannel.prototype[_is_GuaranteeChannel_default] = true;
    dart.addTypeCaches(GuaranteeChannel);
    dart.setMethodSignature(GuaranteeChannel, () => ({
      __proto__: dart.getMethods(GuaranteeChannel.__proto__),
      [_onSinkDisconnected]: dart.fnType(dart.void, [])
    }));
    dart.setGetterSignature(GuaranteeChannel, () => ({
      __proto__: dart.getGetters(GuaranteeChannel.__proto__),
      stream: dart.legacy(async.Stream$(dart.legacy(T))),
      sink: dart.legacy(async.StreamSink$(dart.legacy(T)))
    }));
    dart.setLibraryUri(GuaranteeChannel, L1);
    dart.setFieldSignature(GuaranteeChannel, () => ({
      __proto__: dart.getFields(GuaranteeChannel.__proto__),
      [_sink]: dart.fieldType(dart.legacy(guarantee_channel._GuaranteeSink$(dart.legacy(T)))),
      [_streamController]: dart.fieldType(dart.legacy(async.StreamController$(dart.legacy(T)))),
      [_subscription]: dart.fieldType(dart.legacy(async.StreamSubscription$(dart.legacy(T)))),
      [_disconnected]: dart.fieldType(dart.legacy(core.bool))
    }));
    return GuaranteeChannel;
  });
  guarantee_channel.GuaranteeChannel = guarantee_channel.GuaranteeChannel$();
  dart.addTypeTests(guarantee_channel.GuaranteeChannel, _is_GuaranteeChannel_default);
  var _doneCompleter = dart.privateName(guarantee_channel, "_doneCompleter");
  var _closed = dart.privateName(guarantee_channel, "_closed");
  var _addStreamSubscription = dart.privateName(guarantee_channel, "_addStreamSubscription");
  var _addStreamCompleter = dart.privateName(guarantee_channel, "_addStreamCompleter");
  var _inner$ = dart.privateName(guarantee_channel, "_inner");
  var _channel$ = dart.privateName(guarantee_channel, "_channel");
  var _allowErrors = dart.privateName(guarantee_channel, "_allowErrors");
  var _inAddStream = dart.privateName(guarantee_channel, "_inAddStream");
  var _addError = dart.privateName(guarantee_channel, "_addError");
  const _is__GuaranteeSink_default = Symbol('_is__GuaranteeSink_default');
  guarantee_channel._GuaranteeSink$ = dart.generic(T => {
    var TL = () => (TL = dart.constFn(dart.legacy(T)))();
    var StreamOfTL = () => (StreamOfTL = dart.constFn(async.Stream$(TL())))();
    var StreamLOfTL = () => (StreamLOfTL = dart.constFn(dart.legacy(StreamOfTL())))();
    class _GuaranteeSink extends core.Object {
      get done() {
        return this[_doneCompleter].future;
      }
      get [_inAddStream]() {
        return this[_addStreamSubscription] != null;
      }
      add(data) {
        TL().as(data);
        if (dart.test(this[_closed])) dart.throw(new core.StateError.new("Cannot add event after closing."));
        if (dart.test(this[_inAddStream])) {
          dart.throw(new core.StateError.new("Cannot add event while adding stream."));
        }
        if (dart.test(this[_disconnected])) return;
        this[_inner$].add(data);
      }
      addError(error, stackTrace = null) {
        if (dart.test(this[_closed])) dart.throw(new core.StateError.new("Cannot add event after closing."));
        if (dart.test(this[_inAddStream])) {
          dart.throw(new core.StateError.new("Cannot add event while adding stream."));
        }
        if (dart.test(this[_disconnected])) return;
        this[_addError](error, stackTrace);
      }
      [_addError](error, stackTrace = null) {
        if (dart.test(this[_allowErrors])) {
          this[_inner$].addError(error, stackTrace);
          return;
        }
        this[_doneCompleter].completeError(error, stackTrace);
        this[_onStreamDisconnected]();
        this[_channel$][_onSinkDisconnected]();
        this[_inner$].close().catchError(dart.fn(_ => {
        }, dynamicToNullN()));
      }
      addStream(stream) {
        StreamLOfTL().as(stream);
        if (dart.test(this[_closed])) dart.throw(new core.StateError.new("Cannot add stream after closing."));
        if (dart.test(this[_inAddStream])) {
          dart.throw(new core.StateError.new("Cannot add stream while adding stream."));
        }
        if (dart.test(this[_disconnected])) return FutureOfvoid().value();
        this[_addStreamCompleter] = async.Completer.sync();
        this[_addStreamSubscription] = stream.listen(dart.bind(this[_inner$], 'add'), {onError: dart.bind(this, _addError), onDone: dart.bind(this[_addStreamCompleter], 'complete')});
        return this[_addStreamCompleter].future.then(dart.void, dart.fn(_ => {
          this[_addStreamCompleter] = null;
          this[_addStreamSubscription] = null;
        }, dynamicToNullN()));
      }
      close() {
        if (dart.test(this[_inAddStream])) {
          dart.throw(new core.StateError.new("Cannot close sink while adding stream."));
        }
        if (dart.test(this[_closed])) return this.done;
        this[_closed] = true;
        if (!dart.test(this[_disconnected])) {
          this[_channel$][_onSinkDisconnected]();
          this[_doneCompleter].complete(this[_inner$].close());
        }
        return this.done;
      }
      [_onStreamDisconnected]() {
        this[_disconnected] = true;
        if (!dart.test(this[_doneCompleter].isCompleted)) this[_doneCompleter].complete();
        if (!dart.test(this[_inAddStream])) return;
        this[_addStreamCompleter].complete(this[_addStreamSubscription].cancel());
        this[_addStreamCompleter] = null;
        this[_addStreamSubscription] = null;
      }
    }
    (_GuaranteeSink.new = function(_inner, _channel, opts) {
      let allowErrors = opts && 'allowErrors' in opts ? opts.allowErrors : true;
      this[_doneCompleter] = async.Completer.new();
      this[_disconnected] = false;
      this[_closed] = false;
      this[_addStreamSubscription] = null;
      this[_addStreamCompleter] = null;
      this[_inner$] = _inner;
      this[_channel$] = _channel;
      this[_allowErrors] = allowErrors;
      ;
    }).prototype = _GuaranteeSink.prototype;
    dart.addTypeTests(_GuaranteeSink);
    _GuaranteeSink.prototype[_is__GuaranteeSink_default] = true;
    dart.addTypeCaches(_GuaranteeSink);
    _GuaranteeSink[dart.implements] = () => [async.StreamSink$(dart.legacy(T))];
    dart.setMethodSignature(_GuaranteeSink, () => ({
      __proto__: dart.getMethods(_GuaranteeSink.__proto__),
      add: dart.fnType(dart.void, [dart.legacy(core.Object)]),
      addError: dart.fnType(dart.void, [dart.legacy(core.Object)], [dart.legacy(core.StackTrace)]),
      [_addError]: dart.fnType(dart.void, [dart.dynamic], [dart.legacy(core.StackTrace)]),
      addStream: dart.fnType(dart.legacy(async.Future$(dart.void)), [dart.legacy(core.Object)]),
      close: dart.fnType(dart.legacy(async.Future$(dart.void)), []),
      [_onStreamDisconnected]: dart.fnType(dart.void, [])
    }));
    dart.setGetterSignature(_GuaranteeSink, () => ({
      __proto__: dart.getGetters(_GuaranteeSink.__proto__),
      done: dart.legacy(async.Future$(dart.void)),
      [_inAddStream]: dart.legacy(core.bool)
    }));
    dart.setLibraryUri(_GuaranteeSink, L1);
    dart.setFieldSignature(_GuaranteeSink, () => ({
      __proto__: dart.getFields(_GuaranteeSink.__proto__),
      [_inner$]: dart.finalFieldType(dart.legacy(async.StreamSink$(dart.legacy(T)))),
      [_channel$]: dart.finalFieldType(dart.legacy(guarantee_channel.GuaranteeChannel$(dart.legacy(T)))),
      [_doneCompleter]: dart.finalFieldType(dart.legacy(async.Completer)),
      [_disconnected]: dart.fieldType(dart.legacy(core.bool)),
      [_closed]: dart.fieldType(dart.legacy(core.bool)),
      [_addStreamSubscription]: dart.fieldType(dart.legacy(async.StreamSubscription$(dart.legacy(T)))),
      [_addStreamCompleter]: dart.fieldType(dart.legacy(async.Completer)),
      [_allowErrors]: dart.finalFieldType(dart.legacy(core.bool))
    }));
    return _GuaranteeSink;
  });
  guarantee_channel._GuaranteeSink = guarantee_channel._GuaranteeSink$();
  dart.addTypeTests(guarantee_channel._GuaranteeSink, _is__GuaranteeSink_default);
  const _is_StreamChannel_default = Symbol('_is_StreamChannel_default');
  stream_channel.StreamChannel$ = dart.generic(T => {
    class StreamChannel extends core.Object {
      static new(stream, sink) {
        return new (stream_channel._StreamChannel$(dart.legacy(T))).new(stream, sink);
      }
      static withGuarantees(stream, sink, opts) {
        let allowSinkErrors = opts && 'allowSinkErrors' in opts ? opts.allowSinkErrors : true;
        return new (guarantee_channel.GuaranteeChannel$(dart.legacy(T))).new(stream, sink, {allowSinkErrors: allowSinkErrors});
      }
      static withCloseGuarantee(stream, sink) {
        return new (close_guarantee_channel.CloseGuaranteeChannel$(dart.legacy(T))).new(stream, sink);
      }
    }
    (StreamChannel[dart.mixinNew] = function() {
    }).prototype = StreamChannel.prototype;
    dart.addTypeTests(StreamChannel);
    StreamChannel.prototype[_is_StreamChannel_default] = true;
    dart.addTypeCaches(StreamChannel);
    dart.setLibraryUri(StreamChannel, L0);
    return StreamChannel;
  });
  stream_channel.StreamChannel = stream_channel.StreamChannel$();
  dart.addTypeTests(stream_channel.StreamChannel, _is_StreamChannel_default);
  const _is__StreamChannel_default = Symbol('_is__StreamChannel_default');
  stream_channel._StreamChannel$ = dart.generic(T => {
    class _StreamChannel extends stream_channel.StreamChannelMixin$(dart.legacy(T)) {}
    (_StreamChannel.new = function(stream, sink) {
      this.stream = stream;
      this.sink = sink;
      ;
    }).prototype = _StreamChannel.prototype;
    dart.addTypeTests(_StreamChannel);
    _StreamChannel.prototype[_is__StreamChannel_default] = true;
    dart.addTypeCaches(_StreamChannel);
    dart.setLibraryUri(_StreamChannel, L0);
    dart.setFieldSignature(_StreamChannel, () => ({
      __proto__: dart.getFields(_StreamChannel.__proto__),
      stream: dart.finalFieldType(dart.legacy(async.Stream$(dart.legacy(T)))),
      sink: dart.finalFieldType(dart.legacy(async.StreamSink$(dart.legacy(T))))
    }));
    return _StreamChannel;
  });
  stream_channel._StreamChannel = stream_channel._StreamChannel$();
  dart.addTypeTests(stream_channel._StreamChannel, _is__StreamChannel_default);
  var _streamTransformer = dart.privateName(stream_channel_transformer, "_streamTransformer");
  var _sinkTransformer = dart.privateName(stream_channel_transformer, "_sinkTransformer");
  const _is_StreamChannelTransformer_default = Symbol('_is_StreamChannelTransformer_default');
  var _streamTransformer$ = dart.privateName(stream_channel_transformer, "StreamChannelTransformer._streamTransformer");
  var _sinkTransformer$ = dart.privateName(stream_channel_transformer, "StreamChannelTransformer._sinkTransformer");
  stream_channel_transformer.StreamChannelTransformer$ = dart.generic((S, T) => {
    var SL = () => (SL = dart.constFn(dart.legacy(S)))();
    var StreamTransformerWrapperOfSL$TL = () => (StreamTransformerWrapperOfSL$TL = dart.constFn(stream_transformer_wrapper.StreamTransformerWrapper$(SL(), TL())))();
    var StreamChannelOfSL = () => (StreamChannelOfSL = dart.constFn(stream_channel.StreamChannel$(SL())))();
    var TL = () => (TL = dart.constFn(dart.legacy(T)))();
    var StreamChannelOfTL = () => (StreamChannelOfTL = dart.constFn(stream_channel.StreamChannel$(TL())))();
    var StreamChannelLOfTL = () => (StreamChannelLOfTL = dart.constFn(dart.legacy(StreamChannelOfTL())))();
    class StreamChannelTransformer extends core.Object {
      get [_streamTransformer]() {
        return this[_streamTransformer$];
      }
      set [_streamTransformer](value) {
        super[_streamTransformer] = value;
      }
      get [_sinkTransformer]() {
        return this[_sinkTransformer$];
      }
      set [_sinkTransformer](value) {
        super[_sinkTransformer] = value;
      }
      bind(channel) {
        StreamChannelLOfTL().as(channel);
        return StreamChannelOfSL().withCloseGuarantee(channel.stream.transform(SL(), this[_streamTransformer]), this[_sinkTransformer].bind(channel.sink));
      }
    }
    (StreamChannelTransformer.new = function(_streamTransformer, _sinkTransformer) {
      this[_streamTransformer$] = _streamTransformer;
      this[_sinkTransformer$] = _sinkTransformer;
      ;
    }).prototype = StreamChannelTransformer.prototype;
    (StreamChannelTransformer.fromCodec = function(codec) {
      StreamChannelTransformer.new.call(this, codec.decoder, new (StreamTransformerWrapperOfSL$TL()).new(codec.encoder));
    }).prototype = StreamChannelTransformer.prototype;
    dart.addTypeTests(StreamChannelTransformer);
    StreamChannelTransformer.prototype[_is_StreamChannelTransformer_default] = true;
    dart.addTypeCaches(StreamChannelTransformer);
    dart.setMethodSignature(StreamChannelTransformer, () => ({
      __proto__: dart.getMethods(StreamChannelTransformer.__proto__),
      bind: dart.fnType(dart.legacy(stream_channel.StreamChannel$(dart.legacy(S))), [dart.legacy(core.Object)])
    }));
    dart.setLibraryUri(StreamChannelTransformer, L2);
    dart.setFieldSignature(StreamChannelTransformer, () => ({
      __proto__: dart.getFields(StreamChannelTransformer.__proto__),
      [_streamTransformer]: dart.finalFieldType(dart.legacy(async.StreamTransformer$(dart.legacy(T), dart.legacy(S)))),
      [_sinkTransformer]: dart.finalFieldType(dart.legacy(stream_sink_transformer.StreamSinkTransformer$(dart.legacy(S), dart.legacy(T))))
    }));
    return StreamChannelTransformer;
  });
  stream_channel_transformer.StreamChannelTransformer = stream_channel_transformer.StreamChannelTransformer$();
  dart.addTypeTests(stream_channel_transformer.StreamChannelTransformer, _is_StreamChannelTransformer_default);
  var _local = dart.privateName(stream_channel_controller, "_local");
  var _foreign = dart.privateName(stream_channel_controller, "_foreign");
  const _is_StreamChannelController_default = Symbol('_is_StreamChannelController_default');
  stream_channel_controller.StreamChannelController$ = dart.generic(T => {
    var TL = () => (TL = dart.constFn(dart.legacy(T)))();
    var StreamControllerOfTL = () => (StreamControllerOfTL = dart.constFn(async.StreamController$(TL())))();
    var StreamChannelOfTL = () => (StreamChannelOfTL = dart.constFn(stream_channel.StreamChannel$(TL())))();
    class StreamChannelController extends core.Object {
      get local() {
        return this[_local];
      }
      get foreign() {
        return this[_foreign];
      }
    }
    (StreamChannelController.new = function(opts) {
      let allowForeignErrors = opts && 'allowForeignErrors' in opts ? opts.allowForeignErrors : true;
      let sync = opts && 'sync' in opts ? opts.sync : false;
      this[_local] = null;
      this[_foreign] = null;
      let localToForeignController = StreamControllerOfTL().new({sync: sync});
      let foreignToLocalController = StreamControllerOfTL().new({sync: sync});
      this[_local] = StreamChannelOfTL().withGuarantees(foreignToLocalController.stream, localToForeignController.sink);
      this[_foreign] = StreamChannelOfTL().withGuarantees(localToForeignController.stream, foreignToLocalController.sink, {allowSinkErrors: allowForeignErrors});
    }).prototype = StreamChannelController.prototype;
    dart.addTypeTests(StreamChannelController);
    StreamChannelController.prototype[_is_StreamChannelController_default] = true;
    dart.addTypeCaches(StreamChannelController);
    dart.setGetterSignature(StreamChannelController, () => ({
      __proto__: dart.getGetters(StreamChannelController.__proto__),
      local: dart.legacy(stream_channel.StreamChannel$(dart.legacy(T))),
      foreign: dart.legacy(stream_channel.StreamChannel$(dart.legacy(T)))
    }));
    dart.setLibraryUri(StreamChannelController, L3);
    dart.setFieldSignature(StreamChannelController, () => ({
      __proto__: dart.getFields(StreamChannelController.__proto__),
      [_local]: dart.fieldType(dart.legacy(stream_channel.StreamChannel$(dart.legacy(T)))),
      [_foreign]: dart.fieldType(dart.legacy(stream_channel.StreamChannel$(dart.legacy(T))))
    }));
    return StreamChannelController;
  });
  stream_channel_controller.StreamChannelController = stream_channel_controller.StreamChannelController$();
  dart.addTypeTests(stream_channel_controller.StreamChannelController, _is_StreamChannelController_default);
  var _streamCompleter = dart.privateName(stream_channel_completer, "_streamCompleter");
  var _sinkCompleter = dart.privateName(stream_channel_completer, "_sinkCompleter");
  var _channel = dart.privateName(stream_channel_completer, "_channel");
  var _set = dart.privateName(stream_channel_completer, "_set");
  const _is_StreamChannelCompleter_default = Symbol('_is_StreamChannelCompleter_default');
  stream_channel_completer.StreamChannelCompleter$ = dart.generic(T => {
    var TL = () => (TL = dart.constFn(dart.legacy(T)))();
    var StreamCompleterOfTL = () => (StreamCompleterOfTL = dart.constFn(stream_completer.StreamCompleter$(TL())))();
    var StreamSinkCompleterOfTL = () => (StreamSinkCompleterOfTL = dart.constFn(stream_sink_completer.StreamSinkCompleter$(TL())))();
    var StreamChannelOfTL = () => (StreamChannelOfTL = dart.constFn(stream_channel.StreamChannel$(TL())))();
    var StreamChannelLOfTL = () => (StreamChannelLOfTL = dart.constFn(dart.legacy(StreamChannelOfTL())))();
    var NullStreamSinkOfTL = () => (NullStreamSinkOfTL = dart.constFn(null_stream_sink.NullStreamSink$(TL())))();
    class StreamChannelCompleter extends core.Object {
      get channel() {
        return this[_channel];
      }
      static fromFuture(channelFuture) {
        let completer = new stream_channel_completer.StreamChannelCompleter.new();
        channelFuture.then(dart.void, dart.bind(completer, 'setChannel'), {onError: dart.bind(completer, 'setError')});
        return completer.channel;
      }
      setChannel(channel) {
        StreamChannelLOfTL().as(channel);
        if (dart.test(this[_set])) dart.throw(new core.StateError.new("The channel has already been set."));
        this[_set] = true;
        this[_streamCompleter].setSourceStream(channel.stream);
        this[_sinkCompleter].setDestinationSink(channel.sink);
      }
      setError(error, stackTrace = null) {
        if (dart.test(this[_set])) dart.throw(new core.StateError.new("The channel has already been set."));
        this[_set] = true;
        this[_streamCompleter].setError(error, stackTrace);
        this[_sinkCompleter].setDestinationSink(new (NullStreamSinkOfTL()).new());
      }
    }
    (StreamChannelCompleter.new = function() {
      this[_streamCompleter] = new (StreamCompleterOfTL()).new();
      this[_sinkCompleter] = new (StreamSinkCompleterOfTL()).new();
      this[_channel] = null;
      this[_set] = false;
      this[_channel] = StreamChannelOfTL().new(this[_streamCompleter].stream, this[_sinkCompleter].sink);
    }).prototype = StreamChannelCompleter.prototype;
    dart.addTypeTests(StreamChannelCompleter);
    StreamChannelCompleter.prototype[_is_StreamChannelCompleter_default] = true;
    dart.addTypeCaches(StreamChannelCompleter);
    dart.setMethodSignature(StreamChannelCompleter, () => ({
      __proto__: dart.getMethods(StreamChannelCompleter.__proto__),
      setChannel: dart.fnType(dart.void, [dart.legacy(core.Object)]),
      setError: dart.fnType(dart.void, [dart.dynamic], [dart.legacy(core.StackTrace)])
    }));
    dart.setGetterSignature(StreamChannelCompleter, () => ({
      __proto__: dart.getGetters(StreamChannelCompleter.__proto__),
      channel: dart.legacy(stream_channel.StreamChannel$(dart.legacy(T)))
    }));
    dart.setLibraryUri(StreamChannelCompleter, L4);
    dart.setFieldSignature(StreamChannelCompleter, () => ({
      __proto__: dart.getFields(StreamChannelCompleter.__proto__),
      [_streamCompleter]: dart.finalFieldType(dart.legacy(stream_completer.StreamCompleter$(dart.legacy(T)))),
      [_sinkCompleter]: dart.finalFieldType(dart.legacy(stream_sink_completer.StreamSinkCompleter$(dart.legacy(T)))),
      [_channel]: dart.fieldType(dart.legacy(stream_channel.StreamChannel$(dart.legacy(T)))),
      [_set]: dart.fieldType(dart.legacy(core.bool))
    }));
    return StreamChannelCompleter;
  });
  stream_channel_completer.StreamChannelCompleter = stream_channel_completer.StreamChannelCompleter$();
  dart.addTypeTests(stream_channel_completer.StreamChannelCompleter, _is_StreamChannelCompleter_default);
  const _is_MultiChannel_default = Symbol('_is_MultiChannel_default');
  multi_channel.MultiChannel$ = dart.generic(T => {
    class MultiChannel extends core.Object {
      static new(inner) {
        return new (multi_channel._MultiChannel$(dart.legacy(T))).new(inner);
      }
    }
    (MultiChannel[dart.mixinNew] = function() {
    }).prototype = MultiChannel.prototype;
    dart.addTypeTests(MultiChannel);
    MultiChannel.prototype[_is_MultiChannel_default] = true;
    dart.addTypeCaches(MultiChannel);
    MultiChannel[dart.implements] = () => [stream_channel.StreamChannel$(dart.legacy(T))];
    dart.setLibraryUri(MultiChannel, L5);
    return MultiChannel;
  });
  multi_channel.MultiChannel = multi_channel.MultiChannel$();
  dart.addTypeTests(multi_channel.MultiChannel, _is_MultiChannel_default);
  var _innerStreamSubscription = dart.privateName(multi_channel, "_innerStreamSubscription");
  var _mainController = dart.privateName(multi_channel, "_mainController");
  var _controllers = dart.privateName(multi_channel, "_controllers");
  var _pendingIds = dart.privateName(multi_channel, "_pendingIds");
  var _closedIds = dart.privateName(multi_channel, "_closedIds");
  var _nextId = dart.privateName(multi_channel, "_nextId");
  var _inner$0 = dart.privateName(multi_channel, "_inner");
  var _closeChannel = dart.privateName(multi_channel, "_closeChannel");
  var _closeInnerChannel = dart.privateName(multi_channel, "_closeInnerChannel");
  const _is__MultiChannel_default = Symbol('_is__MultiChannel_default');
  multi_channel._MultiChannel$ = dart.generic(T => {
    var TL = () => (TL = dart.constFn(dart.legacy(T)))();
    var StreamChannelControllerOfTL = () => (StreamChannelControllerOfTL = dart.constFn(stream_channel_controller.StreamChannelController$(TL())))();
    var StreamChannelControllerLOfTL = () => (StreamChannelControllerLOfTL = dart.constFn(dart.legacy(StreamChannelControllerOfTL())))();
    var IdentityMapOfintL$StreamChannelControllerLOfTL = () => (IdentityMapOfintL$StreamChannelControllerLOfTL = dart.constFn(_js_helper.IdentityMap$(intL(), StreamChannelControllerLOfTL())))();
    var TLTovoid = () => (TLTovoid = dart.constFn(dart.fnType(dart.void, [TL()])))();
    var VoidToStreamChannelControllerLOfTL = () => (VoidToStreamChannelControllerLOfTL = dart.constFn(dart.fnType(StreamChannelControllerLOfTL(), [])))();
    var VirtualChannelOfTL = () => (VirtualChannelOfTL = dart.constFn(multi_channel.VirtualChannel$(TL())))();
    var _EmptyStreamOfTL = () => (_EmptyStreamOfTL = dart.constFn(async._EmptyStream$(TL())))();
    var NullStreamSinkOfTL = () => (NullStreamSinkOfTL = dart.constFn(null_stream_sink.NullStreamSink$(TL())))();
    class _MultiChannel extends stream_channel.StreamChannelMixin$(dart.legacy(T)) {
      get stream() {
        return this[_mainController].foreign.stream;
      }
      get sink() {
        return this[_mainController].foreign.sink;
      }
      virtualChannel(id = null) {
        let inputId = null;
        let outputId = null;
        if (id != null) {
          inputId = id;
          outputId = dart.notNull(id) + 1;
        } else {
          inputId = dart.notNull(this[_nextId]) + 1;
          outputId = this[_nextId];
          this[_nextId] = dart.notNull(this[_nextId]) + 2;
        }
        if (this[_inner$0] == null) {
          return new (VirtualChannelOfTL()).__(this, inputId, new (_EmptyStreamOfTL()).new(), new (NullStreamSinkOfTL()).new());
        }
        let controller = null;
        if (dart.test(this[_pendingIds].remove(inputId))) {
          controller = this[_controllers][$_get](inputId);
        } else if (dart.test(this[_controllers][$containsKey](inputId)) || dart.test(this[_closedIds].contains(inputId))) {
          dart.throw(new core.ArgumentError.new("A virtual channel with id " + dart.str(id) + " already exists."));
        } else {
          controller = new (StreamChannelControllerOfTL()).new({sync: true});
          this[_controllers][$_set](inputId, controller);
        }
        controller.local.stream.listen(dart.fn(message => this[_inner$0].sink.add(JSArrayOfObjectL().of([outputId, message])), TLTovoid()), {onDone: dart.fn(() => this[_closeChannel](inputId, outputId), VoidTovoid())});
        return new (VirtualChannelOfTL()).__(this, outputId, controller.foreign.stream, controller.foreign.sink);
      }
      [_closeChannel](inputId, outputId) {
        this[_closedIds].add(inputId);
        let controller = this[_controllers][$remove](inputId);
        controller.local.sink.close();
        if (this[_inner$0] == null) return;
        this[_inner$0].sink.add(JSArrayOfintL().of([outputId]));
        if (dart.test(this[_controllers][$isEmpty])) this[_closeInnerChannel]();
      }
      [_closeInnerChannel]() {
        this[_inner$0].sink.close();
        this[_innerStreamSubscription].cancel();
        this[_inner$0] = null;
        for (let controller of core.List.from(this[_controllers][$values])) {
          dart.dsend(dart.dload(dart.dload(controller, 'local'), 'sink'), 'close', []);
        }
        this[_controllers][$clear]();
      }
    }
    (_MultiChannel.new = function(_inner) {
      this[_innerStreamSubscription] = null;
      this[_mainController] = new (StreamChannelControllerOfTL()).new({sync: true});
      this[_controllers] = new (IdentityMapOfintL$StreamChannelControllerLOfTL()).new();
      this[_pendingIds] = new (_IdentityHashSetOfintL()).new();
      this[_closedIds] = new (_IdentityHashSetOfintL()).new();
      this[_nextId] = 1;
      this[_inner$0] = _inner;
      this[_controllers][$_set](0, this[_mainController]);
      this[_mainController].local.stream.listen(dart.fn(message => this[_inner$0].sink.add(JSArrayOfObjectL().of([0, message])), TLTovoid()), {onDone: dart.fn(() => this[_closeChannel](0, 0), VoidTovoid())});
      this[_innerStreamSubscription] = this[_inner$0].stream.listen(dart.fn(message => {
        let id = dart.dsend(message, '_get', [0]);
        if (dart.test(this[_closedIds].contains(id))) return;
        let controller = this[_controllers][$putIfAbsent](intL().as(id), dart.fn(() => {
          this[_pendingIds].add(intL().as(id));
          return new (StreamChannelControllerOfTL()).new({sync: true});
        }, VoidToStreamChannelControllerLOfTL()));
        if (dart.dtest(dart.dsend(dart.dload(message, 'length'), '>', [1]))) {
          controller.local.sink.add(TL().as(dart.dsend(message, '_get', [1])));
        } else {
          controller.local.sink.close();
        }
      }, dynamicToNullN()), {onDone: dart.bind(this, _closeInnerChannel), onError: dart.bind(this[_mainController].local.sink, 'addError')});
    }).prototype = _MultiChannel.prototype;
    dart.addTypeTests(_MultiChannel);
    _MultiChannel.prototype[_is__MultiChannel_default] = true;
    dart.addTypeCaches(_MultiChannel);
    _MultiChannel[dart.implements] = () => [multi_channel.MultiChannel$(dart.legacy(T))];
    dart.setMethodSignature(_MultiChannel, () => ({
      __proto__: dart.getMethods(_MultiChannel.__proto__),
      virtualChannel: dart.fnType(dart.legacy(multi_channel.VirtualChannel$(dart.legacy(T))), [], [dart.legacy(core.int)]),
      [_closeChannel]: dart.fnType(dart.void, [dart.legacy(core.int), dart.legacy(core.int)]),
      [_closeInnerChannel]: dart.fnType(dart.void, [])
    }));
    dart.setGetterSignature(_MultiChannel, () => ({
      __proto__: dart.getGetters(_MultiChannel.__proto__),
      stream: dart.legacy(async.Stream$(dart.legacy(T))),
      sink: dart.legacy(async.StreamSink$(dart.legacy(T)))
    }));
    dart.setLibraryUri(_MultiChannel, L5);
    dart.setFieldSignature(_MultiChannel, () => ({
      __proto__: dart.getFields(_MultiChannel.__proto__),
      [_inner$0]: dart.fieldType(dart.legacy(stream_channel.StreamChannel)),
      [_innerStreamSubscription]: dart.fieldType(dart.legacy(async.StreamSubscription)),
      [_mainController]: dart.finalFieldType(dart.legacy(stream_channel_controller.StreamChannelController$(dart.legacy(T)))),
      [_controllers]: dart.finalFieldType(dart.legacy(core.Map$(dart.legacy(core.int), dart.legacy(stream_channel_controller.StreamChannelController$(dart.legacy(T)))))),
      [_pendingIds]: dart.finalFieldType(dart.legacy(core.Set$(dart.legacy(core.int)))),
      [_closedIds]: dart.finalFieldType(dart.legacy(core.Set$(dart.legacy(core.int)))),
      [_nextId]: dart.fieldType(dart.legacy(core.int))
    }));
    return _MultiChannel;
  });
  multi_channel._MultiChannel = multi_channel._MultiChannel$();
  dart.addTypeTests(multi_channel._MultiChannel, _is__MultiChannel_default);
  var _parent$ = dart.privateName(multi_channel, "_parent");
  const _is_VirtualChannel_default = Symbol('_is_VirtualChannel_default');
  var id$ = dart.privateName(multi_channel, "VirtualChannel.id");
  var stream$ = dart.privateName(multi_channel, "VirtualChannel.stream");
  var sink$ = dart.privateName(multi_channel, "VirtualChannel.sink");
  multi_channel.VirtualChannel$ = dart.generic(T => {
    class VirtualChannel extends stream_channel.StreamChannelMixin$(dart.legacy(T)) {
      get id() {
        return this[id$];
      }
      set id(value) {
        super.id = value;
      }
      get stream() {
        return this[stream$];
      }
      set stream(value) {
        super.stream = value;
      }
      get sink() {
        return this[sink$];
      }
      set sink(value) {
        super.sink = value;
      }
      virtualChannel(id = null) {
        return this[_parent$].virtualChannel(id);
      }
    }
    (VirtualChannel.__ = function(_parent, id, stream, sink) {
      this[_parent$] = _parent;
      this[id$] = id;
      this[stream$] = stream;
      this[sink$] = sink;
      ;
    }).prototype = VirtualChannel.prototype;
    dart.addTypeTests(VirtualChannel);
    VirtualChannel.prototype[_is_VirtualChannel_default] = true;
    dart.addTypeCaches(VirtualChannel);
    VirtualChannel[dart.implements] = () => [multi_channel.MultiChannel$(dart.legacy(T))];
    dart.setMethodSignature(VirtualChannel, () => ({
      __proto__: dart.getMethods(VirtualChannel.__proto__),
      virtualChannel: dart.fnType(dart.legacy(multi_channel.VirtualChannel$(dart.legacy(T))), [], [dart.legacy(core.int)])
    }));
    dart.setLibraryUri(VirtualChannel, L5);
    dart.setFieldSignature(VirtualChannel, () => ({
      __proto__: dart.getFields(VirtualChannel.__proto__),
      [_parent$]: dart.finalFieldType(dart.legacy(multi_channel.MultiChannel$(dart.legacy(T)))),
      id: dart.finalFieldType(dart.legacy(core.int)),
      stream: dart.finalFieldType(dart.legacy(async.Stream$(dart.legacy(T)))),
      sink: dart.finalFieldType(dart.legacy(async.StreamSink$(dart.legacy(T))))
    }));
    return VirtualChannel;
  });
  multi_channel.VirtualChannel = multi_channel.VirtualChannel$();
  dart.addTypeTests(multi_channel.VirtualChannel, _is_VirtualChannel_default);
  var C0;
  var _sinkTransformer$0 = dart.privateName(json_document_transformer, "_sinkTransformer");
  var C1;
  var _streamTransformer$0 = dart.privateName(json_document_transformer, "_streamTransformer");
  var C2;
  json_document_transformer._JsonDocument = class _JsonDocument extends core.Object {
    bind(channel) {
      StreamChannelLOfStringL().as(channel);
      let stream = channel.stream.map(dart.dynamic, C0 || CT.C0);
      let sink = StreamSinkTransformerOfObjectL$StringL().fromHandlers({handleData: dart.fn((data, sink) => {
          sink.add(convert.jsonEncode(data));
        }, ObjectLAndEventSinkLOfStringLToNullN())}).bind(channel.sink);
      return StreamChannelOfObjectL().withCloseGuarantee(stream, sink);
    }
    get [_sinkTransformer]() {
      return StreamSinkTransformerLOfObjectL$StringL().as(this.noSuchMethod(new core._Invocation.getter(C1 || CT.C1)));
    }
    get [_streamTransformer]() {
      return StreamTransformerLOfStringL$ObjectL().as(this.noSuchMethod(new core._Invocation.getter(C2 || CT.C2)));
    }
  };
  (json_document_transformer._JsonDocument.new = function() {
    ;
  }).prototype = json_document_transformer._JsonDocument.prototype;
  dart.addTypeTests(json_document_transformer._JsonDocument);
  dart.addTypeCaches(json_document_transformer._JsonDocument);
  json_document_transformer._JsonDocument[dart.implements] = () => [stream_channel_transformer.StreamChannelTransformer$(dart.legacy(core.Object), dart.legacy(core.String))];
  dart.setMethodSignature(json_document_transformer._JsonDocument, () => ({
    __proto__: dart.getMethods(json_document_transformer._JsonDocument.__proto__),
    bind: dart.fnType(dart.legacy(stream_channel.StreamChannel$(dart.legacy(core.Object))), [dart.legacy(core.Object)])
  }));
  dart.setGetterSignature(json_document_transformer._JsonDocument, () => ({
    __proto__: dart.getGetters(json_document_transformer._JsonDocument.__proto__),
    [_sinkTransformer]: dart.legacy(stream_sink_transformer.StreamSinkTransformer$(dart.legacy(core.Object), dart.legacy(core.String))),
    [_streamTransformer]: dart.legacy(async.StreamTransformer$(dart.legacy(core.String), dart.legacy(core.Object)))
  }));
  dart.setLibraryUri(json_document_transformer._JsonDocument, L6);
  var C3;
  dart.defineLazy(json_document_transformer, {
    /*json_document_transformer.jsonDocument*/get jsonDocument() {
      return C3 || CT.C3;
    }
  }, true);
  var _sinks = dart.privateName(disconnector, "_sinks");
  var _disconnectMemo = dart.privateName(disconnector, "_disconnectMemo");
  var _disconnect = dart.privateName(disconnector, "_disconnect");
  var _sinkTransformer$1 = dart.privateName(disconnector, "_sinkTransformer");
  var C4;
  var _streamTransformer$1 = dart.privateName(disconnector, "_streamTransformer");
  var C5;
  const _is_Disconnector_default = Symbol('_is_Disconnector_default');
  disconnector.Disconnector$ = dart.generic(T => {
    var TL = () => (TL = dart.constFn(dart.legacy(T)))();
    var _DisconnectorSinkOfTL = () => (_DisconnectorSinkOfTL = dart.constFn(disconnector._DisconnectorSink$(TL())))();
    var _DisconnectorSinkLOfTL = () => (_DisconnectorSinkLOfTL = dart.constFn(dart.legacy(_DisconnectorSinkOfTL())))();
    var JSArrayOf_DisconnectorSinkLOfTL = () => (JSArrayOf_DisconnectorSinkLOfTL = dart.constFn(_interceptors.JSArray$(_DisconnectorSinkLOfTL())))();
    var _DisconnectorSinkLOfTLToFutureLOfvoid = () => (_DisconnectorSinkLOfTLToFutureLOfvoid = dart.constFn(dart.fnType(FutureLOfvoid(), [_DisconnectorSinkLOfTL()])))();
    var StreamChannelOfTL = () => (StreamChannelOfTL = dart.constFn(stream_channel.StreamChannel$(TL())))();
    var StreamChannelLOfTL = () => (StreamChannelLOfTL = dart.constFn(dart.legacy(StreamChannelOfTL())))();
    var StreamSinkOfTL = () => (StreamSinkOfTL = dart.constFn(async.StreamSink$(TL())))();
    var StreamSinkLOfTL = () => (StreamSinkLOfTL = dart.constFn(dart.legacy(StreamSinkOfTL())))();
    var StreamSinkLOfTLTo_DisconnectorSinkLOfTL = () => (StreamSinkLOfTLTo_DisconnectorSinkLOfTL = dart.constFn(dart.fnType(_DisconnectorSinkLOfTL(), [StreamSinkLOfTL()])))();
    var StreamSinkTransformerOfTL$TL = () => (StreamSinkTransformerOfTL$TL = dart.constFn(stream_sink_transformer.StreamSinkTransformer$(TL(), TL())))();
    var StreamSinkTransformerLOfTL$TL = () => (StreamSinkTransformerLOfTL$TL = dart.constFn(dart.legacy(StreamSinkTransformerOfTL$TL())))();
    var StreamTransformerOfTL$TL = () => (StreamTransformerOfTL$TL = dart.constFn(async.StreamTransformer$(TL(), TL())))();
    var StreamTransformerLOfTL$TL = () => (StreamTransformerLOfTL$TL = dart.constFn(dart.legacy(StreamTransformerOfTL$TL())))();
    class Disconnector extends core.Object {
      get isDisconnected() {
        return this[_disconnectMemo].hasRun;
      }
      disconnect() {
        return this[_disconnectMemo].runOnce(dart.fn(() => {
          let futures = this[_sinks][$map](FutureLOfvoid(), dart.fn(sink => sink[_disconnect](), _DisconnectorSinkLOfTLToFutureLOfvoid()))[$toList]();
          this[_sinks][$clear]();
          return async.Future.wait(dart.void, futures, {eagerError: true});
        }, VoidToFutureLOfListLOfvoid()));
      }
      bind(channel) {
        StreamChannelLOfTL().as(channel);
        return channel.changeSink(dart.fn(innerSink => {
          let sink = new (_DisconnectorSinkOfTL()).new(innerSink);
          if (dart.test(this.isDisconnected)) {
            sink[_disconnect]().catchError(dart.fn(_ => {
            }, dynamicToNullN()));
          } else {
            this[_sinks][$add](sink);
          }
          return sink;
        }, StreamSinkLOfTLTo_DisconnectorSinkLOfTL()));
      }
      get [_sinkTransformer]() {
        return StreamSinkTransformerLOfTL$TL().as(this.noSuchMethod(new core._Invocation.getter(C4 || CT.C4)));
      }
      get [_streamTransformer]() {
        return StreamTransformerLOfTL$TL().as(this.noSuchMethod(new core._Invocation.getter(C5 || CT.C5)));
      }
    }
    (Disconnector.new = function() {
      this[_sinks] = JSArrayOf_DisconnectorSinkLOfTL().of([]);
      this[_disconnectMemo] = new async_memoizer.AsyncMemoizer.new();
      ;
    }).prototype = Disconnector.prototype;
    dart.addTypeTests(Disconnector);
    Disconnector.prototype[_is_Disconnector_default] = true;
    dart.addTypeCaches(Disconnector);
    Disconnector[dart.implements] = () => [stream_channel_transformer.StreamChannelTransformer$(dart.legacy(T), dart.legacy(T))];
    dart.setMethodSignature(Disconnector, () => ({
      __proto__: dart.getMethods(Disconnector.__proto__),
      disconnect: dart.fnType(dart.legacy(async.Future$(dart.void)), []),
      bind: dart.fnType(dart.legacy(stream_channel.StreamChannel$(dart.legacy(T))), [dart.legacy(core.Object)])
    }));
    dart.setGetterSignature(Disconnector, () => ({
      __proto__: dart.getGetters(Disconnector.__proto__),
      isDisconnected: dart.legacy(core.bool),
      [_sinkTransformer]: dart.legacy(stream_sink_transformer.StreamSinkTransformer$(dart.legacy(T), dart.legacy(T))),
      [_streamTransformer]: dart.legacy(async.StreamTransformer$(dart.legacy(T), dart.legacy(T)))
    }));
    dart.setLibraryUri(Disconnector, L7);
    dart.setFieldSignature(Disconnector, () => ({
      __proto__: dart.getFields(Disconnector.__proto__),
      [_sinks]: dart.finalFieldType(dart.legacy(core.List$(dart.legacy(disconnector._DisconnectorSink$(dart.legacy(T)))))),
      [_disconnectMemo]: dart.finalFieldType(dart.legacy(async_memoizer.AsyncMemoizer))
    }));
    return Disconnector;
  });
  disconnector.Disconnector = disconnector.Disconnector$();
  dart.addTypeTests(disconnector.Disconnector, _is_Disconnector_default);
  var _isDisconnected = dart.privateName(disconnector, "_isDisconnected");
  var _closed$ = dart.privateName(disconnector, "_closed");
  var _addStreamSubscription$ = dart.privateName(disconnector, "_addStreamSubscription");
  var _addStreamCompleter$ = dart.privateName(disconnector, "_addStreamCompleter");
  var _inner$1 = dart.privateName(disconnector, "_inner");
  var _inAddStream$ = dart.privateName(disconnector, "_inAddStream");
  const _is__DisconnectorSink_default = Symbol('_is__DisconnectorSink_default');
  disconnector._DisconnectorSink$ = dart.generic(T => {
    var TL = () => (TL = dart.constFn(dart.legacy(T)))();
    var StreamOfTL = () => (StreamOfTL = dart.constFn(async.Stream$(TL())))();
    var StreamLOfTL = () => (StreamLOfTL = dart.constFn(dart.legacy(StreamOfTL())))();
    class _DisconnectorSink extends core.Object {
      get done() {
        return this[_inner$1].done;
      }
      get [_inAddStream$]() {
        return this[_addStreamSubscription$] != null;
      }
      add(data) {
        TL().as(data);
        if (dart.test(this[_closed$])) dart.throw(new core.StateError.new("Cannot add event after closing."));
        if (dart.test(this[_inAddStream$])) {
          dart.throw(new core.StateError.new("Cannot add event while adding stream."));
        }
        if (dart.test(this[_isDisconnected])) return;
        this[_inner$1].add(data);
      }
      addError(error, stackTrace = null) {
        if (dart.test(this[_closed$])) dart.throw(new core.StateError.new("Cannot add event after closing."));
        if (dart.test(this[_inAddStream$])) {
          dart.throw(new core.StateError.new("Cannot add event while adding stream."));
        }
        if (dart.test(this[_isDisconnected])) return;
        this[_inner$1].addError(error, stackTrace);
      }
      addStream(stream) {
        StreamLOfTL().as(stream);
        if (dart.test(this[_closed$])) dart.throw(new core.StateError.new("Cannot add stream after closing."));
        if (dart.test(this[_inAddStream$])) {
          dart.throw(new core.StateError.new("Cannot add stream while adding stream."));
        }
        if (dart.test(this[_isDisconnected])) return FutureOfvoid().value();
        this[_addStreamCompleter$] = async.Completer.sync();
        this[_addStreamSubscription$] = stream.listen(dart.bind(this[_inner$1], 'add'), {onError: dart.bind(this[_inner$1], 'addError'), onDone: dart.bind(this[_addStreamCompleter$], 'complete')});
        return this[_addStreamCompleter$].future.then(dart.void, dart.fn(_ => {
          this[_addStreamCompleter$] = null;
          this[_addStreamSubscription$] = null;
        }, dynamicToNullN()));
      }
      close() {
        if (dart.test(this[_inAddStream$])) {
          dart.throw(new core.StateError.new("Cannot close sink while adding stream."));
        }
        this[_closed$] = true;
        return this[_inner$1].close();
      }
      [_disconnect]() {
        this[_isDisconnected] = true;
        let future = this[_inner$1].close();
        if (dart.test(this[_inAddStream$])) {
          this[_addStreamCompleter$].complete(this[_addStreamSubscription$].cancel());
          this[_addStreamCompleter$] = null;
          this[_addStreamSubscription$] = null;
        }
        return future;
      }
    }
    (_DisconnectorSink.new = function(_inner) {
      this[_isDisconnected] = false;
      this[_closed$] = false;
      this[_addStreamSubscription$] = null;
      this[_addStreamCompleter$] = null;
      this[_inner$1] = _inner;
      ;
    }).prototype = _DisconnectorSink.prototype;
    dart.addTypeTests(_DisconnectorSink);
    _DisconnectorSink.prototype[_is__DisconnectorSink_default] = true;
    dart.addTypeCaches(_DisconnectorSink);
    _DisconnectorSink[dart.implements] = () => [async.StreamSink$(dart.legacy(T))];
    dart.setMethodSignature(_DisconnectorSink, () => ({
      __proto__: dart.getMethods(_DisconnectorSink.__proto__),
      add: dart.fnType(dart.void, [dart.legacy(core.Object)]),
      addError: dart.fnType(dart.void, [dart.legacy(core.Object)], [dart.legacy(core.StackTrace)]),
      addStream: dart.fnType(dart.legacy(async.Future$(dart.void)), [dart.legacy(core.Object)]),
      close: dart.fnType(dart.legacy(async.Future$(dart.void)), []),
      [_disconnect]: dart.fnType(dart.legacy(async.Future$(dart.void)), [])
    }));
    dart.setGetterSignature(_DisconnectorSink, () => ({
      __proto__: dart.getGetters(_DisconnectorSink.__proto__),
      done: dart.legacy(async.Future$(dart.void)),
      [_inAddStream$]: dart.legacy(core.bool)
    }));
    dart.setLibraryUri(_DisconnectorSink, L7);
    dart.setFieldSignature(_DisconnectorSink, () => ({
      __proto__: dart.getFields(_DisconnectorSink.__proto__),
      [_inner$1]: dart.finalFieldType(dart.legacy(async.StreamSink$(dart.legacy(T)))),
      [_isDisconnected]: dart.fieldType(dart.legacy(core.bool)),
      [_closed$]: dart.fieldType(dart.legacy(core.bool)),
      [_addStreamSubscription$]: dart.fieldType(dart.legacy(async.StreamSubscription$(dart.legacy(T)))),
      [_addStreamCompleter$]: dart.fieldType(dart.legacy(async.Completer))
    }));
    return _DisconnectorSink;
  });
  disconnector._DisconnectorSink = disconnector._DisconnectorSink$();
  dart.addTypeTests(disconnector._DisconnectorSink, _is__DisconnectorSink_default);
  var _inner$2 = dart.privateName(delegating_stream_channel, "_inner");
  const _is_DelegatingStreamChannel_default = Symbol('_is_DelegatingStreamChannel_default');
  delegating_stream_channel.DelegatingStreamChannel$ = dart.generic(T => {
    class DelegatingStreamChannel extends stream_channel.StreamChannelMixin$(dart.legacy(T)) {
      get stream() {
        return this[_inner$2].stream;
      }
      get sink() {
        return this[_inner$2].sink;
      }
    }
    (DelegatingStreamChannel.new = function(_inner) {
      this[_inner$2] = _inner;
      ;
    }).prototype = DelegatingStreamChannel.prototype;
    dart.addTypeTests(DelegatingStreamChannel);
    DelegatingStreamChannel.prototype[_is_DelegatingStreamChannel_default] = true;
    dart.addTypeCaches(DelegatingStreamChannel);
    dart.setGetterSignature(DelegatingStreamChannel, () => ({
      __proto__: dart.getGetters(DelegatingStreamChannel.__proto__),
      stream: dart.legacy(async.Stream$(dart.legacy(T))),
      sink: dart.legacy(async.StreamSink$(dart.legacy(T)))
    }));
    dart.setLibraryUri(DelegatingStreamChannel, L8);
    dart.setFieldSignature(DelegatingStreamChannel, () => ({
      __proto__: dart.getFields(DelegatingStreamChannel.__proto__),
      [_inner$2]: dart.finalFieldType(dart.legacy(stream_channel.StreamChannel$(dart.legacy(T))))
    }));
    return DelegatingStreamChannel;
  });
  delegating_stream_channel.DelegatingStreamChannel = delegating_stream_channel.DelegatingStreamChannel$();
  dart.addTypeTests(delegating_stream_channel.DelegatingStreamChannel, _is_DelegatingStreamChannel_default);
  var _stream = dart.privateName(close_guarantee_channel, "_stream");
  var _sink$ = dart.privateName(close_guarantee_channel, "_sink");
  var _disconnected$ = dart.privateName(close_guarantee_channel, "_disconnected");
  var _subscription$ = dart.privateName(close_guarantee_channel, "_subscription");
  const _is_CloseGuaranteeChannel_default = Symbol('_is_CloseGuaranteeChannel_default');
  var _subscription$0 = dart.privateName(close_guarantee_channel, "CloseGuaranteeChannel._subscription");
  close_guarantee_channel.CloseGuaranteeChannel$ = dart.generic(T => {
    var TL = () => (TL = dart.constFn(dart.legacy(T)))();
    var _CloseGuaranteeSinkOfTL = () => (_CloseGuaranteeSinkOfTL = dart.constFn(close_guarantee_channel._CloseGuaranteeSink$(TL())))();
    var _CloseGuaranteeStreamOfTL = () => (_CloseGuaranteeStreamOfTL = dart.constFn(close_guarantee_channel._CloseGuaranteeStream$(TL())))();
    var StreamSubscriptionOfTL = () => (StreamSubscriptionOfTL = dart.constFn(async.StreamSubscription$(TL())))();
    var StreamSubscriptionLOfTL = () => (StreamSubscriptionLOfTL = dart.constFn(dart.legacy(StreamSubscriptionOfTL())))();
    class CloseGuaranteeChannel extends stream_channel.StreamChannelMixin$(dart.legacy(T)) {
      get [_subscription$]() {
        return this[_subscription$0];
      }
      set [_subscription$](value) {
        this[_subscription$0] = StreamSubscriptionLOfTL().as(value);
      }
      get stream() {
        return this[_stream];
      }
      get sink() {
        return this[_sink$];
      }
    }
    (CloseGuaranteeChannel.new = function(innerStream, innerSink) {
      this[_stream] = null;
      this[_sink$] = null;
      this[_subscription$0] = null;
      this[_disconnected$] = false;
      this[_sink$] = new (_CloseGuaranteeSinkOfTL()).new(innerSink, this);
      this[_stream] = new (_CloseGuaranteeStreamOfTL()).new(innerStream, this);
    }).prototype = CloseGuaranteeChannel.prototype;
    dart.addTypeTests(CloseGuaranteeChannel);
    CloseGuaranteeChannel.prototype[_is_CloseGuaranteeChannel_default] = true;
    dart.addTypeCaches(CloseGuaranteeChannel);
    dart.setGetterSignature(CloseGuaranteeChannel, () => ({
      __proto__: dart.getGetters(CloseGuaranteeChannel.__proto__),
      stream: dart.legacy(async.Stream$(dart.legacy(T))),
      sink: dart.legacy(async.StreamSink$(dart.legacy(T)))
    }));
    dart.setLibraryUri(CloseGuaranteeChannel, L9);
    dart.setFieldSignature(CloseGuaranteeChannel, () => ({
      __proto__: dart.getFields(CloseGuaranteeChannel.__proto__),
      [_stream]: dart.fieldType(dart.legacy(close_guarantee_channel._CloseGuaranteeStream$(dart.legacy(T)))),
      [_sink$]: dart.fieldType(dart.legacy(close_guarantee_channel._CloseGuaranteeSink$(dart.legacy(T)))),
      [_subscription$]: dart.fieldType(dart.legacy(async.StreamSubscription$(dart.legacy(T)))),
      [_disconnected$]: dart.fieldType(dart.legacy(core.bool))
    }));
    return CloseGuaranteeChannel;
  });
  close_guarantee_channel.CloseGuaranteeChannel = close_guarantee_channel.CloseGuaranteeChannel$();
  dart.addTypeTests(close_guarantee_channel.CloseGuaranteeChannel, _is_CloseGuaranteeChannel_default);
  var _inner$3 = dart.privateName(close_guarantee_channel, "_inner");
  var _channel$0 = dart.privateName(close_guarantee_channel, "_channel");
  const _is__CloseGuaranteeStream_default = Symbol('_is__CloseGuaranteeStream_default');
  close_guarantee_channel._CloseGuaranteeStream$ = dart.generic(T => {
    class _CloseGuaranteeStream extends async.Stream$(dart.legacy(T)) {
      listen(onData, opts) {
        let onError = opts && 'onError' in opts ? opts.onError : null;
        let onDone = opts && 'onDone' in opts ? opts.onDone : null;
        let cancelOnError = opts && 'cancelOnError' in opts ? opts.cancelOnError : null;
        if (dart.test(this[_channel$0][_disconnected$])) {
          onData = null;
          onError = null;
        }
        let subscription = this[_inner$3].listen(onData, {onError: onError, onDone: onDone, cancelOnError: cancelOnError});
        if (!dart.test(this[_channel$0][_disconnected$])) {
          this[_channel$0][_subscription$] = subscription;
        }
        return subscription;
      }
    }
    (_CloseGuaranteeStream.new = function(_inner, _channel) {
      this[_inner$3] = _inner;
      this[_channel$0] = _channel;
      _CloseGuaranteeStream.__proto__.new.call(this);
      ;
    }).prototype = _CloseGuaranteeStream.prototype;
    dart.addTypeTests(_CloseGuaranteeStream);
    _CloseGuaranteeStream.prototype[_is__CloseGuaranteeStream_default] = true;
    dart.addTypeCaches(_CloseGuaranteeStream);
    dart.setMethodSignature(_CloseGuaranteeStream, () => ({
      __proto__: dart.getMethods(_CloseGuaranteeStream.__proto__),
      listen: dart.fnType(dart.legacy(async.StreamSubscription$(dart.legacy(T))), [dart.legacy(dart.fnType(dart.void, [dart.legacy(T)]))], {cancelOnError: dart.legacy(core.bool), onDone: dart.legacy(dart.fnType(dart.void, [])), onError: dart.legacy(core.Function)}, {})
    }));
    dart.setLibraryUri(_CloseGuaranteeStream, L9);
    dart.setFieldSignature(_CloseGuaranteeStream, () => ({
      __proto__: dart.getFields(_CloseGuaranteeStream.__proto__),
      [_inner$3]: dart.finalFieldType(dart.legacy(async.Stream$(dart.legacy(T)))),
      [_channel$0]: dart.finalFieldType(dart.legacy(close_guarantee_channel.CloseGuaranteeChannel$(dart.legacy(T))))
    }));
    return _CloseGuaranteeStream;
  });
  close_guarantee_channel._CloseGuaranteeStream = close_guarantee_channel._CloseGuaranteeStream$();
  dart.addTypeTests(close_guarantee_channel._CloseGuaranteeStream, _is__CloseGuaranteeStream_default);
  const _is__CloseGuaranteeSink_default = Symbol('_is__CloseGuaranteeSink_default');
  close_guarantee_channel._CloseGuaranteeSink$ = dart.generic(T => {
    class _CloseGuaranteeSink extends stream_sink.DelegatingStreamSink$(dart.legacy(T)) {
      close() {
        let done = super.close();
        this[_channel$0][_disconnected$] = true;
        if (this[_channel$0][_subscription$] != null) {
          this[_channel$0][_subscription$].onData(null);
          this[_channel$0][_subscription$].onError(null);
        }
        return done;
      }
    }
    (_CloseGuaranteeSink.new = function(inner, _channel) {
      this[_channel$0] = _channel;
      _CloseGuaranteeSink.__proto__.new.call(this, inner);
      ;
    }).prototype = _CloseGuaranteeSink.prototype;
    dart.addTypeTests(_CloseGuaranteeSink);
    _CloseGuaranteeSink.prototype[_is__CloseGuaranteeSink_default] = true;
    dart.addTypeCaches(_CloseGuaranteeSink);
    dart.setMethodSignature(_CloseGuaranteeSink, () => ({
      __proto__: dart.getMethods(_CloseGuaranteeSink.__proto__),
      close: dart.fnType(dart.legacy(async.Future$(dart.void)), [])
    }));
    dart.setLibraryUri(_CloseGuaranteeSink, L9);
    dart.setFieldSignature(_CloseGuaranteeSink, () => ({
      __proto__: dart.getFields(_CloseGuaranteeSink.__proto__),
      [_channel$0]: dart.finalFieldType(dart.legacy(close_guarantee_channel.CloseGuaranteeChannel$(dart.legacy(T))))
    }));
    return _CloseGuaranteeSink;
  });
  close_guarantee_channel._CloseGuaranteeSink = close_guarantee_channel._CloseGuaranteeSink$();
  dart.addTypeTests(close_guarantee_channel._CloseGuaranteeSink, _is__CloseGuaranteeSink_default);
  dart.trackLibraries("packages/stream_channel/stream_channel", {
    "package:stream_channel/src/guarantee_channel.dart": guarantee_channel,
    "package:stream_channel/stream_channel.dart": stream_channel,
    "package:stream_channel/src/stream_channel_transformer.dart": stream_channel_transformer,
    "package:stream_channel/src/stream_channel_controller.dart": stream_channel_controller,
    "package:stream_channel/src/stream_channel_completer.dart": stream_channel_completer,
    "package:stream_channel/src/multi_channel.dart": multi_channel,
    "package:stream_channel/src/json_document_transformer.dart": json_document_transformer,
    "package:stream_channel/src/disconnector.dart": disconnector,
    "package:stream_channel/src/delegating_stream_channel.dart": delegating_stream_channel,
    "package:stream_channel/src/close_guarantee_channel.dart": close_guarantee_channel
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["stream_channel.dart","src/guarantee_channel.dart","src/stream_channel_transformer.dart","src/stream_channel_controller.dart","src/stream_channel_completer.dart","src/multi_channel.dart","src/json_document_transformer.dart","src/disconnector.dart","src/delegating_stream_channel.dart","src/close_guarantee_channel.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;gCAuJ6B;AACF,QAAvB,AAAO,iBAAK,AAAM,KAAD;AACM,QAAvB,AAAM,AAAO,KAAR,aAAa;MACpB;;mGAG6D;AACzD,cAAA,AAAY,YAAD,MAAM;MAAK;;uCAG+B;AACrD,iCAAyB,UAAZ,WAAW;MAAM;;2CAGyB;AACvD,+BAAuB,UAAZ,WAAW;MAAM;;uCAGQ;AACpC,cAAc,wCAAmB,AAAM,MAAA,CAAC,cAAS;MAAK;;+CAGhB;AACtC,cAAc,wCAAmB,aAAQ,AAAM,MAAA,CAAC;MAAM;;AAG5B,iEACT,8CAAM,cAA8B,uDAAM;MAAM;;;;IACvE;;;;;;;;;;;;;;;;;;;;;;;;;;;;ACpK0B,cAAA,AAAkB;MAAM;;AAGtB;MAAK;;AA+CT,QAApB,sBAAgB;AAChB,YAAI,uBAAiB,MAAM,AAAc,AAAQ;AACxB,QAAzB,AAAkB;MACpB;;qCAlC2B,aAA2B;UAC5C;MAhBQ;MAOE;MAGE;MAGjB,sBAAgB;AAIqD,MAAxE,cAAQ,+BAAkB,SAAS,EAAE,oBAAmB,eAAe;AAIvE,oBAAI,AAAY,WAAD;AAEmD,QADhE,cACI,AAAY,WAAD,iBAAW;;AAeb,MAZf,0BAAoB,sCACN;AAGR,wBAAI,sBAAe;AAMjB,UAJF,sBAAgB,AAAY,WAAD,QAA0B,UAAlB,2CACJ,UAAlB,8CAAoC;AAClB,cAA7B,AAAM;AACmB,cAAzB,AAAkB;;iCAGhB;IACZ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;AAyByB,cAAA,AAAe;MAAM;;AAqBrB,cAAA,AAAuB,iCAAG;MAAI;;gBAY5C;AACT,sBAAI,gBAAS,AAAmD,WAA7C,wBAAW;AAC9B,sBAAI;AACuD,UAAzD,WAAM,wBAAW;;AAEnB,sBAAI,sBAAe;AAEH,QAAhB,AAAO,kBAAI,IAAI;MACjB;eAGc,OAAmB;AAC/B,sBAAI,gBAAS,AAAmD,WAA7C,wBAAW;AAC9B,sBAAI;AACuD,UAAzD,WAAM,wBAAW;;AAEnB,sBAAI,sBAAe;AAES,QAA5B,gBAAU,KAAK,EAAE,UAAU;MAC7B;kBAMe,OAAmB;AAChC,sBAAI;AACgC,UAAlC,AAAO,uBAAS,KAAK,EAAE,UAAU;AACjC;;AAG6C,QAA/C,AAAe,mCAAc,KAAK,EAAE,UAAU;AAGvB,QAAvB;AAC8B,QAA9B,AAAS;AAIwB,QAAjC,AAAO,AAAQ,iCAAW,QAAC;;MAC7B;;yBAGiC;AAC/B,sBAAI,gBAAS,AAAoD,WAA9C,wBAAW;AAC9B,sBAAI;AACwD,UAA1D,WAAM,wBAAW;;AAEnB,sBAAI,sBAAe,MAAc;AAEK,QAAtC,4BAAgC;AAE6B,QAD7D,+BAAyB,AAAO,MAAD,QAAe,UAAP,2CAC1B,0BAAuC,UAApB;AAChC,cAAO,AAAoB,AAAO,kDAAK,QAAC;AACZ,UAA1B,4BAAsB;AACO,UAA7B,+BAAyB;;MAE7B;;AAIE,sBAAI;AACwD,UAA1D,WAAM,wBAAW;;AAGnB,sBAAI,gBAAS,MAAO;AACN,QAAd,gBAAU;AAEV,uBAAK;AAC2B,UAA9B,AAAS;AAC8B,UAAvC,AAAe,8BAAS,AAAO;;AAGjC,cAAO;MACT;;AAOsB,QAApB,sBAAgB;AAChB,uBAAK,AAAe,mCAAa,AAAe,AAAU;AAE1D,uBAAK,qBAAc;AAC0C,QAA7D,AAAoB,mCAAS,AAAuB;AAC1B,QAA1B,4BAAsB;AACO,QAA7B,+BAAyB;MAC3B;;mCA5FoB,QAAa;UAAgB;MA5B3C,uBAAiB;MAMlB,sBAAgB;MAGhB,gBAAU;MAIO;MAIZ;MAWU;MAAa;MACd,qBAAE,WAAW;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;iBDzCA,QAAsB;AAClD,wEAAkB,MAAM,EAAE,IAAI;MAAC;4BAYY,QAAsB;YACvD;AACV,6EAAiB,MAAM,EAAE,IAAI,oBAAmB,eAAe;MAAC;gCAYlD,QAAsB;AACpC,wFAAsB,MAAM,EAAE,IAAI;MAAC;;;;;;;;;;;;;;;mCA4CnB,QAAa;MAAb;MAAa;;IAAK;;;;;;;;;;;;;;;;;;;;;;;;;;;MEpHR;;;;;;MAGI;;;;;;;gCAsBK;AACnC,sDACI,AAAQ,AAAO,OAAR,wBAAkB,2BACzB,AAAiB,4BAAK,AAAQ,OAAD;MAAO;;6CApBnC,oBAAyB;MAAzB;MAAyB;;IAAiB;mDAMJ;8CACpC,AAAM,KAAD,UACgB,4CAAsB,AAAM,KAAD;IAAU;;;;;;;;;;;;;;;;;;;;;;;;;;;ACVvC;MAAM;;AAOJ;MAAQ;;;UAaV;UAAgC;MAnB7C;MAOA;AAaX,qCAA2B,kCAA0B,IAAI;AACzD,qCAA2B,kCAA0B,IAAI;AAEM,MADnE,eAAS,mCACL,AAAyB,wBAAD,SAAS,AAAyB,wBAAD;AAGrB,MAFxC,iBAAW,mCACP,AAAyB,wBAAD,SAAS,AAAyB,wBAAD,yBACxC,kBAAkB;IACzC;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ACvCgC;MAAQ;wBAcc;AAChD,wBAAY;AACqD,QAArE,AAAc,aAAD,iBAAgB,UAAV,SAAS,2BAAgC,UAAV,SAAS;AAC3D,cAAO,AAAU,UAAD;MAClB;;gCAYiC;AAC/B,sBAAI,aAAM,AAAqD,WAA/C,wBAAW;AAChB,QAAX,aAAO;AAEyC,QAAhD,AAAiB,uCAAgB,AAAQ,OAAD;AACO,QAA/C,AAAe,wCAAmB,AAAQ,OAAD;MAC3C;eASc,OAAmB;AAC/B,sBAAI,aAAM,AAAqD,WAA/C,wBAAW;AAChB,QAAX,aAAO;AAEqC,QAA5C,AAAiB,gCAAS,KAAK,EAAE,UAAU;AACQ,QAAnD,AAAe,wCAAmB;MACpC;;;MAzDM,yBAAmB;MAGnB,uBAAiB;MAIN;MAGZ,aAAO;AAiB+D,MAAzE,iBAAW,wBAAiB,AAAiB,+BAAQ,AAAe;IACtE;;;;;;;;;;;;;;;;;;;;;;;;;;;;iBCiB4C;AAAU,sEAAiB,KAAK;MAAC;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;AA+BrD,cAAA,AAAgB,AAAQ;MAAM;;AAE5B,cAAA,AAAgB,AAAQ;MAAI;qBA6EhB;AAChC;AACA;AACJ,YAAI,EAAE,IAAI;AAII,UAAZ,UAAU,EAAE;AACK,UAAjB,WAAc,aAAH,EAAE,IAAG;;AAKK,UAArB,UAAkB,aAAR,iBAAU;AACF,UAAlB,WAAW;AACC,UAAZ,gBAAQ,aAAR,iBAAW;;AAKb,YAAI,AAAO,kBAAG;AACZ,gBAAsB,+BAAE,MAAM,OAAO,EAAS,gCAAS;;AAG9B;AAC3B,sBAAI,AAAY,yBAAO,OAAO;AAGM,UAAlC,aAAa,AAAY,0BAAC,OAAO;cAC5B,eAAI,AAAa,iCAAY,OAAO,gBACvC,AAAW,0BAAS,OAAO;AACuC,UAApE,WAAM,2BAAc,AAA+C,wCAAnB,EAAE;;AAEF,UAAhD,aAAa,+CAA8B;AACT,UAAlC,AAAY,0BAAC,OAAO,EAAI,UAAU;;AAKe,QAFnD,AAAW,AAAM,AAAO,UAAd,qBACN,QAAC,WAAY,AAAO,AAAK,wBAAI,uBAAC,QAAQ,EAAE,OAAO,2BACvC,cAAM,oBAAc,OAAO,EAAE,QAAQ;AACjD,cAAsB,+BAClB,MAAM,QAAQ,EAAE,AAAW,AAAQ,UAAT,iBAAiB,AAAW,AAAQ,UAAT;MAC3D;sBAIuB,SAAa;AACX,QAAvB,AAAW,qBAAI,OAAO;AAClB,yBAAa,AAAa,4BAAO,OAAO;AACf,QAA7B,AAAW,AAAM,AAAK,UAAZ;AAEV,YAAI,AAAO,kBAAG,MAAM;AAIO,QAA3B,AAAO,AAAK,wBAAI,oBAAC,QAAQ;AACzB,sBAAI,AAAa,+BAAS,AAAoB;MAChD;;AAIqB,QAAnB,AAAO,AAAK;AACqB,QAAjC,AAAyB;AACZ,QAAb,iBAAS;AAIT,iBAAS,aAAmB,gBAAK,AAAa;AACf,UAAP,WAAL,WAAN,WAAX,UAAU;;AAEQ,QAApB,AAAa;MACf;;kCA7GmB;MA5CS;MAQtB,wBAAkB,+CAAiC;MAInD,qBAAgD;MAIhD,oBAAc;MAId,mBAAa;MAsBf,gBAAU;MAEK;AAGgB,MAAjC,AAAY,0BAAC,GAAK;AAGoB,MAFtC,AAAgB,AAAM,AAAO,0CACzB,QAAC,WAAY,AAAO,AAAK,wBAAI,uBAAC,GAAG,OAAO,2BAChC,cAAM,oBAAc,GAAG;AA4Bc,MA1BjD,iCAA2B,AAAO,AAAO,6BAAO,QAAC;AAC3C,iBAAY,WAAP,OAAO,WAAC;AAIjB,sBAAI,AAAW,0BAAS,EAAE,IAAG;AAEzB,yBAAa,AAAa,2CAAY,EAAE,GAAE;AAIzB,UAAnB,AAAY,gCAAI,EAAE;AAClB,gBAAO,gDAA8B;;AAGvC,uBAAmB,WAAP,WAAR,OAAO,mBAAU;AACkB,UAArC,AAAW,AAAM,AAAK,UAAZ,wBAAuB,WAAP,OAAO,WAAC;;AAML,UAA7B,AAAW,AAAM,AAAK,UAAZ;;+CAGF,oCAC4B,UAA3B,AAAgB,AAAM;IACrC;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MA4FU;;;;;;MAGM;;;;;;MAEI;;;;;;qBAKc;AAAQ,cAAA,AAAQ,+BAAe,EAAE;MAAC;;kCAH9C,SAAc,IAAS,QAAa;MAApC;MAAc;MAAS;MAAa;;IAAK;;;;;;;;;;;;;;;;;;;;;;;;;;;;mCCjPd;AAC3C,mBAAS,AAAQ,AAAO,OAAR;AAChB,iBAAO,AAGR,mEAFa,SAAC,MAAM;AACK,UAA1B,AAAK,IAAD,KAAK,mBAAW,IAAI;0DAClB,AAAQ,OAAD;AACf,YAAqB,6CAAmB,MAAM,EAAE,IAAI;IACtD;;;;;;;;;;EAVqB;;;;;;;;;;;;;;;;MAJwB,sCAAY;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ACC9B,cAAA,AAAgB;MAAM;;AAcpB,cAAA,AAAgB,+BAAQ;AAC3C,wBAAU,AAAO,AAAkC,oCAA9B,QAAC,QAAS,AAAK,IAAD;AACzB,UAAd,AAAO;AACP,gBAAc,8BAAK,OAAO,eAAc;;MACxC;;gCAIiC;AACrC,cAAO,AAAQ,QAAD,YAAY,QAAC;AACrB,qBAAO,kCAAqB,SAAS;AAEzC,wBAAI;AAGmC,YAArC,AAAK,AAAc,IAAf,2BAA0B,QAAC;;;AAEf,YAAhB,AAAO,mBAAI,IAAI;;AAGjB,gBAAO,KAAI;;MAEf;;;;;;;;;MA7BM,eAA+B;MAY/B,wBAAkB;;IAkB1B;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;AAQ2B,cAAA,AAAO;MAAI;;AAiBX,cAAA,AAAuB,kCAAG;MAAI;;gBAK5C;AACT,sBAAI,iBAAS,AAAmD,WAA7C,wBAAW;AAC9B,sBAAI;AACuD,UAAzD,WAAM,wBAAW;;AAEnB,sBAAI,wBAAiB;AAEL,QAAhB,AAAO,mBAAI,IAAI;MACjB;eAGc,OAAmB;AAC/B,sBAAI,iBAAS,AAAmD,WAA7C,wBAAW;AAC9B,sBAAI;AACuD,UAAzD,WAAM,wBAAW;;AAEnB,sBAAI,wBAAiB;AAEa,QAAlC,AAAO,wBAAS,KAAK,EAAE,UAAU;MACnC;;yBAGiC;AAC/B,sBAAI,iBAAS,AAAoD,WAA9C,wBAAW;AAC9B,sBAAI;AACwD,UAA1D,WAAM,wBAAW;;AAEnB,sBAAI,wBAAiB,MAAc;AAEG,QAAtC,6BAAgC;AAEmC,QADnE,gCAAyB,AAAO,MAAD,QAAe,UAAP,kCACnB,UAAP,qCAA6C,UAApB;AACtC,cAAO,AAAoB,AAAO,mDAAK,QAAC;AACZ,UAA1B,6BAAsB;AACO,UAA7B,gCAAyB;;MAE7B;;AAIE,sBAAI;AACwD,UAA1D,WAAM,wBAAW;;AAGL,QAAd,iBAAU;AACV,cAAO,AAAO;MAChB;;AAOwB,QAAtB,wBAAkB;AACd,qBAAS,AAAO;AAEpB,sBAAI;AAC2D,UAA7D,AAAoB,oCAAS,AAAuB;AAC1B,UAA1B,6BAAsB;AACO,UAA7B,gCAAyB;;AAG3B,cAAO,OAAM;MACf;;sCAlEuB;MAhBnB,wBAAkB;MAGlB,iBAAU;MAIQ;MAIZ;MAKa;;IAAO;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ACpEN,cAAA,AAAO;MAAM;;AAEX,cAAA,AAAO;MAAI;;;MAER;;IAAO;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MCId;;;;;;;AARE;MAAO;;AAIL;MAAK;;0CASC,aAA2B;MAZlC;MAIF;MAGD;MAGjB,uBAAgB;AAG4B,MAA/C,eAAQ,oCAAuB,SAAS,EAAE;AACW,MAArD,gBAAU,sCAAyB,WAAW,EAAE;IAClD;;;;;;;;;;;;;;;;;;;;;;;;;;aAiBkC;YACpB;YAAc;YAAe;AAGzC,sBAAI,AAAS;AACE,UAAb,SAAS;AACK,UAAd,UAAU;;AAGR,2BAAe,AAAO,sBAAO,MAAM,YAC1B,OAAO,UAAU,MAAM,iBAAiB,aAAa;AAClE,uBAAK,AAAS;AACyB,UAArC,AAAS,mCAAgB,YAAY;;AAEvC,cAAO,aAAY;MACrB;;0CAlB2B,QAAa;MAAb;MAAa;AAAxC;;IAAiD;;;;;;;;;;;;;;;;;;;;;;AAiC3C,mBAAa;AACY,QAA7B,AAAS,mCAAgB;AACzB,YAAI,AAAS,oCAAiB;AAEO,UAAnC,AAAS,AAAc,wCAAO;AACM,UAApC,AAAS,AAAc,yCAAQ;;AAEjC,cAAO,KAAI;MACb;;wCAZkC;MAAY;AAAY,mDAAM,KAAK;;IAAC","file":"stream_channel.ddc.js"}');
  // Exports:
  return {
    src__guarantee_channel: guarantee_channel,
    stream_channel: stream_channel,
    src__stream_channel_transformer: stream_channel_transformer,
    src__stream_channel_controller: stream_channel_controller,
    src__stream_channel_completer: stream_channel_completer,
    src__multi_channel: multi_channel,
    src__json_document_transformer: json_document_transformer,
    src__disconnector: disconnector,
    src__delegating_stream_channel: delegating_stream_channel,
    src__close_guarantee_channel: close_guarantee_channel
  };
}));

//# sourceMappingURL=stream_channel.ddc.js.map
