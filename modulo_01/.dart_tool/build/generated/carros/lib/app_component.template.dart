// **************************************************************************
// Generator: AngularDart Compiler
// **************************************************************************

import 'app_component.dart';
export 'app_component.dart';
import 'package:angular/src/di/reflector.dart' as _ngRef;
import 'package:angular/angular.template.dart' as _ref0;
import 'package:carros/src/components/cadastro_component/cadastro_component.template.dart' as _ref1;
import 'package:carros/src/services/carro_service.template.dart' as _ref2;
import 'src/components/navbar_component/navbar_component.template.dart' as _ref3;
import 'src/components/tabela_component/tabela_component.template.dart' as _ref4;
import 'package:carros/app_component.css.shim.dart' as import0;
import 'package:angular/src/core/linker/app_view.dart';
import 'app_component.dart' as import2;
import 'src/components/navbar_component/navbar_component.template.dart' as import3;
import 'src/components/navbar_component/navbar_component.dart' as import4;
import 'src/components/cadastro_component/cadastro_component.template.dart' as import5;
import 'src/components/cadastro_component/cadastro_component.dart' as import6;
import 'src/components/tabela_component/tabela_component.template.dart' as import7;
import 'src/components/tabela_component/tabela_component.dart' as import8;
import 'package:angular/src/core/linker/style_encapsulation.dart' as import9;
import 'package:angular/src/core/linker/view_type.dart' as import10;
import 'package:angular/src/core/change_detection/change_detection.dart';
import 'dart:html' as import12;
import 'package:angular/src/runtime.dart' as import13;
import 'package:angular/angular.dart';
import 'package:angular/src/di/errors.dart' as import15;
import 'src/services/carro_service.dart' as import16;
import 'package:angular/src/core/linker/app_view_utils.dart' as import17;

final List<dynamic> styles$AppComponent = [import0.styles];

class ViewAppComponent0 extends AppView<import2.AppComponent> {
  import3.ViewNavbarComponent0 _compView_0;
  import4.NavbarComponent _NavbarComponent_0_5;
  import5.ViewCadastroComponent0 _compView_1;
  import6.CadastroComponent _CadastroComponent_1_5;
  import7.ViewTabelaComponent0 _compView_2;
  import8.TabelaComponent _TabelaComponent_2_5;
  static import9.ComponentStyles _componentStyles;
  ViewAppComponent0(AppView<dynamic> parentView, int parentIndex) : super(import10.ViewType.component, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways) {
    initComponentStyles();
    rootEl = import12.document.createElement('my-app');
  }
  static String get _debugComponentUrl {
    return (import13.isDevMode ? 'asset:carros/lib/app_component.dart' : null);
  }

  @override
  ComponentRef<import2.AppComponent> build() {
    final _rootEl = rootEl;
    final import12.HtmlElement parentRenderNode = initViewRoot(_rootEl);
    _compView_0 = import3.ViewNavbarComponent0(this, 0);
    final _el_0 = _compView_0.rootEl;
    parentRenderNode.append(_el_0);
    addShimC(_el_0);
    _NavbarComponent_0_5 = (import13.isDevMode
        ? import15.debugInjectorWrap(import4.NavbarComponent, () {
            return import4.NavbarComponent(parentView.injectorGet(import16.CarroService, viewData.parentIndex));
          })
        : import4.NavbarComponent(parentView.injectorGet(import16.CarroService, viewData.parentIndex)));
    _compView_0.create0(_NavbarComponent_0_5);
    _compView_1 = import5.ViewCadastroComponent0(this, 1);
    final _el_1 = _compView_1.rootEl;
    parentRenderNode.append(_el_1);
    addShimC(_el_1);
    _CadastroComponent_1_5 = (import13.isDevMode
        ? import15.debugInjectorWrap(import6.CadastroComponent, () {
            return import6.CadastroComponent(parentView.injectorGet(import16.CarroService, viewData.parentIndex));
          })
        : import6.CadastroComponent(parentView.injectorGet(import16.CarroService, viewData.parentIndex)));
    _compView_1.create0(_CadastroComponent_1_5);
    _compView_2 = import7.ViewTabelaComponent0(this, 2);
    final _el_2 = _compView_2.rootEl;
    parentRenderNode.append(_el_2);
    addShimC(_el_2);
    _TabelaComponent_2_5 = (import13.isDevMode
        ? import15.debugInjectorWrap(import8.TabelaComponent, () {
            return import8.TabelaComponent(parentView.injectorGet(import16.CarroService, viewData.parentIndex));
          })
        : import8.TabelaComponent(parentView.injectorGet(import16.CarroService, viewData.parentIndex)));
    _compView_2.create0(_TabelaComponent_2_5);
    init0();
  }

  @override
  void detectChangesInternal() {
    bool firstCheck = (this.cdState == 0);
    if (((!import17.AppViewUtils.throwOnChanges) && firstCheck)) {
      _CadastroComponent_1_5.ngOnInit();
    }
    if (((!import17.AppViewUtils.throwOnChanges) && firstCheck)) {
      _TabelaComponent_2_5.ngOnInit();
    }
    _compView_0.detectChanges();
    _compView_1.detectChanges();
    _compView_2.detectChanges();
  }

  @override
  void destroyInternal() {
    _compView_0.destroyInternalState();
    _compView_1.destroyInternalState();
    _compView_2.destroyInternalState();
  }

  @override
  void initComponentStyles() {
    var styles = _componentStyles;
    if (identical(styles, null)) {
      (_componentStyles = (styles = (_componentStyles = import9.ComponentStyles.scoped(styles$AppComponent, _debugComponentUrl))));
    }
    componentStyles = styles;
  }
}

const ComponentFactory<import2.AppComponent> _AppComponentNgFactory = const ComponentFactory('my-app', viewFactory_AppComponentHost0);
ComponentFactory<import2.AppComponent> get AppComponentNgFactory {
  return _AppComponentNgFactory;
}

final List<dynamic> styles$AppComponentHost = const [];

class _ViewAppComponentHost0 extends AppView<import2.AppComponent> {
  ViewAppComponent0 _compView_0;
  import2.AppComponent _AppComponent_0_5;
  import16.CarroService __CarroService_0_6;
  _ViewAppComponentHost0(AppView<dynamic> parentView, int parentIndex) : super(import10.ViewType.host, parentView, parentIndex, ChangeDetectionStrategy.CheckAlways);
  import16.CarroService get _CarroService_0_6 {
    if ((__CarroService_0_6 == null)) {
      (__CarroService_0_6 = import16.CarroService());
    }
    return __CarroService_0_6;
  }

  @override
  ComponentRef<import2.AppComponent> build() {
    _compView_0 = ViewAppComponent0(this, 0);
    rootEl = _compView_0.rootEl;
    _AppComponent_0_5 = import2.AppComponent();
    _compView_0.create(_AppComponent_0_5, projectedNodes);
    init1(rootEl);
    return ComponentRef(0, this, rootEl, _AppComponent_0_5);
  }

  @override
  dynamic injectorGetInternal(dynamic token, int nodeIndex, dynamic notFoundResult) {
    if ((identical(token, import16.CarroService) && (0 == nodeIndex))) {
      return _CarroService_0_6;
    }
    return notFoundResult;
  }

  @override
  void detectChangesInternal() {
    _compView_0.detectChanges();
  }

  @override
  void destroyInternal() {
    _compView_0.destroyInternalState();
  }
}

AppView<import2.AppComponent> viewFactory_AppComponentHost0(AppView<dynamic> parentView, int parentIndex) {
  return _ViewAppComponentHost0(parentView, parentIndex);
}

var _visited = false;
void initReflector() {
  if (_visited) {
    return;
  }
  _visited = true;

  _ngRef.registerComponent(AppComponent, AppComponentNgFactory);
  _ref0.initReflector();
  _ref1.initReflector();
  _ref2.initReflector();
  _ref3.initReflector();
  _ref4.initReflector();
}
