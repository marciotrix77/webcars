define(['dart_sdk', 'packages/angular/src/core/change_detection/change_detection', 'packages/http/src/base_client', 'packages/angular/angular.template', 'packages/carros/app_component.template', 'packages/angular/src/bootstrap/modules', 'packages/carros/app_component'], (function load__web__main(dart_sdk, packages__angular__src__core__change_detection__change_detection, packages__http__src__base_client, packages__angular__angular$46template, packages__carros__app_component$46template, packages__angular__src__bootstrap__modules, packages__carros__app_component) {
  'use strict';
  const core = dart_sdk.core;
  const dart = dart_sdk.dart;
  const dartx = dart_sdk.dartx;
  const hierarchical = packages__angular__src__core__change_detection__change_detection.src__di__injector__hierarchical;
  const injector = packages__angular__src__core__change_detection__change_detection.src__di__injector__injector;
  const client = packages__http__src__base_client.src__client;
  const angular$46template = packages__angular__angular$46template.angular$46template;
  const app_component$46template = packages__carros__app_component$46template.app_component$46template;
  const run = packages__angular__src__bootstrap__modules.src__bootstrap__run;
  const app_component = packages__carros__app_component.app_component;
  var main$46template = Object.create(dart.library);
  var main = Object.create(dart.library);
  var HierarchicalInjectorL = () => (HierarchicalInjectorL = dart.constFn(dart.legacy(hierarchical.HierarchicalInjector)))();
  var ClientL = () => (ClientL = dart.constFn(dart.legacy(client.Client)))();
  var InjectorL = () => (InjectorL = dart.constFn(dart.legacy(injector.Injector)))();
  var AppComponentL = () => (AppComponentL = dart.constFn(dart.legacy(app_component.AppComponent)))();
  var InjectorLToInjectorL = () => (InjectorLToInjectorL = dart.constFn(dart.fnType(InjectorL(), [], [InjectorL()])))();
  const CT = Object.create(null);
  var L0 = "org-dartlang-app:///web/main.template.dart";
  dart.defineLazy(CT, {
    get C0() {
      return C0 = dart.const({
        __proto__: core.Object.prototype
      });
    },
    get C1() {
      return C1 = dart.fn(main$46template.injector$Injector, InjectorLToInjectorL());
    }
  }, false);
  var _field0 = dart.privateName(main$46template, "_field0");
  var _getClient$0 = dart.privateName(main$46template, "_getClient$0");
  var _getInjector$1 = dart.privateName(main$46template, "_getInjector$1");
  var C0;
  main$46template._Injector$injector = class _Injector$36injector extends hierarchical.HierarchicalInjector {
    [_getClient$0]() {
      let t0;
      t0 = this[_field0];
      return t0 == null ? this[_field0] = client.Client.new() : t0;
    }
    [_getInjector$1]() {
      return this;
    }
    injectFromSelfOptional(token, orElse = C0 || CT.C0) {
      if (token === dart.wrapType(ClientL())) {
        return this[_getClient$0]();
      }
      if (token === dart.wrapType(InjectorL())) {
        return this[_getInjector$1]();
      }
      return orElse;
    }
  };
  (main$46template._Injector$injector.__ = function(parent = null) {
    this[_field0] = null;
    main$46template._Injector$injector.__proto__.new.call(this, HierarchicalInjectorL().as(parent));
    ;
  }).prototype = main$46template._Injector$injector.prototype;
  dart.addTypeTests(main$46template._Injector$injector);
  dart.addTypeCaches(main$46template._Injector$injector);
  dart.setMethodSignature(main$46template._Injector$injector, () => ({
    __proto__: dart.getMethods(main$46template._Injector$injector.__proto__),
    [_getClient$0]: dart.fnType(dart.legacy(client.Client), []),
    [_getInjector$1]: dart.fnType(dart.legacy(injector.Injector), []),
    injectFromSelfOptional: dart.fnType(dart.legacy(core.Object), [dart.legacy(core.Object)], [dart.legacy(core.Object)])
  }));
  dart.setLibraryUri(main$46template._Injector$injector, L0);
  dart.setFieldSignature(main$46template._Injector$injector, () => ({
    __proto__: dart.getFields(main$46template._Injector$injector.__proto__),
    [_field0]: dart.fieldType(dart.legacy(client.Client))
  }));
  main$46template.injector$Injector = function injector$36Injector(parent = null) {
    return new main$46template._Injector$injector.__(parent);
  };
  main$46template.initReflector = function initReflector() {
    if (dart.test(main$46template._visited)) {
      return;
    }
    main$46template._visited = true;
    main$46template.initReflector();
    angular$46template.initReflector();
    app_component$46template.initReflector();
  };
  dart.defineLazy(main$46template, {
    /*main$46template._visited*/get _visited() {
      return false;
    },
    set _visited(_) {}
  }, true);
  main.main = function main$() {
    run.runApp(AppComponentL(), app_component$46template.AppComponentNgFactory, {createInjector: main.injector});
  };
  var C1;
  dart.defineLazy(main, {
    /*main.injector*/get injector() {
      return C1 || CT.C1;
    }
  }, true);
  main$46template.main = main.main;
  dart.trackLibraries("web/main", {
    "org-dartlang-app:///web/main.template.dart": main$46template,
    "org-dartlang-app:///web/main.dart": main
  }, {
  }, '{"version":3,"sourceRoot":"","sources":["main.template.dart","main.dart"],"names":[],"mappings":";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;AAoB+B;mBAAQ,OAAR,gBAAgB;IAAQ;;AACpB;IAAI;2BAEA,OAAe;AAClD,UAAI,AAAU,KAAK,KAAM;AACvB,cAAO;;AAET,UAAI,AAAU,KAAK,KAAM;AACvB,cAAO;;AAET,YAAO,OAAM;IACf;;oDAfmC;IAExB;AAFmC,2FAAM,MAAM;;EAAC;;;;;;;;;;;;;;mEAHhB;AAAY,UAAmB,2CAAE,MAAM;EAAC;;AAuBnF,kBAAI;AACF;;AAEa,IAAf,2BAAW;AAEU,IAArB;AACqB,IAArB;AACqB,IAArB;EACF;;MAVI,wBAAQ;YAAG;;;;;ACnB6C,IAA1D,4BAAU,iEAAuC;EACnD;;;MAJsB,aAAQ","file":"main.ddc.js"}');
  // Exports:
  return {
    web__main$46template: main$46template,
    web__main: main
  };
}));

//# sourceMappingURL=main.ddc.js.map
