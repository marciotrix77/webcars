import 'package:angular/angular.dart';
import 'package:carros/src/services/carro_service.dart';

@Component(
  selector: 'navbar',
  templateUrl: 'navbar_component.html',
  styleUrls: ['navbar_component.css']
)
class NavbarComponent {

  final CarroService _carroService;

  NavbarComponent(this._carroService);

  void PesquisarCarro(String termo){
    print(termo);
    _carroService.streamControllerPesquisa.add(termo);
  }

}