import 'package:angular/angular.dart';

import 'package:carros/src/components/cadastro_component/cadastro_component.dart';

import 'package:carros/src/services/carro_service.dart';

import 'src/components/navbar_component/navbar_component.dart';

import 'src/components/tabela_component/tabela_component.dart';



// AngularDart info: https://angulardart.dev
// Components info: https://angulardart.dev/components

@Component(
  selector: 'my-app',
  styleUrls: ['app_component.css'],
  templateUrl: 'app_component.html',
  directives: [NavbarComponent, TabelaComponent, CadastroComponent],
  providers: [ClassProvider(CarroService)],
)
class AppComponent {
  // Nothing here yet. All logic is in TodoListComponent.
  
}
